\subsection{Busy}
This class implements a busy flag which should be set when the interpreter 
is busy doing something, like scanning for instance. The primary use
is for AMOR where operations are possible while writing data. This is 
not caught by the normal device executor logic.  In the long run, this
should become the standard way to control access to the
interpreter. In order to ensure access control, a test for the busy
flag is included into the SCMatchRights procedure. The busy flag is 
 installed into the interpreter.

@o Busy.h @{
/*------------------------------------------------------------------------
  A busy flag module for SICS. 

  Mark Koennecke, July 2002
-------------------------------------------------------------------------*/
#ifndef SICSBUSY
#define SICSBUSY


typedef struct BUSY__ *busyPtr;

busyPtr makeBusy(void);
void killBusy(void *self);

void incrementBusy(busyPtr self);
void decrementBusy(busyPtr self);
void clearBusy(busyPtr self);
void setBusy(busyPtr self, int val);

int isBusy(busyPtr self);

int BusyAction(SConnection *pCon, SicsInterp *pSics,  void *pData,
	int argc, char *argv[]);

busyPtr findBusy(SicsInterp  *pInter);
#endif

@}


