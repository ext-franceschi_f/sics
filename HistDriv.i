
#line 470 "histogram.w"

/*---------------------------------------------------------------------------
                        H I S T D R I V
  internal header file which includes the definition of the Histogram memory
  driver structure.

  Mark Koennecke, April 1997
----------------------------------------------------------------------------*/
#ifndef SICSHISTDRIV
#define SICSHISTDRIV
#include "hmdata.h"


#line 89 "histogram.w"

   typedef struct __HistDriver {
                                 pHMdata data;
                               /* counting operations data */
                                 CounterMode  eCount;
                                 float fCountPreset;
                               /* status flags */
                                 int iReconfig;
                                 int iUpdate;
                                 pStringDict pOption;
                               /* interface functions */
                                 int (*Configure)(pHistDriver self,
                                               SConnection *pCon,
                                               pStringDict pOpt,
                                               SicsInterp *pSics);
                                 int (*Start)(pHistDriver self, 
                                           SConnection *pCon);
                                 int (*Halt)(pHistDriver self);
                                 int (*GetCountStatus)(pHistDriver self,
                                                    SConnection *pCon);
                                 int (*GetError)(pHistDriver self, 
                                              int *iCode,
                                              char *perror,
                                              int iErrlen);
                                 int (*TryAndFixIt)(pHistDriver self,
                                                 int iCode);
                                 int (*GetData)(pHistDriver self,
                                             SConnection *pCon);
                                 int (*GetHistogram)(pHistDriver self,
                                                  SConnection *pCon,
                                                  int i,
                                                  int iStart, int iEnd,
                                                  HistInt *pData);

                                 int (*SetHistogram)(pHistDriver self,
                                                  SConnection *pCon,
                                                  int i,
                                                  int iStart, int iEnd,
                                                  HistInt *pData);
                                 long (*GetMonitor)(pHistDriver self, 
                                                    int i,
                                                    SConnection *pCon);
                                 float (*GetTime)(pHistDriver self,
                                                  SConnection *pCon);
                                 HistInt *(*SubSample)(pHistDriver self, 
                                        SConnection *pCon,int bank,
                                                 char *command);                 
                                 int (*Preset)(pHistDriver self,
                                            SConnection *pCon,
                                            HistInt iVal);
                                 int (*Pause)(pHistDriver self,
                                              SConnection *pCon);
                                 int (*Continue)(pHistDriver self,
                                                 SConnection *pCon);
                                 int (*FreePrivate)(pHistDriver self);
                                 void *pPriv;
                               } HistDriver;

#line 482 "histogram.w"


#line 232 "histogram.w"

   pHistDriver CreateHistDriver(pStringDict pDict);
   void        DeleteHistDriver(pHistDriver self);
   int         HistDriverConfig(pHistDriver self, pStringDict pOpt,
                                SConnection *pCon);
   HistInt *DefaultSubSample(pHistDriver self, SConnection *pCon, 
                int bank, char *command);                              

#line 483 "histogram.w"


#endif
