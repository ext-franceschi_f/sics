
#line 488 "histogram.w"

/*---------------------------------------------------------------------------
                        H I S T M E M -- Internal
  internal header file which includes the definition of the Histogram memory
  data structure.

  Mark Koennecke, April 1997
----------------------------------------------------------------------------*/
#ifndef SICSHISTMEMINT
#define SICSHISTMEMINT

#line 256 "histogram.w"

   typedef struct __HistMem {
                              pObjectDescriptor pDes;
                              int iAccess;
                              int iExponent;
                              pHistDriver pDriv;
                              int iInit;
                              pICountable pCountInt;
                              pICallBack  pCall;
                            } HistMem;

#line 498 "histogram.w"


#endif
