/*--------------------------------------------------------------------------

		              THE  SICS SERVER 
		         
		         
  This file contains the main entry point into the world of SICS.
  


	Mark Koennecke,    October 1996

	Copyright: see copyright.h

	Labor fuer Neutronenstreuung
	Paul Scherrer Institut
	CH-5423 Villigen-PSI


----------------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "nserver.h"
#include "logv2.h"


extern void KeepStartupCommands(); /* ofac.c */

/***************************** Necessary Globals ****************************/


#define DEFAULTINIFILE "servo.tcl"

int usage(const char *name)
{
  fprintf(stderr, "usage: %s [-d] [-nolog] <config.tcl>\n", name);
  fprintf(stderr, "      -d: daemonize the process\n");
  fprintf(stderr, "  -nolog: disable log file writing\n");
#ifdef SITE_ANSTO
  fprintf(stderr, "      -v: print version and exit\n");
#endif
  return 1;
}

/*---------------------------------------------------------------------------
  The Servers Main program. May take one argument: the name of an 
  initialisation file
*/

int main(int argc, char *argv[])
{
  int iRet;
  int daemonize = 0;
  char *file = NULL;
  int i, firstArg = 1;
  extern pServer pServ;
  char *argv0;

  argv0 = argv[0];
  if (argc < 2)
    return usage(argv[0]);

  /* initialise, will die on you if problems */

  for (i = firstArg; i < argc; i++) {
    if (argv[i][0] == '-') {
      if (strcasecmp(argv[i], "-nolog") == 0) {
        DisableLog();
      }else if(strcmp(argv[i],"-keepstartup") == 0){
        KeepStartupCommands();
#ifdef SITE_ANSTO
      } else if (strcasecmp(argv[i], "-v") == 0) {
        extern void SiteReportVersion(void);
        SiteReportVersion();
        return 0;
#endif
      } else if (strcasecmp(argv[i], "-d") == 0) {
        daemonize = 1;
      } else {
        fprintf(stderr, "Unrecognized option ignored: %s\n", argv[i]);
      }
    } else if (file == NULL) {
      file = argv[i];
    } else {
      fprintf(stderr, "Unrecognized argument ignored: %s\n", argv[i]);
    }
  }

  if (file == NULL)
    return usage(argv[0]);

  iRet = InitServer(file, &pServ);
  if (!iRet) {
    printf("Unrecoverable error on server startup, exiting.........\n");
    exit(1);
  }
#ifdef SITE_ANSTO
  if (daemonize == 1)
    daemon(1, 1);
#endif

  RunServer(pServ);

  StopServer(pServ);
  pServ = NULL;
  exit(0);
}

