/* ------------------------------------------------------------------------
   The Accesscode names for SICS
  
  Mark Koennecke, November 1996
----------------------------------------------------------------------------*/
#include <string.h>
#include <sics.h>
#include <splitter.h>

static char *aCode[] = {
  "internal",
  "mugger",
  "user",
  "spy",
  NULL
};
static int iCodes = 4;
/*--------------------------------------------------------------------------*/
int decodeSICSPriv(char *privText)
{
  int code = 0;

  strtolower(privText);
  if(strcmp(privText,"manager") == 0){
	  return 1;
  }
  while (aCode[code] != NULL) {
    if (strcmp(aCode[code], privText) == 0) {
      return code;
    }
    code++;
  }
  if (code >= iCodes) {
    return -1;
  }
  return -1;
}
