/*------------------------------------------------------------------------
				A L I A S
				
  Implementation of the alias command. This is a configuration command 
  which allows additional names "aliases" for an existing object.
 
 Mark Koennecke, March 1997

 copyright: see implementation file
 
-------------------------------------------------------------------------*/
#ifndef SICSALIAS
#define SICSALIAS

int SicsAlias(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[]);
int MakeAlias(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[]);
int LocateAliasAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                      int argc, char *argv[]);
#endif
