##NXDICT-1.0
#----------------------------------------------------------------------------
# NeXus dictionary file for the SINQ instrument AMOR
#
# DO NOT EDIT WHEN YOU DO NOT KNOW WHAT YOU ARE DOING!
# This file determines the placement of data items in the AMOR NeXus 
# data file. Your data may not be readable if this file is messed up.
#
# Mark Koennecke, September 1999
#----------------------------------------------------------------------------
# AMOR may have variable time binning. In order
# to cope with that, we use NXDICT's text replacement feature and define
# these things
timebin=512
detxsize=255
detysize=128
scanlength = 10
chunk = 
#---------- NXentry level
etitle=/entry1,NXentry/SDS title -type DFNT_CHAR -rank 1 -dim {132} 
estart=/entry1,NXentry/SDS start_time -type DFNT_CHAR -rank 1 -dim {132} 
eend=/entry1,NXentry/SDS end_time -type DFNT_CHAR -rank 1 -dim {132} 
#----------------- NXinstrument
iname=/entry1,NXentry/reflectometer,NXinstrument/SDS name -type DFNT_CHAR \
      -rank 1 -dim {132}
#----------------- NXsource
sname=/entry1,NXentry/reflectometer,NXinstrument/SINQ,NXsource/SDS name \
 -type DFNT_CHAR -rank 1 -dim {132}
stype=/entry1,NXentry/reflectometer,NXinstrument/SINQ,NXsource/SDS type \
 -type DFNT_CHAR -rank 1 -dim {132}
#----------------- Chopper
cname=/entry1,NXentry/reflectometer,NXinstrument/chopper,NXchopper/SDS name \
       -type DFNT_CHAR -rank 1 -dim {132}
crot=/entry1,NXentry/reflectometer,NXinstrument/chopper,NXchopper/SDS \
       rotation_speed -attr {units,rpm}
#---------------- frame overlap mirror
fomname=/entry1,NXentry/reflectometer,NXinstrument/frame_overlap_mirror,NXfilter/SDS name \
      -type  DFNT_CHAR -rank 1 -dim {132}
fomh=/entry1,NXentry/reflectometer,NXinstrument/frame_overlap_mirror,NXfilter/SDS \ 
     omega_height -attr {units,mm}
fomom=/entry1,NXentry/reflectometer,NXinstrument/frame_overlap_mirror,NXfilter/SDS \
     omega -attr {units,degree}
fodist=/entry1,NXentry/reflectometer,NXinstrument/frame_overlap_mirror,NXfilter/SDS \
     distance -attr {units,mm}
#-------------- first slit
d1l=/entry1,NXentry/reflectometer,NXinstrument/diaphragm1,NXfilter/SDS left \
      -attr {units,mm}
d1r=/entry1,NXentry/reflectometer,NXinstrument/diaphragm1,NXfilter/SDS right \
      -attr {units,mm}
d1t=/entry1,NXentry/reflectometer,NXinstrument/diaphragm1,NXfilter/SDS top \
      -attr {units,mm}
d1b=/entry1,NXentry/reflectometer,NXinstrument/diaphragm1,NXfilter/SDS bottom \
      -attr {units,mm}
d1dist=/entry1,NXentry/reflectometer,NXinstrument/diaphragm1,NXfilter/SDS distance \
      -attr {units,mm}
#---------- polarizing mirror
polname=/entry1,NXentry/reflectometer,NXinstrument/polarizer,NXfilter/SDS name \
      -type  DFNT_CHAR -rank 1 -dim {132}
polz=/entry1,NXentry/reflectometer,NXinstrument/polarizer,NXfilter/SDS height \
      -attr {units,mm}
polzom=/entry1,NXentry/reflectometer,NXinstrument/polarizer,NXfilter/SDS omega_height \
      -attr {units,mm}
polom=/entry1,NXentry/reflectometer,NXinstrument/polarizer,NXfilter/SDS omega \
      -attr {units,degree}
poly=/entry1,NXentry/reflectometer,NXinstrument/polarizer,NXfilter/SDS y_position \
      -attr {units,mm}
poldist=/entry1,NXentry/reflectometer,NXinstrument/polarizer,NXfilter/SDS distance \
      -attr {units,mm}
#-------------- second slit
d2l=/entry1,NXentry/reflectometer,NXinstrument/diaphragm2,NXfilter/SDS left \
      -attr {units,mm}
d2r=/entry1,NXentry/reflectometer,NXinstrument/diaphragm2,NXfilter/SDS right \
      -attr {units,mm}
d2t=/entry1,NXentry/reflectometer,NXinstrument/diaphragm2,NXfilter/SDS top \
      -attr {units,mm}
d2b=/entry1,NXentry/reflectometer,NXinstrument/diaphragm2,NXfilter/SDS bottom \
      -attr {units,mm}
d2dist=/entry1,NXentry/reflectometer,NXinstrument/diaphragm2,NXfilter/SDS distance \
      -attr {units,mm}
#-------------- third slit
d3l=/entry1,NXentry/reflectometer,NXinstrument/diaphragm3,NXfilter/SDS left \
      -attr {units,mm}
d3r=/entry1,NXentry/reflectometer,NXinstrument/diaphragm3,NXfilter/SDS right \
      -attr {units,mm}
d3t=/entry1,NXentry/reflectometer,NXinstrument/diaphragm3,NXfilter/SDS top \
      -attr {units,mm}
d3b=/entry1,NXentry/reflectometer,NXinstrument/diaphragm3,NXfilter/SDS bottom \
      -attr {units,mm}
d3dist=/entry1,NXentry/reflectometer,NXinstrument/diaphragm3,NXfilter/SDS distance \
      -attr {units,mm}
#---------------- sample table
saname=/entry1,NXentry/sample,NXsample/SDS name \
      -type  DFNT_CHAR -rank 1 -dim {132}
baseheight=/entry1,NXentry/sample,NXsample/SDS base_height \
      -attr {units,mm}
somheight=/entry1,NXentry/sample,NXsample/SDS omega_height \
      -attr {units,mm}
schi=/entry1,NXentry/sample,NXsample/SDS chi \
      -attr {units,degree}
somega=/entry1,NXentry/sample,NXsample/SDS omega \
      -attr {units,degree}
stheight=/entry1,NXentry/sample,NXsample/SDS table_height \
      -attr {units,mm}
stdist=/entry1,NXentry/sample,NXsample/SDS distance \
      -attr {units,mm}
#------------ fourth slit
d4l=/entry1,NXentry/reflectometer,NXinstrument/diaphragm4,NXfilter/SDS left \
      -attr {units,mm}
d4r=/entry1,NXentry/reflectometer,NXinstrument/diaphragm4,NXfilter/SDS right \
      -attr {units,mm}
d4t=/entry1,NXentry/reflectometer,NXinstrument/diaphragm4,NXfilter/SDS top \
      -attr {units,mm}
d4b=/entry1,NXentry/reflectometer,NXinstrument/diaphragm4,NXfilter/SDS bottom \
      -attr {units,mm}
d4dist=/entry1,NXentry/reflectometer,NXinstrument/diaphragm4,NXfilter/SDS \
 distance_to_sample  -attr {units,mm}
d4base =/entry1,NXentry/reflectometer,NXinstrument/diaphragm4,NXfilter/SDS \
 base_height -attr {units,mm}
#------------ analyzer
anname=/entry1,NXentry/reflectometer,NXinstrument/polarizer,NXfilter/SDS name \
      -type  DFNT_CHAR -rank 1 -dim {132}
anoz=/entry1,NXentry/reflectometer,NXinstrument/analyzer,NXfilter/SDS omega_height \
      -attr {units,mm}
abase=/entry1,NXentry/reflectometer,NXinstrument/analyzer,NXfilter/SDS base_height \
      -attr {units,mm}
adis=/entry1,NXentry/reflectometer,NXinstrument/analyzer,NXfilter/SDS  \
 distance_to_sample   -attr {units,mm}
anom=/entry1,NXentry/reflectometer,NXinstrument/analyzer,NXfilter/SDS omega \
      -attr {units,degree}
antz=/entry1,NXentry/reflectometer,NXinstrument/analyzer,NXfilter/SDS height \
      -attr {units,mm}
andist=/entry1,NXentry/reflectometer,NXinstrument/analyzer,NXfilter/SDS distance \
      -attr {units,mm}
#--------------- fifth slit!!
d5l=/entry1,NXentry/reflectometer,NXinstrument/diaphragm5,NXfilter/SDS left \
      -attr {units,mm}
d5r=/entry1,NXentry/reflectometer,NXinstrument/diaphragm5,NXfilter/SDS right \
      -attr {units,mm}
d5t=/entry1,NXentry/reflectometer,NXinstrument/diaphragm5,NXfilter/SDS top \
      -attr {units,mm}
d5b=/entry1,NXentry/reflectometer,NXinstrument/diaphragm5,NXfilter/SDS bottom \
      -attr {units,mm}
d5dist=/entry1,NXentry/reflectometer,NXinstrument/diaphragm5,NXfilter/SDS \
 distance_to_sample  -attr {units,mm}
d5base =/entry1,NXentry/reflectometer,NXinstrument/diaphragm5,NXfilter/SDS \
 base_height -attr {units,mm}
#---------- count control
cnmode=/entry1,NXentry/reflectometer,NXinstrument/counter,NXmonitor/SDS count_mode \
       -type DFNT_CHAR -rank 1 -dim {30}
cnpreset=/entry1,NXentry/reflectometer,NXinstrument/counter,NXmonitor/SDS preset \
         -attr {units,countsOrseconds}
cntime=/entry1,NXentry/reflectometer,NXinstrument/counter,NXmonitor/SDS time \
         -attr {units,seconds}
cnmon1=/entry1,NXentry/reflectometer,NXinstrument/counter,NXmonitor/SDS monitor1 \
         -type DFNT_INT32 -attr {units,counts}
cnmon2=/entry1,NXentry/reflectometer,NXinstrument/counter,NXmonitor/SDS monitor2 \
         -type DFNT_INT32 -attr {units,counts}
#-------------- detector-TOF mode
dettype=/entry1,NXentry/reflectometer,NXinstrument/TOF,NXdetector/SDS type \
         -type DFNT_CHAR -rank 1 -dim {132}
dety=/entry1,NXentry/reflectometer,NXinstrument/TOF,NXdetector/SDS y_detector \
         -type DFNT_FLOAT32 -rank 1 -dim {$(detysize)} -attr {axis,1} \
         -attr {units,mm}
detxx=/entry1,NXentry/reflectometer,NXinstrument/TOF,NXdetector/SDS x_detector \
         -type DFNT_FLOAT32 -rank 1 -dim {$(detxsize)} -attr {axis,2} \
         -attr {units,mm}
detz=/entry1,NXentry/reflectometer,NXinstrument/TOF,NXdetector/SDS z \
         -type DFNT_FLOAT32 -rank 1 -dim {$(detxsize)} -attr {axis,2} \
         -attr {units,mm}
detx=/entry1,NXentry/reflectometer,NXinstrument/TOF,NXdetector/SDS x \
         -type DFNT_FLOAT32 -attr {units,mm} 
detom=/entry1,NXentry/reflectometer,NXinstrument/TOF,NXdetector/SDS omega \
         -type DFNT_FLOAT32 -attr {units,degree} 
detheight=/entry1,NXentry/reflectometer,NXinstrument/TOF,NXdetector/SDS height \
         -type DFNT_FLOAT32 -attr {units,mm} 
detdist=/entry1,NXentry/reflectometer,NXinstrument/TOF,NXdetector/SDS \
 distance_to_sample  -type DFNT_FLOAT32 -attr {units,mm} 
detbase=/entry1,NXentry/reflectometer,NXinstrument/TOF,NXdetector/SDS \
 base_height  -type DFNT_FLOAT32 -attr {units,mm} 
dettime=/entry1,NXentry/reflectometer,NXinstrument/TOF,NXdetector/SDS time_binning \
         -type DFNT_FLOAT32 -rank 1 -dim {$(timebin)} -attr {axis,3} \
         -attr {units,ms}
spinup=/entry1,NXentry/reflectometer,NXinstrument/TOF,NXdetector/SDS spinup \
         -type DFNT_INT32 -rank 3 -dim {$(detxsize),$(detysize),$(timebin)} \
          -LZW $(chunk) -attr {signal,1} 
#spinup=/entry1,NXentry/reflectometer,NXinstrument/TOF,NXdetector/SDS spinup \
#         -type DFNT_INT32 -rank 3 -dim {$(detxsize),$(detysize),$(timebin)} \
#          $(chunk) -attr {signal,1} 
detchunk=/entry1,NXentry/reflectometer,NXinstrument/TOF,NXdetector/SDS \
    chunksize -type DFNT_INT32 -rank 1 -dim {3} 
spinup2d=/entry1,NXentry/reflectometer,NXinstrument/TOF,NXdetector/SDS spinup \
         -type DFNT_INT32 -rank 2 -dim {$(detxsize),$(detysize)} \
          -LZW $(chunk) -attr {signal,1} 
spindown=/entry1,NXentry/reflectometer,NXinstrument/TOF,NXdetector/SDS spindown \
         -type DFNT_INT32 -rank 3 -dim {$(detxsize),$(detysize),$(timebin)} \
          -LZW -attr {signal,1}
#------------ single detectors TOF -------------------------------------
singleup=/entry1,NXentry/reflectometer,NXinstrument/single,NXdetector/SDS \
         spinup -type DFNT_INT32 -rank 2 -dim {2, $(timebin)} -LZW \
         -attr {signal,1} 
singledown=/entry1,NXentry/reflectometer,NXinstrument/single,NXdetector/SDS \
         spinup -type DFNT_INT32 -rank 2 -dim {2, $(timebin)} -LZW \
         -attr {signal,1} 
singletime=/entry1,NXentry/reflectometer,NXinstrument/single,NXdetector/SDS \
         time_binning -type DFNT_FLOAT32 -rank 1 -dim {$(timebin)}  \
         -attr {axis,2} 
singletofmon=/entry1,NXentry/reflectometer,NXinstrument/single,NXdetector/SDS \
         tof_monitor -type DFNT_INT32 -rank 1 -dim {$(timebin)}  
#------------ detector: scan mode
scanroot=/entry1,NXentry/reflectometer,NXinstrument/scan,NXdetector/SDS 
sdetx=/entry1,NXentry/reflectometer,NXinstrument/scan,NXdetector/SDS x \
         -type DFNT_FLOAT32 -attr {units,mm} 
sdetom=/entry1,NXentry/reflectometer,NXinstrument/scan,NXdetector/SDS omega \
         -type DFNT_FLOAT32 -attr {units,degree} 
sdetheight=/entry1,NXentry/reflectometer,NXinstrument/scan,NXdetector/SDS height \
         -type DFNT_FLOAT32 -attr {units,mm} 
spinupup=/entry1,NXentry/reflectometer,NXinstrument/scan,NXdetector/SDS spinup_upper \
         -type DFNT_INT32 -rank 1 -dim {$(scanlength)} -attr {signal,1} 
spindownup=/entry1,NXentry/reflectometer,NXinstrument/scan,NXdetector/SDS \
           spindown_upper \
         -type DFNT_INT32 -rank 1 -dim {$(scanlength)} -attr {signal,2} 
spinuplo=/entry1,NXentry/reflectometer,NXinstrument/scan,NXdetector/SDS spinup_lower \
         -type DFNT_INT32 -rank 1 -dim {$(scanlength)} -attr {signal,3} 
spindownlo=/entry1,NXentry/reflectometer,NXinstrument/scan,NXdetector/SDS \
           spindown_lower \
         -type DFNT_INT32 -rank 1 -dim {$(scanlength)} -attr {signal,4} 
somega=/entry1,NXentry/reflectometer,NXinstrument/scan,NXdetector/SDS \
           omega -attr {units,degree} \
         -type DFNT_FLOAT32 -rank 1 -dim {$(scanlength)} -attr {axis,1} 
smonitor1=/entry1,NXentry/reflectometer,NXinstrument/scan,NXdetector/SDS \
           monitor1 \
         -type DFNT_INT32 -rank 1 -dim {$(scanlength)}  
smonitor2=/entry1,NXentry/reflectometer,NXinstrument/scan,NXdetector/SDS \
           monitor2 \
         -type DFNT_INT32 -rank 1 -dim {$(scanlength)}  
stime=/entry1,NXentry/reflectometer,NXinstrument/scan,NXdetector/SDS \
           time \
         -type DFNT_FLOAT32 -rank 1 -dim {$(scanlength)} -attr {units,s}
sdetdist=/entry1,NXentry/reflectometer,NXinstrument/scan,NXdetector/SDS distance \
         -type DFNT_FLOAT32 -attr {units,mm} 
#------------------- data vGroup
dana=/entry1,NXentry/TOF,NXdata/NXVGROUP
singledana=/entry1,NXentry/single,NXdata/NXVGROUP
sdana=/entry1,NXentry/scan,NXdata/NXVGROUP

