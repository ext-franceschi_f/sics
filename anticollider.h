
/*----------------------------------------------------------------------
  This is the header file for the AntiCollider, a complex movements
  control module for SICS. See anticoliider.tex for more information.
  
  copyright: see file copyright

  Mark Koennecke, August 2002
------------------------------------------------------------------------*/
#ifndef ANTICOLLIDER
#define ANTICOLLIDER


int AntiColliderFactory(SConnection * pCon, SicsInterp * pSics,
                        void *pData, int argc, char *argv[]);
int AntiColliderAction(SConnection * pCon, SicsInterp * pSics,
                       void *pData, int argc, char *argv[]);


#endif
