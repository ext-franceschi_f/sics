/*
 * arrayutil.c
 *
 * copyright: see file COPYRIGHT
 *
 * some utilities for dealing with arrays
 *
 *  Created on: Mar 16, 2011
 *      Author: koennecke
 */

long sumWindow(int *data, int xstart, int xend, int xlength,
		int ystart, int yend, int ylength)
{
	int i,j;
	long result = 0;
	int *row;

	if(xstart < 0 || xstart > xlength){
		return -2;
	}
	if(xend < 0 || xend > xlength){
		return -2;
	}
	if(xend < xstart){
		return -2;
	}
	if(ystart < 0 || ystart > ylength){
		return -2;
	}
	if(yend < 0 || yend > ylength){
		return -2;
	}
	if(yend < ystart){
		return -2;
	}


	/* for(j = ystart; j < yend; j++){ */
	/* 	row = data + j*xlength; */
	/* 	for(i = xstart; i <  xend; i++){ */
	/* 		result += row[i]; */
	/* 	} */
	/* } */
	for(i = xstart; i < xend; i++){
		row = data + i*ylength;
		for(j = ystart; j <  yend; j++){
			result += row[j];
		}
	}

	return result;
}
