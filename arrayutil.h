/*
 * arrayutil.h
 *
 * copyright: GPL
 *
 * Start of some array processing utilities. This may go if
 * a more generalized array handling gets implemented into
 * SICS.
 *
 *  Created on: Mar 16, 2011
 *      Author: koennecke
 */

#ifndef ARRAYUTIL_H_
#define ARRAYUTIL_H_

/**
 * sum data in a window
 * @param data The data pointer
 * @param xstart
 * @param xend
 * @param xlength length of x dimension
 * @param ystart
 * @param yend
 * @param ylength size of y dimension
 * @return a sum or -2 if the limits fail to make sense
 */
long sumWindow(int *data, int xstart, int xend, int xlength,
		int ystart, int yend, int ylength);

#endif /* ARRAYUTIL_H_ */
