#ifndef ASCON_H
#define ASCON_H

#include "sics.h"

/** \file
 * \brief Asynchronous connection handling for devices controlled over tcp-ip
 * connections. Interface for higher level modules.
 */

/** \brief the asynchronous connection
 */
typedef struct Ascon Ascon;

/** \brief the possible results of AsconTask
 */
typedef enum {
  AsconOffline,
  AsconUnconnected,
  AsconConnectPending,
  AsconPending,
  AsconReady,
  AsconFailure /* codes after this indicate also failure */ 
} AsconStatus;

/** \brief make a new asynchronous connection
 *  \param con the SICS connection
 *  \param argc number of arguments
 *  \param argv the arguments. argv[0] must be the protocol name, the other arguments
 *     are protocol specific, but argv[1] is usually host::port
 *  \return the created connection or NULL on failure
 */
Ascon *AsconMake(SConnection * con, int argc, char *argv[]);

/** \brief kill function
 *  \param a the connection to be killed
 */
void AsconKill(Ascon * a);
/** \brief disconnect and disable
 * \param a the connection to disconnect
 */
void AsconDisconnect(Ascon * a);

/** \brief Reconnect function
 * \param a the connection to reconnect
 * \param hostport <host>:<port> to reconnect to (empty for reconnecting to the same port)
 */
void AsconReconnect(Ascon * a, char *hostport);

/** \brief the task handler. To be called repeatedly.
 *  \param a the connection
 *  \return the state of the connection
 */
AsconStatus AsconTask(Ascon * a);

/** \brief write to the connection. allowed only when the state is AsconReady
 *  \param a the connection
 *  \param command the command to be sent
 *  \param noResponse 0 normally, 1 if no reponse is expected
 *  \return 1 on success, 0 when not ready
 */
int AsconWrite(Ascon * a, char *command, int noResponse);

/** \brief read from the connection. allowed only when a response is available
 *  \param a the connection
 *  \return the response when a response is ready
 *      NULL when the command has not completed and the response is not yet finished
 *      "" when the command has completed, but no response was expected.
 *      The result is only valid until the next call to any AsconXxx function
 *      with the same connection and has to be duplicated if needed later.
 */
char *AsconRead(Ascon * a);

/** \brief get the last error message
 *  \return the error message
 *      The result is only valid until the next call to any AsconXxx function
 *      with the same connection and has to be duplicated if needed later.
 */
char *AsconGetError(Ascon *a);

/** \brief a helper function
 *  \param argc the number of args
 *  \param argv the args to be concatenated
 *  \result a allocated string containing the concatenated arguments
 *     the args are properly quoted to be used as tcl proc arguments
 */
char *ConcatArgs(int argc, char *argv[]);

/** \brief emit an error message. The state switches to AsconFailed.
 *  \param a the connection
 *  \param msg, a message to be emitted
 *  \param errorno, for user messages, this should be 0. After
 *         detection of a system error, eerno may be placed as argument
 *         for adding strerror(errno) to the message.
 */
void AsconError(Ascon *a, char *msg, int errorno);

/**
 * \brief return the last ascon state. Only used for statistics
 * \param a The Ascon to query
 * \return the AsconState as an integer.
 */
int AsconLastState(Ascon *a);

/**
 * \brief return host:port
 * \param a The Ascon to query
 * \return the host and port
 */
char *AsconHostport(Ascon *a);

/**
 * \brief return IP address
 * \param a The Ascon to query
 * \return the IP address (dotted numbers)
 */
char *AsconIP(Ascon *a);

/**
 * \brief set or get timeout
 * \param a the Ascon
 * \param timeout the timeout to set
 * \param setmode 0: get, 1: set
 * \return the timeout value
 */
double AsconGetSetTimeout(Ascon *a, double timeout, int setmode);

/**
 * \brief set reconnectInterval
 * \param a the Ascon
 * \param interval the interval to set (0: no reconnect, -1: read value)
 * \return the value
 */
int AsconReconnectInterval(Ascon *a, int interval);

#endif
