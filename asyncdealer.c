/*
 * AsyncDealer
 *
 * This module manages an Async Dealer ZeroMQ socket
 *
 * Douglas Clowes, May 2018
 *
 */
#include "asyncdealer.h"
#include "dynstring.h"
#include "sicszmq.h"
#include "nwatch.h"
#include "rbtree.h"
#include <stdlib.h>
#include <string.h>

static int dealer_dispatch(void *context, int mode);
static int keycmp(const void *left_key, const void* right_key);

/*--------------------------------------------------------------------------*/
/**
 * The socket control structure
 */
struct __AsyncDealer {
  int trans;
  char *cmddef;
  pAsyncDealerHandler unsolHandler;
  HANDLE_T zmqHandle;
  pNWContext nHandle;
  struct rbtree_t rb_unique;
  void *private;
};

/*--------------------------------------------------------------------------*/
/**
 * The transaction control structure
 */
typedef struct {
    struct rbtree_node_t rbnode;
    int keyval;
    pAsyncDealerHandler replyHandler;
} data_node;

/*--------------------------------------------------------------------------*/
pAsyncDealer AsyncDealerCreate(const char *address,
                               const char *cmd,
                               pAsyncDealerHandler unsolHandler,
                               void *pCtx)
{
  pAsyncDealer self = calloc(1, sizeof(AsyncDealer));

  if (!self)
    return NULL;

  self->unsolHandler = unsolHandler;
  self->private = pCtx;
  if (cmd)
    self->cmddef = strdup(cmd);
  self->zmqHandle = sics_zmq_dealer(address);
  if (!self->zmqHandle) {
    free(self);
    return NULL;
  }
  if (!NetWatchRegisterZMQCallback(&self->nHandle,
                                   sics_zmq_dealer_zsock(self->zmqHandle),
                                   dealer_dispatch, self)) {
    sics_zmq_undealer(&self->zmqHandle);
    free(self);
    return NULL;
  }

  rbtree_init(&self->rb_unique, (rbtree_compare_func) keycmp);
  return self;
}

/*--------------------------------------------------------------------------*/
void AsyncDealerDelete(pAsyncDealer self)
{
  NetWatchRemoveCallback(self->nHandle);
  sics_zmq_undealer(&self->zmqHandle);
  free(self);
}

/*--------------------------------------------------------------------------*/
int AsyncDealerSend(pAsyncDealer self,
                    const char *text,
                    pAsyncDealerHandler replyHandler)
{
  return AsyncDealerSend4(self, text, self->cmddef, replyHandler);
}

/*--------------------------------------------------------------------------*/
int AsyncDealerSend4(pAsyncDealer self,
                     const char *text,
                     const char *cmd,
                     pAsyncDealerHandler replyHandler)
{
  json_object *msg_json;  /**< JSON message object */
  const char *json_text;
  rbtree_node rnode = NULL;
  data_node *dnode = NULL;
  if (!self)
    return 0;

  /* Find a free transaction identifier */
  do {
    ++self->trans;
    if (self->trans > 999999)
      self->trans = 1;
  } while (rbtree_node_lookup(&self->rb_unique, (void *) &self->trans));

  /* Create and set up the outstanding transaction entity */
  dnode = calloc(1, sizeof(data_node));
  rnode = &dnode->rbnode;
  rnode->key = &dnode->keyval;
  rnode->value = dnode;
  dnode->keyval = self->trans;
  dnode->replyHandler = replyHandler;

  /* save it in the cache */
  rbtree_insert(&self->rb_unique, rnode);

  /* prepare and send the message */
  msg_json = json_object_new_object();
  json_object_object_add(msg_json, "trans", json_object_new_int(self->trans));
  json_object_object_add(msg_json, "cmd", json_object_new_string(cmd));
  json_object_object_add(msg_json, "text", json_object_new_string(text));
  json_text = json_object_to_json_string(msg_json);
  sics_zmq_dealer_send(self->zmqHandle, json_text);
  json_object_put(msg_json);
  return self->trans;
}

/**
 * ZMQ DEALER socker Interrupt Service Routine
 *
 * Receive the message from the socket and dispatch it to the handler
 *
 * @param context the supplied context (self)
 * @param mode ignored
 */
static int dealer_dispatch(void *context, int mode)
{
  pAsyncDealer self = (pAsyncDealer) context;
  pDEALER_MESSAGE pMsg;   /**< ZMQ message */
  const char *json_text;
  struct json_tokener *jtok;
  json_object *msg_json = NULL, *jnode = NULL;
  enum json_tokener_error tokerr;
  int len;

  jtok = json_tokener_new();
  while ((pMsg = sics_zmq_dealer_recv(self->zmqHandle, 20))) {

    /* grab the content */
    len = sics_zmq_dealer_size(pMsg);
    json_text = sics_zmq_dealer_data(pMsg);

    /* Prepare a Tasker message with the context and payload */
    json_tokener_reset(jtok);
    msg_json = json_tokener_parse_ex(jtok, json_text, len);
    tokerr = json_tokener_get_error(jtok);
    if (tokerr == json_tokener_success &&
        json_object_get_type(msg_json) == json_type_object) {
      /* Valid JSON */
      rbtree_node rnode = NULL;
      data_node *dnode = NULL;
      int trans = 0;
      bool final = false;

      if (json_object_object_get_ex(msg_json, "trans", &jnode)) {
        if (json_object_get_type(jnode) == json_type_int) {
          /* get the transaction id as an integer */
          trans = json_object_get_int(jnode);
        } else if (json_object_get_type(jnode) == json_type_string) {
          char *endptr;
          const char *trans_str = json_object_get_string(jnode);
          trans = strtol(trans_str, &endptr, 10);
        } else {
          /* TODO logging */
        }
      }

      if (json_object_object_get_ex(msg_json, "final", &jnode)) {
        if (json_object_get_type(jnode) == json_type_int) {
          /* get the transaction id as an integer */
          final = json_object_get_int(jnode) == 0 ? false : true;
        } else if (json_object_get_type(jnode) == json_type_boolean) {
          final = json_object_get_boolean(jnode);
        } else if (json_object_get_type(jnode) == json_type_string) {
          /* TODO parse string */
        } else {
          /* TODO logging */
        }
      }

      rnode = rbtree_node_lookup(&self->rb_unique , &trans);
      if (rnode) {
        dnode = rnode->value;
        if (dnode->replyHandler) {
          dnode->replyHandler(self->private, msg_json, trans, final);
        } else if (self->unsolHandler) {
          self->unsolHandler(self->private, msg_json, trans, final);
        }
        if (final) {
          rbtree_node_delete(&self->rb_unique, &dnode->rbnode);
          free(dnode);
        }
      } else {
        if (self->unsolHandler)
          self->unsolHandler(self->private, msg_json, trans, final);
      }
    }

    json_object_put(msg_json);
    sics_zmq_dealer_free(pMsg);
  }
  json_tokener_free(jtok);
  return 0;
}
/**
 * rbtree integer key comparisson routine
 */
static int keycmp(const void *left_key, const void* right_key)
{
  int *left = (int *)left_key;
  int *right = (int *)right_key;
  if (*left < *right)
    return -1;
  if (*left > *right)
    return 1;
  return 0;
}

