/*
 * AsyncDealer
 *
 * This module manages an ZeroMQ Async Dealer socket
 *
 * Douglas Clowes, May 2018
 *
 */
#ifndef ASYNCDEALER_H
#define ASYNCDEALER_H

#include <json.h>
#include <stdbool.h>

/*
 * opaque handle type
 */
typedef struct __AsyncDealer AsyncDealer, *pAsyncDealer;

/**
 * Callback function prototype
 *
 * Used for callbacks for solicited or unsolicited messages.
 * Solicited messages have a iTrans value that matches an outstanding
 * unfinalized request. Unsolicited messages are unmatched.
 *
 * @param pCtx the context from the create
 * @param msgText the message reply body
 * @param iTrans the transaction identifier
 * @param isFinal the value of the final flag
 */
typedef void (*pAsyncDealerHandler)(void *pCtx,
                                   struct json_object *msg_json,
                                   int iTrans,
                                   bool isFinal);

/**
 * Create a connection
 *
 * @param address the ZMQ connect address (e.g. "tcp://there:9999")
 * @param cmd the default message type (e.g. "SICS")
 * @param hUnsol the unsolicited message handler
 * @param pCtx the context returned in callbacks
 *
 * @return the pointer handle or NULL on error
 */
pAsyncDealer AsyncDealerCreate(const char *address,
                               const char *cmd,
                               pAsyncDealerHandler hUnsol,
                               void *pCtx);

void AsyncDealerDelete(pAsyncDealer self);

int AsyncDealerSend(pAsyncDealer self,
                    const char *text,
                    pAsyncDealerHandler hReply);

int AsyncDealerSend4(pAsyncDealer self,
                    const char *text,
                    const char *cmd,
                    pAsyncDealerHandler hReply);

#endif /* ASYNCDEALER_H */
