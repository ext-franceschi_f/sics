#ifndef ASYNCPROTOCOL
#define ASYNCPROTOCOL
#include "sics.h"

/** @file
 * @brief Asynchronous protocol handling for Asyncqueue
 */

/**
 * @brief The unit is multiplexed over the queue
 */
typedef struct __AsyncUnit AsyncUnit, *pAsyncUnit;

/**
 * @brief The transaction I/O Request Packet
 */
typedef struct __async_txn AsyncTxn, *pAsyncTxn;

/**
 * @brief Transaction response handler
 *
 * Called when a response has been received
 */
typedef int (*AsyncTxnHandler) (pAsyncTxn pTxn);

typedef struct __async_protocol AsyncProtocol, *pAsyncProtocol;

/**
 * @brief Constructor for a new protocol
 * @param pSics pointer to SICS Interpreter
 * @param protocolName the name of the new protocol
 * @param pFunc the protocol command wrapper
 * @param pKFunc the protocol destructor function
 *
 * @return address of new protocol object or NULL on error
 */
pAsyncProtocol AsyncProtocolCreate(SicsInterp * pSics,
                                   const char *protocolName,
                                   ObjectFunc pFunc, KillFunc pKFunc);

/**
 * @brief Command wrapper function for generic protocol
 *
 * @param pCon pointer to SICS connection object
 * @param pSics pointer to the SICS Interpreter
 * @param pData pointer to user context registered to command
 * @param argc number of command parameters
 * @param argv set of command parameters starting with command
 *
 * @return 1 for success, 0 for failure
 */
int AsyncProtocolAction(SConnection * pCon, SicsInterp * pSics,
                        void *pData, int argc, char *argv[]);

/**
 * @brief Protocol Factory command wrapper function
 *
 * @param pCon pointer to SICS connection object
 * @param pSics pointer to the SICS Interpreter
 * @param pData pointer to user context registered to command
 * @param argc number of command parameters
 * @param argv set of command parameters starting with command
 *
 * @return 1 for success, 0 for failure
 *
 * Creates a protocol from the command arguments given.
 */
int AsyncProtocolFactory(SConnection * pCon, SicsInterp * pSics,
                         void *pData, int argc, char *argv[]);

typedef enum {
  ATX_NULL = 0,
  ATX_TIMEOUT = -1,
  ATX_ACTIVE = 1,
  ATX_COMPLETE = 2,
  ATX_DISCO = 3
} ATX_STATUS;

#define AQU_TIMEOUT     -1
#define AQU_DISCONNECT  -2
#define AQU_RECONNECT   -3
#define AQU_RETRY_CMD   -4
#define AQU_POP_CMD     -5

/**
 * @brief Asynchronous Transaction I/O Request Packet
 *
 * Used for sending a command and receiving a response
 */
struct __async_txn {
  int ref_counter;  /**< reference counter (reference counted object) */
  pAsyncUnit unit;  /**< unit that transaction is associated with */
  int txn_state;         /**< protocol handler transaction parse state */
  ATX_STATUS txn_status;  /**< status of the transaction OK, Error, ... */
  int txn_timeout;         /**< transaction timeout in milliseconds */
  char *out_buf;       /**< output buffer for sendCommand */
  int out_len;         /**< length of data to be sent */
  int out_idx;         /**< index of next character to transmit */
  char *inp_buf;       /**< input buffer for transaction response */
  int inp_len;         /**< length of input buffer */
  int inp_idx;         /**< index of next character (number already received) */
  AsyncTxnHandler handleResponse; /**< Txn response handler of command sender */
  void *proto_private; /**< Protocol Private structure */
  void (*kill_private) (pAsyncTxn pTxn); /**< if it needs killing */
  void *cntx;       /**< opaque context used by command sender */
  /* The cntx field may be used by protocol handler from sendCommand
   * as long as it is restored when response is complete
   */
};

/**
 * @brief The async protocol interface virtual function table
 */
struct __async_protocol {
  /** Object descriptor (SICS requirement) */
  pObjectDescriptor pDes;
  /** Name of the protocol object */
  char *protocolName;
  /** Terminator string to append to outgoing commands */
  char *sendTerminator;
  /** Set of possible terminators on received responses */
  char *replyTerminator[10];
  /** Protocol private data */
  void *privateData;

  /**
   * @brief Transaction initiator
   * @param p Protocol object
   * @param txn Transaction object
   * @return 1 on success, 0 on error
   */
  int (*sendCommand) (pAsyncProtocol p, pAsyncTxn txn);

  /**
   * @brief handle one input character
   * @param p Protocol object
   * @param txn Transaction object
   * @param ch Received character
   * @return AQU_POP_CMD if complete else 1
   */
  int (*handleInput) (pAsyncProtocol p, pAsyncTxn txn, int ch);

  /**
   * @brief handle exception (timeout) events
   * @param p Protocol object
   * @param txn Transaction object
   * @param event the event type (AQU_TIMEOUT)
   * @return AQU_POP_CMD, AQU_RETRY_CMD, AQU_RECONNECT
   */
  int (*handleEvent) (pAsyncProtocol p, pAsyncTxn txn, int event);

  /**
   * @brief Prepare a transaction for execution
   * @param p Protocol object
   * @param txn Transaction object
   * @param cmd internal format device command
   * @param cmd_len length of the command
   * @param rsp_len expected length of the response (0=no response)
   * @return 1 on success, 0 on error
   *
   * Translates internal format to external and prepares the
   * transaction for transmission
   */
  int (*prepareTxn) (pAsyncProtocol p, pAsyncTxn txn, const char *cmd,
                     int cmd_len, int rsp_len);

  /**
   * @brief Private destructor
   *
   * Cleans up and releases resources
   */
  void (*killPrivate) (pAsyncProtocol p);
};

/**
 * @brief send a request and receive a response
 *
 * @param p protocol instance
 * @param txn transaction request packet
 * @return 1 on success, 0 on error
 *
 * Called by AsyncQueue when a transaction is dequeued for sending.
 */
int AsyncProtocol_sendCommand(pAsyncProtocol p, pAsyncTxn txn);

/**
 * @brief handle one input character
 * @param p Protocol object
 * @param txn Transaction object
 * @param ch Received character
 * @return AQU_POP_CMD if complete else 1
 *
 * Called for each character received to parse a response.
 */
int AsyncProtocol_handleInput(pAsyncProtocol p, pAsyncTxn txn, int ch);

/**
 * @brief handle exception (timeout) events
 * @param p Protocol object
 * @param txn Transaction object
 * @param event the event type (AQU_TIMEOUT)
 * @return AQU_POP_CMD, AQU_RETRY_CMD, AQU_RECONNECT
 *
 * Called from AsyncQueue when a response timeout occurs.
 */
int AsyncProtocol_handleEvent(pAsyncProtocol p, pAsyncTxn txn, int event);

/**
 * @brief Prepare a transaction for execution
 * @param p Protocol object
 * @param txn Transaction object
 * @param cmd internal format device command
 * @param cmd_len length of the command
 * @param rsp_len expected length of the response (0=no response)
 * @return 1 on success, 0 on error
 *
 * Translates internal format to external and prepares the
 * transaction for transmission
 */
int AsyncProtocol_prepareTxn(pAsyncProtocol p, pAsyncTxn txn, const char *cmd,
                             int cmd_len, int rsp_len);

/**
 * @brief Private destructor
 * @param p Protocol object
 *
 * Cleans up and releases resources
 */
void AsyncProtocol_killPrivate(pAsyncProtocol p);

#endif                          /* ASYNCPROTOCOL */
