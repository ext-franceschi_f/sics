/*--------------------------------------------------------------------------

	Macros for handling bits in a character array. Stolen somewhere on the
	internet. Working!
	
	Dr. Mark Koennecke 15.6.1994
	
----------------------------------------------------------------------------*/


#include <limits.h>             /* for CHAR_BIT */

#define BITMASK(bit) (1 << ((bit) % CHAR_BIT))
#define BITSLOT(bit) ((bit) / CHAR_BIT)
#define BITSET(ary, bit) ((ary)[BITSLOT(bit)] |= BITMASK(bit))
#define BITTEST(ary, bit) ((ary)[BITSLOT(bit)] & BITMASK(bit))
#define BITUNSET(ary, bit) ((ary)[BITSLOT(bit)] ^= BITMASK(bit))
