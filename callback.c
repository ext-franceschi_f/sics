/*--------------------------------------------------------------------------

			S I C S    C A L L B A C K

  Functions needed to deal with the SICSCallback interface. Description is
  in file interface.h, interface.w and interface.w.
  
  Mark Koennecke, Juli 1997

  Added ScriptCallback, Mark Koennecke, June 2003
  
       Copyright:

       Labor fuer Neutronenstreuung
       Paul Scherrer Institut
       CH-5423 Villigen-PSI


      The authors hereby grant permission to use, copy, modify, distribute,
      and license this software and its documentation for any purpose, provided
      that existing copyright notices are retained in all copies and that this
      notice is included verbatim in any distributions. No written agreement,
      license, or royalty fee is required for any of the authorized uses.
      Modifications to this software may be copyrighted by their authors
      and need not follow the licensing terms described here, provided that
      the new terms are clearly indicated on the first page of each file where
      they apply.

      IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
      FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
      ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
      DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
      POSSIBILITY OF SUCH DAMAGE.

      THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
      FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
      IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
      NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
      MODIFICATIONS.
----------------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <tcl.h>
#include "fortify.h"
#include "lld.h"
#include "sics.h"
#include "macro.h"
#include "splitter.h"

#define CALLBACK 17777
static int debug = 0;

/*-------------- The data stored for a single callback ------------------*/
typedef struct __CBItem {
  long iID;
  SICSCallBack pFunc;
  void *pUserData;
  KillFuncIT pKill;
  int iEvent;
  int killFlag;
  struct __CBItem *next;
} CallBackItem, *pCallBackItem;
/*--------------------- The interface datastructure ---------------------*/
typedef struct __ICallBack {
  int iID;
  pCallBackItem head;
} ICallBack;

/*------------------------------------------------------------------------*/
static int CheckPointer(pICallBack self)
{
  if (self == NULL)
    return 0;
  if (self->iID != CALLBACK) {
    return 0;
  }
  return 1;
}

/*-------------------------------------------------------------------------*/

pICallBack CreateCallBackInterface(void)
{
  pICallBack pNew = NULL;

  pNew = (pICallBack) malloc(sizeof(ICallBack));
  if (!pNew) {
    return 0;
  }

  pNew->iID = CALLBACK;
  pNew->head = NULL;
  return pNew;
}

/*--------------------------------------------------------------------------*/
void DeleteCallBackInterface(pICallBack self)
{
  int iRet;
  pCallBackItem pItem, pTmp;

  if (!CheckPointer(self)) {
    return;
  }

  /* kill all userdata associated with callbacks */
  pItem = self->head;
  while (pItem != NULL) {
    pTmp = pItem->next;
    if (pItem->pKill != NULL) {
      pItem->pKill(pItem->pUserData);
    }
    free(pItem);
    pItem = pTmp;
  }
  free(self);
}

/*--------------------------------------------------------------------------*/
static void markUserdata4Kill(pICallBack self, void *pData)
{
  pCallBackItem pItem = NULL;

  pItem = self->head;
  while (pItem != NULL) {
    if (pData != NULL && pItem->pUserData == pData) {
      pItem->killFlag = 1;
    }
    pItem = pItem->next;
  }
}

/*-------------------------------------------------------------------------*/
static void cleanCallbackList(pICallBack self)
{
  pCallBackItem toKill, current, previous;

  previous = NULL;
  current = self->head;
  while (current != NULL) {
    if (current->killFlag == 1) {
      toKill = current;
      current = current->next;

      /* update link from previous item or from head */
      if (previous == NULL) {
        self->head = current;
      } else {
        previous->next = current;
      }

      if (toKill->pKill != NULL) {
        toKill->pKill(toKill->pUserData);
      }
      free(toKill);
    } else {
      previous = current;
      current = current->next;
    }
  }
}

/*--------------------------------------------------------------------------*/
int InvokeCallBack(pICallBack self, int iEvent, void *pEventData)
{
  pCallBackItem pItem;
  int iCurrent, iRet;
  int iResult = 1, iKill = 0;;

  if (!CheckPointer(self)) {
    return 0;
  }

  pItem = self->head;
  while (pItem != NULL) {
    if (pItem->iEvent == iEvent && pItem->killFlag == 0) {
      iRet = pItem->pFunc(iEvent, pEventData, pItem->pUserData);
      if (iRet < 0) {
        pItem->killFlag = 1;
        if (pItem->pUserData != NULL) {
          markUserdata4Kill(self, pItem->pUserData);
          iKill = 1;
        }
      } else if (iRet != 1) {
        iResult = 0;
      }
    }
    pItem = pItem->next;
  }

  /* kill run */
  if (iKill == 1) {
    cleanCallbackList(self);
  }

  return iResult;
}

/*--------------------------------------------------------------------------*/
static long lCount = 1L;

long RegisterCallback(pICallBack self, int iEvent,
                      SICSCallBack pFunc, void *pUserData, KillFunc pKFunc)
{
  pCallBackItem pItem = NULL;

  if (!CheckPointer(self)) {
    return 0;
  }

  pItem = calloc(1, sizeof(CallBackItem));
  if (pItem == NULL) {
    return -1;
  }
  pItem->iID = lCount++;
  assert(pFunc);
  pItem->pFunc = pFunc;
  pItem->iEvent = iEvent;
  pItem->pUserData = pUserData;
  pItem->pKill = pKFunc;
  pItem->killFlag = 0;
  pItem->next = self->head;
  self->head = pItem;
  if (debug) {
    printf("Registered callback at %p\n", self);
  }
  return pItem->iID;
}

/*------------------------------------------------------------------------*/
static void markById(pICallBack self, int lID)
{
  pCallBackItem pItem = NULL;

  pItem = self->head;
  while (pItem != NULL) {
    if (pItem->iID == lID) {
      pItem->killFlag = 1;
    }
    pItem = pItem->next;
  }
}

/*-------------------------------------------------------------------------*/
int RemoveCallback(pICallBack self, long lID)
{
  CallBackItem sItem;
  int iCurrent;

  if (!CheckPointer(self)) {
    return 0;
  }
  markById(self, lID);
  cleanCallbackList(self);
  return 0;
}

/*--------------------------------------------------------------------------*/
int RemoveCallback2(pICallBack self, void *pUserData)
{
  CallBackItem sItem;
  int iCurrent;

  if (!CheckPointer(self)) {
    return 0;
  }
  markUserdata4Kill(self, pUserData);
  cleanCallbackList(self);
  return 1;
}

/*--------------------------------------------------------------------------*/
/*
 * Remove callback in context -
 * if the callback function matches and the user function returns zero
 */
int RemoveCallbackUsr(pICallBack self, SICSCallBack pFunc, int (*userfunc)(const void* pContext, const void* pUserData), void *pCtx)
{
  pCallBackItem pItem;

  if (!CheckPointer(self))
  {
    return 0;
  }

  pItem = self->head;
  while (pItem != NULL) {
    if (pItem->pFunc == pFunc) {
      if(userfunc(pCtx, pItem->pUserData) == 0) {
        if (debug) {
          printf("Killing callback at %p\n", self);
        }
        pItem->killFlag = 1;
      }
    }
    pItem = pItem->next;
  }
  cleanCallbackList(self);
  return 1;
}

  /*--------------------------------------------------------------------------*/
int RemoveCallbackCon(pICallBack self, SConnection * con)
{
  pCallBackItem pItem;
  SConnection *tst = NULL;

  if (!CheckPointer(self)) {
    return 0;
  }

  pItem = self->head;
  while (pItem != NULL) {
    tst = (SConnection *) pItem->pUserData;
    if (VerifyConnection(tst) && SCGetIdent(tst) == SCGetIdent(con)) {
      if (debug) {
        printf("Killing callback on connection.ident = %ld\n", SCGetIdent(con));
      }
      pItem->killFlag = 1;
    }
    pItem = pItem->next;
  }
  cleanCallbackList(self);
  return 1;
}

/*-------------------------------------------------------------------
 a write function for the connection which writes to stdout
 -------------------------------------------------------------------*/
static int CallbackWrite(SConnection * pCon, char *message, int outCode)
{
  if (outCode >= eWarning) {
    fputs(message, stdout);
    fputs("\n", stdout);
  }
  return 1;
}

/*-----------------------------------------------------------------------
  the actual callback function invoking the script
  ------------------------------------------------------------------------*/
static int ScriptCallback(int iEvent, void *pEventData, void *pUserData)
{
  SConnection *pCon = NULL;
  Tcl_Interp *pTcl;
  int status;

  pCon = SCCreateDummyConnection(pServ->pSics);
  if (!pCon) {
    fprintf(stdout, "ERROR: failed to create dummy connection\n");
    return 0;
  }
  if (pUserData == NULL) {
    SCDeleteConnection(pCon);
    fprintf(stdout, "ERROR: ScriptCallback: no script to execute\n");
    return 0;
  }
  SCSetRights(pCon, usInternal);
  SCSetWriteFunc(pCon,SCNotWrite);
  status = InterpExecute(pServ->pSics, pCon, (char *) pUserData);

  SCDeleteConnection(pCon);
  return 1;
}

/*------------------------------------------------------------------------*/
int CallbackScript(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[])
{
  long lID;
  int iEvent, status;
  pICallBack pCall = NULL;
  CommandList *pCom = NULL;
  char pBuffer[132];

  if (argc < 2) {
    SCWrite(pCon,
            "ERROR: insufficient number of arguments to callbackScript",
            eError);
    return 0;
  }
  /*
     only managers may do this
   */
  if (!SCMatchRights(pCon, usMugger)) {
    return 0;
  }

  strtolower(argv[1]);
  if (strcmp(argv[1], "connect") == 0) {
    if (argc < 4) {
      SCWrite(pCon,
              "ERROR: not enough arguments to CallbackScript connect",
              eError);
      return 0;
    }
    strtolower(argv[2]);
    pCom = FindCommand(pSics, argv[2]);
    if (!pCom) {
      SCWrite(pCon, "ERROR: object to connect to not found", eError);
      return 0;
    }
    pCall = GetCallbackInterface(pCom->pData);
    if (!pCall) {
      SCWrite(pCon, "ERROR: object has no callback interface", eError);
      return 0;
    }
    iEvent = Text2Event(argv[3]);
    if (iEvent < 0) {
      SCWrite(pCon, "ERROR: event type not known", eError);
      return 0;
    }
    Arg2Text(argc - 4, &argv[4], pBuffer, 131);
    lID = RegisterCallback(pCall,
                           iEvent, ScriptCallback, strdup(pBuffer), free);
    sprintf(pBuffer, "callback = %ld", lID);
    SCWrite(pCon, pBuffer, eValue);
    return 1;
  } else if (strcmp(argv[1], "remove") == 0) {
    if (argc < 4) {
      SCWrite(pCon, "ERROR: not enough arguments to CallbackScript remove",
              eError);
      return 0;
    }
    strtolower(argv[2]);
    pCom = FindCommand(pSics, argv[2]);
    if (!pCom) {
      SCWrite(pCon, "ERROR: object to remove to not found", eError);
      return 0;
    }
    pCall = GetCallbackInterface(pCom->pData);
    if (!pCall) {
      SCWrite(pCon, "ERROR: object has no callback interface", eError);
      return 0;
    }
    status = Tcl_GetInt(InterpGetTcl(pSics), argv[3], &iEvent);
    if (status != TCL_OK) {
      SCWrite(pCon, "ERROR: failed to convert callback ID to int", eError);
      return 0;
    }
    RemoveCallback(pCall, (long) iEvent);
    SCSendOK(pCon);
    return 1;
  }

  SCWrite(pCon, "ERROR: subcommand to CallbackScript not known", eError);
  return 0;
}
