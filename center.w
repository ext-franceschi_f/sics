\subsection{Fit and Center}
This is a fit routine for SICS. It takes a scan and tries to find a peak and
its position. Trials showed that fitting a gauss function is not robust
enough for this facility which has to cope with bizarre peak shapes, half
finished measurements and the like. The algorithm choosen tries to find the
center of gravity of the peak. It does this by searching for the maximum
point in the diagram first. Then the points where the peak is below
half maximum are searched for either side of the peak.  Within these
limits the COG is calculated. When this is done fit will print some
info about the peak.   

Center will the drive the scan variable to the COG of the peak.

The interface to this simple facility is simple:

@d fitinter @{
   typedef struct __FitCenter *pFit;

/*--------------------------------------------------------------------------*/
   pFit CreateFitCenter(pScanData pScan);
   void DeleteFitCenter(void *pData);
/*-------------------------------------------------------------------------*/
  int CalculateFit(pFit self);
  /* 
      CalcluateFit returns: -1 when left FWHM could not be found
                            -2 when right FWHM could not be found
                             1 on success
  */
  int CalculateFitFromData(pFit self, float fAxis[], long lSum[], 
                           int iLen);
  void GetFitResults(pFit self, float *fNewCenter, float *fStdDev,
                    float *FWHM, float *fMax);
  int DriveCenter(pFit self, SConnection *pCon, SicsInterp *pSics);
/*-------------------------------------------------------------------------*/
  int FitFactory(SConnection *pCon,SicsInterp *pSics, void *pData,
                 int argc, char *argv[]);
  int FitWrapper(SConnection *pCon,SicsInterp *pSics, void *pData,
                 int argc, char *argv[]);
  int CenterWrapper(SConnection *pCon,SicsInterp *pSics, void *pData,
                 int argc, char *argv[]);
@}

@o fitcenter.h @{
/*---------------------------------------------------------------------------
                          F I T  C E N T E R

 A simple peak finding and center of gravity determination facility for
 SICS.

 copyright: see copyright.h

 Mark Koennecke, October 1997
----------------------------------------------------------------------------*/
#ifndef SICSFITCENTER
#define SICSFITCENTER
@<fitinter@>
#endif
@}
