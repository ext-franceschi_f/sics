
/*------------------------------------------------------------------------
                    C H a d a p t e  r

 This is the header file for a drive adapter for collaboration with a
 general device controller as implemented in choco.*

 Mark Koennecke, January 1998
--------------------------------------------------------------------------*/
#ifndef SICSCHADA
#define SICSCHADA

#include "SCinter.h"
#include "conman.h"
#include "codri.h"
#include "evdriver.h"

typedef struct __CHADAPTER *pCHAdapter;
/*-----------------------------------------------------------------------*/
int CHAdapterFactory(SConnection * pCon, SicsInterp * pSics,
                     void *pData, int argc, char *argv[]);

int CHAdapterAction(SConnection * pCon, SicsInterp * pSics,
                    void *pData, int argc, char *argv[]);

pEVDriver MakeControllerEnvironmentDriver(int argc, char *argv[]);


#ifdef CHADAINTERNAL

typedef struct __CHADAPTER {
  pObjectDescriptor pDes;
  pCodri pDriv;
  pIDrivable pInt;
  float fUpper;
  float fLower;
  float fTarget;
  char *pParName;
} CHAdapter;


typedef struct __CHEV {
  char *pParName;
  pCodri pDriv;
  int iLastError;
} CHev, *pCHev;

#endif
#endif
