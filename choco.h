
/*-----------------------------------------------------------------------
                C h o p p e r C o n t r o l l e r

  This is both the header file for a general controller and a SICS
  chopper controller.

  Mark Koennecke, January 1998
--------------------------------------------------------------------------*/
#ifndef CHOCOSICS
#define CHOCOSICS

#include "SCinter.h"
#include "conman.h"
#include "codri.h"

typedef struct __CHOCO *pChoco;
/*------------------------------------------------------------------------*/
int CHGetParameter(pChoco self, char *pParName,
                   char *pParValue, int iBuflen);

pCodri CHGetDriver(pChoco self);
int CHList(pChoco self, SConnection * pCon, char *name);
/*------------------------------------------------------------------------*/
void KillChoco(void *pData);
int ChocoAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);
int ChocoFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[]);


#ifdef CHOCOINTERNAL

typedef struct __CHOCO {
  pObjectDescriptor pDes;
  pCodri pDriv;
} Choco;

#endif
#endif
