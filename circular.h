/*--------------------------------------------------------------------------
                     C I R C U L A R

  This is the implementation of a general purpose circular buffer facility.
  
  Mark Koennecke, October 1999
  --------------------------------------------------------------------------*/
#ifndef CIRCULAR
#define CIRCULAR

typedef struct __CIRCULAR *pCircular;
typedef void (*CirKillFunc) (void *pData);

/* ----------------- birth and death -----------------------------------*/
pCircular createCircular(int iSize, CirKillFunc kf);
  /*
     iSize is the size of the circular Buffer.
     KillFunc is a function which can safely delete the data item held
     as content of the circular buffer.
   */
void deleteCircular(pCircular self);

/*-------------- access and modify data item at current position ----------*/
void setCircular(pCircular self, void *pData);
void *getCircular(pCircular self);

/*---------------- pointer movement --------------------------------------*/
void nextCircular(pCircular self);
void previousCircular(pCircular self);

#endif
