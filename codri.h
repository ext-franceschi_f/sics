
/*-------------------------------------------------------------------------
                 C o n t r o l l e r D r i v e r

 This file contains the description of the data structure for a
 general controller driver.

 Mark Koennecke, January 1998
--------------------------------------------------------------------------*/
#ifndef CODRIV
#define CODRIV
#define CHFAIL -1
#define CHREDO -2
#define CHOK   -3

typedef struct __CODRI *pCodri;
typedef struct __CODRI {
  int (*Init) (pCodri self);
  int (*Close) (pCodri self);
  int (*Delete) (pCodri self);
  int (*SetPar) (pCodri self, char *parname, float fValue);
  int (*SetPar2) (pCodri self, char *parname, char *value);
  int (*GetPar) (pCodri self, char *parname, char *pBuffer, int iBufLen);
  int (*CheckPar) (pCodri self, char *parname);
  int (*GetError) (pCodri self, int *iCode, char *pError, int iErrLen);
  int (*TryFixIt) (pCodri self, int iCode);
  int (*Halt) (pCodri self);
  char *pParList;
  void *pPrivate;
} Codri;


#endif
