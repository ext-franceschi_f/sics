/*---------------------------------------------------------------------------

		C O M E N T R Y 
		
  some helper stuff for implementing MultiMotors. Functions in mumo.c
  
  Mark Koennecke, February 1997
---------------------------------------------------------------------------*/
#ifndef COMENTRY
#define COMENTRY

#include "conman.h"
#include "motor.h"

#define MAXDEV 10
typedef struct {
  void *pData;
  char name[80];
  pObjectDescriptor pDescriptor;
  float fVal;
  int iCount;
} DevEntry;

/* -------------------The Entry per registered command --------------------*/
typedef struct __ComEntry {
  char name[10];
  char *pCommand;
  int iDevice;
  DevEntry pDevice[MAXDEV];
  struct __ComEntry *pNext;
  struct __ComEntry *pPrevious;
} ComEntry, *pComEntry;

typedef struct __NAMPOS {
  char *name;                   /* the name */
  pComEntry pCom;               /* the positions */
  char *text;                   /* explanatory text */
  struct __NAMPOS *pNext;
  struct __NAMPOS *pPrevious;
} NamPos, *pNamPos;

typedef struct __NAMMAP {
  char *alias;
  char *motname;
  pMotor pMot;
} NamMap, *pNamMap;

int CheckComEntryBounds(pComEntry self, SConnection * pCon);
int AddExeEntry(pExeList self, pComEntry pNew, SConnection * pCon);
pComEntry CreateComEntry(void);
pComEntry CopyComEntry(pComEntry pOld);
int AddDevEntry(pComEntry pCom, char *name, void *pData,
                pObjectDescriptor pDes, float fVal);
pNamPos LinkNamPos(pNamPos pHead, pNamPos pNew);
pNamPos UnlinkNamPos(pNamPos pHead, pNamPos pOld);
#endif
