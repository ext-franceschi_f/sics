/*-------------------------------------------------
  This file holds the command context structure which 
  is needed to make the sycamore protocol work.

  Mark Koennecke, December 2005
  -------------------------------------------------*/
#ifndef SICSCOMCONTEXT
#define SICSCOMCONTEXT

typedef struct {
  int transID;
  char deviceID[256];
} commandContext, *pCommandContext;
#define SCDEVIDLEN 256

#endif
