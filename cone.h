
/*----------------------------------------------------------------------
  SICS cone module for cone scans. Form more details see conescan.tex
  and cone.tex.
  
  COPYRIGHT: see file COPYRIGHT
  
  Mark Koennecke, March 2006
------------------------------------------------------------------------*/
#ifndef SICSCONE
#define SICSCONE
#include "sics.h"
#include "ubcalc.h"

/*-----------------------------------------------------------------------*/
typedef struct {
  pIDrivable pDriv;
  float lastConeAngle;
  pHKL pHkl;
} coneData, *pConeData;
/*----------------------------------------------------------------------*/
int MakeCone(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[]);
#endif
