/*--------------------------------------------------------------------------
`	This is a common header for rarely used configuration
        functions. Implementations are distributed across several
        files. Therefor they are mentioned in a comment.
       Mark Koennecke, December 1996
       
       copyrights: see implementation files 
--------------------------------------------------------------------------*/
#ifndef SICSCONFIG
#define SICSCONFIG

#include "SCinter.h"
#include "conman.h"

int AddHalt(SConnection * pCon, SicsInterp * pSics, void *pData,
            int argc, char *argv[]);
  /* configuration command: enters argv objects into Halt List of Interrupt.
     This is the means how the server knows which hardware to halt in an
     case of emergency. Implemented in intserv.c
   */


int ListObjects(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);
  /*
     lists all avialable objects. Realised in Scinter.c
   */
int SicsAtt(SConnection * pCon, SicsInterp * pSics, void *pData,
            int argc, char *argv[]);
  /*
     handling of SICS object attributes. In SCinter.c
   */
#endif
