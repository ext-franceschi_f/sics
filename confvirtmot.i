
/*-----------------------------------------------------------------------
  Configurable Virtual Motor. This is an generated file describing the
  internal data structure of this object. Do not edit.
-----------------------------------------------------------------------*/

typedef struct __CONFVIRTMOT {
        pObjectDescriptor pDes;
        pIDrivable pDriv;
        pICallBack pCall;
        char *name;
        char *scriptName;
        char *readScript;
        char *checkScript;
        char *state;
        int motorList;
        float targetValue;
        int targetReached;
        int posCount;
        double last_report_time;
        char scriptError[512];
        int  parseOK;
}ConfigurableVirtualMotor, *pConfigurableVirtualMotor;


