\subsection{Configurable Virtual Motor}
This is an object which can help in realizing complex scans in space.
In principle this object implements a virtual motor. When this motor is
started a script is called with the desired new position as a parameter.
This script is supposed to either return an error or a comma separated
list of strings motor=value. The name of this script is a configurable
parameter. The object then takes care of starting all the motors (or 
better Drivables) and watching over them during the driving operation.

This objects data structure:
@d confvirt @{
typedef struct __CONFVIRTMOT {
	pObjectDescriptor pDes;
	pIDrivable pDriv;
	pICallBack pCall;
	char *name;
	char *scriptName;
	char *readScript;
	char *checkScript;
	char *state;
	int motorList;
	float targetValue;
	int targetReached;
	int posCount;
        double last_report_time;
	char scriptError[512];
	int  parseOK;
}ConfigurableVirtualMotor, *pConfigurableVirtualMotor;
@}  
The fields are:
\begin{description}
\item[pDes] The standard object descriptor.
\item[pDriv] The drivable interface
\item[pCall] The callback interface
\item[name] The name of the virtual motor
\item[scriptName] The name of the program to calculate the new positions
of the real motors.
\item[readScript] A script to read back the current value of the virtual motor.
\item[checkScript] A script to check for success at the end of driving.
\item[motorList] A list of the motors to start and control.
\item[targetValue] The target value we wanted to have.
\item[posCount] This counter prevents the callback from being invoked too often.  The frequency is currently set to every 10 cycles through the task loop.
\item[targetReached] A flag which becomes true when a check has showed that
all desired motors have reached their targets.
\item[scriptError] A temporary buffer holding all errors encountered
during processing of the script.
\item[parseOK] a flag which indicates if parsing and execution of the
script went OK.
\end{description}


All the magic of this object is hidden in the implementation of the methods
of the drivable interface. Additioanlly there is the standard interpreter 
interface.

@o confvirtmot.i @{
/*-----------------------------------------------------------------------
  Configurable Virtual Motor. This is an generated file describing the
  internal data structure of this object. Do not edit.
-----------------------------------------------------------------------*/
@< confvirt @>

@}

@o confvirtmot.h @{
/*-----------------------------------------------------------------------
  A configurable virtual motor object. At motor start a script is called
  which returns the motors and positions to drive as a comma separated 
  list holding entries of the form mot=value.

  COPYRIGHT: see file COPYRIGHT

  Mark Koennecke, August 2004
-------------------------------------------------------------------------*/
#ifndef CONFIGURABLEVIRTUALMOTOR
#define CONFIGURABLEVIRTUALMOTOR

#include "sics.h"

int MakeConfigurableVirtualMotor(SConnection *pCon, SicsInterp *pSics,
	void *data, int aargc, char *argv[]);
int ConfigurableVirtualMotorAction(SConnection *pCon, SicsInterp *pSics,
	void *data, int argc, char *argv[]);
#endif

@}
