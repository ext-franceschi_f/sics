/*--------------------------------------------------------------------------
                       C O N N E C T I O N   O B J E C T

 This file defines the connection object data structure and the interface to
 this data structure. This is one of the most important SICS components.


  Substantially revised from a prior version.

  Mark Koennecke, September 1997

  Mark Koennecke, Aprl 2003

  Mark Koennecke, December 2004

  copyright: see copyright.h
  
  substantially revised for the new asynchronous I/O system
  
  Mark Koennecke, January 2009
----------------------------------------------------------------------------*/
#ifndef SICSCONNECT
#define SICSCONNECT
#include <stdio.h>
#include "costa.h"
#include "SCinter.h"
#include "network.h"
#include "asynnet.h"
#include "obdes.h"
#include "commandcontext.h"
#include "dynstring.h"

#define MAXLOGFILES 10

typedef int (*writeFunc) (struct __SConnection * pCon,
                          char *pMessage, int iCode);

typedef struct __SConnection SConnection;

#include "nserver.h"

/*------------------------------ live & death ----------------------------*/
SConnection *SCreateConnection(SicsInterp * pSics, int sockHandle,
                               int iUserRights);
SConnection *SCCreateDummyConnection(SicsInterp * pSics);
void SCDeleteConnection(void *pVictim);
SConnection *SCCopyConnection(SConnection * pCon);
SConnection *SCfindMaster(SConnection * pCon);
int SCisConnected(SConnection * pCon);
int VerifyConnection(SConnection * pCon);
int SCVerifyConnection(SConnection * self);
void SCClose(SConnection *pCon);
/*------------------------------- tasking --------------------------------*/
int SCTaskFunction(void *pCon);
void SCSignalFunction(void *pCon, int iSignal, void *pSigData);
SConnection *GetSendingConnection(void);
/* ***************************** I/O **************************************  */
void SCSetOutputClass(SConnection * self, int iClass);
int SCWrite(SConnection * self, const char *pBuffer, int iOut);
#if __GNUC__ > 2
#define G_GNUC_PRINTF( format_idx, arg_idx )    \
  __attribute__((__format__ (__printf__, format_idx, arg_idx)))
#else
#define G_GNUC_PRINTF( format_idx, arg_idx )
#endif
int SCPrintf(SConnection * self, int iOut, const char *fmt, ...) G_GNUC_PRINTF (3, 4);
int SCPf(writeFunc func, SConnection * self, int iOut, const char *fmt, ...) G_GNUC_PRINTF (4, 5);
#undef G_GNUC_PRINTF
int SCRead(SConnection * self, char *pBuffer, int iBufLen);
int SCPrompt(SConnection * pCon, char *pPrompt, char *pResult, int iLen);
int SCPromptTMO(SConnection * pCon, char *pPrompt, char *pResult, int iLen, int timeout);
int SCSendOK(SConnection * self);
int SCnoSock(SConnection * pCon);
int SCWriteUUencoded(SConnection * pCon, char *pName, void *iData,
                     int iLen);
int SCWriteZipped(SConnection * pCon, char *pName, void *pData,
                  int iDataLen);
int SCWriteBinary(SConnection * pCon, char *pName, void *pData,
                  int iDataLen);
writeFunc SCGetWriteFunc(SConnection * pCon);
void SCSetWriteFunc(SConnection * pCon, writeFunc x);
int SCOnlySockWrite(SConnection * self, char *buffer, int iOut);
int SCPureSockWrite(SConnection * self, char *buffer, int iOut);
int SCLogWrite(SConnection * self, char *buffer, int iOut);
int SCFileWrite(SConnection * self, char *buffer, int iOut);
int SCNotWrite(SConnection * self, char *buffer, int iOut);
int SCNormalWrite(SConnection * self, char *buffer, int iOut);
int SCWriteWithOutcode(SConnection * self, char *buffer, int iOut);
int SCACTWrite(SConnection * self, char *buffer, int iOut);
/*********************** I/O Buffering ***********************************/
int SCStartBuffering(SConnection * pCon);
pDynString SCEndBuffering(SConnection * pCon);
/******************************* Error **************************************/
void SCSetInterrupt(SConnection * self, int eCode);
int SCGetInterrupt(SConnection * self);
/****************************** Macro ***************************************/
int SCinMacro(SConnection * pCon);
int SCsetMacro(SConnection * pCon, int iMode);
/************************** parameters changed ? **************************/
void SCparChange(SConnection * pCon);
/* *************************** Info *************************************** */
int SCGetRights(SConnection * self);
int SCSetRights(SConnection * pCon, int iNew);
int SCMatchRights(SConnection * pCon, int iCode);
int SCGetOutClass(SConnection * self);
int SCGetGrab(SConnection * pCon);
int SCActive(SConnection * pCon);
int SCGetRunLevel(SConnection *pCon);
long SCGetIdent(SConnection *pCon);
int SCGetSicsError(SConnection *pCon);
int SCGetTransID(SConnection *pCon);
int SCGetProtocolID(SConnection *pCon);
int SCGetSockHandle(SConnection *pCon);
int SCGetConStatus(SConnection *pCon);
int SCGetEventType(SConnection *pCon);
char *SCGetDeviceID(SConnection *pCon);
int SCGetEnd(SConnection *pCon);
int SCGetListening(SConnection * pCon);
void SCSetListening(SConnection * pCon, int newVal);
void SCIncrInUse(SConnection *pCon);
void SCDecrInUse(SConnection *pCon);
/************************* connection parameter change **********************/
void SCSetConStatus(SConnection *pCon, int conStatus);
void SCSetEventType(SConnection *pCon, int eventType);
void SCSetSicsError(SConnection *pCon, int sicsError);
void SCSetProtocolID(SConnection *pCon, int proID);
void SCSetGrab(SConnection *pCon, int iGrab);
void SCSetEnd(SConnection *pCon, int val);
void SCSetTelnet(SConnection *pCon, int val);
void SCSetDeviceID(SConnection *pCon, const char *newID);
void SCSetTransID(SConnection *pCon, int newID);
/* **************************** Invocation ******************************** */
int SCInvoke(SConnection * self, SicsInterp * pInter, char *pCommand);

/*************************** User Command **********************************/
int ConfigCon(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[]);
int ConSicsAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                  int argc, char *argv[]);

/*------------------------------------------------------------------------*/
int SCDoSockWrite(SConnection * self, char *buffer);
int SCWriteInContext(SConnection * pCon, const char *buffer, int code,
                     commandContext cc);


/* ================== =====================================================
 * These routines are obsolete and may not even work anymore.
 * Mark Koennecke, January 2009
 * ==========================================================================*/
void SCWriteToLogFiles(SConnection * self, char *buffer);
long SCTagContext(SConnection * self, char *tagName);
long SCAdvanceContext(SConnection * self, char *tagName);
int SCPushContext(SConnection * pCon, int ID, char *deviceID);
int SCPushContext2(SConnection * pCon, commandContext cc);
int SCPopContext(SConnection * pCon);
commandContext SCGetContext(SConnection * pCon);
/*************************   CallBack *********************************** */
int SCRegister(SConnection * pCon, SicsInterp * pSics,
               void *pInter, long lID);
int SCUnregister(SConnection * pCon, void *pInter);
   /**
    * delete a callback with the given ID
    */
int SCUnregisterID(SConnection * pCon, long ID);
   /**
    * retrieve the ID of a callback on the callback interface
    * given in pData. This, together with SCUnregisterID allows to
    * ceanly remove all callbacks on a connection
    * returns -1 if no ID can be found.
    */
long SCgetCallbackID(SConnection * pCon, void *pData);
/*************************** command stack handling **********************/
void SCCostaLock(SConnection *pCon);
void SCCostaUnLock(SConnection *pCon);
int SCCostaLocked(SConnection *pCon);
int SCCostaTop(SConnection *pCon, char *command);
int SCCostaPop(SConnection *pCon, char **command);

#endif
