/*----------------------------------------------------------------------------
                           C O S T A
                           
  Implementation of a command stack for use with connection objects in
  SICS
  
  Mark Koennecke, Septemeber 1997

       Copyright:

       Labor fuer Neutronenstreuung
       Paul Scherrer Institut
       CH-5423 Villigen-PSI


      The authors hereby grant permission to use, copy, modify, distribute,
      and license this software and its documentation for any purpose, provided
      that existing copyright notices are retained in all copies and that this
      notice is included verbatim in any distributions. No written agreement,
      license, or royalty fee is required for any of the authorized uses.
      Modifications to this software may be copyrighted by their authors
      and need not follow the licensing terms described here, provided that
      the new terms are clearly indicated on the first page of each file where
      they apply.

      IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
      FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
      ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
      DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
      POSSIBILITY OF SUCH DAMAGE.

      THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
      FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
      IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
      NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
      MODIFICATIONS.
----------------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include "fortify.h"
#include "lld.h"
#include "costa.h"
#include "costa.i"


/*-------------------------------------------------------------------------*/
pCosta CreateCommandStack(void)
{
  pCosta pNew = NULL;

  pNew = (pCosta) malloc(sizeof(Costa));
  if (!pNew) {
    return NULL;
  }
  memset(pNew, 0, sizeof(Costa));
  pNew->iList = LLDcreate(sizeof(char *));
  if (pNew->iList < 0) {
    free(pNew);
    return NULL;
  }
  pNew->iCount = 0;
  pNew->iMaxSize = INT_MAX;
  return pNew;
}

/*-------------------------------------------------------------------------*/
void DeleteCommandStack(pCosta self)
{
  int iRet;
  char *pPtr;

  assert(self);

  iRet = LLDnodePtr2First(self->iList);
  while (iRet != 0) {
    pPtr = NULL;
    LLDnodeDataTo(self->iList, &pPtr);
    if (pPtr) {
      free(pPtr);
    }
    iRet = LLDnodePtr2Next(self->iList);
  }
  LLDdelete(self->iList);
  free(self);
}

/*--------------------------------------------------------------------------*/
int SetCommandStackMaxSize(pCosta self, int iNew)
{
  assert(self);

  if (iNew > 0) {
    self->iMaxSize = iNew;
    return 1;
  } else {
    return 0;
  }
}

/*--------------------------------------------------------------------------*/
int CostaTop(pCosta self, char *pCommand)
{
  char *pPtr = NULL;
  int iRet, iRes = 1;

  assert(self);

  /* check Size */
  if (self->iCount >= self->iMaxSize) {
    return 0;
  }

  /* do not want 0 commands */
  if (pCommand == NULL || strlen(pCommand) < 1) {
    return 1;
  }

  pPtr = strdup(pCommand);
  iRet = LLDnodeAppendFrom(self->iList, &pPtr);
  if (iRet < 0) {
    iRes = 0;
  }
  self->iCount++;
  return iRes;
}

/*--------------------------------------------------------------------------*/
int CostaBottom(pCosta self, char *pCommand)
{
  char *pPtr = NULL;
  int iRet, iRes = 1;
  assert(self);

  /* do not want 0 commands */
  if (strlen(pCommand) < 1) {
    return 1;
  }

  pPtr = strdup(pCommand);
  iRet = LLDnodePrependFrom(self->iList, &pPtr);
  if (iRet < 0) {
    iRes = 0;
  }
  self->iCount++;
  return iRes;
}

/*--------------------------------------------------------------------------*/
int CostaPop(pCosta self, char **pBuf)
{
  char *pPtr = NULL;
  int iRet;

  assert(self);
  iRet = LLDnodePtr2First(self->iList);
  if (iRet != 0) {
    LLDnodeDataTo(self->iList, &pPtr);
    *pBuf = pPtr;
    LLDnodeDelete(self->iList);
    self->iCount--;
    return 1;
  }
  *pBuf = NULL;
  return 0;
}

/*--------------------------------------------------------------------------*/
void CostaLock(pCosta self)
{
  self->iLock = 1;
}

/*--------------------------------------------------------------------------*/
void CostaUnlock(pCosta self)
{
  self->iLock = 0;
}

/*--------------------------------------------------------------------------*/
  int CostaLocked(pCosta self)
  {
    return self->iLock;
  }

