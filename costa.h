
/*-------------------------------------------------------------------------
                             C O S T A

  A command stack implementation for SICS. To be used by each connection.

  Mark Koennecke, September 1997

  copyright: see implementation file.

----------------------------------------------------------------------------*/
#ifndef SICSCOSTA
#define SICSCOSTA

typedef struct __costa *pCosta;

/*----------------------------- live & death ----------------------------*/
pCosta CreateCommandStack(void);
void DeleteCommandStack(pCosta self);
int SetCommandStackMaxSize(pCosta self, int iNewSize);
/*----------------------------------------------------------------------*/
int CostaTop(pCosta self, char *pCommand);
int CostaBottom(pCosta self, char *pCommand);
int CostaPop(pCosta self, char **pPtr);
/*----------------------------------------------------------------------*/
void CostaLock(pCosta self);
void CostaUnlock(pCosta self);
int CostaLocked(pCosta self);

#endif
