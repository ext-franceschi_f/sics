/*------------------------------------------------------------------------
                     G E N C O U N T

      Some general stuff for handling a CounterDriver.


	Mark Koennecke,    January 1997

	Copyright:

	Labor fuer Neutronenstreuung
	Paul Scherrer Institut
	CH-5423 Villigen-PSI


        The authors hereby grant permission to use, copy, modify, distribute,
        and license this software and its documentation for any purpose, provided
        that existing copyright notices are retained in all copies and that this
        notice is included verbatim in any distributions. No written agreement,
        license, or royalty fee is required for any of the authorized uses.
        Modifications to this software may be copyrighted by their authors
        and need not follow the licensing terms described here, provided that
        the new terms are clearly indicated on the first page of each file where
        they apply.

        IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
        FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
        ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
        DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
        POSSIBILITY OF SUCH DAMAGE.

        THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
        INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
        IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
        NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
        MODIFICATIONS.
----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "fortify.h"
#include <string.h>
#include "sics.h"
#include "countdriv.h"

/*-------------------------------------------------------------------------*/
pCounterDriver CreateCounterDriver(char *name, char *type)
{
  pCounterDriver pRes = NULL;

  pRes = (pCounterDriver) malloc(sizeof(CounterDriver));
  if (!pRes) {
    return NULL;
  }
  memset(pRes, 0, sizeof(CounterDriver));

  pRes->name = strdup(name);
  pRes->type = strdup(type);
  pRes->eMode = eTimer;
  pRes->fPreset = 1000.;
  pRes->fTime = 0.;
  pRes->iNoOfMonitors = 0;
  pRes->iControlMonitor = 0;
  pRes->iPause = 0;
  pRes->Start = NULL;
  pRes->GetStatus = NULL;
  pRes->ReadValues = NULL;
  pRes->GetError = NULL;
  pRes->TryAndFixIt = NULL;
  pRes->Halt = NULL;
  pRes->pData = NULL;
  pRes->Pause = NULL;
  pRes->Continue = NULL;

  return pRes;
}

/*-------------------------------------------------------------------------*/
void DeleteCounterDriver(pCounterDriver self)
{
  assert(self);
  if (self->name) {
    free(self->name);
  }
  if (self->type) {
    free(self->type);
  }
  if (self->pData) {
    if (self->KillPrivate != NULL) {
      self->KillPrivate(self);
    }
  }
  free(self);
}
