/*---------------------------------------------------------------------------
 	                 C O U N T E R D R I V E R


 	This is the interface to a Sics-Counter driver. This means a
 	single counter managing possibly several monitors in one go.

 	A counter can run for a predefined time or until a predefined
 	monitor count has been reached.
 
 	Mark Koennecke, January 1996

        General parameter setting added:
        Mark Koennecke, April 1999

 	copyright: see implementation file.

 ---------------------------------------------------------------------------*/
#ifndef SICSCOUNTERDRIVER
#define SICSCOUNTERDRIVER

#define COTERM 0
#define COREDO 1

#define MAXCOUNT 9              /* No of monitors + actual counter */


/* Parameter codes for the Set/Get pair of routines */
/*-------- threshold */
#define PARTHRESHOLD 1

/* counter driver additional error codes*/
#define UNKNOWNPAR -5000
#define BADCOUNTER  -5001

#include "sics.h"

typedef struct __COUNTER {
  /* variables */
  char *name;
  char *type;
  CounterMode eMode;
  float fPreset;
  float fLastCurrent;
  float fTime;
  int iNoOfMonitors;
  int iControlMonitor;
  long lCounts[MAXCOUNT];
  int iPause;
  int iErrorCode;
  /* functions */
  int (*GetStatus) (struct __COUNTER * self, float *fControl);
  int (*Start) (struct __COUNTER * self);
  int (*Pause) (struct __COUNTER * self);
  int (*Continue) (struct __COUNTER * self);
  int (*Halt) (struct __COUNTER * self);
  int (*ReadValues) (struct __COUNTER * self);
  int (*GetError) (struct __COUNTER * self, int *iCode,
                   char *error, int iErrLen);
  int (*TryAndFixIt) (struct __COUNTER * self, int iCode);
  int (*Set) (struct __COUNTER * self, char *name, int iCter, float fVal);
  int (*Get) (struct __COUNTER * self, char *name, int iCter, float *fVal);
  int (*Send) (struct __COUNTER * self, char *pText,
               char *pReply, int iReplyLen);
  void (*KillPrivate) (struct __COUNTER * self);
  void *pData;                  /* counter specific data goes here, ONLY for
                                   internal driver use!
                                 */
} CounterDriver, *pCounterDriver;

/*------------------------------------------------------------------------*/

/* countdriv.c */
pCounterDriver CreateCounterDriver(char *name, char *type);
void DeleteCounterDriver(pCounterDriver self);

  /* PSI EL737 Counter */
pCounterDriver NewEL737Counter(char *name, char *host, int iPort,
                               int iChannel);
void KillEL737Counter(pCounterDriver self);

  /* PSI Simulation counter, if you have no hardware */
/* simcter.c */
pCounterDriver NewSIMCounter(char *name, float fVal);
void KillSIMCounter(pCounterDriver self);

  /*
   * McStas simulation counter driver
   * file: mcstascounter.c
   */
pCounterDriver NewMcStasCounter(char *name);

    /*
     * for regression testing
     * file: regresscter.c
     */
pCounterDriver NewRegressCounter(char *name);
#endif
