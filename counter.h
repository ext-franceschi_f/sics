/*-------------------------------------------------------------------------

			C O U N T E R
			
	The SICS Interface to a single detector and his associated 
	monitors.
	
        Mark Koennecke, January 1996
        
        copyright: see implementation file.
        
   Massively reworked to accomodate second generation counter objects.
   
   Mark Koennecke, January 2009 
----------------------------------------------------------------------------*/
#ifndef SICSCOUNTER
#define SICSCOUNTER
#include "SCinter.h"
#include "conman.h"
#include "countdriv.h"

typedef struct __Counter{
  pObjectDescriptor pDes;
  pHdb objectNode;
  char *name;
  int isUpToDate;
  int iExponent;
  pICountable pCountInt;
  pCounterDriver pDriv;
  pICallBack pCall;
  unsigned long tStart;
  int iCallbackCounter;
  int badStatusCount;
  int haltFixFlag; /*  solely here to prevent multiple calls to the halt function on overrun timers in countersec.c*/
  int tbLength; /* These two for caching float time bins in second generation HM's */ 
  float *timeBinning;
  char *error;
  int (*setMode)(struct __Counter *self, CounterMode eMode);
  CounterMode (*getMode)(struct __Counter *self);
  int (*getNMonitor)(struct __Counter *self);
  int (*setPreset)(struct __Counter *self, float val);
  float (*getPreset)(struct __Counter *self);
  float (*getControlValue)(struct __Counter *self);
  long (*getCounts)(struct __Counter *self, SConnection *pCon);
  long (*getMonitor)(struct __Counter *self, int iNum, SConnection *pCon);
  void (*setMonitor)(struct __Counter *self, int iNum, long val);
  float (*getTime)(struct __Counter *self, SConnection *pCon);
} Counter, *pCounter;

/*----------------------------- birth & death -----------------------------*/

pCounter CreateSecCounter(SConnection *pCon, char *type, char *name, int length);
pCounter CreateCounter(char *name, pCounterDriver pDriv);
void DeleteCounter(void *self);
int MakeCounter(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);
/* in countersec.c */
int MakeSecCter(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);
/*------------------------- set/get Parameters ----------------------------*/
int SetCounterMode(pCounter self, CounterMode eNew);
CounterMode GetCounterMode(pCounter self);

int SetCounterPreset(pCounter self, float fVal);
float GetCounterPreset(pCounter self);
float GetControlValue(pCounter self);

long GetCounts(pCounter self, SConnection * pCon);
long GetMonitor(pCounter self, int iNum, SConnection * pCon);
int GetNMonitor(pCounter self);
int GetControlMonitor(pCounter self);
int SetControlMonitor(pCounter self, int channel);
void SetMonitorValue(pCounter self, int index, long value);
float GetCountTime(pCounter self, SConnection * pCon);

int DoCount(pCounter self, float fPreset, SConnection * pCon, int iBlock);

/*-------------------------------------------------------------------------
   the real action: starting and checking is packaged with the 
   ObjectDescriptor.
*/

int CountAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);

#endif
