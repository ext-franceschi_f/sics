\subsection{Data Number}
In some points of its life SICS has to write data files. The file names 
usually consist of a header, a serial number and an indicator for the 
year. The serial number must be unique and steadliy increasing. In order to
achieve this, the serial number are written into a file after any change.
Incrementing the serial number thus involves the following steps:
\begin{itemize}
\item Open file and read current number.
\item Increment number
\item Write File and close
\end{itemize}
This little task is implemented in this module.

The interface to this looks like:
@d dni @{
    typedef struct __DataNumber *pDataNumber;

    pDataNumber CreateDataNumber(char *pFilename);
    void        DeleteDataNumber(void *pData);

    int IncrementDataNumber(pDataNumber self, int *iYear);

    int DecrementDataNumber(pDataNumber self);

    int DNWrapper(SConnection *pCon, SicsInterp *pSics, void *pData,
                  int argc, char *argv[]);

    int DEWrapper(SConnection *pCon, SicsInterp *pSics, void *pData,
                  int argc, char *argv[]);

    int DNFactory(SConnection *pCon, SicsInterp *pSics, void *pData,
                  int argc, char *argv[]);
@}
\begin{description}
\item [CreateDataNumber] creates a data number data structure. It checks if
the file requested as parameter exists and asserts if not. Returns a pointer
on success, NULL else.
\item [DeleteDataNumber] deletes a data number structure form memory. 
\item [IncrementDataNumber] is the main working function of this module.
It performs the steps listed above. It returns a new id for the data number
in case of success, a negative value otherwise. iYear is filled with a value
for the year. 
\item[DecrementDataNumber] decrements the data number and is used for 
 implementing the killdata function. Whis is the invalidation of a 
 data file by overwriting it.
\item[DNWrapper] is the wrapper function which makes DataNumber accessible
from within SICS. 
\item[DEWrapper] is the wrapper for the killdata functionality.
\item [DNFactory] is the SICS factory function which creates a Data Number
object from the initialisation file. Only parameter is the filename.
\end{description}

@o danu.h -d @{
/*-----------------------------------------------------------------------
                        D A T A N U M B E R

  A module to provide a unique data number for data file writing.

  Mark Koennecke, Juli 1997
  
  copyright: see implementation file.

---------------------------------------------------------------------------*/
#ifndef SICSDATANUMBER
#define SICSDATANUMBER

@<dni@>

#endif
@}







