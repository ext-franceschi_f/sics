/*--------------------------------------------------------------------------

        D E F I N E A L I A S E S . C
	
	Markus Zolliker, September 2000
	
	copyright: see implementation file

	More general and safe Aliases:

        - an alias may be defined even if the corresponding command
          does not yet exist

        - SICS does not crash when the original command of an alias is
          removed

---------------------------------------------------------------------------*/
#ifndef DEFINE_ALIAS
#define DEFINE_ALIAS
#include "conman.h"
#include "definealias.i"

/*---------------------------------------------------------------------------*/
const char *TranslateAlias(AliasList * pAList, const char *pCmd);
  /*
     translate the command *pCmd
     - the translation may go through several steps
     - if no translation is found, the return value is equal to pCmd
     - no strings are copied
     - the return value becomes invalid when the corresponding alias is removed
     - *pCmd must be lowercase
   */

/*---------------------------------------------------------------------------*/
int RemoveAlias(AliasList * pAList, const char *pCmd);
  /*
     remove the alias *pCmd
     - returns 1 when the alias existed, 0 otherwise
     - *pCmd must be lowercase
   */

/*---------------------------------------------------------------------------*/
void FreeAliasList(AliasList * pAList);
  /*
     dispose the alias list
   */

/*---------------------------------------------------------------------------*/
const char *CreateAlias(AliasList * pAList, const char *pName, const char *pTranslation);

  /*
     create a new alias *pName with the translation *pTranslation

     - the alias *pName must not yet exist
     - *pTranslation is translated first
     - recursive definitions are prohibited
     - *pName and *pTranslation must be lowercase

     if the creation is successful, the return value is NULL, otherwise
     it points to one of the following error messages:

     "recursive alias not allowed"
     "alias already exists"
     "not enough memory to create an alias"

   */

/*---------------------------------------------------------------------------*/
int DefineAlias(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);

  /* this command requires manager privileges

     argv[1]: the alias to define
     - must not be a proper SICS command
     - if an alias with this name exists already, it is removed first

     argv[2]: the original command
     - if omitted, the alias is removed
     - if it is an alias the definiton refers to it's translation
     - may be an unknown command (probably defined later)

     - AddCommand removes an eventual alias matching the command name

     - RemoveCommand does not remove it's aliases
     Trying to use an alias of a removed command leads to the error message
     "object > ... < NOT found"

     - trying to define a recursive alias leads the error message
     "recursive alias not allowed"
   */

#endif
