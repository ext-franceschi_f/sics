/*--------------------------------------------------------------------------

	AliasList datastructure
	
	Markus Zolliker, September 2000
	
	copyright: see implementation file

---------------------------------------------------------------------------*/
#ifndef DEFINE_ALIAS_I
#define DEFINE_ALIAS_I

typedef struct {
    void *pFirst;
  } AliasList;

  /*
    initialize pFirst to NULL to create an empty list
  */
#endif
