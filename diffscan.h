
/*-------------------------------------------------------------------
  diffscan is an operator which can perform a fast differential scan 
  while a motor is running. 

  copyright: see file COPYRIGHT

  Mark Koennecke, November 2004
---------------------------------------------------------------------*/
#ifndef SICSDIFFSCAN
#define SICSDIFFSCAN
#include "obpar.h"
#include "scan.h"
#include "scan.i"
#include "sicsvar.h"
#include "hipadaba.h"
#include "sicshipadaba.h"

typedef struct {
  pObjectDescriptor pDes;
  char *name;
  ObPar *parArray;
  int normalizationScale;
  int scaleMonitor;
  CountEntry last;
  int skip;
  int skipCount;
  pScanData scanObject;
  double last_report_time;
  pSicsVariable sicsvar;
  pHdb hdbnode;
  float *fSave;
} DiffScan, *pDiffScan;

/*==================================================================*/

  /**
    * RunDiffScan runs a differential scan.
    * @param self The Diffscan object to use
    * @param pScan The scan object to use for configuration and for
    * for storing the results.
    * @param pCon The connection to use for output and errors.
    * @param fEnd The end value for the diffscan
    */
int RunDiffScan(pDiffScan self, pScanData pScan,
                SConnection * pCon, float fEnd);
/*==================== interpreter wrappers ==========================*/
int DiffScanWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
                    int argc, char *argv[]);
int MakeDiffScan(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[]);



#endif
