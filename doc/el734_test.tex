%	[...DOC.SINQ]EL734_TEST.TEX	- Ident 1D02
%	===========================
%%
%%====================================================================
%%
%%  +--------------------------------------------------------------+
%%  |			Paul Scherrer Institute			   |
%%  |                       SINQ Division			   |
%%  |                                                              |
%%  | This software may be used freely by non-profit organizations.|
%%  | It may be copied provided that the name of P.S.I. and of the |
%%  | author is included. Neither P.S.I. nor the author assume any |
%%  | responsibility for the use of this software outside of P.S.I.|
%%  +--------------------------------------------------------------+
%%
%%  Project  . . . . . . . . . . : SINQ
%%  Brief Document Title . . . . : Description of EL734 Step Motor Test Prog
%%  Author . . . . . . . . . . . : D.Maden
%%  Date of creation . . . . . . : 29-Jan-1996
%%
%%  Updates:
%%   13-Nov-1996 DM.	Vn 1D02 of program.
%%====================================================================
%%
\documentclass[openbib,a4paper,twoside,11pt]{article}
\usepackage{array}
%
\addtolength{\textheight}{35mm}
\setlength{\textwidth}{160mm}
\setlength{\oddsidemargin}{8mm}
\setlength{\evensidemargin}{-10mm}
\setlength{\topmargin}{-20mm}
%
\setcounter{tocdepth}{2}
%
\newcommand{\pdp}{\protect\makebox[3.7em][l]{PDP-11}}
\newcommand{\uvax}{\protect\makebox[4.8em][l]{microVAX}}
%
\newcounter{two}   \setcounter{two}{2}
\newcommand{\uvaxii}{\protect\makebox[5.8em][l]{microVAX~\Roman{two}}}
\newcounter{three}   \setcounter{three}{3}
\newcommand{\uvaxiii}{\protect\makebox[6.2em][l]{microVAX~\Roman{three}}}
%
\newcommand{\PreserveBackslash}[1]{\let\temp=\\#1\let\\=\temp}
\let\PBS=\PreserveBackslash	% shorthand
%
%		Define "\usc" to get a proper underscore!!
\newcommand{\usc}{\protect\makebox[0.6em]{\protect\rule{0.5em}{0.1ex}}}
%
\newcommand{\camac}{\protect\makebox[4.1em][l]{CAMAC}}
%
%       Define insertplot is insert a PostScript plot.
%       Usage:
%               \insertplot{<file>}{<height>}{<left-margin>}
%       Example:
%               \insertplot{dvipsdoc.pal}{70mm}{5.0mm}
%
\def\insertplot#1#2#3{\par
 \hbox{%
  \hskip #3
  \vbox to #2{
    \vfil
    \special{ps: plotfile #1}
  }%
 }
}
%
\title{EL734 Stepping Motor Test Program}
\author{D. Maden}
\date{12th November, 1996}
%
\begin{document}
\maketitle
    \section{Introduction}
%   ======================
%
    This note describes the program {\bf el734{\usc}test}, which can be used to
    test a stepping motor connected to an EL734 Stepping Motor Controller.
    The current version of the program is {\em Ident 1D02}.

    The program runs under either the Unix\footnote{So far, the program has been
    tested only under the DEC version of Unix, i.e. {\em Digital Unix}.}
    operating system or
    the OpenVMS operating system. The program is normally invoked via a
    simple command line from the console prompt and then runs to completion.

    In the interests of simplicity, this note describes how to
    run {\bf el734{\usc}test} on a Digital Unix workstation 
    when logged in as user ``lnsg''. It assumes that the shell being used
    is {\em tcsh}. Once the correct environment has been defined, however,
    the operation of the program is identical under both Unix and OpenVMS.
    Notes specific to the OpenVMS implementation will be found in
    Appendix~\ref{OpenVMS-Notes}.
%
    \section{Hardware Configuration}
%   ================================
%
    Although other configurations are possible, it is assumed in this
    description that the EL734 is connected via an asynchronous serial
    line to a Macintosh (or PC) running a LabVIEW ``terminal server'' program.
    This program is referred to as {\em TS} in the following. See ??? for
    details of {\em TS}. A Description of how to start {\em TS} is given in
    Appendix~\ref{TS-Notes}.

    {\bf el734{\usc}test} accesses the EL734 via {\em TS} by
    creating a TCP/IP network socket and opening a connection to it.
    {\bf el734{\usc}test} is a so-called ``TCP/IP client'' and the LabVIEW
    program
    is a so-called ``TCP/IP server''. {\bf el734{\usc}test} buffers commands
    for
    the EL734 into packets which it sends to the terminal server via the socket.
    The terminal server extracts the commands from each packet, executes them in
    sequence and compiles the various responses from the EL734 into a response
    packet which it then sends back to {\bf el734{\usc}test}. The details of
    this
    packet protocol are described elsewhere.

    In order for {\bf el734{\usc}test} to be able to establish a connection to
    the
    terminal server, it is necessary to know the Internet host name
    of the Macintosh or PC on which the terminal server program is running,
    the Internet port number on which this program accepts
    connections from its clients and the channel number of the asynchronous
    serial line which is connected to the EL734.

    The host name is usually indicated by a label on the Macintosh. The
    following is the current list of LabVIEW host assignments:
%
    \begin{center}\begin{tabular}{|c|c|c|c|} \hline
    Host & Alias & Internet Address & Instrument \\
    \hline
    pswm18.psi.ch & lnsw02 & 129.129.90.18  & ? \\
    ?             & lnsp22 & 129.129.90.172 & TOPSI \\
    pswm60.psi.ch & lnsw15 & 129.129.90.60  & ? \\
    \hline
    \end{tabular}\end{center}
%
    The default port number is 4000. The channel number is a small integer. A
    value of zero for the channel number usually refers to
    to the Macintosh's ``modem port''.
%
    \section{Running {\bf el734{\usc}test}}
%   =======================================
%
    The startup files of the ``lnsg'' accounts are normally set up so that
    {\bf el734{\usc}test} can be invoked merely by typing the command: \\[1.0ex]
%
    \hspace*{20mm} \verb# el734_test [options ...]# \\[1.0ex]
%
    A list of the options recognised by the program can be
    obtained by issuing the command: \\[1.0ex]
%
    \hspace*{20mm} \verb# el734_test -help# \\[1.0ex]
%
    It generates the following information about the program:%
%
    \begin{verbatim}
        EL734 Motor Controller Test Program 1, Ident 1D02.
        Started at Mon Nov 11 13:03:43 1996
        My name is "el734_test"

        Usage:  el734_test [options ...]
 
         Valid options are:
            -help                        Generates this help text.
            -?        <option>           Get detailed help on <option>.
            -?res                        Get list of Xlib resources.
            -?par                        Get help on EL734 parameters.
            -host     <host-name>        Specify name of TCP/IP server.
            -port     <port-number>      Specify its TCP/IP port number.
            -chan     <channel>          Specify channel number of EL734.
            -motor    <index>            Specify motor number (1 .. 12)
        
            -id       <device-id>        Specify expected EL734 ID.
            -tmo      <secs>             Specify time-out to wait for response.
            -low      <low-limit>        Specify low  limit for -ref, -step
                                              and -rndm.
            -high     <high-limit>       Specify high limit for -ref, -step
                                              and -rndm.
            -n        <n-moves>          Specify number of moves to make.
            -seed     <seed-value>       Specify random number seed.
            -verbose  <wid>              Specify "verbosity". Dflt = 10.
            -wait     <secs>             Specify wait time between moves.
        Parameter setting/saving options:
            -limits   <lo>/<hi>          Set the Lo/Hi software limits.
            -ur@      <pos>              Set the current position to be <pos>.
            -save     <file>             Read motor params and save to file.
            -load     <file>             Read file and set motor params.
        Positioning options:
            -stop                        Stop the motor moving.
            -position <target>           Position the motor.
            -random                      Perform <n-moves> random moves.
            -saw                         Perform <n-moves> moves between -low
                                              and -high
            -step     <step-size>        Perform incremental moves from -low
                                              to -high
            -ff                          Fast forward  to high software limit.
            -fb                          Fast backward to low  software limit.
            -sf                          Slow forward  to high software limit.
            -sb                          Slow backward to low  software limit.
            -ref                         Perform reference seek.
            -hunt                        Hunt for reference point.

            -m    is an abbreviation for -motor
            -p    is an abbreviation for -position
            -lo   is an abbreviation for -low
            -hi   is an abbreviation for -high
            -rndm is an abbreviation for -random
            -s    is an abbreviation for -stop

         No default is allowed for -host, -chan or -motor, i.e. values for
         these parameters must be supplied either via a resource file or on
         the command line. If <file> is specified as "-" or "=", standard
         input or output will be used rather than a file.

         When the program starts, the device identifier will be used to ensure
         that an EL734 is being addressed. <device-id> defaults to "STPMC
         EL734". Specifying "ignore" for <device-id> will suppress the ID
         check.

         The positioning options, i.e. -position, -ref, -hunt, -random, -saw,
         -step, -ff, -fb, -sf and -sb are mutually exclusive. If neither a
         positioning option nor a -save, -load, -stop, -ur@ nor -limits
         option has been specified, "-save -" will be assumed.

         Regardless of the order of options on the command line, -load will be
         done before any positioning commands and -save will be done last.

         A motor movement can be interrupted by hitting <Ctrl-C>.
    \end{verbatim}%
%
    \subsection{Options}
    The various options recognised by the {\bf el734{\usc}test} program are
    described in the following table. Note that the positioning options,
    i.e. \verb#-position#, \verb#-ref#, \verb#-hunt#,
    \verb#-random#, \verb#-saw#, \verb#-step#, \verb#-ff#, \verb#-fb#,
    \verb#-sf#  and \verb#-sb# as well as the \verb#-stop# option, are
    mutually exclusive. If neither a positioning option nor a
    \verb#-limits#, \verb#-save# nor \verb#-load# option
    has been specified,
    ``\verb#-save -#'' will be assumed. \\[1.5ex]
%
    \hspace*{3mm}\begin{tabular}{|p{40mm}@{}p{105mm}|} \hline
    \verb#-? <option># & More detailed help about \verb#<option>#
		will be given. \\ \hline
    \verb#-?res# & Gives a list of option resource names. \\ \hline
    \verb#-?par# & Gives a list of EL734 parameters. \\ \hline
    \verb#-chan <channel># & \verb#<channel>#
		specifies the serial port to which the EL734
	        is connected. There is no default. \\ \hline
    \verb#-fb# & A {\em fb} (Fast-Backward) command will be sent to the motor.
		The motor will move at full speed, with ramp, to
		its lower software limit ({\em h} parameter). \\ \hline
    \end{tabular}
\newpage
    \hspace*{3mm}\begin{tabular}{|p{40mm}@{}p{105mm}|} \hline
    \verb#-ff# & A {\em ff} (Fast-Forward) command will be sent to the motor.
		The motor will move at full speed, with ramp, to
		its upper software limit ({\em h} parameter). \\ \hline
    \verb#-high <high-limit># & This option sets an upper limit for the
		\verb#-random#, \verb#-saw#, and \verb#-step# options.
		Its default is the
		motor's upper software limit ({\em h} parameter). \verb#-high#
		may be abbreviated to \verb#-hi#. \\ \hline
    \verb#-host <host-name># & \verb#<host-name># specifies
		the name of the host where {\em TS} is running. There is no
		default. It may be specified as a name, e.g. {\em lnsw02}, or
		as an Internet address, e.g. {\em 129.129.90.18}. \\ \hline
    \verb#-hunt# & This will search for the motor's reference point and
		then perform a \verb#-ref# operation. Refer
		to Appendix~\ref{Hunting} for more information. \\ \hline
    \verb#-id <device-id># & When {\bf el734{\usc}test} first starts, it sets
		the EL734 into on-line mode and then sends an {\em id} command
		to the controller. The response should begin with the characters
		``STPMC EL734''. If, for some reason, the response to the {\em
		id} command is expected to be something else, the \verb#-id#
		option can be used to specify what is expected. Specifying
		\verb#<device-id># as ``ignore'' will suppress the identifier
		check. \\ \hline
    \verb#-limits <lo>/<hi># & This option will set the motor's lower and
		upper software limits ({\em h} parameters). The \verb#/# is
		mandatory and no white space is permitted in the
		\verb#<lo>/<hi># argument. \\ \hline
    \verb#-load <file># & The file \verb#<file># is read and the motor
		parameters are set accordingly. If \verb#<file># is specified
		as ``\verb#-#'' or ``\verb#=#'', standard input is used as
		the source of
		parameter specifications. The \verb#-load# option is discussed
		in more detail in Section~\ref{Load-Save}. \\ \hline
    \verb#-low <low-limit># & This option sets a lower limit for the
		\verb#-random#, \verb#-saw#, and \verb#-step# options.
		Its default is the
		motor's lower software limit ({\em h} parameter). \verb#-low#
		may be abbreviated to \verb#-lo#. \\ \hline
    \verb#-motor <index># & \verb#<index># is an integer between 1
		and 12 specifying the motor to be used. There is no default.
		\verb#-motor# may be abbreviated to \verb#-m#. \\ \hline
    \verb#-n <n-moves># & This option sets the number of moves to be
		performed by the \verb#-random# and \verb#-saw# options. The
		default for \verb#<n-moves># is 5. \\ \hline
    \verb#-port <port-number># & \verb#<port-number>#
		specifies the TCP/IP port number which is being used by
		{\em TS} to listen for connections.
		The default is 4000. \\ \hline
    \verb#-position <target># & The motor will be positioned to \verb#<target>#
		and the program will wait until the final position
		is reached. If \verb#<target># is outside the software limits
		for the motor, a range error will be reported. If other errors
		occur during the movement, they will also be reported.
		\verb#-position# may be abbreviated to \verb#-p# \\ \hline
    \verb#-random# & The motor will be positioned to random target positions
		within the range specified by the \verb#-low# and \verb#-high#
		options. The number of movements is specified by the \verb#-n#
		option and the delay period between movements is specified by
		the \verb#-wait# option. \verb#-random# may be abbreviated to
		\verb#-rndm#. \\ \hline
    \verb#-ref# & The motor will be positioned to its reference point. The
		location of the reference point and the method used to reach it
		are defined as part of the motor's parameter set. This option is
		not available for motors with absolute encoders. \\ \hline
    \end{tabular}
\newpage
    \hspace*{3mm}\begin{tabular}{|p{40mm}@{}p{105mm}|} \hline
    \verb#-save <file># & The motor's parameters will be read from the EL734
		and saved into file \verb#<file># in a form which can be used
		by the \verb#-load# option. If \verb#<file># is specified
		as ``\verb#-#'', the list of parameters is written to standard
		output. The \verb#-save# option is discussed in
		more detail in Section~\ref{Load-Save}. \\ \hline
    \verb#-saw# & The motor will be positioned alternately to the positions
		specified by the \verb#-low# and \verb#-high# options. The
		\verb#-n# and \verb#-wait# options have the same meaning as
		for the \verb#-random# option. \\ \hline
    \verb#-sb# & A {\em sb} (Slow-Backward) command will be sent to the motor.
		The motor will move at its start/stop speed to
		its lower software limit ({\em h} parameter). \\ \hline
    \verb#-seed <seed-value># & The random number generator used to generate
		random wait times and random positions is itself, by default,
		initialised with a random seed derived from the time of day. A
		specific random number sequence can be achieved by specifying an
		integer seed value via the \verb#-seed# option. \\ \hline
    \verb#-sf# & A {\em sf} (Slow-Forward) command will be sent to the motor.
		The motor will move at its start/stop speed to
		its upper software limit ({\em h} parameter). \\ \hline
    \verb#-step <step-size># & If \verb#<step-size># is positive, the
		motor will be positioned to the position specified by the
		\verb#-low# option and then moved in steps of size
		\verb#<step-size># until the position specified by the
		\verb#-high# option is reached or would be exceeded. If
		\verb#<step-size># is negative, the motor will first be
		positioned to \verb#-high# and then stepped downwards. The
		\verb#-n# and \verb#-wait# options have the same meaning as
		for the \verb#-random# option.\\ \hline	 
    \verb#-stop# & An {\em s} (Stop) command will be sent to the
		motor. \\ \hline
    \verb#-tmo <secs># & The \verb#-tmo# option specifies the time-out to be
		used in waiting for a response to a command sent to the EL734.
		The default is 5 seconds unless the \verb#-ref# or
		\verb#-hunt# option is
		specified, in which case it is 30 seconds. \verb#<secs># may
		be specified as a fixed point number. Only 1 decimal place
		is significant and the
		maximum is 999.9. A negative value (not recommended) will
		cause the server to wait
		for ever and a value of zero
		will cause the server not to wait at all. \\
    ~ &		{\bf Note:} the value of the \verb#-tmo# option is {\em not}
		related to the time required for the motor to complete its
		motion. It relates to the time which the motor requires to
		free itself from an end-stop or to perform the final seek
		of its reference point.\\ \hline
    \verb#-ur@ <pos># & This option (you-are-at) will set the motor's current
		position to be \verb#<pos># via the {\em uu} command. \\ \hline
    \verb#-verbose <wid># & Specify the {\em verbosity} (default = 10). During
		any motor movement, an updating display of the current motor
		position is generated in a field of width \verb#<wid>#.
		If \verb#<wid># is set to zero, this updating display
		is suppressed. If \verb#<wid># is too small,
		odd results are obtained! \\ \hline
    \verb#-wait <secs># & \verb#<secs># is an integer specifying the time
		to wait between movements performed by the \verb#-random#,
		\verb#-saw# and \verb#-step# commands. If \verb#<secs># is
		zero, no wait is performed. If it is negative, a random wait
		time between 1 and \verb#-<secs># seconds is performed. \\
		\hline
    \end{tabular}
\newpage
    \section{Loading and Saving Motor Parameters}
%   =============================================
    \label{Load-Save}
    The \verb#-load# and \verb#-save# options are used for loading a motor's
    parameters from a file and saving a motor's parameters to a file,
    respectively. An example of a file generated via the \verb#-save# option
    and suitable for use by the \verb#-load# option is shown in
    Appendix~\ref{Save-File}.

    The best way to create a file for the \verb#-load# option is
    to use the \verb#-save# option to create it and then to modify it with one's
    favourite editor. For normal use, this should suffice. The following details
    are intended as an aid to anyone who wishes to use {\bf el734{\usc}test}
    interactively via the ``\verb#-load -#'' option.

    The format of records in the load file should be of the form: \\[1.0ex]
%
    \hspace*{20mm} \verb# <cmnd>  [%d]  [<par1>]  [<par2>]  [! Comments ...]# %
    \\[1.0ex]
    where items in square brackets are optional depending on \verb#<cmnd>#.
    \verb#<cmnd># is one of the commands accepted by the EL734. It may also
    be {\em wait}, {\em exit} or {\em quit}. If \verb#%d# is present, it
    will be substituted with the motor number as specified via the
    \verb#-motor# option.

    Specifying \verb#<cmnd># to be either {\em exit} or {\em quit} will cause
    {\bf el734{\usc}test} to exit\footnote{End-of-file will also cause {\bf
    el734{\usc}test} to exit. Note that, if commands are being read from the
    terminal, end-of-file is indicated by $<$Ctrl-D$>$ on a Unix system, {\em
    not} $<$Ctrl-Z$>$.}. The value {\em wait}
    will cause {\bf
    el734{\usc}test} to wait until the motor specified via the \verb#-motor#
    option is idle.
    
    {\bf el734{\usc}test} also recognises the special status reading commands
    {\em msr} and {\em ss}. In these cases the program tests the various status
    bits in the response from the EL734 and displays appropriate messages.

    Some of the EL734 commands, in particular the {\em fm} and {\em fd}
    commands, cause the motor position to be reset to zero. For this reason,
    {\bf el734{\usc}test} reads the motor position just before starting a load
    operation and attempts to set this position again using the {\em uu}
    command when the load operation is completed. If however, {\bf
    el734{\usc}test} detects any motor commands which may cause the motor to
    move, this resetting of the motor position is suppressed.

    Note that {\bf el734{\usc}test} is not able to handle commands to the
    EL734 which generate multiple output records such as the {\em cfg},
    {\em ?}, {\em ?c}, {\em ?m} or {\em ?p} commands.
%
    \section{Examples}
%   ==================
%
    The following are a few examples of using {\bf el734{\usc}test}. For
    clarity, it is assumed that the following alias has been defined: \\[1.0ex]
%
    \hspace*{20mm}%
    \verb#alias do_el734 "el734_test -host lnsw15 -chan 5 -motor 4"#%
	\\[1.0ex]
%
    so that all the following examples will operate on Motor 4 of the EL734
    connected to Channel 5 of the {\em Terminal Server} \verb#lnsw15#.
    \begin{itemize}
    \item[$>$] \verb#do_el734#\\
	This will read the status of the motor and display it on the terminal.
    \item[$>$] \verb#do_el734 -save m4_params#\\
	This will read all the motor's parameters and save them in file
	\verb#m4_params# in the current directory.
    \item[$>$] \verb#do_el734 -p 150.4#\\
	This will position the motor to 150.4 and wait until it has stopped
	moving.
    \item[$>$] \verb#do_el734 -n 100 -wait -20 -lo 20 -hi 80 -rndm#\\
	This will perform 100 random movements of the motor in the range from 20
	to 80. After each movement, the program will wait for a randomly
	selected interval of up to 20 seconds.
    \end{itemize}
    To test several motors simultaneously, {\bf el734{\usc}test} may be started
    from several terminals at the same time. In this case, one should be careful
    not to drive the same motor simultaneously
    from more than one copy of the program!
%
    \section{Setup for {\bf el734{\usc}test}}
%   =========================================
%
    \subsection{Resource Database}
    \label{resources}
%
    In addition to specifying options for {\bf el734{\usc}test} on the command
    line, it is possible to set them in a so-called ``resource database''
    in a manner similar to that used by the X-window software package. In order
    to do this, it is first necessary to know the name of the resources
    associated with the various options. The following list of resource names
    is obtained via the \verb#-?res# option:%
    \begin{verbatim}
        The correspondence between options and resource names is as follows:
 
            Option            Equivalent resource name
            ======            ========================
            -?                  *el734HelpItem
            -?res               *el734HelpRes
            -?par               *el734HelpPar
            -chan               *el734Chan
            -fb                 *el734Fb
            -ff                 *el734Ff
            -help               *el734Help
            -high               *el734High
            -host               *el734Host
            -hunt               *el734Hunt
            -id                 *el734Id
            -limits             *el734Limits
            -load               *el734Load
            -low                *el734Low
            -motor              *el734Motor
            -n                  *el734N
            -port               *el734Port
            -position           *el734Position
            -random             *el734Random
            -ref                *el734Ref
            -save               *el734Save
            -saw                *el734Saw
            -sb                 *el734Sb
            -seed               *el734Seed
            -sf                 *el734Sf
            -step               *el734Step
            -stop               *el734Stop
            -tmo                *el734Tmo
            -wait               *el734Wait
    \end{verbatim}%
%
    When {\bf el734{\usc}test} is started, it scans several files to compile
    its resource database. On Unix systems, these files, in order of decreasing
    precedence, are: \\[1.0ex]
%
    \hspace*{20mm} \verb#~/SinQ_rc#\\
    \hspace*{20mm} \verb#/usr/lib/X11/app-defaults/SinQ_rc#\\
    \hspace*{20mm} \verb#~/.Xdefaults#\\
    \hspace*{20mm} \verb#/usr/lib/X11/app-defaults/Xdefaults#\\[1.0ex]
%
    Any options specified on the command line supersede specifications in any of
    these files. As an example,  the host on which the terminal server program
    is running could be specified by including the record: \\[1.0ex]
%
    \hspace*{20mm} \verb#*el734Host:  lnsw15#\\[1.0ex]
%
    in one of the resource database files listed above. It would then not be
    necessary to specify the \verb#-host# option on the command line.
    Other defaults could be set similarly by using the appropriate resource
    names as specified in the above table.
%
    \subsection{Paths and Aliases}
%
    The compiled and linked version of {\bf el734{\usc}test} is located 
    in directory \verb#~maden/motor#. There is a
    symbolic link to this file from \verb#/public/bin/el734_test#.

    The directory \verb#/public/bin# should be included in the default path
    of the ``lnsg'' account by including a statement of the form: \\[1.0ex]
%
    \hspace*{20mm} \verb#set path = ( $path /public/bin )#\\[1.0ex]
%
    in the file \verb#~lnsg/.cshrc#. Actually, on the SINQ data acquisition
    computers, \verb#~lnsg/.cshrc# is set up so that it invokes
    the system startup script, \verb#~/.cshrc#, and this is where
    \verb#~/public/bin# is added to the default path.

    It is clearly a simple matter to define an alias to facilitate usage of the
    program. For example: \\[1.0ex]
%
    \hspace*{20mm} %
	\verb#alias xxx "el734_test -host lnsw15 -chan 5 -m 6"#
%
\newpage
    \appendix
    \section{Notes on the OpenVMS Version of {\bf el734{\usc}test}}
    \label{OpenVMS-Notes}
    On OpenVMS systems, {\bf el734{\usc}test} compiles its resource database
    from the following files, in order of decreasing precedence: \\[1.0ex]
%
    \hspace*{20mm} \verb#decw$user_defaults:SinQ_rc.dat#\\
    \hspace*{20mm} \verb#decw$group_defaults:SinQ_rc.dat#\\
    \hspace*{20mm} \verb#decw$system_defaults:SinQ_rc.dat#\\
    \hspace*{20mm} \verb#decw$user_defaults:decw$xdefaults.dat#\\
    \hspace*{20mm} \verb#decw$group_defaults:decw$xdefaults.dat#\\
    \hspace*{20mm} \verb#decw$system_defaults:decw$xdefaults.dat#\\[1.0ex]
%
    {\bf el734{\usc}test}  should be run via a so-called {\em DCL foreign
    command}. Assuming that the executable image of {\bf el734{\usc}test} is in
    directory \verb#psi_public:[exe_axp]#,
    one would do this via the DCL command:
    \\[1.0ex]
    \hspace*{20mm} \verb#$ el734_test :== $psi_public:[exe_axp]el734_test#\\[1.0ex]
%
    located in one's \verb#login.com# file. The program can then be run
    exactly as on a Unix system.
%
\vspace*{40mm}
    \section{Notes on the LabVIEW Terminal Server Program}
    \label{TS-Notes}
    The LabVIEW terminal server program is set up so that it automatically runs
    when it is started. It is therefore sufficient to double-click on its icon
    with the Macintosh \verb#finder# to get the program to run. The terminal
    server program is located in directory: \\[1.0ex]
%
    \hspace*{20mm} \verb#System Utilities:LabVIEW 3.1.1:Users:# \\
    \hspace*{40mm}     \verb#A SerPortServer:A Main Server.vi# \\[1.0ex]
%
    The Macintosh is also usually configured so that there is an alias of the
    terminal server program in the start-up directory. The program then starts
    to run automatically whenever the Macintosh is powered up.
%
\newpage
    \section{An Example of a Motor Parameter File}
    \label{Save-File}
    The following is an example of a file generated by the \verb#-save# option.
    This file is suitable for use by the \verb#-load# option. Records beginning
    with \verb#!# are ignored by \verb#-load#.

    Note that the {\em ec} command is not supported prior to Version~1.0
    of the EL734 controller firmware.
    \begin{verbatim}
        ! EL734 Status at Fri Nov  8 09:56:24 1996
        ! ============
        ! Controller ID = "STPMC EL734 V1.0 FFF"
        ! Server lnsp22, Port 4000, Channel 5, Motor 6
        !
         mn  %d  TOPSI_6_STT            ! Motor name
         ec  %d  0  0                   ! Zero the encoder mapping
         ec  %d  1  6                   ! Encoder mapping (type/number)
         a   %d  2                      ! Precision
         fd  %d -100  1                 ! Encoder gearing (numer/denom)
         fm  %d  200  1                 ! Motor   gearing (numer/denom)
         d   %d  0.1                    ! Inertia tolerance
         e   %d  7                      ! Start/stop ramp (kHz/sec)
         f   %d  1                      ! Open loop/Closed loop (0/1)
         g   %d  300                    ! Start/stop frequency (Mot-S/sec)
         h   %d  1.00  38.00            ! Low/High Software Limits
         j   %d  2000                   ! Top speed (Mot-S/sec)
         k   %d -1                      ! Reference mode
                                        !   -11 = LoLim+Index is ref. pt.
                                        !    -1 = LoLim is ref. pt.
                                        !     0 = Abs encoder
                                        !     1 = HiLim is ref. pt.
                                        !     2 = Separate ref. pt.
                                        !    11 = HiLim+Index is ref. pt.
                                        !    12 = Separate+Index ref. pt.
         l   %d  0                      ! Backlash/Spielausgleich (Mot-S)
         m   %d  3                      ! Position tolerance (Enc-Steps)
         q   %d  99.0                   ! Reference switch width
         t   %d  0                      ! One-sided operation flag (0 = no)
         v   %d  0.00                   ! Null point
         w   %d  0                      ! Air-cushion dependency
         z   %d  0                      ! Circumf. of encoder (Enc-Steps)
         mem %d  0.0/38.7               ! User data register
        !
        ! # of positionings,         SP = 41
        ! # of positioning faults,   ST = 0
        ! # of positioning failures, SR = 0
        ! # of air-cushion failures, SA = 0
        !
        ! Current position is 20.00
        ! Status, MSR = Idle. 
        ! Flags, SS = 0
        ! Input status is "off".
    \end{verbatim}
%
\newpage
    \section{The Reference Point Search Algorithm}
    \label{Hunting}
    The procedure used by the \verb#-hunt# option for searching for a
    motor's reference point depends on the
    setting of the {\em k} parameter. The following table describes the
    various search algorithms\\[3.0ex]
    %
    \begin{tabular}%
	{|c%
	 |>{\PBS\raggedright\hspace{0pt}}p{30mm}%
	 |>{\PBS\raggedright\hspace{0pt}}p{105mm}|}
    \hline
    {\bf K} & {\bf Type} & {\bf Algorithm} \\ \hline \hline
    0 & Absolute encoder & \verb#-hunt# is not supported. \\ \hline
    -1 & Low-limit switch is reference point &
    The value of the {\em q} parameter (reference-point width) is read and
    checked to be greater than zero.
    The {\em h} and {\em v} parameters (software limits and zero-point) are
    read and checked to be consistent. If the {\em v} parameter is greater
    than the low software limit, a warning is displayed but this is not
    considered to be a fatal error.
    The {\em h} parameters are set to large values and the current position
    is set to zero ({\em uu} command).
    The motor is then driven to
    its low-limit switch via an {\em fb} (fast-backward) command.
    The program waits until the motor stops moving. If no \verb#-tmo# option
    has been specified, a time-out of 30 seconds is used for this operation.
    The motor status is then checked
    to ensure that the low-limit switch was reached. It also
    checks the {\em ss} status to ensure that the reference point (LSX)
    is not active. If all checks are successful, 
    the current position is set to the value of the {\em v} parameter
    via the {\em uu} command, the software limits are restored and a
    reference seek is initiated via the {\em rf} command. The program
    again waits until the motor stops moving and checks the motor status to
    ensure that the reference seek was successful. The motor is finally
    positioned to its lower software limit, if this is greater than {\em v}.
    \\ \hline
    1 & High-limit switch is reference point &
    This is analogue to the K~=~-1 case. \\ \hline
    2 & Separate reference point &
    The {\em q}, {\em h} and {\em v} parameters are checked as in the
    K~=~-1 case. A warning is issued if the zero-point is outside
    the software limits but this is not treated as a fatal error.
    The motor is then driven backwards in steps with a step-size equal to
    95\% of the {\em q} parameter until a position is found where the
    reference-point (LSX) is active or the low-limit is reached. If the
    lower-limit is reached, the motor is re-positioned to its start position
    and it is then stepped forward to its high-limit. Assuming the
    reference-point is eventually found,
    the current position is set to the value of the
    {\em v} parameter via the {\em uu} command, the software limits are
    restored and a reference seek is initiated via the {\em rf} command.
    \\ \hline
    \end{tabular}
    \\[5.0ex]
    \begin{tabular}%
	{|c%
	 |>{\PBS\raggedright\hspace{0pt}}p{80mm}%
	 |>{\PBS\raggedright\hspace{0pt}}p{54mm}|}
    \hline
    {\bf K} & {\bf Type} & {\bf Algorithm} \\ \hline \hline
    -11 & Low-limit switch is reference point with reference
		mark on encoder  & Same as K~=~-1 \\ \hline
    11 & High-limit switch is reference point with reference
		mark on encoder  & Same as K~=~1 \\ \hline
    12 & Separate reference point with reference
		mark on encoder  & Same as K~=~2 \\ \hline
    \end{tabular}
    \end{document}
