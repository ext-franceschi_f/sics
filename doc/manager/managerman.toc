\contentsline {chapter}{\numberline {1}Introduction}{4}
\contentsline {chapter}{\numberline {2}SICS programs, Scripts and Prerequisites}{5}
\contentsline {section}{\numberline {2.1}Hardware}{5}
\contentsline {section}{\numberline {2.2}Server programs}{5}
\contentsline {chapter}{\numberline {3}General SICS Setup}{7}
\contentsline {section}{\numberline {3.1}System Control}{8}
\contentsline {section}{\numberline {3.2}Moving SICS}{9}
\contentsline {subsection}{\numberline {3.2.1} Moving the SICS Server to a new Computer}{9}
\contentsline {subsection}{\numberline {3.2.2}Exchanging the Serial Port Server}{9}
\contentsline {subsection}{\numberline {3.2.3}Exchanging the Histogram Memory}{10}
\contentsline {section}{\numberline {3.3}SICS Trouble Shooting }{10}
\contentsline {subsection}{\numberline {3.3.1}Check Server Status}{10}
\contentsline {subsection}{\numberline {3.3.2}Inspecting Log Files}{10}
\contentsline {subsection}{\numberline {3.3.3}Restarting SICS}{11}
\contentsline {subsection}{\numberline {3.3.4}Restart Everything}{11}
\contentsline {subsection}{\numberline {3.3.5}Starting SICS Manually}{11}
\contentsline {subsection}{\numberline {3.3.6}Test the SerPortServer Program}{11}
\contentsline {subsection}{\numberline {3.3.7}Trouble with Environment Devices}{12}
\contentsline {subsection}{\numberline {3.3.8} HELP debugging!!!!}{12}
\contentsline {chapter}{\numberline {4}The SICS Initialization File}{13}
\contentsline {section}{\numberline {4.1}Overview of SICS Initialization}{13}
\contentsline {section}{\numberline {4.2}SICS Options and Users}{14}
\contentsline {section}{\numberline {4.3}SICS Variables }{15}
\contentsline {section}{\numberline {4.4}SICS Hardware Configuration}{16}
\contentsline {subsection}{\numberline {4.4.1}Bus Access}{16}
\contentsline {subsubsection}{Direct Access to RS232 Controllers or TCP/IP Controllers.}{16}
\contentsline {subsubsection}{Accessing Serial Ports (Old System)}{17}
\contentsline {subsubsection}{GPIB Controller Access}{19}
\contentsline {subsection}{\numberline {4.4.2}Controllers}{20}
\contentsline {subsubsection}{ECB Controllers}{20}
\contentsline {subsubsection}{Siematic SPS Controllers}{21}
\contentsline {subsubsection}{General Controller Object and Choppers}{22}
\contentsline {subsection}{\numberline {4.4.3} Motors}{23}
\contentsline {subsection}{\numberline {4.4.4}Counting Devices}{24}
\contentsline {subsubsection}{Histogram Memory}{25}
\contentsline {subsection}{\numberline {4.4.5}Velocity Selectors}{26}
\contentsline {section}{\numberline {4.5}Initialization of General Commands}{27}
\contentsline {subsection}{\numberline {4.5.1}Monochromators}{28}
\contentsline {subsection}{\numberline {4.5.2}Reoccuring Tasks}{28}
\contentsline {subsection}{\numberline {4.5.3}The SICS Online Help System}{29}
\contentsline {subsection}{\numberline {4.5.4}Aliases in SICS}{29}
\contentsline {subsubsection}{Object Aliases}{29}
\contentsline {subsubsection}{Runtime Aliases}{29}
\contentsline {subsubsection}{Command Aliases}{30}
\contentsline {subsection}{\numberline {4.5.5}The AntiCollision Module}{30}
\contentsline {section}{\numberline {4.6}The Internal Scan Commands}{31}
\contentsline {subsection}{\numberline {4.6.1}Scan Concepts}{31}
\contentsline {subsection}{\numberline {4.6.2}User Definable Scan Functions}{34}
\contentsline {subsection}{\numberline {4.6.3}User Defined Scans(Old Style)}{35}
\contentsline {subsection}{\numberline {4.6.4}The Scan Command Header Description File}{35}
\contentsline {subsection}{\numberline {4.6.5}Differential Scans}{36}
\contentsline {subsection}{\numberline {4.6.6}Peak Analysis}{37}
\contentsline {subsection}{\numberline {4.6.7}Common Scan Scripts}{37}
\contentsline {section}{\numberline {4.7}Scripting NeXus Files}{37}
\contentsline {subsection}{\numberline {4.7.1}Usage}{38}
\contentsline {subsubsection}{File Opening and Closing}{38}
\contentsline {subsubsection}{Writing Things}{38}
\contentsline {section}{\numberline {4.8}Automatic Updating of NeXus Files}{39}
\contentsline {subsection}{\numberline {4.8.1}Prerequisites for Using the Automatic Update Manager}{39}
\contentsline {subsection}{\numberline {4.8.2}Installing Automatic Update}{40}
\contentsline {subsection}{\numberline {4.8.3}Configuring Automatic Update}{40}
\contentsline {section}{\numberline {4.9}Instrument Specific SICS Initializations}{40}
\contentsline {subsection}{\numberline {4.9.1}Initialization for Four Circle Diffractometers}{40}
\contentsline {subsection}{\numberline {4.9.2}Triple Axis Spectrometer Specific Commands}{42}
\contentsline {subsection}{\numberline {4.9.3}Special Commands for the Reflectometer (AMOR)}{42}
\contentsline {subsubsection}{AMOR Status Display Commands}{43}
\contentsline {subsection}{\numberline {4.9.4}SANS Special Commands}{44}
\contentsline {subsection}{\numberline {4.9.5}Special FOCUS Initializations}{45}
\contentsline {subsubsection}{Special Internal FOCUS Support Commands}{45}
\contentsline {chapter}{\numberline {5}Programming SICS Macros}{46}
\contentsline {section}{\numberline {5.1}Input/Output}{46}
\contentsline {section}{\numberline {5.2}Error Handling}{47}
\contentsline {section}{\numberline {5.3}Interacting with SICS within a Script}{47}
\contentsline {section}{\numberline {5.4}SICS Interfaces in Tcl}{47}
\contentsline {subsection}{\numberline {5.4.1}The Object Interface}{47}
\contentsline {subsection}{\numberline {5.4.2}Overriding the Drivable Interface with Tcl}{48}
\contentsline {chapter}{\numberline {6}The McStas SICS Interface}{49}
\contentsline {section}{\numberline {6.1}McStas Requirements and SICS Requirements}{50}
\contentsline {section}{\numberline {6.2}The McStas Reader}{50}
\contentsline {section}{\numberline {6.3}The McStas Controller}{51}
