<HTML>
<HEAD>
<TITLE>The AMOR Client</TITLE>
</HEAD>
<BODY>
<H1>The AMOR Client</H1>
<P>
The AMOR Client is the dedicated SICS client program for the
reflectometer AMOR.  It is a Java application which runs on all
computers for which a Java runtime better then jdk 1.1.6 is available. 
</P>
<h2>Starting the AMOR Client</h2>
<p>
There are various ways to start the AMOR client. On the unix systems,
simply type <em>amor &</em> at the command prompt. For PC's and
proper Macintosh systems (proper means MacOS &gt 10) it is recommended
to install Java WebStart and start the application from:
http://lns00.psi.ch/sics/wstart. The AMOR client can be exited through
the File/Exit menu option. 
</p>
<h2>Connecting to a SICS Server</h2>
<p>
Before anything useful can be done with the AMOR client, it has to be
connected to a SICS server. This can be done through the Connect menu
in the application. Normally choose AMOR in this menu and everything
will be fine. The option Custom Connect is used if the SICS server had
to be relocated to another computer (because of a hardware problem)
and the connection parameters: computer and port number have to be
given explicitly.
</p>
<h2>Using the AMOR Client</h2>
<p>After starting the AMOR client you see a menu, a row of buttons
beneath the menu and a central display area, showing AMOR's fantastic
logo. Now, the AMOR client has six different views of the
instrument. These views can be selected through the button row below
the menubar. The views are:
<dl>
<dt>AMOR logo
<DD>The fantastic AMOR logo. IMHO, AMOR got the nicest logo. 
<dt>AMOR Schema
<dd>A drawing of AMORS components annotated with motor names.
<dt>Command
<dd>The main interaction window for typing commands to the
instrument. I/O with the server is displayed in the large central text
area. Commands can be typed into the text field at the bottom. Then
there is a yellow line displaying the progress of the current counting
operation.  To the left of the command entry field is a little red
button labelled <em>Interrupt</em>. This button aborts any current
operation. There is a menu point corresponding to this window. This
allows to select the following functions:
<dl>
<dt>User Rights
<dd>A dialog for gaining privileges in SICS. You need to specify a
username and  a password.
<dt>Open Logfile
<dd>Open a log file on the local machine running the client.
<dt>Close Logfile
<dd>Close a local logfile.
</dl>
<dt>Histogram
<dd>This window displays a histogram of the current data
available. What can be seen here depends on AMOR's mode of operation:
In single detector mode, the current scan data is displayed. In TOF
mode, the counts summed over a rectangular region of the detector
(defined in the Area Detector view)  is displayed against
time-of-flight. Yoy may <em>zoom in</em> on selected regions of the
histogram by dragging a rectangle from top to bottom. You can <em>zoom
out</em> through the opposite movement. Further histogram commands
hide under the Histogram menu entry. Here you can:
<dl>
<dt>Reset
<dd>Reset the plot boundaries to show everything.
<dt>Print
<dd>print the current plot into a postscript file.
<dt>Logarithmic
<dd>Toggle the logarithmic flag which, when set, causes the counts to
be displayed on a logarithmic basis.
</dl>
<dt>Area Detector
<dd>This display only makes sence in TOF-mode, with a PSD. It shows
the data in the histogram memory projected along the time axis. In the
text area below the picture, the current x, y position and the current
value at the cursor position are displayed. Double clicking on the
display opens a dialog which allows to set things like: <em>scale,
colour mapping, logarithmic mapping and the mapping range</em>.   
Dragging a rectangle in this display will pop up a dialog asking you
for a name. The rectangle selected then becomes a <em>named
region</em> which will then be summed and plotted in the Histogram
display. Named regions can be removed through the Update Control/Clear
TOF Regions menu entry. 
<dt>Parameter
<dd>Shows selected instrument parameters in textual form. 
</dl>
</p>
<h3>Updating the Display</h3>
<p>
In single detector mode scan data will be automatically updated. In
TOF mode, updates to either the histogram display or the area detector
display have to obtained either:
<ul>
<li>Manually by hitting the green Update button at the bottom of the
screen.
<li>or automatically at certain time intervalls configured through the
Update Control/Update Intervall and enabled by toggling the Update
Control/Automatic Update flag. 
</ul>
</p>

</BODY>
</HTML>
