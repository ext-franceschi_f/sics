<! This file will be included for all invocation sections to doc>
<h2>Accessing SICS through Telnet</h2>
<p>
SICS is able to communicate with standard TCP/IP telnet clients. Suitable
telnet clients are available on allmost all computer platforms free of
charge as part of the network software. In order to access SICS with telnet
you need to know the following five bits of information:
<ul>
<li>The name of the computer where the SICS server is running.
<li>The port number at which the SICS server lsitens for telnet connections.
<li>The login word.
<li>A valid username.
<li>A valid password for your user name.
</ul>
This information will be supplied to you by your instrument scientist if she
finds you and your cause honorable enough. 
</p>

<p>
Loging in to SICS through telnet requires the following steps:
<ul>
<li>Invoke your telnet client and try to contact machine name at the port
number given. For example on a Unix or VMS this looks like:
<pre>
     telnet machine.psi.ch 7654
</pre>
Of course this may differ if you use a telnet client on a different
platform.
<li> If things go well, you'll be connected to the SICS server then, though
he does not tell you about it. However, SICS will not allow you to type
commands yet. You have to login with the magic three words: <b>loginword
username password</b>. Only if you get this right, the SICS server will
print a welcome message and you may type commands to the server.
</ul>
You can logoff from the SICS server by typing <b>logoff</b>.
</p>
