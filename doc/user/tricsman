\documentclass[12pt,a4paper]{report}
%%\usepackage[dvips]{graphics}
%%\usepackage{epsf}
\setlength{\textheight}{24cm}
\setlength{\textwidth}{16cm}
\setlength{\headheight}{0cm}
\setlength{\headsep}{0cm}
\setlength{\topmargin}{0cm}
\setlength{\oddsidemargin}{0cm}
\setlength{\evensidemargin}{0cm}
\setlength{\hoffset}{0cm}
\setlength{\marginparwidth}{0cm}

\begin{document}
%html -d hr " "
%html -s report
\begin{center}
\begin{huge}
TRICS--Reference Manual \\
\end{huge}
\today\\
Dr. Mark K\"onnecke \\
Labor f\"ur Neutronenstreuung\\
Paul Scherrer Institut\\
CH--5232 Villigen--PSI\\
Switzerland\\
\end{center}
\clearpage
\clearpage
\tableofcontents
\clearpage

%html trics.htm 1 
%html tricsgen.htm 1
%html basic.htm 1
%html drive.htm 1
%html motor.htm 2
%html trimot.htm 2
%html counter.htm 2
%html samenv.htm 2
%html logbook.htm 2
%html commandlog.htm 2
%html macro.htm 1
%html exeman.htm 3
%html buffer.htm 2
%html hkl.htm 2
%html config.htm 1
%html system.htm 1
%html tricsingle.htm 1
%html optimise.htm 2

\section{External FORTRAN 77 Programs}
\subsection{INDEX}

	     The  program  indexes  reflections	 on  the   basis   of	observed
	     2Theta, Omega, Chi, Phi   angles	when   the  cell  constants  and
	     wavelength are known.
	     It does not take into account systematic extinctions.

	     The process, when successful, has three steps.

	     First, it calculates, for each set of  observations,  all	possible
	     HKL's   for   which    theta(calc)	  lies	 within	  theta(obs) +/-
	       delta  theta.
	      delta  theta is given - see below -.
	     For  delta  theta = 0, the value defaults to 0.05.

	     Second, it finds for all combinations of two sets of  observations,
	     the  angle	 between  the  indexed HKL's for which  angle(calc) lies
	     within  angle (obs) +/-  delta.
	      delta given  - see below -. Delta = 0 causes default to 0.2

	     Finally, it finds all sets of indexed HKL's that explain all angles
     between the observed sets of Omega, Chi and Phi.

	     The user will normally be presented with several possible	sets  of
	     HKL's which fit within the limits given.
	     - they are already tested for right-handedness -
	     and he must now choose which set he likes the most.

	     If he wishes he may now specify which set of reflections  he  likes
	     and  the  program	will  then set up the input file for the program
	      rafin, - see next section -.
	     The program ensures that the first two reflections	 are  acceptable
	     to	  rafin.   The	user  must  say	 whether he wants the  ub matrix
	     written directly into  lsd ( for instant use ) and	 which	file  he
	     wants his  rafin input to come from (usually  rafin.dat).
	     The program  rafin is then automatically started.


	     Input to  index can be done either from the terminal or from a file
	      index.dat
	     The format is the same, an example is given here.
\begin{verbatim}
THIS IS A DUMMY EXAMPLE	      ( text )
5.82 16.15 4.09 90 103.2 90 .84 .15 (cell constants, lambda delta)
16.18 9.01 34.71 14.74 0	 (2Th, Om, Ch, Ph, delta theta)
13.21 7.8 .71 -56.13 0.1	 (2Th, Om, Ch, Ph, delta theta)
etc. etc.
0				 ( end list with 2Th = 0 )
\end{verbatim}

		The program will only suggest  sets  of	 indexed  HKL's	 if  all
	     reflections  are  explained.   If	not,  the user must himself look
	     through the list of  observed  and	 calculated  angles  to	 find  a
	     partial list.


\subsection{RAFIN}
	     This program determines orientation matrix ( ub) from two	or  more
	     sets   of	 orientation   angles	for   reflections,  and	 refines
	     (optionally) wavelength; zeroes of 2Theta, Omega or Chi; a,  b,  c,
	     alpha, beta or gamma.


		To call the program, type :--
	     rafin
	     after having set up the input file :

	     The input data are on  rafin. dat
	     (teletype input can be used, but is cumbersome)

	     Use  teco to make or correct the file.

	     An example of the input file ( comments in parentheses ) :--
\begin{verbatim}
0 1     (second no. 0/1 for  ub not transferred/transferred to lsd)
0       (always)
0 -4 -2 28.01 13.75  81.59  42.05  (H K L 2Theta Omega Chi Phi)
4 -6  7 50.84 25.37  34.04  18.41
-2 -6  0 41.55 20.53  66.93  59.99
4  0  4 19.74  9.94 -16.92  -5.40
1 -5 -3 35.59 17.70  82.32   1.40
6  0  0 18.47  9.26  -2.32 -46.95
0 .8405		(0/1 do not/do refine lambda; and lambda)
0 0.0 1 0.0 1 0.0	(0/1 for do not/do refine, 2Theta zero,
			-0/1 for do not/do refine, Om zero,
			-0/1 for do not/do refine, Chi zero.
0 15.9158 0 7.1939 0 14.277 0 90 0 98.72 0 90 (ditto for  a,  b,  c,  alpha,  beta and  gamma)
2 0 0			(H K L list for angles to be calculated)
0 2 0
0 0 2
0 0 0			(end of list)
-1			(end of input file)
\end{verbatim}

	       Ensure that  lsd is not running	if  you	 wish  to  transfer  the
	     matrix   and   wavelength	directly  into	its  parameter	section,
	     otherwise it may not be successful.
	      rafin will never modify the zeroes for you. This is for you to  do
	     by  adding them to the values in  zer of  par.  Remember that for a
	     well aligned diffractometer, they will never change by very much.

	     Note: the first two  reflections  should  be  far	away  enough  in
	     reciprocal	 space	to  define a plane. They must be at least 45 deg
	     apart in Phi and only one may have Chi greater than 45 deg.

	     Note also that higher angle  (Theta)  reflections	usually	 give  a
	     better fit.

	     You cannot, obviously, refine lambda and  your  cell  at  the  same
	       time.


\subsubsection{Acknowledgements}

	     The  index program was written by M.S.Lehmann,
	      and J.M.Savariault.

	     The  rafin program was implemented at  the	  ill  by  A.Filhol  and
	     M.Thomas.
	     It was implemented on the  pdp 11 system by A.Barthelemy.



\subsection{HKLGEN}

	     THIS PROGRAM IS USED TO GENERATE A LIST OF HKL's which can be  used
	     for input to the measurement routines of  lsd.
	     Indices can be generated internally in  lsd, but  it  is  generally
	     considered easier to create a list, and measure from this.

	      hklgen will generate HKL's according  to	min  and  max  specified
	     indices, and will write them into output file(s) if they are inside
	     the Theta limits.

	     If  chi and  phi limits are specified, the program will  also  look
	     to see if the  hkl is measurable inside these machine limits.

	     If not, it will see if the Friedel Pair is inside limits

	     If this is also outside limits, it will see if the	 reflection  can
	     be measured for  hkl  psi=180

	     -  note this option means  chi = 90 -> 180 i.e.  up-side-down.

	     If measurement is not possible for any  of	 these	conditions,  the
	      hkl is declared  blind.

	     Comments like  fr.pr  hichi  blind indicate these on  tty output.

	     To run the program do :--

	     hklgen
	     Input to the program is either from the terminal or   from	 a  file
	      hklgen.dat, already created by the user.

\subsubsection{Input from Terminal}
	     The first question asked  under  this  option  is	whether	 a  file
	      hklgen.dat should be created.
	     If subsequent runs are envisaged, this might be a	good  idea.   In
	     this  case  teco can be used to make small changes to the input and
	     the program can be quickly re-run.
	      hklgen then asks for the following parameters :--
\begin{enumerate}
\item Title. Up to 80 characters to be displayed at the top	 of  the
		  output.
\item Wavelength and the 9 'rules limiting possible	 reflections'  -
		  see  appendix C -- If you give wavelength = 0, the wavelength,
		  extinction rules, and orientation matrix will	 be  taken  from
		   lsd's parameter files.
		   chi	and   phi  software  limits  are  also	taken	but   an
		  opportunity is given to over-write them.
		  -- If the wavelength	is given explictly, followed by up to  9
		  numbers  for the extinction rules, the orientation matrix must
		  then be given line by line.
\item Theta limits. ( Note  not 2-Theta limits ).
		   chi and  phi limits ( if required ) must  also  be  given  in
		  this line.
		  -- If zeroes are given, no limits  will  be  included	 in  the
		  calculations.
		  -- If nothing is given, LSD's limits will be used  if data was
		  taken	 from  the parameter files, or it will default to zeroes
		  if the data above was given by the user.
\item Three numbers indicating the relative speed  of  variation  of
		   h,  k and  l.
		  First number is the slowest changing index,  third  number  is
		  the fastest changing index.
		  1/2/3 is used to represent  h/k/l.
		  e.g. 3 2 1 means L changes slowest, then K,  with  H	changing
		  fastest.
\item Minimum and maximum indices in  hkl are now requested.
		  You must give  hmin  hmax  kmin  kmax  lmin  lmax.
		  Note however that before starting  the  calculations,	  hklgen
		  calculates itself what is the maximum value for each index for
		  the specified Theta range and if this is inside these	 values,
		  they will be modified.
		  Therfore, if, for example 0 999 0 999 -999 999 is given,
		   hklgen will calculate the maximum values and give HKL's for
		  positive H, positive K, positive and negative L.
\item Four numbers concerning various outputs from the program.
		  a) The first  npunch concerns the  hkl output file.
	  0 = no file for output
	  1 = file for output containing  hkl only in 3I4 format.
	  2 = file for  d15-ren ( not for  d8-d9 )
	  3 = file for output containing  hkl and setting angles.
	     b) The second  ipara concerns machine geometry.
	  0 = Bissecting geometry - (normal for  d8-d9 ).
	  1 = Normal beam geometry - ( rarely used on  d8-d9 )
	  3 =  d15 lifting counter mode ( used with  npunch = 2 )
		  c) The third number  nbites concerns  hkl output.
	  1 = write  hkl for each case in four separate files.
	  0 = write all HKL's in one single file FOR00x. dat
	       ( x specified below ).
		  d) The fourth number  nlist concerns terminal output.
	  0 =  write each  hkl with angles and comment on terminal.
	       ( can take time and consume paper ).
	  1 = suppress most of the output on  tty.
\item If in previous line FOR00x. dat was specified for  hkl output,
		  X must be given.
		  This is  the	last  line  in	the  input  but	 is  not  always
		  necessary.
\end{enumerate}
		  The program then generates as specified, creating  file(s)  if
	     required. It gives a resume at the end and exits.

\subsubsection{Input from file hklgen.dat}

	     Input is given in exactly the same order as above, so  for	 a  more
	     detailed description of each parameter see previous section.

	     Two possible examples are given below.
\begin{verbatim}
KNUDDERKRYSTAL LAVET AF AARKVARD, 120K      (Text, 80 characters)
0.8405 0 0 0 0 0 0 0 0 0		  (Wavelength and the 9 Extinction rules. )
0.043361 -.04190  .5399	     (  ub given in three separate lines
-.046409 -.032053 .03721	     - as wavelength is given explicitly
-.00256  -.12861 -.02687
0 36 -20 95		      ( Theta limits and Chi limits - note - no limits on Phi. )
2 1 3			      ( relative speeds of  hkl. - K slowest - L fastest. )
-99 -1 0 5 -99 99		      ( Hmin,Hmax,Kmin, etc. - note all L's with all neg
1 0 0 1			      ( a) Output file of  hkl.
			        b) Bisecting geom - usual.
				c) All HKL's in  for00x.dat.
				d) Suppress most  tty output. )
3			(  hkl file on  for003.dat )
\end{verbatim}

	      hklgen is a program which has evolved in the hands of :
	     A.Filhol, S.Mason. A.Barthelemy and J.Allibon.

\subsection{Encoding of Extinction Rules}
\begin{verbatim}
HKL
              0 : NO CONDITIONS
	      1 : H + K + L = 2n
	      2 : H, K, L all even or all odd
	      3 : -H + K + L = 3n
	      4 : H = K + L = 3n
	      5 : H + K = 2n
	      6 : K + L = 2n
	      7 : H + L = 2n
	      8 : H + K + L = 6n
	      9 : H, K, L all even
	      10 : H, K, L all odd
    	      11 : If H - K = 3n, then L = 6n
H K 0
              0 : No conditions
	      1 : H = 2n
	      2 : K = 2n
	      3 : H + K = 2n
	      4 : H + K = 4n
0 K L
              0 : No conditions
	      1 : K = 2n
	      2 : K + L = 2n
	      3 : K + L = 3n
	      4 : K + L = 4n
	      5 : L = 2n
H 0 L
	      0 : No conditions
	      1 : L = 2n
	      2 : H = 2n
	      3 : L + H = 2n
	      4 : L + H = 4n
H H L
	      0 : No conditions
	      1 : L = 2n
	      2 : H = 2n
	      3 : 2H + L = 4n

\end{verbatim}
%html Conescan.html 2
%html ubcalc.htm  2
%html mesure.htm  2
%html hklscan.htm 2

%html tricspsd.htm 1
%html histogram.htm 2
%html nextrics.htm 2
%html peaksearch.htm 2
%html lowmax.htm 2
%html trscan.htm 2

%html psddata.htm 1
\end{document}
