# Docker Container for SICS

This is about running and developing SICS in docker containers. This is largely based 
on work by Douglas Clowes at ANSTO.

## Running SICS in a Docker Container

A suitable docker container can be built by running: rebuild_base.sh

For starting, there are two options:

echidna.sh is an example how an instrument can be started at ANSTO

runsinq.sh starts a SINQ instrument. It takes two command line arguments: 

1. The name of the instrument
2. The path to SICSROOT  

SICS listens then on localhost at port 50003


Please note that **SICSROOT cannot be on AFS**. The docker container has no token and 
thus cannot access AFS. 

Exiting SICS will stop the container.

## Developing SICS in a Container

You need to have built the base docker container first as decribed above. Then you 
can run rebuil_dev.sh in order to build a development container. 

You can run the development container with rundevspi.sh You will need to adapt the path directly after -v to point to the ROOT of your SICS source tree. 

Once the container is running you can attach to it by runnig attachpsi.sh. You need to cd 
to /home/sics and set SICSROOT to /home/sics before you can compile. 

## Further Development

A number of rpm's are provided in directory extrarpm. In the epics directory there is 
EPICS base. All of this is now for RHEL7. If you ever want to container into something 
else, you either need to find the appropriate packages or rebuild them. 



