animal=echidna
base=$(dirname ${PWD})
docker run -d -it \
  --name ${animal} --hostname ics9-${animal}.nbi.ansto.gov.au \
  -v ${PWD}/${animal}:/usr/local/sics \
  -v ${base}/site_ansto/instrument:/usr/local/sics/server:ro \
  -v ${base}/build:/usr/local/sics/bin:ro \
  -w /usr/local/sics/server \
  -p 60003:60003/tcp \
  local/c7-dev /usr/local/sics/bin/SICServer barebones.tcl
