#!/bin/bash
if test $# -lt 2; then
  echo "Usage: "
  echo "       runsinq.sh instrument sicsroot"
  exit 1
fi
animal=$1
base=$(dirname ${PWD})
#SICSROOT=/home/scratch/dev/docker
SICSROOT=$2
if  [[ "$SICSROOT" =~ ^/afs* ]]; then
    echo "Cannot run SICS in docker with files on AFS"
    exit 1
fi

docker run -d -it -e SICSROOT=/usr/local/sics \
  --name ${animal} --hostname ics9-${animal}.psi.ch \
  -v ${SICSROOT}/bin:/usr/local/sics/bin \
  -v ${SICSROOT}/${animal}_sics:/usr/local/sics/${animal}_sics \
  -v ${SICSROOT}/sicscommon:/usr/local/sics/sicscommon \
  -v ${SICSROOT}/tmp:/usr/local/sics/tmp \
  -w /usr/local/sics/${animal}_sics \
  -p 50003:2911/tcp \
   local/c7-base /usr/local/sics/bin/SICServer /usr/local/sics/${animal}_sics/${animal}.tcl
#  local/c7-base 
