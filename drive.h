/*-------------------------------------------------------------------------
	This object implements the general drive command for SICS.
	It can drive parameters and motors and allmost everything.
	
	Mark Koennecke, November 1996
	
        copyright: see implementation file 
-----------------------------------------------------------------------------*/
#ifndef SICSDRIVE
#define SICSDRIVE
#include "conman.h"

int Drive(SConnection * pCon, SicsInterp * pSics, char *name, float fNew);

int Start2Run(SConnection * pCon, SicsInterp * pSics, char *name, 
			int level, float fNew);

int DriveWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[]);
        /* 
           the wrapper function for the drive command 
         */

int RunWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[]);
        /* 
           the wrapper function for the run command 
         */


int MakeDrive(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[]);
        /* 
           the factory function for the Drive & Run command
         */

#endif
