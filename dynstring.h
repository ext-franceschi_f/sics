/*---------------------------------------------------------------------------
                           D Y N S T R I N G
                           
    A dynamic String Implementation. You can append and insert into this
    string at will. It automatically takes care of allocating more
    memory when needed.

    COPYRIGHT:
              CopyLeft, 1998, Mark Koennecke
              There are two things you MAY NOT DO with this code:
              - Sue me or my employer because it does not work.
              - Use it in a critical environment, i.e health, 
                radioactive device control, military control
                systems and the like. 
              You may do everything else with this code. It has been
              written with swiss taxpayers money. 
          
    NOTE:
         All functions return 1 or a pointer on success,
         0, or NULL on failure           
    
    Mark Koennecke, March 1998 
----------------------------------------------------------------------------*/
#ifndef DYNAMICSTRING
#define DYNAMICSTRING
/*
 * Legacy definitions
 */
#define CreateDynString DynStringCreate
#define DeleteDynString DynStringDelete
#define GetCharArray DynStringGetArray
#define GetDynStringLength DynStringGetLength
#define Dyn2Cstring DynStringGetString
#define DynStringCapacity DynStringSetCapacity
typedef struct __DynString *pDynString;

/*----------------------- live and death ----------------------------------*/
pDynString DynStringCreate(int iInitialLength, int iResizeLength);
pDynString DynStringFromC(const char *source);
   /*
      Create a new DynString Object. Its initial length will be iInitialLength.
      It will be resized in iResizeLength steps. This allows for efficient
      storage management. It woul be seriously inefficient to allocate
      per added character.
    */

void DynStringDelete(pDynString self);
pDynString DynStringHold(pDynString self);
/*----------------------- interface to it --------------------------------- */

int DynStringCopy(pDynString self, const char *pText);

   /* 
      Copies the text in Text into the DynString starting at 0 and over
      writing anything there beforehand.
    */

int DynStringConcat(pDynString self, const char *pText);
   /*
      Concatenates the string in DynString with the one supplied
      in string.
    */
int DynStringConcatLine(pDynString self, const char *pText);
   /*
      Concatenates the string in DynString with the one supplied
      in string. And add a newline. 
    */

int DynStringConcatChar(pDynString self, char c);
   /*
      adds one character at the end of the string
    */
int DynStringConcatBytes(pDynString self, const char *data, int datalen);
/*
 * adds datalen bytes from data to the buffer
 */
int DynStringInsert(pDynString self, const char *pText, int iPos);

   /*
      inserts the text in pText at Position iPos in the DynString.
      Everything behind iPos will be pushed outwards in order to create
      space for pText.
    */

int DynStringReplace(pDynString self, const char *pText, int iPos);

   /*
      Starting at iPos, replace everything after it with ptext. In
      contrats to insert this won't push data backwards.
    */

char *DynStringGetArray(pDynString self);

   /* 
      retrieves a pointer to the character array keeping the current 
      text. NEVER, ever free this pointer, otherwise you are rewarded
      with a core dump. The pointer belongs to DynString and will be
      deleted when deleting the DynString.
    */

int DynStringGetLength(pDynString self);

   /*
      returns the current length of the dynamic string.
    */

int DynStringClear(pDynString self);
   /*
      removes all old data from the dynstring
    */
int DynStringBackspace(pDynString self);
   /* 
      removes one character at the end from the dynstring
    */
int DynStringShorten(pDynString self, int length);
   /* 
      shorten the dynstring to the given length
    */
char *DynStringGetString(pDynString self);
   /* 
      convert to C string and delete dynstring. The result must be freed when no longer used.
    */
/*
  Ensures that the DynString has the capacity length. In order to avoid unnecessary
  concatenation when the length is already known.
*/
int DynStringSetCapacity(pDynString self, int length);

#if __GNUC__ > 2
#define G_GNUC_PRINTF( format_idx, arg_idx )    \
  __attribute__((__format__ (__printf__, format_idx, arg_idx)))
#else
#define G_GNUC_PRINTF( format_idx, arg_idx )
#endif
/*
 * Utility print function - to end of current string
 */
int DynStringPrintf(pDynString self, char *fmt, ...) G_GNUC_PRINTF (2, 3);
#undef G_GNUC_PRINTF
#endif
