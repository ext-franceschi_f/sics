/*--------------------------------------------------------------------------
   Textual representation of the error-status codes defined in
   Scommon.h
   
   Mark Koennecke, February 1997
----------------------------------------------------------------------------*/
#ifndef ERRSTAT
#define ERRSTAT

static char *pErrStat[] = {
  "  ",
  "OK",
  "HWIdle",
  "HWBusy",
  "HWFault",
  "HWPosFault",
  "HWCrash",
  "Out of memory",
  "No Beam",
  "Paused",
  NULL
};


#endif
