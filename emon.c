/*--------------------------------------------------------------------------

 		E N V I R O N M E N T  M O N I T O R
 		
  The envirornment monitor: Implementation

  Mark Koennecke, Juli 1997
  
  Reduced polling frequency to any 20 seconds, Mark Koennecke, August 2011

       Copyright:

       Labor fuer Neutronenstreuung
       Paul Scherrer Institut
       CH-5423 Villigen-PSI


      The authors hereby grant permission to use, copy, modify, distribute,
      and license this software and its documentation for any purpose, provided
      that existing copyright notices are retained in all copies and that this
      notice is included verbatim in any distributions. No written agreement,
      license, or royalty fee is required for any of the authorized uses.
      Modifications to this software may be copyrighted by their authors
      and need not follow the licensing terms described here, provided that
      the new terms are clearly indicated on the first page of each file where
      they apply.

      IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
      FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
      ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
      DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
      POSSIBILITY OF SUCH DAMAGE.

      THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
      FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
      IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
      NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
      MODIFICATIONS.
----------------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <tcl.h>                /* for DString */
#include "fortify.h"
#include "lld.h"
#include "sics.h"
#include "emon.i"
#include "emon.h"
#include "event.h"

/*
 * any how many seconds we check
 */
#define SCANINTERVALL 20
/*--------------------------------------------------------------------------*/
pEnvMon CreateEnvMon(void)
{
  pEnvMon pNew = NULL;

  pNew = (pEnvMon) malloc(sizeof(EnvMon));
  if (!pNew) {
    return NULL;
  }
  memset(pNew, 0, sizeof(EnvMon));

  pNew->pDes = CreateDescriptor("Environment Monitor");
  if (!pNew->pDes) {
    free(pNew);
    return NULL;
  }
  pNew->iList = LLDcreate(sizeof(EVEntry));
  if (pNew->iList < 0) {
    DeleteDescriptor(pNew->pDes);
    free(pNew);
    return NULL;
  }
  pNew->nextRun = time(NULL)+SCANINTERVALL;
  AddCommand(pServ->pSics, "emon", EVWrapper, DeleteEnvMon, pNew);
  return pNew;
}

/*---------------------------------------------------------------------------*/
void DeleteEnvMon(void *pData)
{
  pEnvMon self;
  int iRet;
  EVEntry sEntry;

  self = (pEnvMon) pData;
  assert(self);


  /* remove all the names */
  iRet = LLDnodePtr2First(self->iList);
  while (iRet != 0) {
    LLDnodeDataTo(self->iList, &sEntry);
    if (sEntry.pName) {
      free(sEntry.pName);
      sEntry.pName = NULL;
    }
    iRet = LLDnodePtr2Next(self->iList);
  }
  /* remove list */
  LLDdelete(self->iList);

  /* remove descriptor */
  if (self->pDes) {
    DeleteDescriptor(self->pDes);
  }
  free(self);
}

/*---------------------------------------------------------------------------*/
int EVRegisterController(pEnvMon self, char *pName, void *pData,
                         SConnection * pCon)
{
  EVEntry sEntry;
  char pBueffel[512];

  assert(self);
  assert(pData);
  assert(pCon);

  sEntry.pDum = (pDummy) pData;
  sEntry.pInter = sEntry.pDum->pDescriptor->GetInterface(pData,
                                                         ENVIRINTERFACE);
  if (sEntry.pInter == NULL) {
    snprintf(pBueffel,511,
            "ERROR: cannot register %s, no environment interface", pName);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
  sEntry.pName = strdup(pName);

  LLDnodeAppendFrom(self->iList, &sEntry);
  return 1;
}

/*------------------------------------------------------------------------*/
int EVUnregister(pEnvMon self, char *pName)
{
  EVEntry sEntry;
  int iRet;

  assert(self);

  iRet = LLDnodePtr2First(self->iList);
  while (iRet != 0) {
    LLDnodeDataTo(self->iList, &sEntry);
    if (strcmp(sEntry.pName, pName) == 0) {
      free(sEntry.pName);
      LLDnodeDelete(self->iList);
      return 1;
    }
    iRet = LLDnodePtr2Next(self->iList);
  }
  return 0;
}

/*---------------------------------------------------------------------------*/
static int EVMonitorSingle(pEnvMon self, EVEntry sEntry)
{
  EVMode eMode;
  int iRet, iStatus;

  eMode = sEntry.pInter->GetMode(sEntry.pDum);
  if (eMode == EVMonitor) {
    iRet = sEntry.pInter->IsInTolerance(sEntry.pDum);
    if (iRet == 0) {
      iStatus = sEntry.pInter->HandleError(sEntry.pDum);
      return 0;
    }
  }
  return 1;
}

/*--------------------------------------------------------------------------*/
int EVMonitorControllers(pEnvMon self)
{
  int iRet, status;
  int iRes = 1;
  EVEntry sEntry;

  assert(self);

  iRet = LLDnodePtr2First(self->iList);
  while (iRet != 0) {
    LLDnodeDataTo(self->iList, &sEntry);
    status = EVMonitorSingle(self, sEntry);
    if (status != 1) {
      iRes = 0;
    }
    iRet = LLDnodePtr2Next(self->iList);
  }
  return iRes;
}

/*---------------------------------------------------------------------------*/
int EVList(pEnvMon self, SConnection * pCon)
{
  int iRet;
  float fVal;
  Tcl_DString xString, *pResult;
  pIDrivable pDriv;
  EVEntry sEntry;
  EVMode eMode;
  char pNumber[30];
  char *pPtr;

  assert(self);
  assert(pCon);

  pResult = &xString;
  Tcl_DStringInit(pResult);
  Tcl_DStringAppend(pResult, "EMON={", -1);

  iRet = LLDnodePtr2First(self->iList);
  while (iRet != 0) {
    LLDnodeDataTo(self->iList, &sEntry);
    pDriv = sEntry.pDum->pDescriptor->GetInterface(sEntry.pDum, DRIVEID);
    assert(pDriv);              /* a controller must have a drivable and 
                                   a environment interface! */
    fVal = pDriv->GetValue(sEntry.pDum, pCon);
    sprintf(pNumber, ",%f,", fVal);
    eMode = sEntry.pInter->GetMode(sEntry.pDum);
    Tcl_DStringAppend(pResult, sEntry.pName, -1);
    Tcl_DStringAppend(pResult, pNumber, -1);
    switch (eMode) {
    case EVIdle:
      Tcl_DStringAppend(pResult, "Idle", -1);
      break;
    case EVDrive:
      Tcl_DStringAppend(pResult, "Driving", -1);
      break;
    case EVMonitor:
      Tcl_DStringAppend(pResult, "In Monitor", -1);
      break;
    case EVError:
      Tcl_DStringAppend(pResult, "Out of range", -1);
      break;
    }
    Tcl_DStringAppend(pResult, ";\n", -1);
    iRet = LLDnodePtr2Next(self->iList);
  }
  Tcl_DStringAppend(pResult, "}", -1);
  pPtr = Tcl_DStringValue(pResult);
  SCWrite(pCon, pPtr, eValue);
  Tcl_DStringFree(pResult);
  return 1;
}

/*--------------------------------------------------------------------------
  The Environment Monitor understands a few commands:
                list            : list the current state of affairs
                register   name : registers name as a controller to emon
                unregister name : removes name from emon
---------------------------------------------------------------------------*/
int EVWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[])
{
  pEnvMon self = NULL;
  char pBueffel[512];
  int iRet;
  CommandList *pCom = NULL;

  self = (pEnvMon) pData;
  assert(self);
  assert(pCon);
  assert(pSics);

  argtolower(argc, argv);
  if (argc < 2) {
    SCWrite(pCon, "ERROR: Insufficient arguments specified for emon",
            eError);
    return 0;
  }

  if (strcmp(argv[1], "list") == 0) {   /* list command */
    EVList(self, pCon);
    return 1;
  } else if (strcmp(argv[1], "register") == 0) {
    if (argc < 3) {
      SCWrite(pCon,
              "ERROR: Insufficient arguments specified for emon register",
              eError);
      return 0;
    }
    if (!SCMatchRights(pCon, usUser)) {
      SCWrite(pCon, "ERROR: Insufficient privilege to register", eError);
      return 0;
    }
    pCom = FindCommand(pSics, argv[2]);
    if (!pCom) {
      snprintf(pBueffel,511, "ERROR: object %s not found, not registered",
              argv[2]);
      SCWrite(pCon, pBueffel, eError);
      return 0;
    }
    return EVRegisterController(self, argv[2], pCom->pData, pCon);
  } else if (strcmp(argv[1], "unregister") == 0) {
    if (argc < 3) {
      SCWrite(pCon,
              "ERROR: Insufficient arguments specified for emon unregister",
              eError);
      return 0;
    }
    if (!SCMatchRights(pCon, usUser)) {
      SCWrite(pCon, "ERROR: Insufficient privilege to unregister", eError);
      return 0;
    }
    iRet = EVUnregister(self, argv[2]);
    if (!iRet) {
      snprintf(pBueffel,511, "WARNING: %s not found in emon", argv[2]);
      SCWrite(pCon, pBueffel, eWarning);
    }
    return 1;
  } else {
    snprintf(pBueffel, 511, "ERROR: %s not recognized as command to emon",
            argv[1]);
    SCWrite(pCon, pBueffel, eError);
  }
  return 0;
}

/*---------------------------------------------------------------------------*/
pEnvMon FindEMON(SicsInterp * pSics)
{
  CommandList *pCom = NULL;

  assert(pSics);

  pCom = FindCommand(pSics, "emon");
  assert(pCom);

  return (pEnvMon) pCom->pData;
}

/*---------------------------------------------------------------------------*/
int EnvMonTask(void *pData)
{
  pEnvMon self = NULL;

  self = (pEnvMon) pData;
  assert(self);

  if (self->iEnd) {
    return 0;
  }

  if(time(NULL) > self->nextRun){
	  EVMonitorControllers(self);
	  self->nextRun = time(NULL) + SCANINTERVALL;
  }
  return 1;
}

/*------------------------------------------------------------------------*/
void EnvMonSignal(void *pUser, int iSignal, void *pEventData)
{
  pEnvMon self = NULL;
  int *iInt;

  self = (pEnvMon) pUser;
  assert(self);
  iInt = (int *) pEventData;

  if (iSignal == SICSINT) {
    iInt = (int *) pEventData;
    if (*iInt == eEndServer) {
      self->iEnd = 1;
    }
  }
}
