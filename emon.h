
#line 149 "emonitor.w"

/*--------------------------------------------------------------------------
                E N V I R O N M E N T   M O N I T O R

  The environment monitor is responsible for monitoring active environment
  devices. 

  Mark Koennecke, Juli 1997

  copyright: see implementation file.
  doc: programmers reference.
----------------------------------------------------------------------------*/
#ifndef SICSEMON
#define SICSEMON
  typedef struct __EnvMon *pEnvMon;
/*-------------------------------------------------------------------------*/

#line 86 "emonitor.w"


   pEnvMon CreateEnvMon(void);
   void    DeleteEnvMon(void *pData);

   int EVRegisterController(pEnvMon self, char *pName, void *pData,
                            SConnection *pCon);
   int EVUnregister(pEnvMon self, char *name);
   
   int EVMonitorControllers(pEnvMon self);

   int EVList(pEnvMon self, SConnection *pCon);

   int EVWrapper(SConnection *pCon, SicsInterp *pSics, void *pData,
                 int argc, char *argv[]);

   pEnvMon FindEMON(SicsInterp *pSics);
   
   int EnvMonTask(void *pEv);
   void EnvMonSignal(void *pEndv, int iSignal, void *pSigData);

#line 165 "emonitor.w"

#endif
