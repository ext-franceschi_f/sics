
/*--------------------------------------------------------------------------
   Internal header file defining the datastructure for the environment
   controller.

   Mark Koennecke, Juli 1997
---------------------------------------------------------------------------*/

   typedef struct __EnvMon {
                            pObjectDescriptor pDes;
                            int iList;
                            int iEnd;
                            time_t nextRun;
                            } EnvMon;
/*---------------------------------------------------------------------------*/
   typedef struct {
                    pDummy pDum;
                    char *pName;
                    pEVInterface pInter;
                  } EVEntry, *pEVEntry;     



