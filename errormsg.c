#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "errormsg.h"

/* compare two strings for euqality, ignoring text within square brackets */
int ErrEqual(char *str1, char *str2)
{
  char *p;

  while (*str1 != '\0' || *str2 != '\0') {
    if (*str1 != *str2) {
      return 0;
    }
    if (*str1 == '[') {
      str1 = strchr(str1, ']');
      str2 = strchr(str2, ']');
      if (str1 == NULL || str2 == NULL) {
        return str1 == str2;
      }
    }
    str1++;
    str2++;
  }
  return 1;
}

void ErrPutMsg(ErrList *list, char *fmt, ...)
{
  ErrMsg *m = NULL;
  ErrMsg **last = NULL;
  va_list ap;
  char buf[256];
  char *text = NULL;
  int l;
  static long id = 0;
  
  va_start(ap, fmt);
  l = vsnprintf(buf, sizeof buf, fmt, ap);
  va_end(ap);
  if (l < sizeof buf) {
    text = buf;
  } else {
    /* assuming we have a C99 conforming snprintf and need a larger buffer */
    text = calloc(l, 1);
    if (!text) return;
    va_start(ap, fmt);
    vsnprintf(text, l, fmt, ap);
    va_end(ap);
  }
  last = &list->current;
  for (m = list->current; m != NULL; m = m->next) {
    if (ErrEqual(text, m->text)) {
      *last = m->next;          /* remove found item from list */
      break;
    }
    last = &m->next;
  }
  if (m == NULL) {              /* make a new item */
    if (text == buf)
      text = strdup(buf);
    m = calloc(1, sizeof(*m));
    m->text = text;
    m->cnt = 1;
    m->dirty = 0;
    id++;
    m->id = id;
  } else {
    if (text != buf)
      free(text);
    m->cnt++;
  }
  m->next = list->current;
  time(&m->last);
  list->current = m;
}

char *ErrGetLastMsg(ErrList *list) {
  if (list == NULL) return "";
  if (list->current == NULL) return "";
  return list->current->text;
}
