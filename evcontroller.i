
#line 249 "evcontroller.w"

/*-------------------------------------------------------------------------
    Environment controller datastructure
    
    Mark Koennecke, Juli 1997

---------------------------------------------------------------------------*/
/*-------- Parameter defines */
#define TOLERANCE 0
#define ACCESS 1
#define ERRORHANDLER 2
#define INTERRUPT 3
#define UPLIMIT   4
#define LOWLIMIT  5
#define SAFEVALUE 6
#define MAXWAIT   7
#define SETTLE    8


#line 29 "evcontroller.w"

   typedef struct __EVControl {
                               pObjectDescriptor  pDes;
                               pIDrivable         pDrivInt;
                               pEVInterface       pEnvir;
                               pICallBack         pCall;
                               int                callCount;
                               pEVDriver          pDriv;
                               EVMode             eMode;
                               float              fTarget;
                               time_t             start;
                               time_t             lastt;
                               char               *pName;
                               char               *driverName;
                               char               *errorScript;
                               ObPar              *pParam;
                               int                iLog;
                               pVarLog            pLog;
                               int                iWarned;
                               int                iTcl;
                               int                iStop;
                               SConnection       *conn;
                               char *creationArgs;
                               char *runScript;
                               void *pPrivate;
                               void (*KillPrivate)(void *pData);
                               } EVControl;

#line 267 "evcontroller.w"

