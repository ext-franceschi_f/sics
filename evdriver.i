
#line 270 "evcontroller.w"

/*-------------------------------------------------------------------------
    Environment device driver datastructure
    
    Mark Koennecke, Juli 1997

---------------------------------------------------------------------------*/
#define DEVOK 1
#define DEVFAULT 0
#define DEVREDO 2


#line 92 "evcontroller.w"

#ifndef __EVDRIVER
#define __EVDRIVER
   typedef struct __EVDriver {
                             int (*SetValue)(pEVDriver self, float fNew);
                             int (*GetValue)(pEVDriver self, float *fPos);
                             int (*GetValues)(pEVDriver self, float *fTarget,
                                  float *fPos, float *fDelta);
                             int (*Send)(pEVDriver self, char *pCommand,
                                         char *pReplyBuffer, int iReplBufLen); 
                             int (*GetError)(pEVDriver self, int *iCode,
                                             char *pError, int iErrLen);
                             int (*TryFixIt)(pEVDriver self, int iCode);
                             int (*SavePars)(pEVDriver self, FILE *fil);
                             int (*Init)(pEVDriver self);
                             int (*Close)(pEVDriver self);
                             void *pPrivate;
                             void (*KillPrivate)(void *pData);
   }EVDriver;

#endif
#line 281 "evcontroller.w"

/*-------------------- life & death of  a driver  --------------------------*/
     pEVDriver CreateEVDriver(int argc, char *argv[]);
     void DeleteEVDriver(pEVDriver pDriv);
