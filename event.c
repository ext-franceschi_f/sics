/*-------------------------------------------------------------------------
                             E V E N T
                             
  This file implements a few event related functions.
  
  Mark Koennecke, 1997
  
       Copyright:

       Labor fuer Neutronenstreuung
       Paul Scherrer Institut
       CH-5423 Villigen-PSI


      The authors hereby grant permission to use, copy, modify, distribute,
      and license this software and its documentation for any purpose, provided
      that existing copyright notices are retained in all copies and that this
      notice is included verbatim in any distributions. No written agreement,
      license, or royalty fee is required for any of the authorized uses.
      Modifications to this software may be copyrighted by their authors
      and need not follow the licensing terms described here, provided that
      the new terms are clearly indicated on the first page of each file where
      they apply.

      IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
      FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
      ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
      DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
      POSSIBILITY OF SUCH DAMAGE.

      THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
      FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
      IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
      NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
      MODIFICATIONS.
----------------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "fortify.h"
#include "event.h"

/*
  WATCHIT! see event.h for defines of event codes! The list below MUST
  match.
*/
static char *pEvent[] = {
  "VARCHANGE",
  "MOTORDRIVE",
  "MONITOR",
  "ROTORSTART",
  "ROTORMOVE",
  "SCANEND",
  "SCANSTART",
  "SCANPOINT",
  "WLCHANGE",
  "REFLECTIONDONE",
  "COUNTSTART",
  "COUNTEND",
  "FILELOADED",
  "MOTEND",
  "BATCHSTART",
  "BATCHAREA",
  "BATCHEND",
  "DRIVSTAT",
  "STATUS",
  "POSITION",
  "HDBVAL",
  "STATESTART",
  "STATEEND",
  "NEWTARGET",
  "DIMCHANGE",
  "PAUSE",
  "CONTINUE",
#ifdef SITE_ANSTO
  "BUSY",
  "IDLE",
#endif
  NULL
};

/*---------------------------------------------------------------------------*/
int Text2Event(char *pText)
{
  int iVal = 0;

  while (pEvent[iVal] != NULL) {
    if (strcmp(pEvent[iVal], pText) == 0) {
      return iVal;
    }
    iVal++;
  }
  return -1;
}
