
#line 108 "event.w"

/*----------------------------------------------------------------------------
                        E V E N T

  This header file lists the event codes known to SICS. These event code
  are used in callback functions. Where apropriate, the event data
  structures are also defined in this file.

  Mark Koennecke, 1997

----------------------------------------------------------------------------*/
#ifndef SICSEVENT
#define SICSEVENT

#line 13 "event.w"

   int Text2Event(char *pText);

#line 121 "event.w"



#line 20 "event.w"

#define VALUECHANGE 0
#define MOTDRIVE    1
#define MONITOR     2  
#define ROTSTART    3
#define ROTMOVE     4
#define SCANEND     5
#define SCANSTART   6
#define SCANPOINT   7
#define WLCHANGE    8
#define REFLECTIONDONE 9
#define COUNTSTART     10
#define COUNTEND       11
#define FILELOADED     12
#define MOTEND     13
#define BATCHSTART 14
#define BATCHAREA  15
#define BATCHEND   16
#define DRIVSTAT   17
#define STATUS     18
#define POSITION   19
#define HDBVAL     20
#define STSTART    21
#define STEND      22
#define NEWTARGET  23
#define DIMCHANGE  24
#define IPAUSE     25
#define CONTINUE   26
#define STPAUSE    25
#define STCONTINUE 26
#define STBUSY     27
#define STIDLE     28


#line 123 "event.w"


/*----------------- event data structure for the NEWTARGET event ---------*/
typedef struct {
  char *name;
  float target;
  } NewTarget, *pNewTarget;
/*--------------- Signals for the Signalfunction of each task ------------*/

#line 90 "event.w"

#define SICSINT 300
#define SICSBROADCAST 301
#define TOKENGRAB     302
#define TOKENRELEASE  303
#define COMLOG        304
#define CRONFUNC      305

#line 131 "event.w"
 
#endif
