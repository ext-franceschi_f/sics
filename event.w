\subsection{SICS Events}
This section lists the  callback events known to Sics, their purpose, and
their associated event datastructures when applicable. See the
\ref{inter} section about callbacks for more information about
callbacks. This only defines the integer codes possible. Please be
aware that the implementation of the callbacks is a duty of the SICS
objects generating it. The mentioning of an event code here means it
is used somewhere in SICS but does not mean that it is implemented by
all objects which might implement the event.   

This module defines several event related functions as well.

@d eFunc @{
   int Text2Event(char *pText);
@} 

{\bf Text2Event } converts a text value into an event code. It returns -1
if the event code is not known, else the apropriate event code.

@d VE @{
#define VALUECHANGE 0
#define MOTDRIVE    1
#define MONITOR     2  
#define ROTSTART    3
#define ROTMOVE     4
#define SCANEND     5
#define SCANSTART   6
#define SCANPOINT   7
#define WLCHANGE    8
#define REFLECTIONDONE 9
#define COUNTSTART     10
#define COUNTEND       11
#define FILELOADED     12
#define MOTEND     13
#define BATCHSTART 14
#define BATCHAREA  15
#define BATCHEND   16
#define DRIVSTAT   17
#define STATUS     18
#define POSITION   19
#define HDBVAL     20
#define STSTART    21
#define STEND      22
#define NEWTARGET  23
#define DIMCHANGE  24
#define IPAUSE     25
#define CONTINUE   26
@}
\begin{description}
\item[VALUECHANGE] This is a variable changing its value. As event data a pointer to the
SicsVariable is passed along. This is defined in sicsvar.h.
\item[MOTDRIVE] a motor is moving. The event data is the current position.
\item[MONITOR] The control value for a counting operation has changed.
The event data is the new value.
\item[ROTSTART] A velocity selector is starting up.
\item[ROTMOVE]  A velocity selector changes its rotation speed.
\item[SCANEND] A scan has ended.
\item[SCANSTART] A new scan has been started.
\item[SCANPOINT] A scan point has finished and there is new data.
\item[WLCHANGE] is incoked when the wavelength of a crystal monochromator 
changes. The event parameter is a pointer to the wave length or energy
variable being driven. Client code can then get what it needs to know.
\item[REFLECTIONDONE] is issued when a single reflection of a four circle
fiffractometer has been measured.
\item[COUNTSTART] is an event which signals the start of a counting
operation.
\item[COUNTEND] is an event signalling the end of a counting operation.
\item[MOTEND] signals the end of a motor driving operation.
\item[BATCHSTART] signals starting batch processing a new buffer
\item[BATCHAREA] signals that a new area of the batch file is processing.
\item[BATCHEND] signals the end of the batch buffers processing.
\item[DRIVSTAT] signals a change in the status of a driving operation
(start, finished, fault)
\item[STATUS] ANSTO defined code.
\item[POSITION] ANSTO defined code
\item[HDBVAL] The Hdb is notified of a value change. The eventData will be 
 the object on which the data changed.
 \item[NEWTARGET] is invoked when a new target has been set on a drivable.
\item[PAUSE] Pause counting
\item[CONTINUE] Continue counting
\end{description}


Furthermore event contains system wide signal codes which are interpreted in 
the  signal functions provided by each SICS task. These code are
evaluated by the TaskSignalFunction which may be configured for each
SICS task. See the task section for more information. Here, only
possible codes are defined. 

@d VSIG @{
#define SICSINT 300
#define SICSBROADCAST 301
#define TOKENGRAB     302
#define TOKENRELEASE  303
#define COMLOG        304
#define CRONLIST      305
@}
\begin{description}
\item[SICSINT] An interrupt has ocurred. The signal data is the interrupt 
number which ocurred.
\item[SICSBROADCAST] A message shall be sent to each SICS client. The signal
data is the string to send.
\item[TOKENGRAB] A connection has successfully grabbed the control token.
\item[TOKENRELEASE] A connection has released the control token. 
\item[COMLOG] A command log message. This is to implement listen mode to the command log.
\item[CRONLIST] Tell the cron tasks to inform about themselves.
\end{description}
@o event.h -d @{
/*----------------------------------------------------------------------------
                        E V E N T

  This header file lists the event codes known to SICS. These event code
  are used in callback functions. Where apropriate, the event data
  structures are also defined in this file.

  Mark Koennecke, 1997

----------------------------------------------------------------------------*/
#ifndef SICSEVENT
#define SICSEVENT
@<eFunc@>

@<VE@>

/*----------------- event data structure for the NEWTARGET event ---------*/
typedef struct {
  char *name;
  float target;
  } NewTarget, *pNewTarget;
/*--------------- Signals for the Signalfunction of each task ------------*/
@<VSIG@> 
#endif
@}
