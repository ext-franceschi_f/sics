
#line 227 "exe.w"

/**
  * Buffer handling code for the Exe Buffer batch file processing
  * module.
  *
  * copyright: see file COPYRIGHT
  *
  * Mark Koennecke, November 2004
  */
#ifndef EXEBUF
#define EXEBUF
#include <sics.h>
#include "dynstring.h"

#line 44 "exe.w"

typedef struct __EXEBUF *pExeBuf;

  /**
    * create an exe buffer
    * @param The name to use for the exe buffer
    * @return A new exe buffer or NULL if out of memory
    */
pExeBuf exeBufCreate(char *name);
  /**
    * delete an exe buffer
    * @param data The exe buffer to delete
    */
void exeBufKill(void *data);
void exeBufDelete(pExeBuf data);
  /**
    * add a a line to the exe buffer
    * @param self The exe buffer the line is to be added to
    * @param line The text to add
    * @return 1 on success, 0 when out of memory
    */
int exeBufAppend(pExeBuf self, char *line);
  /**
    * load an exe buffer from a file
    * @param self The exe buffer to load
    * @param filename The name of the file to load
    * @return 1 on success, 0 if the file could not be opened or
    * memory is exhausted.
    */
int exeBufLoad(pExeBuf self, char *filename);
  /**
    * save an exe buffer to a file. 
    * @param self The exe buffer to laod
    * @param filename The name of the file to laod
    * @return 1 on success, 0 if the file could not be opened.
    */
int exeBufSave(pExeBuf self, char *filename);
  /**
    * process an exe buffer
    * @param self The exe buffer to process
    * @param pSics The SICS interpreter to use for processing
    * @param pCon The connection object providing the environment for
    * processing this buffer.
    * @pCall The callback interface to use for automatic notifications
    * @return 1 on success, 0 on error
    */
int exeBufProcess(pExeBuf self, SicsInterp * pSics,
                  SConnection * pCon, pICallBack pCall, int echo);
 /**
    * Process an exe buffer, but store commands causing errors in a list.
    * This is used for restoring status files. 
    * @param self The exe buffer to process
    * @param pSics The SICS interpreter to use for processing
    * @param pCon The connection object providing the environment for
    * processing this buffer.
    * @param errCommandList A lld string list for storing commands which 
    * had errors.
    * @return 1 on success, 0 on error
    */
int exeBufProcessErrList(pExeBuf self, SicsInterp * pSics,
                         SConnection * pCon, int errCommandList);
  /**
    * retrieves the executing range
    * @param self The exe buffer to query
    * @param start The start of the executing range
    * @param end The end of the executing range
    * @param lineno The current line number
    */
void exeBufRange(pExeBuf self, int *start, int *end, int *lineno);
  /**
    * retrieves some portion  of text
    * @param self The exe buffer to query
    * @param start The start index from which to copy
    * @param end The end pointer until which to copy
    * @return A dynamic string holding the currently executing text. It is
    * the clients task to delete this buffer sfter use. Or NULL if out of
    * memory.
    */
pDynString exeBufText(pExeBuf self, int start, int end);
  /**
    * retrieves the name of the batch buffer
    * @param self The exe buffer to query
    * @return A pointer to the buffer name. Do not delete! The name
    * is still owned by the exe beuffer.
    */
char *exeBufName(pExeBuf self);

#line 240 "exe.w"

unsigned long exeBufHash(pExeBuf self);

#endif
