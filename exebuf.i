
#line 218 "exe.w"

/*--------------------------------------------------------------------
  Internal header file for the exe buffer module. Do not edit. This is
  automatically generated from exe.w
---------------------------------------------------------------------*/

#line 25 "exe.w"

typedef struct __EXEBUF{
        char *name;
        pDynString bufferContent;
        int start;
        int end;
        int lineno;
        unsigned long hash;
} ExeBuf;

#line 223 "exe.w"


