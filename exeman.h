
/**
  * The batch buffer manager module. For more information see exe.tex.
  *
  * copyright: see file COPYRIGHT
  *
  * Mark Koennecke, November 2004
*/
#ifndef EXEMAN
#define EXEMAN

int MakeExeManager(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[]);
int ExeManagerWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
                      int argc, char *argv[]);
int runExeBatchBuffer(void *pData, SConnection * pCon, SicsInterp * pSics,
                      char *name);
pDynString findBatchFile(SicsInterp * pSics, char *name);
int exeHdbBuffer(SConnection * pCon, SicsInterp * pSics, char *name);
int exeHdbNode(pHdb exeNode, SConnection * pCon);

int isBatchRunning();

#endif
