
#line 196 "exe.w"

/*-------------------------------------------------------------------
  Internal header file for the exe manager module. Do not edit. This
  is automatically generated from exe.w
-------------------------------------------------------------------*/

#line 151 "exe.w"

typedef struct __EXEMAN{
        pObjectDescriptor pDes;
        pICallBack pCall;
        char *sysPath;
        char *batchPath;
        pDynar exeStack;
        int exeStackPtr;
        int runList;
        pExeBuf uploadBuffer;
        int echo;
        SConnection *runCon;
}ExeMan, *pExeMan;

#line 201 "exe.w"

