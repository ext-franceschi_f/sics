/**
 * This is a new version of the four circle reflection measurement module. Its task is 
 * to provide functionality required for measuring a list of reflections. This is done 
 * with the help of some scripts bind things together. This module provides:
 * - a table of scan parameters for measuring reflections at different angles. 
 * - the functions required to produce the scan data files for single detector 
 *   mode.
 * - a list of reflections to measure. 
 * 
 * In contrast to the old mesure module, this module is more flexible and 
 * allows for reflection list processing in all modes and for all detectors.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, July 2008
 */

#ifndef _FOURMESS
#define _FOURMESS
#include "sics.h"

void InstallFourMess(SConnection * pCon, SicsInterp * pSics);

#endif
