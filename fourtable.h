
/*---------------------------------------------------------------------------
                  F O U R T A B L E

 A SICS object which holds the variation of scan parameters for four circle
 reflection list measurements.
 copyright: see copyright.h

 Mark Koennecke, February 2005
---------------------------------------------------------------------------*/
#ifndef FOURTABLE
#define FOURTABLE
#include "sicsobj.h"

pSICSOBJ MakeFourCircleTable();
void DeleteFourCircleTable(pSICSOBJ self);
int HandleFourCircleCommands(pSICSOBJ self, SConnection * pCon,
                             int argc, char *argv[], int *err);
char *GetFourCircleScanVar(pSICSOBJ self, double two_theta);
double GetFourCircleStep(pSICSOBJ self, double two_theta);
float GetFourCirclePreset(pSICSOBJ self, double twoTheta);
int GetFourCircleScanNP(pSICSOBJ self, double twoTheta);

#endif
