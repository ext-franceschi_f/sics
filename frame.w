\subsection{Frame}
This module allows to retrieve data frames from a 3D histogram (PSD plus
time dimension). This can be done either from a Sinq histogram memory
and old data files visible from the SICS server.

This module has no data structure and only implements the usual
interpreter interface  functions.


@o frame.h @{
/*-------------------------------------------------------------------------
  A module implementing functionality for reading single time frames
  from PSD time-of-flight datasets. This can be done either from
  SINQHM histogram memories or from old data files visible from the
  SICS server.
  
  copyright: see file COPYRIGHT
  
  Mark Koennecke, February-March 2003
*/
#ifndef SICSFRAME
#define SICSFRAME

int MakeFrameFunc(SConnection *pCon, SicsInterp *pSics, void *pData,
	int argc, char *argv[]);

int PSDFrameAction(SConnection *pCon, SicsInterp *pSics, void *pData,
	int argc, char *argv[]);


#endif
@}