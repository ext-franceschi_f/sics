/*---------------------------------------------------------------------------

		F U N C T I O N P A R S E R
		
	Some code to automatically parse textual arguments to
	function arguments, thereby checking for errors on the way.
	


	Mark Koennecke,    January 1997

	Copyright:

	Labor fuer Neutronenstreuung
	Paul Scherrer Institut
	CH-5423 Villigen-PSI


        The authors hereby grant permission to use, copy, modify, distribute,
        and license this software and its documentation for any purpose, provided
        that existing copyright notices are retained in all copies and that this
        notice is included verbatim in any distributions. No written agreement,
        license, or royalty fee is required for any of the authorized uses.
        Modifications to this software may be copyrighted by their authors
        and need not follow the licensing terms described here, provided that
        the new terms are clearly indicated on the first page of each file where
        they apply.

        IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
        FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
        ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
        DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
        POSSIBILITY OF SUCH DAMAGE.

        THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
        INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
        IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
        NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
        MODIFICATIONS.
---------------------------------------------------------------------------*/
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include "fortify.h"
#include "splitter.h"
#include "fupa.h"

int EvaluateFuPa(pFuncTemplate pTemplate, int iFunc, int argc,
                 char *argv[], FuPaResult * pRes)
{
  TokenList *pList = NULL;
  TokenList *pCurrent;
  int i;
  int iRet = -1;

  pList = SplitArguments(argc, argv);
  if (!pList) {
    strcpy(pRes->pError, "ERROR: cannot parse argument list");
    return iRet;
  }

  /* first one must be name, try locate that one first */
  pCurrent = pList;
  for (i = 0; i < iFunc; i++) {
    if (strcmp(pCurrent->text, pTemplate[i].name) == 0) {
      iRet = i;
      break;
    }
  }
  if (iRet == -1) {
    snprintf(pRes->pError,sizeof(pRes->pError)-1, "ERROR: function %s not available",
            pCurrent->text);
    goto end;
  }

  /* now try to find arguments */
  pRes->iArgs = pTemplate[iRet].iArgs;
  for (i = 0; i < pRes->iArgs; i++) {
    pCurrent = pCurrent->pNext;
    /* check presence */
    if (!pCurrent) {
      if (pTemplate[iRet].pArgs[i] == FUPAOPT) {
        pRes->Arg[i].iVal = 0;
        goto end;
      } else {
        snprintf(pRes->pError,sizeof(pRes->pError)-1,
                "ERROR: Insufficient number of arguments to %s",
                pTemplate[iRet].name);
        iRet = -1;
        goto end;
      }
    }
    /* check type, optional first */
    if (pTemplate[iRet].pArgs[i] == FUPAOPT) {
      pRes->Arg[i].iVal = 1;
      strcpy(pRes->Arg[i].text, pCurrent->text);
      if (pCurrent->Type == eInt) {
        pRes->Arg[i].fVal = pCurrent->iVal;
      } else if (pCurrent->Type == eFloat) {
        pRes->Arg[i].fVal = pCurrent->fVal;
      }
      goto end;
    }

    /* now text */
    if (pTemplate[iRet].pArgs[i] == FUPATEXT) { /* text is simple */
      strcpy(pRes->Arg[i].text, pCurrent->text);
      continue;
    }

    /* check integer */
    if (pTemplate[iRet].pArgs[i] == FUPAINT) {
      if (pCurrent->Type == eInt) {
        pRes->Arg[i].iVal = pCurrent->iVal;
        continue;
      } else if (pCurrent->Type == eFloat) {
        pRes->Arg[i].iVal = (int) pCurrent->fVal;
        continue;
      } else {
        snprintf(pRes->pError,sizeof(pRes->pError)-1, "ERROR: expected integer parameter, got %s",
                pCurrent->text);
        iRet = -1;
        goto end;
      }
    }

    /* check float */
    if (pTemplate[iRet].pArgs[i] == FUPAFLOAT) {
      if (pCurrent->Type == eInt) {
        pRes->Arg[i].fVal = (float) pCurrent->iVal;
        continue;
      } else if (pCurrent->Type == eFloat) {
        pRes->Arg[i].fVal = pCurrent->fVal;
        continue;
      } else {
        snprintf(pRes->pError,sizeof(pRes->pError)-1, "ERROR: expected float parameter, got %s",
                pCurrent->text);
        iRet = -1;
        goto end;
      }
    }
  }
end:
  DeleteTokenList(pList);
  return iRet;
}
