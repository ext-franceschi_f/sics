/*---------------------------------------------------------------------------

			F U N C T I O N P A R S E
			
   A utility module to take some of the pain from parsing input while
   writing text wrapper function for SICS.
   
   The idea is that you specify an array of function templates.
   These function templates will contain function names and argument
   information. This module will than parse the input against this
   template. It will return -1 if an error occurs and provide error 
   information.
   
   On success  the index of the function found  will be returned. 
   Arguments will have been updated with their proper values.

   Invoking functions ist still your job, probably in a switch statement
   follwoing the call to the evaluation function.
   
   Mark Koennecke, January 1996
   
   copyright: see implementation file.
-----------------------------------------------------------------------------*/
#ifndef FUNCPARSE
#define FUNCPARSE

#define MAXARG 20
#define FUPATEXT  0
#define FUPAINT   1
#define FUPAFLOAT 2
#define FUPAOPT   3             /* optional argument, in this case text contains it
                                   and iVal indicates its presence 
                                 */

typedef struct {
  char *name;
  int iArgs;
  int pArgs[MAXARG];
} FuncTemplate, *pFuncTemplate;

typedef struct {
  char text[80];
  int iVal;
  float fVal;
} FuPaArg;

typedef struct {
  char pError[132];
  int iArgs;
  FuPaArg Arg[MAXARG];
} FuPaResult;

int EvaluateFuPa(pFuncTemplate pTemplate, int iFunc, int argc,
                 char *argv[], FuPaResult * pRes);


#endif
