/**
 * Promises and Futures
 *
 * Douglas Clowes (dcl@ansto.gov.au) April 2018
 *
 */
#include "future.h"
/*
 * Promises and Futures
 */
typedef enum {
  eIdle = 0,    /**< Newly created, not yet started */
  eRunning,     /**< Being worked on (from Idle) */
  eDone,        /**< Result or Exception (from Running) */
  eCancelled    /**< Cancel before execution (from Idle) */
} State;

#define MAX_CB 10

typedef struct __PromiseFuture {
  State state;
  bool lock;        /**< Prevent a callback deleting the object */
  int refCount;     /**< One for Promise and each Future */
  void *context;    /**< User context: set on Promise, shared on Future */
  void *result;     /**< Positive result from Promise to Future */
  void *exception;  /**< Negative result from Promise to Future */
  pTaskMan tasker;  /**< Tasker to use on Yields */
  void (*dtor)(pTaskPromise);
  int cbCount;
  void (*cb[MAX_CB])(pTaskFuture);
} PromiseFuture, *pPromiseFuture;

/*-------------------------------------------------------------------------------*/
/*
 * Loop through performing callbacks in order registered
 */
static void perform_callbacks(pPromiseFuture self);

static void PromiseFutureRelease(pPromiseFuture self);

static void PromiseFutureWait(pPromiseFuture self, double timeout);

/*-------------------------------------------------------------------------------*/
pTaskPromise PromiseCreate(pTaskMan tasker)
{
  pPromiseFuture self = calloc(1, sizeof(PromiseFuture));
  if (self) {
    self->refCount = 1;
    self->tasker = tasker;
  }
  return (pTaskPromise) self;
}

/*-------------------------------------------------------------------------------*/
void PromiseSetContext(pTaskPromise data, void *context, void(*cb)(pTaskPromise))
{
  pPromiseFuture self = (pPromiseFuture) data;
  self->context = context;
  self->dtor = cb;
}

/*-------------------------------------------------------------------------------*/
void *PromiseGetContext(pTaskPromise data)
{
  pPromiseFuture self = (pPromiseFuture) data;
  return self->context;
}

/*-------------------------------------------------------------------------------*/
bool PromiseRelease(pTaskPromise data)
{
  pPromiseFuture self = (pPromiseFuture) data;
  if (--self->refCount <= 0) {
    PromiseFutureRelease(self);
    return true;
  }
  return false;
}

/*-------------------------------------------------------------------------------*/
bool PromiseSetRunning(pTaskPromise data)
{
  pPromiseFuture self = (pPromiseFuture) data;
  if (self->state != eIdle)
    return false;
  self->state = eRunning;
  perform_callbacks(self);
  return true;
}

/*-------------------------------------------------------------------------------*/
bool PromiseSetResult(pTaskPromise data, void *rslt)
{
  pPromiseFuture self = (pPromiseFuture) data;
  if (self->state != eRunning)
    return false;
  self->state = eDone;
  self->result = rslt;
  perform_callbacks(self);
  return true;
}

/*-------------------------------------------------------------------------------*/
bool PromiseSetException(pTaskPromise data, void *expn)
{
  pPromiseFuture self = (pPromiseFuture) data;
  if (self->state != eRunning)
    return false;
  self->state = eDone;
  self->exception = expn;
  perform_callbacks(self);
  return true;
}

/*-------------------------------------------------------------------------------*/
pTaskFuture FutureFromPromise(pTaskPromise data)
{
  pPromiseFuture self = (pPromiseFuture) data;
  if (self) {
    self->refCount += 1;
  }
  return (pTaskFuture) self;
}

/*-------------------------------------------------------------------------------*/
pTaskFuture FutureFromFuture(pTaskFuture data)
{
  pPromiseFuture self = (pPromiseFuture) data;
  if (self) {
    /* increment the reference count for new future */
    self->refCount += 1;
  }
  return (pTaskFuture) self;
}

/*-------------------------------------------------------------------------------*/
bool FutureRelease(pTaskFuture data)
{
  pPromiseFuture self = (pPromiseFuture) data;
  if (--self->refCount <= 0) {
    PromiseFutureRelease(self);
    return true;
  }
  return false;
}

/*-------------------------------------------------------------------------------*/
bool FutureCancel(pTaskFuture data)
{
  pPromiseFuture self = (pPromiseFuture) data;
  if (self->state != eIdle)
    return false;
  self->state = eCancelled;
  perform_callbacks(self);
  return true;
}

/*-------------------------------------------------------------------------------*/
bool FutureIsCancelled(pTaskFuture data)
{
  pPromiseFuture self = (pPromiseFuture) data;
  return self->state == eCancelled;
}

/*-------------------------------------------------------------------------------*/
bool FutureIsRunning(pTaskFuture data)
{
  pPromiseFuture self = (pPromiseFuture) data;
  return self->state == eRunning;
}

/*-------------------------------------------------------------------------------*/
bool FutureIsDone(pTaskFuture data)
{
  pPromiseFuture self = (pPromiseFuture) data;
  return self->state == eDone;
}

/*-------------------------------------------------------------------------------*/
bool FutureWait(pTaskFuture data, double timeout)
{
  pPromiseFuture self = (pPromiseFuture) data;
  if (timeout < 0) {
    while (self->state != eDone && self->state != eCancelled)
      TaskYield(self->tasker);
    return true;
  } else {
    double DoubleTime(void);
    double now = DoubleTime();
    double then = now + timeout;
    while (now < then && self->state != eDone && self->state != eCancelled) {
      TaskYield(self->tasker);
      now = DoubleTime();
    }
  }
  return self->state == eDone || self->state == eCancelled;
}

/*-------------------------------------------------------------------------------*/
void *FutureGetResult(pTaskFuture data, double timeout)
{
  pPromiseFuture self = (pPromiseFuture) data;
  FutureWait(data, timeout);
  return self->result;
}

/*-------------------------------------------------------------------------------*/
void *FutureGetException(pTaskFuture data, double timeout)
{
  pPromiseFuture self = (pPromiseFuture) data;
  FutureWait(data, timeout);
  return self->exception;
}

/*-------------------------------------------------------------------------------*/
void *FutureGetContext(pTaskFuture data)
{
  pPromiseFuture self = (pPromiseFuture) data;
  return self->context;
}

/*-------------------------------------------------------------------------------*/
bool FutureAddCallback(pTaskFuture data, void (*cb)(pTaskFuture))
{
  pPromiseFuture self = (pPromiseFuture) data;
  if (self->state == eDone || self->state == eCancelled) {
    /* Prevent the callback deleting this out from under us */
    self->lock = true;
    cb(data);
    self->lock = false;
    /* If it tried, do it now */
    if (self->refCount <= 0)
      PromiseFutureRelease(self);
    return true;
  }
  if (self->cbCount >= MAX_CB)
    return false;
  self->cb[self->cbCount++] = cb;
  return true;
}

static void perform_callbacks(pPromiseFuture self)
{
  int i;
  /* Prevent the callback deleting this out from under us */
  self->lock = true;
  for (i = 0; i < self->cbCount; ++i) {
    self->cb[i]((pTaskFuture) self);
  }
  self->lock = false;
  /* If it tried, do it now */
  if (self->refCount <= 0)
    PromiseFutureRelease(self);
}

static void PromiseFutureRelease(pPromiseFuture self)
{
  if(self && self->lock == false) {
    if (self->dtor)
      self->dtor((pTaskPromise) self);
    free(self);
  }
}
