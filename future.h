/*--------------------------------------------------------------------------*/
/**
 * Promises and Futures
 *
 * Douglas Clowes (dcl@ansto.gov.au) April 2018
 *
 * Based on the concepts in wikipedia:
 *    https://en.wikipedia.org/wiki/Futures_and_promises
 * and in Python 3 concurrent.futures:
 *    https://docs.python.org/3/library/concurrent.futures.html#future-objects
 * and in C++ std::promise and std::future.
 *
 * Create a promise and derive a future from it. Pass the promise to the
 * executor and return the future to the caller.
 *
 * The executor calls PromiseSetRunning before commencing the work and if it
 * returns false the request was cancelled otherwise the work may be performed.
 * The executor calls PromiseSetException or PromiseSetResult to complete the
 * work and notify a result.
 *
 * The caller may test the status of the future, wait for a result or
 * register callbacks that are called when work starts and finishes.
 *
 * Both the promise and the future point to the same shared data which is
 * reference counted. When all promises and futures are released, the shared
 * data is destroyed.
 */
#ifndef FUTURE_H
#define FUTURE_H

#include "task.h"
#include <stdbool.h>

/**
 * Typedefs for the Promise and Future for type-safety
 *
 * There is only one real object and it is reference counted
 */
typedef struct __TaskPromise *pTaskPromise;
typedef struct __TaskFuture *pTaskFuture;

/**
 * Create a promise to be fulfilled at a later time
 *
 * The tasker will be used to wait for a result by calling TaskYield.
 *
 * @param tasker this tasker instance
 * @return pointer to the promise object or NULL on failure
 */
pTaskPromise PromiseCreate(pTaskMan tasker);

/**
 * Set a shared context and an optional destructor
 *
 * @param context anything you like
 * @param dtor an optional destructor called on last release
 */
void PromiseSetContext(pTaskPromise self, void *context, void(*cb)(pTaskPromise));

/**
 * Get the shared context for the promise user
 *
 */
void *PromiseGetContext(pTaskPromise self);

/**
 * Release a promise when fulfilled
 */
bool PromiseRelease(pTaskPromise self);

/**
 * Set the state running
 *
 * The executer calls this at the start of processing and if it
 * returns false, abandons the execution. Returns false if
 * not currently idle (started/cancelled) already.
 *
 * @return False if not idle
 */
bool PromiseSetRunning(pTaskPromise self);

/**
 * Finish with Success
 *
 * @param rslt whatever is agreed with the Future user
 * @return true on success
 */
bool PromiseSetResult(pTaskPromise self, void *rslt);

/**
 * Finish with Failure
 *
 * @param expn whatever is agreed with the Future user
 * @return true on success
 */
bool PromiseSetException(pTaskPromise self, void *expn);

/**
 * Create a Future associated with a Promise
 */
pTaskFuture FutureFromPromise(pTaskPromise self);

/**
 * Duplicate a Future
 */
pTaskFuture FutureFromFuture(pTaskFuture self);

/**
 * Release (free) a future
 */
bool FutureRelease(pTaskFuture self);

/**
 * Cancel an operation
 *
 * Only before execution started
 */
bool FutureCancel(pTaskFuture self);

/**
 * Return True if cancelled
 */
bool FutureIsCancelled(pTaskFuture self);

/**
 * Return True if running
 */
bool FutureIsRunning(pTaskFuture self);

/**
 * Return true if finished
 */
bool FutureIsDone(pTaskFuture self);

/**
 * Get the result if successful
 */
void *FutureGetResult(pTaskFuture self, double timeout);

/**
 * Wait for completion
 *
 * @param timeout in seconds, infinite if negative
 * @return true on complete, false on timeout
 */
bool FutureWait(pTaskFuture self, double timeout);

/**
 * Get the exception if failed
 */
void *FutureGetException(pTaskFuture self, double timeout);

/**
 * Get the shared context the promise was created with
 */
void *FutureGetContext(pTaskFuture self);

/**
 * Add a callback for state change
 */
bool FutureAddCallback(pTaskFuture self, void (*cb)(pTaskFuture));
#endif
