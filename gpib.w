\subsection{GPIB Controller}
GPIB (or IEEE-488) is a 8 bit bus system. The system contains a
controller and up to 30 devices. GPIB will be used at PSI in the Risoe
instruments. And possibly others. The main stay GPIB controller will
be the National Instruments ENET-100 GPIB/TCPIP bridge. But several
drivers will be needed:
\begin{itemize}
\item A simulation
\item The original NI driver
\item Possibly a direct board level NI driver (not for ENET, but PCI)
\item Possibly a reverse engineered TCP/IP only driver.
\end{itemize} 
For this reason the controller is made in a polymorphic fashion. 

This requires the following data structure:
@d gpibdat @{
	struct __GPIB {
			pObjectDescriptor pDes; 
			int (*attach)(int boardNo, int address,
			             int secondaryAddress,
				     int tmo, int eot, int eos);
		        int (*detach)(int devID);
			int (*send)(int devID, void *buffer, 
                                    int count);
			int (*read)(int devID, void *buffer, 
                                    int count);
	                int (*clear)(int devID);
			void (*getErrorDescription)(int code,
						   char *buffer, int maxCount);
		}GPIB;
@}

The interface for this controller:
@d gpibint @{
	typedef struct __GPIB *pGPIB;

	int GPIBattach(pGPIB self, int boardNo, 
		        int address, int secondaryAddress,
			int tmo, int eot, int eos);
	int GPIBdetach(pGPIB self, int devID);
	int GPIBsend(pGPIB self, int devID, void *buffer, int bytesToWrite);
	int GPIBread(pGPIB self, int devID, void *buffer, int bytesToRead);
	char *GPIBreadTillTerm(pGPIB self, int devID, int terminator);
	void GPIBclear(pGPIB self, int devID);
        void GPIBerrorDescription(pGPIB self, int code, 
	             char *buffer, int maxBuffer); 
@}

All functions return a negative value on failure. All functions takes
as their first argument a pointer to a GPIB structure. 
\begin{description}
\item[GPIBattach] initialises the connection to a specific device on
the GPIB bus. The parameters:
\begin{description}
\item[boardNo] The NI board number
\item[address] The address of the device on the bus.
\item[secondaryAddress] The secondary address of th device.
\item[tmo] The timeout for this device.
\item[eot] termination thing I do not understand.
\item[eos] another termination thing.
\end{description}
This call retuns an integer devID which has to be used in calls to the
other functions.
\item[GPIBdetach] closes the connection to the device devID.
\item[GPIBsend] sends bytesToWrite bytes from buffer to the device
identified through devID.
\item[GPIBread] reads bytesToRead bytes from the device
identified through devID into buffer. buffer has to be large enough to
accomodate bytesToRead bytes.
\item[GPIBclear] tries to clear the GPIB device.
\item[GPIBerrorDescription] copies at max MaxBuffer bytes or error
description for code into buffer. 
\end{description} 


There is also an interpreter interface to the GPIB controller:

@d gpibsics @{
	int MakeGPIB(SConnection *pCon, SicsInterp *pSics,
		     void *pData,
	             int argc, char *argv[]);
	int GPIBAction(SConnection *pCon, SicsInterp *pSics,
		     void *pData,
	             int argc, char *argv[]);
 
@}

@o gpibcontroller.h @{
/*----------------------------------------------------------------------
   Interface to a GPIB controller.

   copyright: see file  COPYRIGHT

   Mark Koennecke, January 2003
------------------------------------------------------------------------*/
#ifndef GPIBCONTROLLER
#define GPIBCONTROLLER

/*
	error codes
*/
#define GPIBARG  4
#define GPIBEDVR 0
#define GPIBENEB 7
#define GPIBEABO 6
#define GPIBEADR 3
#define GPIBEBUS 14
#define GPIBENOL 2

@<gpibint@>

@<gpibsics@>

#endif
@}

@o gpibcontroller.i @{
/*-----------------------------------------------------------------------
  GPIBcontroller structure. Automatically generated file, modify in
  gpib.w and run nuweb.
-----------------------------------------------------------------------*/

@<gpibdat@>
@}