
/*----------------------------------------------------------------------
   Interface to a GPIB controller.

   copyright: see file  COPYRIGHT

   Mark Koennecke, January 2003
------------------------------------------------------------------------*/
#ifndef GPIBCONTROLLER
#define GPIBCONTROLLER

/*
        error codes
*/
#define GPIBARG  4
#define GPIBEDVR 0
#define GPIBENEB 7
#define GPIBEABO 6
#define GPIBEADR 3
#define GPIBEBUS 14
#define GPIBENOL 2


typedef struct __GPIB *pGPIB;

int GPIBattach(pGPIB self, int boardNo,
               int address, int secondaryAddress,
               int tmo, int eot, int eos);
int GPIBdetach(pGPIB self, int devID);
int GPIBsend(pGPIB self, int devID, void *buffer, int bytesToWrite);
int GPIBread(pGPIB self, int devID, void *buffer, int bytesToRead);
char *GPIBreadTillTerm(pGPIB self, int devID, int terminator);
void GPIBclear(pGPIB self, int devID);
void GPIBerrorDescription(pGPIB self, int code,
                          char *buffer, int maxBuffer);



int MakeGPIB(SConnection * pCon, SicsInterp * pSics,
             void *pData, int argc, char *argv[]);
int GPIBAction(SConnection * pCon, SicsInterp * pSics,
               void *pData, int argc, char *argv[]);



#endif
