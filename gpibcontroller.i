
/*-----------------------------------------------------------------------
  GPIBcontroller structure. Automatically generated file, modify in
  gpib.w and run nuweb.
-----------------------------------------------------------------------*/


        struct __GPIB {
                        pObjectDescriptor pDes; 
                        int (*attach)(int boardNo, int address,
                                     int secondaryAddress,
                                     int tmo, int eot, int eos);
                        int (*detach)(int devID);
                        int (*send)(int devID, void *buffer, 
                                    int count);
                        int (*read)(int devID, void *buffer, 
                                    int count);
                        int (*clear)(int devID);
                        void (*getErrorDescription)(int code,
                                                   char *buffer, int maxCount);
                }GPIB;

