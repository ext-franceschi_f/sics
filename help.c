/*-----------------------------------------------------------------------
  Implementation file for the SICS help system.

  copyright: see file COPYRIGHT

  Mark Koennecke, December 2003
-----------------------------------------------------------------------*/
#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include "fortify.h"
#include "sics.h"
#include "help.h"

extern char *stptok(const char *s, char *tok, size_t toklen, char *brk);

/*----------------------------------------------------------------------*/
#define PATHSEP ":"
#define DIRSEP "/"
static char *helpDirs = NULL;
static char *defaultFile = "master.txt";
/*----------------------------------------------------------------------*/
void KillHelp(void *pData)
{
  KillDummy(pData);
  if (helpDirs != NULL) {
    free(helpDirs);
    helpDirs = NULL;
  }
}

/*-----------------------------------------------------------------------*/
static FILE *findHelpFile(char *name)
{
  FILE *fd = NULL;
  char pBueffel[256];
  char dir[132];
  char *pPtr;

  if (helpDirs == NULL) {
    return NULL;
  }

  pPtr = helpDirs;
  while ((pPtr = stptok(pPtr, dir, 131, PATHSEP)) != NULL) {
    strlcpy(pBueffel, dir, sizeof pBueffel);
    strlcat(pBueffel, DIRSEP, sizeof pBueffel);
    strlcat(pBueffel, name, sizeof pBueffel);
    fd = fopen(pBueffel, "r");
    if (fd != NULL) {
      return fd;
    }
  }

  /*
     this means: not found!
   */
  return NULL;
}

/*----------------------------------------------------------------------*/
static void printHelpFile(SConnection * pCon, FILE * fd)
{
  char line[132];

  while (fgets(line, 131, fd) != NULL) {
    SCWrite(pCon, line, eValue);
  }
}

/*----------------------------------------------------------------------*/
static void configureHelp(SConnection * pCon,
                          char *option, char *parameter)
{
  char *pPtr = NULL;
  int length;

  strtolower(option);
  if (strcmp(option, "adddir") == 0) {
    if (parameter == NULL) {
      SCWrite(pCon, helpDirs, eValue);
      return;
    } else {
      pPtr = helpDirs;
      if (pPtr != NULL) {
        length = strlen(pPtr) + strlen(PATHSEP) + strlen(parameter) + 2;
        helpDirs = (char *) malloc(length * sizeof(char));
        memset(helpDirs, 0, length * sizeof(char));
        strcpy(helpDirs, pPtr);
        strcat(helpDirs, PATHSEP);
        strcat(helpDirs, parameter);
        free(pPtr);
      } else {
        helpDirs = strdup(parameter);
      }
    }
  } else if (strcmp(option, "defaultfile") == 0) {
    if (parameter == NULL) {
      SCWrite(pCon, defaultFile, eValue);
      return;
    } else {
      if (defaultFile != NULL) {
        free(defaultFile);
      }
      defaultFile = strdup(parameter);
    }
  } else {
    SCWrite(pCon, "Unknown option to configure", eWarning);
    SCWrite(pCon, "Known options: defaultfile, adddir", eWarning);
  }
}

/*-----------------------------------------------------------------------*/
int SicsHelp(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[])
{
  char helpFile[256];
  FILE *fd = NULL;

  strlcpy(helpFile, defaultFile, 255);

  if (argc > 1) {
    strtolower(argv[1]);
    /*
       check for configure
     */
    if (strcmp(argv[1], "configure") == 0) {
      if (argc < 3) {
        SCWrite(pCon, "ERROR: need an option to configure", eError);
        return 0;
      }
      if (argc > 3) {
        configureHelp(pCon, argv[2], argv[3]);
      } else {
        configureHelp(pCon, argv[2], NULL);
      }
      SCSendOK(pCon);
      return 1;
    }
    /*
       the parameter is a help file name
     */
    strlcpy(helpFile, argv[1], 255);
    strlcat(helpFile, ".txt", 255);
  }

  /*
     print the helpFile
   */
  fd = findHelpFile(helpFile);
  if (fd == NULL) {
    SCWrite(pCon, "ERROR: failed to locate helpFile:", eError);
    SCWrite(pCon, helpFile, eError);
    return 0;
  }
  printHelpFile(pCon, fd);
  fclose(fd);
  return 1;
}
/*-----------------------------------------------------------------------*/
void HelpInit(void)
{
  AddCommand(pServ->pSics, "help", SicsHelp, KillHelp, NULL);
}
