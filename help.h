
/*-----------------------------------------------------------------------
  Header file for the SICS help system.

  copyright: see file COPYRIGHT

  Mark Koennecke, December 2003
-----------------------------------------------------------------------*/
#ifndef SICSHELP
#define SICSHELP

int SicsHelp(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[]);

void KillHelp(void *pData);
#endif
