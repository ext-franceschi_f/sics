\subsection{The SICS Help System}
SICS has a very simple help system. It is based on plain text files
containing the help texts. There is a default help file which is
printed when help is called without arguments. Other help files are
printed by the command: help name. The name of the text file is then
name.txt. This file is earched in a configurable small set of
directories. This allows to separate general SICS help files and
instrument specific help files.

This system is implemented as a module defined in help.c. The
interface is simply the interpreter function implementing the help
command. There is also a function for removing the data associated
with help.

@o help.h @{
/*-----------------------------------------------------------------------
  Header file for the SICS help system.

  copyright: see file COPYRIGHT

  Mark Koennecke, December 2003
-----------------------------------------------------------------------*/
#ifndef SICSHELP
#define SICSHELP

	int SicsHelp(SConnection *pCon, SicsInterp *pSics, void *pData,
		int argc, char *argv[]);

	void KillHelp(void *pData);
#endif

@}  





