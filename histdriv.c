/*---------------------------------------------------------------------------
				H I S T D R I V
				
  This file contains the definitions of a few functions which are general
  to all Histogram memory drivers.
  
  Mark Koennecke, April 1997
  
       Copyright:

       Labor fuer Neutronenstreuung
       Paul Scherrer Institut
       CH-5423 Villigen-PSI


      The authors hereby grant permission to use, copy, modify, distribute,
      and license this software and its documentation for any purpose, provided
      that existing copyright notices are retained in all copies and that this
      notice is included verbatim in any distributions. No written agreement,
      license, or royalty fee is required for any of the authorized uses.
      Modifications to this software may be copyrighted by their authors
      and need not follow the licensing terms described here, provided that
      the new terms are clearly indicated on the first page of each file where
      they apply.

      IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
      FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
      ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
      DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
      POSSIBILITY OF SUCH DAMAGE.

      THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
      FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
      IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
      NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
      MODIFICATIONS.
----------------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "fortify.h"
#include "sics.h"
#include "countdriv.h"
#include "counter.h"
#include "splitter.h"
#include "stringdict.h"
#include "HistMem.h"
#include "HistDriv.i"

/*--------------------------------------------------------------------------*/
pHistDriver CreateHistDriver(pStringDict pOption)
{
  pHistDriver pNew = NULL;
  char pDim[20];
  int i;

  /* allocate some more memory */
  pNew = (pHistDriver) malloc(sizeof(HistDriver));
  if (!pNew) {
    return NULL;
  }
  memset(pNew, 0, sizeof(HistDriver));

  pNew->data = makeHMData();
  if (!pNew->data) {
    free(pNew);
    return NULL;
  }

  /* initialise defaults */
  StringDictAddPair(pOption, "rank", "1");
  for (i = 0; i < MAXDIM; i++) {
    snprintf(pDim,sizeof(pDim)-1, "dim%1.1d", i);
    StringDictAddPair(pOption, pDim, "-126");
  }
  pNew->fCountPreset = 10.;
  pNew->eCount = eTimer;
  pNew->iReconfig = 1;
  pNew->iUpdate = 0;

  return pNew;
}

/*-------------------------------------------------------------------------*/
void DeleteHistDriver(pHistDriver self)
{
  assert(self);

  if (self->FreePrivate) {
    self->FreePrivate(self);
  }
  if (self->pOption) {
    DeleteStringDict(self->pOption);
  }
  if (self->data) {
    killHMData(self->data);
  }
  free(self);
}

/*------------------------------------------------------------------------*/
int HistDriverConfig(pHistDriver self, pStringDict pOpt,
                     SConnection * pCon)
{
  int iRet;
  char pBueffel[512];
  char pValue[80];
  float fVal;
  TokenList *pList = NULL, *pCurrent;
  char *pBuf = NULL;
  int i;

  assert(self);
  assert(pOpt);
  assert(pCon);

  return configureHMdata(self->data, pOpt, pCon);
}
