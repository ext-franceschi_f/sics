
/*---------------------------------------------------------------------------
                              H K L
 This SICS object performs angle setting calculations for a four circle 
 diffractometer. The heart of this code is a C transcriptions of routines
 written by Jean Allibon at ILL for the MAD program. Theory is explained in
 the article by W. C. Hamilton in International Tables for Crystallography,
 1974 edition.

 copyright: see copyright.h

 Mark Koennecke, February 1998

----------------------------------------------------------------------------*/
#ifndef SICSHKL
#define SICSHKL
#include "selector.h"
#include "selvar.h"

   typedef struct __HKL *pHKL;
/*-------------------------------------------------------------------------*/
   pHKL CreateHKL();
   void DeleteHKL(void *pData);
 
   int HKLFactory(SConnection *pCon, SicsInterp *pSics, void *pData,
                  int argc, char *argv[]);
/*------------------------------------------------------------------------*/
  void SetHKLScanTolerance(pHKL self, float value);
  int SetUB(pHKL self, float fUB[9]);
  int GetUB(pHKL self, float fUB[9]);
  int SetNOR(pHKL self, int iNOB);
  int GetLambda(pHKL self, float *fVal);
  int GetCurrentHKL(pHKL self, float fVal[3]);
  int GetCurrentPosition(pHKL self, SConnection *pCon, float fPosition[4]);
  int GetHKLFromAngles(pHKL self, SConnection *pCon, float fVal[3]);    

  int CalculateSettings(pHKL self, float fHKL[3], float fPsi, int iHamil,
                        float fSet[4],SConnection *pCon);
  int RunHKL(pHKL self, float fHKL[3], float fPsi, int iHamil, SConnection
             *pCon);
  int DriveHKL(pHKL self, float fHKL[3], float fPsi, int iHamil,
               SConnection *pCon);

  int DriveSettings(pHKL self, float fSet[4],SConnection *pCon);

  int HKLAction(SConnection *pCon, SicsInterp *pSics, void *pData,
                int argc, char *argv[]);  
  int hklInRange(void *data, double fSet[4], int mask[4]);
  int startHKLMotors(pHKL self, SConnection *pCon, float fSet[4]);
  void stopHKLMotors(pHKL self);

#endif  
