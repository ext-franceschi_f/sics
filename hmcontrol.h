
/*------------------------------------------------------------------------
                        H M C O N T R O L

 A module for coordinating several counters and histogram
 memories. One of the counters is the master counter and the rest are
 slaves which have to be kept in sync with the master in their
 operations.

 copyright: see copyright.h
 
 Mark Koennecke, June 2001
-------------------------------------------------------------------------*/
#ifndef HMCONTROL
#define HMCONTROL

/* 
the maximum number of slaves
*/
#include "sics.h"
#include "counter.h"

#define MAXSLAVE 5


typedef struct {
  pObjectDescriptor pDes;
  pICountable pCount;
  pICountable slaves[MAXSLAVE];
  void *slaveData[MAXSLAVE];
  int nSlaves;
  float fPreset;
  CounterMode eMode;
  pICallBack pCall;
  int checkSlaves;
  int stopSlaves;
  time_t counterStop;
} HMcontrol, *pHMcontrol;



int MakeHMControl(SConnection * pCon, SicsInterp * pSics,
                  void *pData, int argc, char *argv[]);
int HMControlAction(SConnection * pCon, SicsInterp * pSics,
                    void *pData, int argc, char *argv[]);


#endif
