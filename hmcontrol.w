\subsection{HM control}
At TRICS (and possibly at other instruments) it it necessary to coordinate the 
operation of the counter and one to many histogram memories. The
neutron counter will act as the actual controlling instance and the
histogram memories will act as slaves. This is implemented in this
module. 

The modules data structure:
@d hmcontroldata @{
	typedef struct {
			pObjectDescriptor pDes;
			pICountable pCount;
			pICountable slaves[MAXSLAVE];
			void *slaveData[MAXSLAVE];
                        int nSlaves;
			float fPreset;
			CounterMode eMode;
                        pICallBack pCall;
			} HMcontrol, *pHMcontrol;
@}
The fields are:
\begin{description}
\item[pDes] The standard SICS object descriptor.
\item[pCount] The countable interface of this module.
\item[slaves] The slave counters or histogram memories.
\item[slaveData] is the data structure for the slave. The first slave
is always the neutron counter. 
\item[nSlaves] The number of active slaves.
\item[fPreset] The counting preset.
\item[eMode] The counting mode.  
\item[pCall] The callback interface.
\end{description}


Most of this modules functionality is built into the countable
interface functions. Just an interface to the interpreter is needed:

@d hmcontrolint @{
	int MakeHMControl(SConnection *pCon, SicsInterp *pSics, 
                          void *pData, int argc, char *argv[]);
	int HMControlAction(SConnection *pCon, SicsInterp *pSics, 
                          void *pData, int argc, char *argv[]);

@}

@o hmcontrol.h @{
/*------------------------------------------------------------------------
                        H M C O N T R O L

 A module for coordinating several counters and histogram
 memories. One of the counters is the master counter and the rest are
 slaves which have to be kept in sync with the master in their
 operations.

 copyright: see copyright.h
 
 Mark Koennecke, June 2001
-------------------------------------------------------------------------*/
#ifndef HMCONTROL
#define HMCONTROL

/* 
the maximum number of slaves
*/
#include "sics.h"
#include "counter.h"

#define MAXSLAVE 5

@<hmcontroldata@>

@<hmcontrolint@>
#endif

@}
