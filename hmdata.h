
/*-----------------------------------------------------------------------
  This is a data handling class for histogram memory data.
  For more information see hmdata.tex.

  copyright: see file COPYRIGHT

  Mark Koennecke, January 2003
-------------------------------------------------------------------------*/
#ifndef SICSHMDATA
#define SICSHMDATA
#include "sics.h"
#include "HistMem.h"
#include "stringdict.h"
#define MAXCHAN 16834
#define MAXDIM 3


typedef struct __hmdata {
  int rank;
  int iDim[MAXDIM];
  int nTimeChan;
  float timeBinning[MAXCHAN];
  int tofMode;
  time_t nextUpdate;
  int updateIntervall;
  HistInt *localBuffer;
  struct __hmdata *timeslave;
} HMdata, *pHMdata;



pHMdata makeHMData(void);
void killHMData(pHMdata self);
void clearHMData(pHMdata self);

int configureHMdata(pHMdata self, pStringDict pOpt, SConnection * pCon);
int resizeBuffer(pHMdata self);
int genTimeBinning(pHMdata self, float start, float step, int noSteps);
int setTimeBin(pHMdata self, int index, float value);

int getNoOfTimebins(pHMdata self);
float *getTimeBinning(pHMdata self);
int isInTOFMode(pHMdata self);
void clearTimeBinning(pHMdata self);

void getHMDataDim(pHMdata self, int iDIM[MAXDIM], int *rank);
long getHMDataLength(pHMdata self);

int getHMDataHistogram(pHistMem hist, SConnection * pCon,
                       int bank, int start, int length, HistInt * lData);
void updateHMData(pHMdata self);
HistInt *getHMDataBufferPointer(pHistMem hist, SConnection * pCon);

long sumHMDataRectangle(pHistMem self, SConnection * pCon,
                        int start[MAXDIM], int end[MAXDIM]);
int loadHMData(pHMdata self, SConnection * pCon, char *filename);

HistInt *subSample(pHMdata self, char *command, char *error, int errLen);

#endif
