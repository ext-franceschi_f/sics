#ifndef HNDBAG_H
#define HNDBAG_H
/*
 * This declares a generic container object.
 * The container itsef is reference counter with retain and release operations.
 * Entries are handles and stale handles are caught and ignored.
 * Entries are abstract references to objects.
 * Entries are reference counted.
 * Entries may be retained to increment the reference count.
 * Entries may be released to decrement the reference count.
 * Entries within the container are unordered.
 * Entries may be visited in (un)ordered sequence.
 * An array of (un)ordered entry pointers can be obtained.
 * A destructor function can be called when an object is no longer referenced.
 *
 * Douglas Clowes, March 2016
 */
#include <stdint.h>
#include <stdbool.h>

/*
 * Make this an opaque abstract data type
 */
typedef uint64_t HANDLE_T;
typedef struct _hndbag_array_t *pHNDBAG;
typedef int (*HNDBAG_COMPARE)(const void *ptr, const void *ctx);
typedef int (*HNDBAG_VISITOR)(void *ptr, void *ctx);

/**
 * Constructor - create a new container
 * \param cp_out the returned pointer to the new container or NULL
 * \param min_size - the initial reservation
 * \param max_size - the expansion limit
 * \return zero on success, -1 on failure
 */
int hndbag_create(pHNDBAG *cp_out, int min_size, int max_size);

/**
 * Increase the reference count for the container
 */
int hndbag_retain(pHNDBAG cp_in);

/**
 * Release a reference to the container
 * The pointer will be set to NULL on return.
 */
int hndbag_release(pHNDBAG *cp_io);

/**
 * Get the currently unique index part of tha handle
 * \param cp_in pointer to the container
 * \param hnd_in handle to be extracted
 * \return index portion of the handle
 */
uint64_t hndbag_get_index(pHNDBAG cp_in, HANDLE_T hnd_in);

/**
 * Set a destructor function for the item pointed to (optional)
 * \param cp_in pointer to the container
 * \param free_func function to free/destroy/destruct a pointed-to-object
 *
 * This sets a function to be used to release an object when the reference
 * count for the item goes to zero.
 *
 * The default value is NULL meaning no function will be called. The caller
 * should manage object lifetime.
 *
 * The function can be free in which case free(pointer) will be called.
 *
 * A more elaborate destructor can be supplied to, for example, release
 * resources contained within the object.
 *
 * Shared reference counting can be implemented by the provided destructor releasing
 * the reference on the pointed-to-object.
 */
void hndbag_set_item_destructor(pHNDBAG cp_in, void (*free_func)(void *ptr));

/**
 * get the unique_flag.
 */
bool hndbag_get_unique(pHNDBAG pb_in);

/**
 * set the unique_flag.
 */
bool hndbag_set_unique(pHNDBAG pb_in, bool check_unique);

/**
 * Return the count of active objects
 */
int hndbag_count(pHNDBAG pb_in);

/**
 * Return an array of pointers to the objects
 */
void **hndbag_array(pHNDBAG cp_in, HNDBAG_COMPARE compare);

/**
 * Return an array of pointers to the objects in the order of compare
 */
void **hndbag_array_retain(pHNDBAG cp_in, HNDBAG_COMPARE compare);

/**
 * Release the array of pointers
 */
void hndbag_array_release(pHNDBAG cp_in, void **array);

/**
 * Insert an item into the collection
 * \param cp_in pointer to the container
 * \param ptr_in pointer to be inserted
 * \param hnd_out pointer to returned handle
 * \return zero on success, -1 on failure
 */
int hndbag_item_insert(pHNDBAG cp_in, void *ptr_in, HANDLE_T *hnd_out);

/**
 * Increment the reference counter for an item
 * \param cp_in pointer to the container
 * \param hnd_in object to be incremented
 * \return zero on success
 */
int hndbag_item_retain(pHNDBAG cp_in, HANDLE_T hnd_in);

/**
 * Decrement the reference counter for an item
 * \param cp_in pointer to the container
 * \param hnd_io address of handle, cleared on exit
 * \return zero on success
 */
int hndbag_item_release(pHNDBAG cp_in, HANDLE_T *hnd_io);

/**
 * Convert a handle to a pointer
 * \param cp_in pointer to the container
 * \param hnd_in input handle
 * \param ptr_out address of pointer to be returned
 * \return ref count on success, -1 on failure
 *
 * If handle is valid, store object pointer, else store NULL in pointer.
 */
int hndbag_get_pointer(pHNDBAG cp_in, HANDLE_T hnd_in, void **ptr_out);

/**
 * Test if an item is already in the collection
 */
bool hndbag_contains(pHNDBAG cp_in, HANDLE_T hnd_in);

/**
 * Find a handle based on the comparison function and the test value
 * \return zero on success
 */
int hndbag_find_handle(pHNDBAG cp_in,
                            HNDBAG_COMPARE compare,
                            const void *ctx,
                            HANDLE_T *obj_out);

/**
 * Find a pointer based on the comparison function and the test value
 * \return pointer on success else NULL
 */
void *hndbag_find(pHNDBAG cp_in, HNDBAG_COMPARE compare, const void *ctx);

/**
 * Visitor pattern - visit each entry with the given function in order of compare
 */
void hndbag_visit(pHNDBAG cp_in, HNDBAG_VISITOR visitor,
    void *ctx, HNDBAG_COMPARE compare);

#endif /* HNDBAG_H */
