
/*--------------------------------------------------------------------------

                             I N T E G R A T E

  Peak integration follwing the method described by Grant & Gabe in
  J. Appl. Cryst (1978), 11, 114-120.

  Mark Koennecke, March 1998
----------------------------------------------------------------------------*/
#ifndef GABEINTEGRATE
#define GABEINTEGRATE

#define INTEGLEFT      -1
#define INTEGRIGHT     -2
#define INTEGNOPEAK    -3
#define INTEGFUNNYBACK -4

int GabePeakIntegrate(int m, int iCounts, long lCounts[],
                      float *fIntens, float *fVariance);
#endif
