\subsection{Integrate}
For four circle diffractometers it is usefull to be able to integrate the
intensity of the peak from a collected counting profile. This module does
just this. It uses the method as described by Grant \& Gabe in J. Appl. Cryst
(1987), 11, 114-120. This method uses a moving window technique for finding
the limits of the peak. This method was choosen for reason of its
simplicity. The authors state, that the results are comparable to the
Lehmann Larsen method used elsewhere. This is a utility module. Just a
single function is implemented:

@o integrate.h @{
/*--------------------------------------------------------------------------

                             I N T E G R A T E

  Peak integration follwing the method described by Grant & Gabe in
  J. Appl. Cryst (1978), 11, 114-120.

  Mark Koennecke, March 1998
----------------------------------------------------------------------------*/
#ifndef GABEINTEGRATE
#define GABEINTEGRATE

#define INTEGLEFT      -1
#define INTEGRIGHT     -2
#define INTEGNOPEAK    -3
#define INTEGFUNNYBACK -4

    int GabePeakIntegrate(int m, int iCounts, long lCounts[], 
                          float *fIntens, float *fVariance);
#endif
@}

Input to this routine is the number of counts and the counts themselves.
Then m, the number of lookahead points to use. As a result fIntens will
contain the summed Intensity and fVariance the sigma of the summed
intensity. The function retusn 1 for success or a negative error code in
case of trouble. These error codes are:
\begin{description}
\item[INTEGLEFT] no left side background found.
\item[INTEGRIGHT] nor right side background found.
\item[INTEGNOPEAK] The data does not contain a peak.
\item[INTEGFUNNYBACK] the data has a strange background.
\end{description}
