
#line 412 "interface.w"

/*---------------------------------------------------------------------------
                        I N T E R F A C E S

  Any object in SICS has to adhere to the object descriptor interface (see
  file obdes.h). Furthermore SICS objects may choose to support other
  interfaces as well. These interfaces are defined.

  Mark Koennecke, June 1997

  For more documentation see interface.w, interface.tex

  copyright: see SICS impelementation files
---------------------------------------------------------------------------*/

#ifndef SICSINTERFACES
#define SICSINTERFACES
#include "commandcontext.h"
#include "future.h"
#include "sics.h"

/* interface ID's used to recognize an interface */
#define DRIVEID 513
#define COUNTID 713
#define CALLBACKINTERFACE 947
#define ENVIRINTERFACE    949

/* ----------------------- The drivable interface -----------------------*/

#line 121 "interface.w"


         typedef struct {
                   int ID;
                   int   (*Halt)(void *self);
                   int   (*CheckLimits)(void *self, float fVal, 
                              char *error, int iErrLen);
                   long  (*SetValue)(void *self, SConnection *pCon,
                                    float fVal);
                   int   (*CheckStatus)(void *self, SConnection *pCon);
                   float  (*GetValue)(void *self, SConnection *pCon); 
                   int iErrorCount;
                   int drivableStatus;
                   } IDrivable, *pIDrivable;

        pIDrivable  GetDrivableInterface(void *pObject);                 
        int GetDrivablePosition(void *pObject, SConnection *pCon,
                                 float *fPos);

        long StartDriveTask(void *self, SConnection *pCon, char *name,  float fTarget);


#line 438 "interface.w"


   pIDrivable CreateDrivableInterface(void);

/* ------------------------ The countable interface ---------------------*/

#line 195 "interface.w"

   typedef struct {
                   int ID;
                   int running;
                   int lastStatus;
                   int (*Halt)(void *self);
                   void (*SetCountParameters)(void *self, float fPreset,
                                             CounterMode eMode);
                   int (*StartCount)(void *self, SConnection *pCon);
                   int (*CheckCountStatus)(void *self, SConnection *pCon);
                   int (*ReadStatus)(void *self, SConnection *pCon);
                   int (*Pause)(void *self, SConnection *pCon);
                   int (*Continue)(void *self, SConnection *pCon);
                   int (*TransferData)(void *self, SConnection *pCon);
                   } ICountable, *pICountable;

        pICountable  GetCountableInterface(void *pObject);                 
            int GetCountLock(pICountable self, SConnection *pCon);
            void ReleaseCountLock(pICountable self);
            int isRunning(pICountable self);
        long StartCountTask(void *self, SConnection *pCon, char *name);

#line 443 "interface.w"


   pICountable CreateCountableInterface(void);

/* ------------------------- The CallBack Interface --------------------*/

#line 266 "interface.w"

   typedef void (*KillFuncIT)(void *pData);
   typedef int (*SICSCallBack)(int iEvent, void *pEventData, 
                 void *pUserData);

#line 448 "interface.w"


#line 288 "interface.w"

   typedef struct __ICallBack *pICallBack;
 
  /* event source side */
   pICallBack CreateCallBackInterface(void);
   void       DeleteCallBackInterface(pICallBack self);
   int InvokeCallBack(pICallBack pInterface, int iEvent, void *pEventData); 

   /* callback client side */
   long RegisterCallback(pICallBack pInterface,  
                        int iEvent, SICSCallBack pFunc,
                        void *pUserData, KillFuncIT pKill);
   int RemoveCallback(pICallBack pInterface, long iID);
   int RemoveCallback2(pICallBack pInterface, void *pUserData);
   int RemoveCallbackUsr(pICallBack self, SICSCallBack pFunc, 
		      int (*userfunc)(const void* pContext, const void* pUserData), void *pCtx);
   int RemoveCallbackCon(pICallBack pInterface, SConnection *pCon);

   int CallbackScript(SConnection *pCon, SicsInterp *pSics, void *pData,
        int argc, char *argv[]); 

   pICallBack GetCallbackInterface(void *pData); 

#line 449 "interface.w"

/*---------------------- The Environment Interface --------------------*/

#line 366 "interface.w"

        typedef enum { EVIdle, EVDrive, EVMonitor, EVError } EVMode;
        typedef struct {
                        int iID;
                        EVMode (*GetMode)(void *self);
                        int    (*IsInTolerance)(void *self);
                        int    (*HandleError)(void *self);
                       } EVInterface, *pEVInterface;

#line 451 "interface.w"


#line 392 "interface.w"

   pEVInterface CreateEVInterface(void);

#line 452 "interface.w"

#endif
