/*----------------------------------------------------------------------------

	 SICS needs a way to interrupt server processsing. This is done
	 with this module.
	 
	 Interrupts may come in through two ways: They may be created
	 internally or they may be user requested. For the last ones
	 the main loop will poll an additional special interrupt
	 port.
	 
	 Mark Koennecke, December 1996
	 
	copyrights: see implementation file
-----------------------------------------------------------------------------*/
#ifndef SICSINTERRUPT
#define SICSINTERRUPT


/* Interrupt codes */
#define eContinue   0
#define eAbortOperation  1
#define eAbortScan       2
#define eAbortBatch 3
#define eHaltSystem 4
#define eFreeSystem 5
#define eEndServer  6

/* ---------------- server side functions ---------------------------------*/
void ServerStopInterrupt(void);
  /*
     stops interrupt processing.
   */

void SetInterrupt(int iCode);
  /*
     send an interrupt to everybody
   */
/*------------------------------ utility ----------------------------------*/
int Interrupt2Text(int iInterrupt, char *text, int iTextLen);
int Text2Interrupt(char *text);

/* --------------------- client side functions ----------------------------*/

int ClientSetupInterrupt(char *host, int iPort);
void ClientStopInterrupt(void);
void SendInterrupt(int iCode);


#endif
