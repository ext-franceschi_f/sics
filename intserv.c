/*--------------------------------------------------------------------------
	The server side implementation of the Interrupt module.
	
	As future version of this module may support adding interrupt
	handlers, the necessary precautions are made, by keeping the 
	interrupt handlers in an array of function pointers. Is
	quick as well.

	Mark Koennecke,    November 1996
        Revised: Mark Koennecke, September 1997

	Copyright: see copyright.h

	Labor fuer Neutronenstreuung
	Paul Scherrer Institut
	CH-5423 Villigen-PSI

-----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <strlutil.h>

#include "fortify.h"
#include "sics.h"
#include "network.h"
#include "interrupt.h"
#include "status.h"
#include "splitter.h"
#include "configfu.h"
#include "devexec.h"
#include "nread.h"
#include "task.h"
#include "event.h"

#define MAXINTERRUPT 7
#define INTERUPTWAIT 5

static mkChannel *IntPort = NULL;
static pTaskMan pTask = NULL;
/*----------------------------------------------------------------------------*/
static char *pIntText[] = {
  "continue",
  "abortop",
  "abortscan",
  "abortbatch",
  "halt",
  "free",
  "end",
  NULL
};

/*---------------------------------------------------------------------------*/
int ServerSetupInterrupt(int iPort, pNetRead pNet, pTaskMan pTasker)
{
  int i;


  pTask = pTasker;
  /* setup interrupt port */
  IntPort = UDPOpen(iPort);
  if (IntPort == NULL) {
    return 0;
  } else {
    NetReadRegister(pNet, IntPort, udp, NULL);
    return 1;
  }
}

/*--------------------------------------------------------------------------*/
void ServerStopInterrupt(void)
{

  /* close the port */
  if (IntPort) {
    NETClosePort(IntPort);
    free(IntPort);
  }
}

/*-------------------------------------------------------------------------*/
void SetInterrupt(int iCode)
{
  int iInt;

  iInt = iCode;

  TaskSignal(pServ->pTasker, SICSINT, &iInt);
}

/*--------------------------------------------------------------------------*/
int Interrupt2Text(int iInterrupt, char *text, int iTextLen)
{
  if ((iInterrupt < 0) || (iInterrupt > MAXINTERRUPT)) {
    return 0;
  }
  strlcpy(text, pIntText[iInterrupt], iTextLen - 1);
  return 1;
}

/*-------------------------------------------------------------------------*/
int Text2Interrupt(char *text)
{
  int i = 0;
  while (pIntText[i] != NULL) {
    if (strcmp(pIntText[i], text) == 0) {
      break;
    }
    i++;
  }
  if (i >= MAXINTERRUPT) {
    return -1;
  }
  return i;
}
