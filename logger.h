/*---------------------------------------------------------------------------
logger.h

Markus Zolliker, Sept 2004
----------------------------------------------------------------------------
*/

#ifndef LOGGER_H
#define LOGGER_H

#include <time.h>

typedef struct Logger {
  /* public */
  char *name;
  int numeric;
  int period;
  int exact;
  /* private: */
  char *old;
  int oldsize;
  time_t last, lastWrite, omitTime;
  float omitValue;
  struct Logger *next;
} Logger;


Logger *LoggerMake(char *name, int period, int exact);
void LoggerKill(Logger * log);
int LoggerWrite(Logger * log, time_t now, int period, char *value);
char *LoggerName(Logger * log);
void LoggerSetNumeric(Logger * log, int numeric);
time_t LoggerSetDir(char *dirarg);
time_t LoggerGetLastLife(char *dirarg);
void LoggerWriteOld(Logger * log, time_t now);
time_t LoggerLastTime(Logger * log);
int LoggerPeriod(Logger * log);
void LoggerChange(Logger * log, int period, char *newname);
int LoggerVarPath(char *dir, char *path, int pathLen, char *name,
                  struct tm *t);
void LoggerFreeAll(void);

#endif
