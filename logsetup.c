#include "logger.h"
#include "sics.h"
#include "sicshipadaba.h"

static char *loggerID = "loggerID";

static hdbCallbackReturn LoggerUpdateCallback(pHdb node,
                                              void *userData,
                                              pHdbMessage message)
{
  Logger *logger = userData;
  pDynString str;
  SConnection *conn = NULL;
  hdbValue value;
  pHdbDataMessage mm = NULL;
  pHdbDataSearch dsm = NULL;
  time_t now;

  if ((dsm = GetHdbDataSearchMessage(message)) != NULL) {
    if (dsm->testPtr == loggerID) {
      dsm->result = userData;
      return hdbAbort;
    }
    return hdbContinue;
  }

  if ((mm = GetHdbUpdateMessage(message)) == NULL) {
    return hdbContinue;
  }

  value = *(mm->v);

  time(&now);
  /* testwise >= */
  if (now >= LoggerLastTime(logger)) { /* never write more than once per second */
    if (GetHdbProp(node, "geterror") == NULL) {
      str = formatValue(value, node);
      LoggerWrite(logger, time(NULL), LoggerPeriod(logger), GetCharArray(str));
      DeleteDynString(str);
    } else {
      LoggerWrite(logger, time(NULL), LoggerPeriod(logger), "");
    }
  }
  return hdbContinue;
}

static int LogSetup(SConnection * pCon, SicsInterp * pSics, void *pData,
                    int argc, char *argv[])
{
  pHdb node;
  pHdbCallback cb;
  static char basepath[1024] = "/";
  char buf[1024];
  char *p, *name;
  static char *loggerDir = NULL;
  int numeric, period;
  Logger *logger;

  if (argc < 2) {
    SCPrintf(pCon, eError,
             "ERROR: should be: logsetup <node> [<period> [<filename>]]");
    /* or logsetup <node> clear */
    return 0;
  }
  if (strcasecmp(argv[1], "basepath") == 0) {
    if (argc > 2) {
      snprintf(basepath, sizeof basepath, "%s", argv[2]);
    }
    SCPrintf(pCon, eValue, "%s", basepath);
    return 1;
  }
  if (loggerDir == NULL) {
    loggerDir = IFindOption(pSICSOptions, "LoggerDir");
    if (loggerDir == NULL)
      loggerDir = "./";
    LoggerSetDir(loggerDir);
  }
  if (strcasecmp(argv[1], "directory") == 0) {
    if (argc > 2) {
      loggerDir = strdup(argv[2]);
    }
    SCPrintf(pCon, eValue, "%s", loggerDir);
    return 1;
  }
  node = FindHdbNode(basepath, argv[1], pCon);
  if (node == NULL) {
    SCPrintf(pCon, eError, "ERROR: %s not found", argv[1]);
    return 0;
  }
  if (argc > 3) {
    snprintf(buf, sizeof buf, "%s", argv[3]);
  } else {
    snprintf(buf, sizeof buf, "%s", argv[1]);
  }
  for (p = buf; *p != '\0'; p++) {
    if (*p == '/')
      *p = '.';
  }
  if (buf[0] == '.') {
    name = buf + 1;
  } else {
    name = buf;
  }
  if (node->value.dataType == HIPFLOAT) {
    numeric = 1;
  } else {
    numeric = 0;
  }
  logger = FindHdbCallbackData(node, loggerID);
  period = 0;
  if (argc > 2) {
    if (logger != NULL && strcasecmp(argv[2], "clear") == 0) {
      LoggerWrite(logger, time(NULL), LoggerPeriod(logger), "");
      return 1; 
    }
    period = atoi(argv[2]);
  }
  if (logger != 0) {            /* logger exists already */
    LoggerChange(logger, period, name);
  } else {
    logger = LoggerMake(name, period, !numeric);
    /* If that failed, we cannot continue - it crashes in the callback */
    if (logger == NULL) {
      SCPrintf(pCon, eError, "ERROR: logger %s not created", argv[1]);
      return 0;
    }
    LoggerSetNumeric(logger, numeric);
    SetHdbProperty(node, "logger_name", name);
    cb = MakeHipadabaCallback(LoggerUpdateCallback, logger,
                              (void (*)(void *)) LoggerKill);
    assert(cb);
    AppendHipadabaCallback(node, cb);
  }

  return 1;
}

void LogSetupInit(void)
{
  AddCmd("LogSetup", LogSetup);
}
