/*
  This is a reimplementation of SICS logging. The aim is to 
  merge all the different logs within SICS to the new logging 
  system

  COPYRIGHT: see file COPYRIGHT

  Mark Koennecke, February 2016
 */

#ifndef __LOGV2
#define __LOGV2
#include <stdarg.h>
#include <sys/time.h>

/*
  severity codes
*/
#define FATAL   1
#define ERROR   2
#define WARN    3
#define INFO    4
#define VERBOSE 5
#define DEBUG   6
#define INVALID 7 /* internal olny, do not use, means invalid severity passed */


/*
  subsystem codes
*/
#define SSYS     0
#define SCOM     1
#define SASQIO   2
#define SIO      3
#define SDEV     4
#define SPAR     5
#define SNOTIFY  6
#define SHISTORY 7
#define SINVALID 8

#define MAXSUB 8
 

/* write a log message
 * \param severity The message severity. Must be one of the defines given above
 * \param subsystem The subsystem reporting the log messages
 * \param format The format string for the message
 * \param ... The data to format
 */
void Log(unsigned int severity, const char *subsystem,const char *format,...); 

/* write a log message, but with the subsystem specified by an integer
 * \param severity The message severity. Must be one of the defines given above
 * \param subsystem The subsystem reporting the log messages
 * \param format The format string for the message
 * \param ... The data to format
 */
void LogIS(unsigned int severity, unsigned int subsystem,const char *format,...); 
/* write a log message, but with the subsystem specified by an integer and user supplied timestamp
 * \param timeStamp A user supplied timestamp
 * \param severity The message severity. Must be one of the defines given above
 * \param subsystem The subsystem reporting the log messages
 * \param format The format string for the message
 * \param ... The data to format
 */
void LogTS(struct timeval *tv, unsigned int severity, unsigned int subsystem,const char *format,...); 
/*
 * write a log entry in hex
 * \param timeStamp A user supplied timestamp. Can be NULL, then the current time is used
 * \param severity The message severity. Must be one of the defines given above
 * \param subsystem The subsystem reporting the log messages
 * \param buffer the bytes to log as hex
 * \param bufferLength The length of buffer
 */
void LogHex(struct timeval*tv, unsigned int severity, unsigned int subsystem,char *buffer, int bufferLength);


/* The callback functio which is called by the logging system on log events
 * \param severity The message severity
 * \param timeStamp The time stamp of the log message
 * \param subsystem The subsystem of the log message
 * \param message The log message
 * \param userData A pointer to a user define ddata structure passed through 
 *        transparantly from RegisterLogCallback
*/

/*
At this time LogCallback do not do any filtering. Thus, they see every message.
The idea is that suitable filtering functions ought to be implemented as helper 
functions once the exact usage of the log \callbacks becomes clearer. 
 */

typedef void (*LogCallback)(unsigned int severity, const char *timeStamp, 
			    const char *subsystem, 
			    const char *message, void *userData);

/* register a LogCallback
 * \param func The function to call whena suitable log message has been found
 * \param userData A pointer to user defined data which is passed through to the 
 *  callback function. The log system does not free that pointer; this is up to the caller
 * \return An callback ID which can be used for RemoveLogCallback
 */  

void RegisterLogCallback(LogCallback func, void *userData);
void RemoveLogCallback(LogCallback func);

/*
 * Allow log helper functions to inject a formatted log message at the same
 * point as the callbacks.
 *
 * Arguments are the same as the callbacks except userData is ignored
 */
void LogInject( unsigned int severity,
               const char *timeText,
               const char *subsystem,
               const char *logMessage,
               void *userData);

/* test if this log entry is filtered. Made a part of the API in support of 
 * log listeners
 * \param severity the severity of the message
 * \param subsystem the subsystem of the message
 * \return 1 when filtered, 0 else
 */
unsigned int logFilter(unsigned int severity, const char *subsystem, const char *logMessage);
/*
 *Disable logging in support of the nolog option in SICSmain.c
 */
void DisableLog(void);
/*
 * close the log as cleanup on exit
 */
void LogClose(void *data);

/*
 * internal use in other logging functions
 * \param severity The severity to encode
 * \param buffer The buffer to write the severity as text too
 * \param bufferLengh The length of buffer in order to prevent buffer overwrites
 */
void formatSeverity(unsigned int severity,
                    char *buffer, unsigned int bufferLength);

extern unsigned int logEnabledArray[];
#define logEnabled(subsystem,severity) (severity <= logEnabledArray[subsystem])

#endif
