
/*-------------------------------------------------------------------------
                   L o c a l     M a x i m u m

 This is a module for searching a local maximum in a 2D histogram as
 collected at TRICS.

 copyright: see copyright.h

 Mark Koennecke, November 2001
-------------------------------------------------------------------------*/
#ifndef LOCALMAXIMUM
#define LOCALMAXIMUM
#include "obpar.h"

typedef struct __LOMAX *pLoMax;


int LoMaxAction(SConnection * pCon, SicsInterp * pSics,
                void *pData, int argc, char *argv[]);

int LoMaxFactory(SConnection * pCon, SicsInterp * pSics,
                 void *pData, int argc, char *argv[]);

int testLocalMaximum(int *iData, int xsize, int ysize,
                     int i, int j,
                     int window, int threshold, int steepness,
                     int *intensity);
int calculateCOG(int *iData, int xsize, int ysize,
                 int *i, int *j, int *intensity, int *count,
                 int cogWindow, float contour);
void calculateStatistics(int *iData, int xsize, int ysize,
                         float *average, float *maximum);
int wellFormed(int *iData, int xsize, int ysize,
               int x, int y, int window, float contour, int maxBad);




#endif
