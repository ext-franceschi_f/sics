
/*-------------------------------------------------------------------------
                   L o c a l     M a x i m u m

 This is a module for searching a local maximum in a 2D histogram as
 collected at TRICS. These are internal definitions only and are
 supposed to be included only into lomax.c

 copyright: see copyright.h

 Mark Koennecke, November 2001
-------------------------------------------------------------------------*/

        typedef struct __LOMAX {
                                  pObjectDescriptor pDes;
                                  ObPar *pParam;
                                }LoMax;                   


