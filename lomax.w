\subsection{Local Maximum Search}
This module implements searching for local maxima in a 2D histogram of 
data. It is mainly intended for use at TRICS where initially
reflections need to be searched for the determination of a UB
matrix. The local maximum algorithm is a little more sophisticated
then commonly used in that the window of the search is user definable,
and a valid maximum has to have a certain steepness against the
bordering points in order to be valid. Another validity test is a
threshold which must be reached. All these enhancements should reduce
the number of false maxima which may turn up as the result of horrible
measurement statistics. 

The position and intensity of a local maximum found can be refined further
 with a center of gravity calculation within a given window including all
 data points above a certain threshold expressed as a fraction of the local
 maximums intensity.

Again this module has a little data structure:
 
@d lomaxdata @{
	typedef struct __LOMAX {
				  pObjectDescriptor pDes;
	                          ObPar *pParam;
				}LoMax;		          
@}
The fields are:
\begin{description}
\item[pDes] The standard object descriptor.
\item[pParam] The parameters of this object.
\end{description}

The exported interface is simple: just the necessary functions for
representing the object in the interpreter and a function which allows
to use the local maxima search from another module. If the latter is
ever needed.

@d lomaxint @{
  int LoMaxAction(SConnection *pCon, SicsInterp *pSics,
		  void *pData, int argc, char *argv[]);

  int LoMaxFactory(SConnection *pCon, SicsInterp *pSics,
		  void *pData, int argc, char *argv[]);

  int testLocalMaximum(int *iData, int xsize, int ysize,
                      int i, int j, 
                      int window, int threshold, int steepness,
                      int *intensity);
  int calculateCOG(int *iData, int xsize, int ysize,
	          int *i, int *j, int *intensity, int *count,
                  int cogWindow, 
                  float contour);
  void calculateStatistics(int *iData, int xsize, int ysize,
	                  float *average, float *maximum);
  int wellFormed(int *iData, int xsize, int ysize, 
	         int x, int y, int window, float contour,   
                  int maxBad);                   

@} 

testLocalMaxima checks if point i, j is a valid local maximum. It
returns true (1) if this is the case, or false (0) if this is not the
case. The window parameter describes how much points shall be searched
in each direction. 

calculateCOG calculates the center of gravity for point i, j,. Points within a
 sqaure window of size cogWindow are considered. Only points whose
 intensity obeys I \gt I(i,j)*countour are used for the COG
calculation. The position of the maximum (i,j) and its intensity are
updated by calculateCOG.

calculateStatistics finds the average and the maximum value of the data
in iData.

wellFormed checks a candidate peak position at x, y for well
formedness. Basically it checks if there are  points above
contour * maximum at the borders of the COG window. If this count is 
larger then maxBad  0 is returned else 1. This should catch powder
lines  and guard against overlapped peaks.

  
@o lomax.h @{
/*-------------------------------------------------------------------------
                   L o c a l     M a x i m u m

 This is a module for searching a local maximum in a 2D histogram as
 collected at TRICS.

 copyright: see copyright.h

 Mark Koennecke, November 2001
-------------------------------------------------------------------------*/
#ifndef LOCALMAXIMUM
#define LOCALMAXIMUM
#include "obpar.h"

  typedef struct  __LOMAX *pLoMax;

@<lomaxint@>


#endif
@}  

@o lomax.i @{
/*-------------------------------------------------------------------------
                   L o c a l     M a x i m u m

 This is a module for searching a local maximum in a 2D histogram as
 collected at TRICS. These are internal definitions only and are
 supposed to be included only into lomax.c

 copyright: see copyright.h

 Mark Koennecke, November 2001
-------------------------------------------------------------------------*/
@<lomaxdata@>

@}
