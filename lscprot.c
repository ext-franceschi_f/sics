#include "ascon.h"
#include "ascon.i"

/*
 * script context protocol for LakeShore 370 / 340 etc. models
 * a 50 msec waiting time is needed after each reply / command
 *
 * Markus Zolliker May 2016
 */

typedef struct {
  double last;
  double delay;
} LscPrivate;

/*----------------------------------------------------------------------------*/
int LscProtHandler(Ascon *a)
{
  LscPrivate *p;
  int res;
  
  if (a->state == AsconWriteStart) {
    p = a->private;
    if (DoubleTime() < p->last + p->delay) { // 50 msec
      return 0;
    }
  }
  res = AsconStdHandler(a);
  if (a->state == AsconReadDone) {
    p = a->private;
    p->last = DoubleTime();
  }
  return res;
}

/*----------------------------------------------------------------------------*/
static void LscPrivateKill(void *p) {
  free(p);
}

/*----------------------------------------------------------------------------*/
int LscProtInit(Ascon *a, SConnection *con, int argc, char *argv[])
{
  enum nPars {NA=4};
  char *pars[NA];
  static char *parn[NA]={
    "sendterminator",
    "timeout",
    "replyterminator",
    "writedelay"
  };
  char *msg;
  LscPrivate *p;
  
  assert(argc>1);
  a->hostport = strdup(argv[1]);

  if (!AsconInterpreteArgs(argc-2, argv+2, NA, parn, pars)) {
    return 0;
  }

  p = calloc(sizeof(*p), 1);
  if (p == NULL) return 0;

  p->last = DoubleTime();
  a->private = p;
  a->killPrivate = LscPrivateKill;

  if (pars[0]) {
    a->sendTerminator = strdup(pars[0]);
  } else {
    a->sendTerminator = strdup("\n");
  }
  if (pars[1] && pars[1][0] != '\0') {
    a->timeout = atof(pars[1]);
  } else {
    a->timeout = 2.0; /* sec */
  }
  if (pars[2] && pars[2][0] != '\0') {
    a->replyTerminator = strdup(pars[2]);
  } else {
    a->replyTerminator = NULL;
  }
  if (pars[3] && pars[3][0] != '\0') {
    p->delay = atof(pars[3]);
  } else {
    p->delay = 0.05;
  }
  AsconCheckTerminators(a);
  return 1;
}

/*----------------------------------------------------------------------------*/
void AddLscProtocol()
{
  static AsconProtocol lscprot;
  lscprot.name = "lsc";
  lscprot.handler = LscProtHandler;
  lscprot.init = LscProtInit;
  AsconInsertProtocol(&lscprot);
}
