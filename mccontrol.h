
 /*--------------------------------------------------------------------------------------
  McSas simulation to SICS controller module header file. For more details see 
  mcstas.tex. 

  copyright: see file COPYRIGHT
  
  Mark Koennecke, June 2005
----------------------------------------------------------------------------------------*/
#ifndef MCSTASCONTROL
#define MCSTASCONTROL
#include "sics.h"
#include "countdriv.h"
#include "stringdict.h"
/*--------------------- data structure -------------------------------------------------*/

typedef struct {
  pObjectDescriptor pDes;
  pStringDict scripts;
  int updateIntervall;
  int pid;
  float monitorScale;
  int lastError;
  char errorText[256];
  CounterMode mode;
  float fPreset;
  time_t lastUpdate;
  time_t startTime;
  time_t stopTime;
  time_t lastMonitorRead;
  float lastMon;
} McStasController, *pMcStasController;

/*---------------------- function prototypes -------------------------------------------*/

int McStasControllerFactory(SConnection * pCon, SicsInterp * pSics,
                            void *pData, int argc, char *argv[]);
int McStasControllerWrapper(SConnection * pCon, SicsInterp * pSics,
                            void *pData, int argc, char *argv[]);

int McStasStart(pMcStasController self, CounterMode mode, float fPreset);
int McStasStatus(pMcStasController self, float *fControl);
int McStasStop(pMcStasController self);
int McStasTransferData(pMcStasController self);
int McStasGetError(pMcStasController self, char *error, int errLen);
int McStasFix(pMcStasController self);
float McStasGetTime(pMcStasController self);

#endif
