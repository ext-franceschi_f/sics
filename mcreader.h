
 /*---------------------------------------------------------------------------------
  Header file for the McStas reader module. This module helps transferring McStas
  result data into SICS counters and histogram memories.

  copyright: see file COPYRIGHT

  Mark Koennecke, June 2005
-------------------------------------------------------------------------------------*/
#ifndef MCSTASREADER
#define MCSTASREADER
#include "sics.h"
#include "napi.h"

typedef struct {
  pObjectDescriptor pDes;
  NXhandle handle;
  char nexusError[1024];
} McStasReader, *pMcStasReader;
/*-----------------------------------------------------------------------------*/
int McStasReaderFactory(SConnection * pCon, SicsInterp * pSics,
                        void *pData, int argc, char *argv[]);
int McStasReaderWrapper(SConnection * pCon, SicsInterp * pSics,
                        void *pData, int argc, char *argv[]);

#endif
