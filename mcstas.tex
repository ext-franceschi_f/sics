\subsection{McStas Integration}
It is useful to drive a simulation of an instrument with the same interface as is used at
the original instruments. One of the better packages for performing simulations of neutron
scattering instruments, including samples, is McStas. This section describes SICS
interface to McStas simulations. The interface consists of three parts:
\begin{itemize}
\item A McStas controller module which controls the actual simulation.
\item A McStas reader which is responsible for reading simulated data into 
 SICS counters and histogram memories. 
 \item Counter and histogram memory drivers which redirect their actions to the 
  McStas controller module. 
\end{itemize}
The general ideas is that all parameters are handled through the normal SICS simulation 
 drivers. The counting operations, however, are linked with the McStas simulation.

\subsubsection{McStas Requirements and SICS Requirements}
In order for the McStas SICS interface to work the McStas simulation has to be configured 
in a certain way:
\begin{itemize}
\item All parameters which have to pass between SICS and McStas have to be declared as 
 simulation parameters in the DEFINE INSTRUMENT section of the instrument definition file.
 Alternatively SICS can write the data to be passed to McStas into a file. But then this
 file must be read in the INITIALIZE section of the instrument definition and values must
  be assigned to the appropriate McStas variables. 
 \item McStas must dump its data into a single file: use the {\it -f filename} option. 
 The format must be {\it --format=\"XML\"}. 
\item In order to count on monitor, a modified PSD_monitor component MUST be used in the
 simulation. This component writes the collected total counts into a file. This file is
 the read by SICS in order to determine the control monitor. Evaluating the McStas XML dump \
 file each time proved to be to inaccurate. 
\end{itemize}

\subsubsection{The McStas Controller}
The connection to the McStas simulation could have been implemented in the counter, 
HM drivers alone. However, these two implementations would share so much code that it 
deemed advisable to separate this logic into a separate module. The McStas controller is
 basically implementing the Countable SICS interface plus some necessary parameters. In order
 to be as general as possible many operations of the McStas controller are offloaded to
 Tcl scripts in order to achieve support for different types of instruments. 
 The following operations will be performed at the various steps:
 \begin{description}
 \item[Start Counting] A script will be called which collects the settings parameters from SICS and 
 builds a command line for the McStas simulation. The simulation will then be started. A note
 is made of the PID of the McStas process. 
 \item[Status Requests]First thing, status checking maps to running a script which decides
  if the McStas process is still running. Else it is finished. Count control depends
  on the counting mode:
\begin{description}
\item[timer mode] Timer mode is handled in SICS internally. The simulation is started with a
 generous number of neutron generations and stopped when the time limit has been reached.
 \item[monitor mode] In monitor mode, the control monitor of McStas is read at regular
  intervalls. Counting is interrupted when the preset monitor has been exceeded.
\end{description} 
\item[Stopping] Interrupting the simulation is achived by sending the TERM signal to the
 McStas process.
 \item[Data Transfer] Data transfer happens by calling a script which is supposed to use
  the McStas reader for transferring simulation data into SICS counters and histogram
  memories. In order to read data during the runtime of the simulation, the signal USR2 
  is sent to the McStas process. This causes McStas to dump a data file. In order for this
  not to happen to frequently, an update intervall is maintained. 
 \end{description}

 
 In order to do its job the McStas controller needs the following data structure:
 \begin{flushleft} \small
\begin{minipage}{\linewidth} \label{scrap1}
$\langle$mcconint {\footnotesize ?}$\rangle\equiv$
\vspace{-1ex}
\begin{list}{}{} \item
\mbox{}\verb@@\\
\mbox{}\verb@ typedef struct{@\\
\mbox{}\verb@        pObjectDescriptor pDes;@\\
\mbox{}\verb@        pStringDict scripts;@\\
\mbox{}\verb@        int updateIntervall;@\\
\mbox{}\verb@        int pid;@\\
\mbox{}\verb@        float monitorScale;@\\
\mbox{}\verb@        int lastError;@\\
\mbox{}\verb@        char errorText[256];@\\
\mbox{}\verb@        CounterMode mode;@\\
\mbox{}\verb@        float fPreset;@\\
\mbox{}\verb@        time_t lastUpdate;@\\
\mbox{}\verb@        time_t startTime;@\\
\mbox{}\verb@        time_t stopTime;@\\
\mbox{}\verb@        time_t lastMonitorRead;@\\
\mbox{}\verb@        float lastMon;@\\
\mbox{}\verb@ }McStasController, *pMcStasController;@\\
\mbox{}\verb@ @$\diamond$
\end{list}
\vspace{-1ex}
\footnotesize\addtolength{\baselineskip}{-1ex}
\begin{list}{}{\setlength{\itemsep}{-\parsep}\setlength{\itemindent}{-\leftmargin}}
\item Macro referenced in scrap ?.
\end{list}
\end{minipage}\\[4ex]
\end{flushleft}
The fields are:
 \begin{description}
 \item[pDes] The usual SICS object descriptor.
 \item[scripts] A dictionary holding the keys and values for all the scripts required.
 \item[updateIntervall] The minimum time to leave between updates.
 \item[pid] The PID of the McStas process. Is negative if the simulation is not running.
 \item[lastError] The last error code.
 \item[mode] The count mode.
 \item[fPreset] The preset value.
 \item[lastUpdate] The time of the last update.
 \item[startTime] The starting time of the counting operation. 
 \item[stopTime] The time when counting finished
 \end{description}
 
 The prototypes for the McStas controller are:
 \begin{flushleft} \small
\begin{minipage}{\linewidth} \label{scrap2}
$\langle$mcconproto {\footnotesize ?}$\rangle\equiv$
\vspace{-1ex}
\begin{list}{}{} \item
\mbox{}\verb@@\\
\mbox{}\verb@        int McStasControllerFactory(SConnection *pCon, SicsInterp *pSics, @\\
\mbox{}\verb@                void *pData, int argc, char *argv[]);@\\
\mbox{}\verb@        int McStasControllerWrapper(SConnection *pCon, SicsInterp *pSics, @\\
\mbox{}\verb@                void *pData, int argc, char *argv[]);@\\
\mbox{}\verb@@\\
\mbox{}\verb@    int McStasStart(pMcStasController self, CounterMode mode, float fPreset);@\\
\mbox{}\verb@    int McStasStatus(pMcStasController self,float *fControl);@\\
\mbox{}\verb@    int McStasStop(pMcStasController self);@\\
\mbox{}\verb@    int McStasTransferData(pMcStasController self); @\\
\mbox{}\verb@    int McStasGetError(pMcStasController self, char *error, int errLen);@\\
\mbox{}\verb@    int McStasFix(pMcStasController self);@\\
\mbox{}\verb@    float McStasGetTime(pMcStasController self);@\\
\mbox{}\verb@ @$\diamond$
\end{list}
\vspace{-1ex}
\footnotesize\addtolength{\baselineskip}{-1ex}
\begin{list}{}{\setlength{\itemsep}{-\parsep}\setlength{\itemindent}{-\leftmargin}}
\item Macro referenced in scrap ?.
\end{list}
\end{minipage}\\[4ex]
\end{flushleft}
The functions implement the interpreter interface to SICS and the required raw functions
  for implementing the countable interface.
 

\subsubsection{The McStas Reader}
A means is needed to map McStas detectors and monitors to SICS monitors and histogram 
memories. This is the purpose of the McStas reader.  The McStas reader uses the NeXus-XML
 API in order to traverse the XML hierarchy. Due to problems with the XML format 
 written by McStas all data is treated as text. The McStas reader decodes this and assigns the data
 to counter or HM monitors or HM arrays. Most of this modules operation is in the 
 interpreter interface. Thus the signature of this module looks like this:
 \begin{flushleft} \small
\begin{minipage}{\linewidth} \label{scrap3}
$\langle$mcreadint {\footnotesize ?}$\rangle\equiv$
\vspace{-1ex}
\begin{list}{}{} \item
\mbox{}\verb@@\\
\mbox{}\verb@typedef struct {@\\
\mbox{}\verb@                        pObjectDescriptor pDes;@\\
\mbox{}\verb@                        NXhandle handle;@\\
\mbox{}\verb@                        char nexusError[1024];@\\
\mbox{}\verb@                        }McStasReader, *pMcStasReader;@\\
\mbox{}\verb@/*-----------------------------------------------------------------------------*/@\\
\mbox{}\verb@        int McStasReaderFactory(SConnection *pCon, SicsInterp *pSics, @\\
\mbox{}\verb@                void *pData, int argc, char *argv[]);@\\
\mbox{}\verb@        int McStasReaderWrapper(SConnection *pCon, SicsInterp *pSics, @\\
\mbox{}\verb@                void *pData, int argc, char *argv[]);           @\\
\mbox{}\verb@ @$\diamond$
\end{list}
\vspace{-1ex}
\footnotesize\addtolength{\baselineskip}{-1ex}
\begin{list}{}{\setlength{\itemsep}{-\parsep}\setlength{\itemindent}{-\leftmargin}}
\item Macro referenced in scrap ?.
\end{list}
\end{minipage}\\[4ex]
\end{flushleft}
\begin{flushleft} \small
\begin{minipage}{\linewidth} \label{scrap4}
\verb@"mccontrol.h"@ {\footnotesize ? }$\equiv$
\vspace{-1ex}
\begin{list}{}{} \item
\mbox{}\verb@@\\
\mbox{}\verb@ /*--------------------------------------------------------------------------------------@\\
\mbox{}\verb@  McSas simulation to SICS controller module header file. For more details see @\\
\mbox{}\verb@  mcstas.tex. @\\
\mbox{}\verb@@\\
\mbox{}\verb@  copyright: see file COPYRIGHT@\\
\mbox{}\verb@  @\\
\mbox{}\verb@  Mark Koennecke, June 2005@\\
\mbox{}\verb@----------------------------------------------------------------------------------------*/@\\
\mbox{}\verb@#ifndef MCSTASCONTROL@\\
\mbox{}\verb@#define MCSTASCONTROL@\\
\mbox{}\verb@#include "sics.h"@\\
\mbox{}\verb@#include "countdriv.h"@\\
\mbox{}\verb@#include "stringdict.h"@\\
\mbox{}\verb@/*--------------------- data structure -------------------------------------------------*/@\\
\mbox{}\verb@@$\langle$mcconint {\footnotesize ?}$\rangle$\verb@@\\
\mbox{}\verb@/*---------------------- function prototypes -------------------------------------------*/@\\
\mbox{}\verb@@$\langle$mcconproto {\footnotesize ?}$\rangle$\verb@@\\
\mbox{}\verb@#endif@\\
\mbox{}\verb@@\\
\mbox{}\verb@@$\diamond$
\end{list}
\vspace{-2ex}
\end{minipage}\\[4ex]
\end{flushleft}
\begin{flushleft} \small
\begin{minipage}{\linewidth} \label{scrap5}
\verb@"mcreader.h"@ {\footnotesize ? }$\equiv$
\vspace{-1ex}
\begin{list}{}{} \item
\mbox{}\verb@@\\
\mbox{}\verb@ /*---------------------------------------------------------------------------------@\\
\mbox{}\verb@  Header file for the McStas reader module. This module helps transferring McStas@\\
\mbox{}\verb@  result data into SICS counters and histogram memories.@\\
\mbox{}\verb@@\\
\mbox{}\verb@  copyright: see file COPYRIGHT@\\
\mbox{}\verb@@\\
\mbox{}\verb@  Mark Koennecke, June 2005@\\
\mbox{}\verb@-------------------------------------------------------------------------------------*/@\\
\mbox{}\verb@#ifndef MCSTASREADER @\\
\mbox{}\verb@#define MCSTASREADER@\\
\mbox{}\verb@#include "sics.h"@\\
\mbox{}\verb@#include "napi.h"@\\
\mbox{}\verb@@$\langle$mcreadint {\footnotesize ?}$\rangle$\verb@@\\
\mbox{}\verb@#endif@\\
\mbox{}\verb@@\\
\mbox{}\verb@ @$\diamond$
\end{list}
\vspace{-2ex}
\end{minipage}\\[4ex]
\end{flushleft}
