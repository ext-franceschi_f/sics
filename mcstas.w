\subsection{McStas Integration}
It is useful to drive a simulation of an instrument with the same interface as is used at
the original instruments. One of the better packages for performing simulations of neutron
scattering instruments, including samples, is McStas. This section describes SICS
interface to McStas simulations. The interface consists of three parts:
\begin{itemize}
\item A McStas controller module which controls the actual simulation.
\item A McStas reader which is responsible for reading simulated data into 
 SICS counters and histogram memories. 
 \item Counter and histogram memory drivers which redirect their actions to the 
  McStas controller module. 
\end{itemize}
The general ideas is that all parameters are handled through the normal SICS simulation 
 drivers. The counting operations, however, are linked with the McStas simulation.

\subsubsection{McStas Requirements and SICS Requirements}
In order for the McStas SICS interface to work the McStas simulation has to be configured 
in a certain way:
\begin{itemize}
\item All parameters which have to pass between SICS and McStas have to be declared as 
 simulation parameters in the DEFINE INSTRUMENT section of the instrument definition file.
 Alternatively SICS can write the data to be passed to McStas into a file. But then this
 file must be read in the INITIALIZE section of the instrument definition and values must
  be assigned to the appropriate McStas variables. 
 \item McStas must dump its data into a single file: use the {\it -f filename} option. 
 The format must be {\it --format=\"XML\"}. 
\item In order to count on monitor, a modified PSD_monitor component MUST be used in the
 simulation. This component writes the collected total counts into a file. This file is
 the read by SICS in order to determine the control monitor. Evaluating the McStas XML dump \
 file each time proved to be to inaccurate. 
\end{itemize}

\subsubsection{The McStas Controller}
The connection to the McStas simulation could have been implemented in the counter, 
HM drivers alone. However, these two implementations would share so much code that it 
deemed advisable to separate this logic into a separate module. The McStas controller is
 basically implementing the Countable SICS interface plus some necessary parameters. In order
 to be as general as possible many operations of the McStas controller are offloaded to
 Tcl scripts in order to achieve support for different types of instruments. 
 The following operations will be performed at the various steps:
 \begin{description}
 \item[Start Counting] A script will be called which collects the settings parameters from SICS and 
 builds a command line for the McStas simulation. The simulation will then be started. A note
 is made of the PID of the McStas process. 
 \item[Status Requests]First thing, status checking maps to running a script which decides
  if the McStas process is still running. Else it is finished. Count control depends
  on the counting mode:
\begin{description}
\item[timer mode] Timer mode is handled in SICS internally. The simulation is started with a
 generous number of neutron generations and stopped when the time limit has been reached.
 \item[monitor mode] In monitor mode, the control monitor of McStas is read at regular
  intervalls. Counting is interrupted when the preset monitor has been exceeded.
\end{description} 
\item[Stopping] Interrupting the simulation is achived by sending the TERM signal to the
 McStas process.
 \item[Data Transfer] Data transfer happens by calling a script which is supposed to use
  the McStas reader for transferring simulation data into SICS counters and histogram
  memories. In order to read data during the runtime of the simulation, the signal USR2 
  is sent to the McStas process. This causes McStas to dump a data file. In order for this
  not to happen to frequently, an update intervall is maintained. 
 \end{description}

 
 In order to do its job the McStas controller needs the following data structure:
 @d mcconint @{
 typedef struct{
 	pObjectDescriptor pDes;
 	pStringDict scripts;
 	int updateIntervall;
 	int pid;
 	float monitorScale;
 	int lastError;
 	char errorText[256];
 	CounterMode mode;
 	float fPreset;
 	time_t lastUpdate;
 	time_t startTime;
 	time_t stopTime;
 	time_t lastMonitorRead;
 	float lastMon;
 }McStasController, *pMcStasController;
 @}
 The fields are:
 \begin{description}
 \item[pDes] The usual SICS object descriptor.
 \item[scripts] A dictionary holding the keys and values for all the scripts required.
 \item[updateIntervall] The minimum time to leave between updates.
 \item[pid] The PID of the McStas process. Is negative if the simulation is not running.
 \item[lastError] The last error code.
 \item[mode] The count mode.
 \item[fPreset] The preset value.
 \item[lastUpdate] The time of the last update.
 \item[startTime] The starting time of the counting operation. 
 \item[stopTime] The time when counting finished
 \end{description}
 
 The prototypes for the McStas controller are:
 @d mcconproto @{
 	int McStasControllerFactory(SConnection *pCon, SicsInterp *pSics, 
 		void *pData, int argc, char *argv[]);
 	int McStasControllerWrapper(SConnection *pCon, SicsInterp *pSics, 
 		void *pData, int argc, char *argv[]);

    int McStasStart(pMcStasController self, CounterMode mode, float fPreset);
    int McStasStatus(pMcStasController self,float *fControl);
    int McStasStop(pMcStasController self);
    int McStasTransferData(pMcStasController self); 
    int McStasGetError(pMcStasController self, char *error, int errLen);
    int McStasFix(pMcStasController self);
    float McStasGetTime(pMcStasController self);
 @}
 The functions implement the interpreter interface to SICS and the required raw functions
  for implementing the countable interface.
 

\subsubsection{The McStas Reader}
A means is needed to map McStas detectors and monitors to SICS monitors and histogram 
memories. This is the purpose of the McStas reader.  The McStas reader uses the NeXus-XML
 API in order to traverse the XML hierarchy. Due to problems with the XML format 
 written by McStas all data is treated as text. The McStas reader decodes this and assigns the data
 to counter or HM monitors or HM arrays. Most of this modules operation is in the 
 interpreter interface. Thus the signature of this module looks like this:
 @d mcreadint @{
typedef struct {
	 		pObjectDescriptor pDes;
 			NXhandle handle;
 			char nexusError[1024];
 			}McStasReader, *pMcStasReader;
/*-----------------------------------------------------------------------------*/
 	int McStasReaderFactory(SConnection *pCon, SicsInterp *pSics, 
 		void *pData, int argc, char *argv[]);
 	int McStasReaderWrapper(SConnection *pCon, SicsInterp *pSics, 
 		void *pData, int argc, char *argv[]);		
 @}
 
 @o mccontrol.h @{
 /*--------------------------------------------------------------------------------------
  McSas simulation to SICS controller module header file. For more details see 
  mcstas.tex. 

  copyright: see file COPYRIGHT
  
  Mark Koennecke, June 2005
----------------------------------------------------------------------------------------*/
#ifndef MCSTASCONTROL
#define MCSTASCONTROL
#include "sics.h"
#include "countdriv.h"
#include "stringdict.h"
/*--------------------- data structure -------------------------------------------------*/
@<mcconint@>
/*---------------------- function prototypes -------------------------------------------*/
@<mcconproto@>
#endif

@}
 
 @o mcreader.h @{
 /*---------------------------------------------------------------------------------
  Header file for the McStas reader module. This module helps transferring McStas
  result data into SICS counters and histogram memories.

  copyright: see file COPYRIGHT

  Mark Koennecke, June 2005
-------------------------------------------------------------------------------------*/
#ifndef MCSTASREADER 
#define MCSTASREADER
#include "sics.h"
#include "napi.h"
@<mcreadint@>
#endif

 @}