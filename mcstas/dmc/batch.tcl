proc SplitReply { text } {
     set l [split $text =]
     return [lindex $l 1]
}

proc batchrun file {
   fileeval [string trim [SplitReply [BatchRoot]]/$file]
}
