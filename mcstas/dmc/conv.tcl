#!/usr/bin/tclsh
#--------------------------------------------------------------------------
# script for extracting detector data with error bars from a simulation
# XML file. The output can be used for plotting with gnuplot
# The program reads from stdin and prints to stdout.
#
# Mark Koennecke, July 2005
#-------------------------------------------------------------------------

#------- locate detector data
while { [gets stdin line] >= 0} {
    if {[string first det9.dat $line] > 0} {
	break
    }
}

#------- locate data record
while { [gets stdin line] >= 0} {
    if {[string first "<data" $line] > 0} {
	break
    }
}

#------ remove XML stuff
set idx [string first > $line]
set line [string range $line [expr $idx + 1] end]

set l [split $line]
set count 0
foreach e $l {
	set e [string trim $e]
	if { [string length $e] > 0} {
	        set data($count) $e
		incr count
	}
}
set maxCount $count

#---- find errors line
while { [gets stdin line] >= 0} {
    if {[string first "<errors" $line] > 0} {
	break
    }
}

#------ remove XML stuff
set idx [string first > $line]
set line [string range $line [expr $idx + 1] end]

set l [split $line]
set count 0
foreach e $l {
	set e [string trim $e]
	if { [string length $e] > 0} {
	        set err($count) $e
		incr count
	}
}

for {set i 0} {$i < $maxCount}  {incr i} {
    puts stdout "$i $data($i) $err($i)"
}
