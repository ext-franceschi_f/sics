#--------------------------------------------------------------------------
# Initialization script for a virtual DMC instrument using a McStas
# simulationas a data source
#
# Dr. Mark Koennecke, June 2005
#---------------------------------------------------------------------------
#                        O P T I O N S
# wwwMode = 1 when running for the WWW-VDMC application
set wwwMode 0
if {$wwwMode == 1} {
    set home /home/lnswww/vinstrument/mcstas/dmc
    set datahome /home/lnswww/www/vinstrument
} else {
    set home $env(HOME)/src/workspace/sics/mcstas/dmc
    ServerOption LoggerDir $env(HOME)/src/workspace/sics/mcstas/dmc/samenv
}
#--------------------------------- first all the server options are set
#ServerOption RedirectFile $home/stdcdmc
ServerOption ReadTimeOut  10
ServerOption AcceptTimeOut 10 
ServerOption ReadUserPasswdTimeout  500000 
ServerOption LogFileBaseName  "$home/vdmclog"
ServerOption LogFileDir  $home/
ServerOption ServerPort 2911
ServerOption statusfile $home/vdmcstatus.tcl
ServerOption InterruptPort 3007
ServerOption TelnetPort 1301
ServerOption TelWord sicslogin
#---------------------------------------------------------------------------
#                        U S E R S
SicsUser lnsmanager lnsSICSlns   1
SicsUser Manager Manager  1
SicsUser user    looser   2
SicsUser Spy    007       1
SicsUser User          07lns1     2
#--------------------------------------------------------------------------
#                D E V I C E S :  M O T O R S
#---------------------------------------------------------------------------
ClientPut "Installing Motors"
Motor  OmegaM    SIM 0 120 .0000001 2.
Motor  TwoThetaM SIM 30 100 .0000001 1.
Motor  MonoX     SIM -30 30 .00000001 3.0
Motor  MonoY     SIM -30 30 .000000001 3.0
Motor  CurveM    SIM 0 20 .000000001 3.0
Motor  MonoPhi   SIM -30 30 .00000001 3.0
Motor  MonoChi   SIM -30 30 .00000001 3.0

# sample Table
Motor  Table     SIM -180 360 .0000001 2.
Motor  TwoThetaD SIM -10  120 .0000001 1.
#-------------------------------------------------------------
# Monochromator
#-------------------------------------------------------------
MakeMono Mono "PG-002" OmegaM TwoThetaM CurveM
Mono DD 3.3537
Mono vk1 -0.025942
Mono vk2 5.351660
# Syntax MakeWaveLength name MonochromatorToUse
MakeWaveLength lambda  Mono
#--------------------------------------------------------------------------
#                          Configure Detector
# NOTE: the McStas objects have to be created first, otherwise
# the initialisation will fail.
#--------------------------------------------------------------------------
MakeMcStasReader
MakeMcStasController
allowexec $home/dmcafter
allowexec $home/dmc_sics05

ClientPut "Installing counter"

MakeCounter counter mcstas
counter SetExponent 1
MakeHM banana mcstas
banana configure HistMode Normal
banana configure OverFlowMode    Ceil
banana configure Rank     1
banana configure dim0 400
banana configure BinWidth 4
banana preset 100.
banana CountMode Timer
banana configure Counter counter
banana configure init 0
banana init
#banana exponent 3
#-------------------------------------------------------------------------
# Aliases
#-------------------------------------------------------------------------
SicsAlias OmegaM    A1
SicsAlias TwoThetaM A2
SicsAlias Table     A3
SicsAlias TwoThetaD A4
SicsAlias MonoX     A5
SicsAlias MonoY     A6
SicsAlias MonoPhi   A7
SicsAlias MonoChi   A8
SicsAlias CurveM    A9
DefineAlias TT temperature
#--------------------------------------------------------------------------
#                        V A R I A B L E S

# now a few general variables are created
# Syntax: VarMake name type access
# type can be one of: Text, Int, Float
#access can be one of: Internal, Mugger, User, Spy
#-----------------------------------------------------------------------------
VarMake SicsDataPath Text Internal
SicsDataPath "$home/"
SicsDataPath lock
VarMake DetStepWidth Float Internal
DetStepWidth 0.2
DetStepWidth lock
VarMake Instrument Text Internal 
Instrument "Virtual DMC driven by McStas"
Instrument lock 
VarMake Title Text User
VarMake User  Text User
VarMake Collimation Text User
VarMake SampleIntern Text User
SampleIntern Kellerit
VarMake comment1 Text User
VarMake comment2 Text User
VarMake comment3 Text User
VarMake starttime Text User
starttime ""
VarMake SicsDataPrefix Text Internal
SicsDataPrefix vdmc

#--------- make data number
MakeDataNumber SicsDataNumber $home/DataNumber
VarMake SicsDataPostFix Text Internal
SicsDataPostFix ".xml"
VarMake Adress Text User
VarMake phone  Text User
VarMake fax    Text User
VarMake email  Text User
VarMake sample_mur Float User
VarMake lastdatafile Text User
VarMake lastscancommand Text User
lastscancommand "unknown scan"
#--------------------------------------------------------------------------
#                           P R O C E D U R E S
#--------------------------------------------------------------------------
MakeDrive
MakeBatchManager
MakeNXScript
MakeRuenBuffer
#------------------------------------------------------------------------
# simulated scanning for demo purposes
#-----------------------------------------------------------------------
MakeCounter lieselotte SIM -1
MakeMultiCounter scanCter lieselotte
#------------------------------
proc SICSValue {command} {
	set txt [eval $command]
	set l [split $txt =]
	return [string trim [lindex $l 1]]
}
#-----------------------------------------------------------------------
proc scantransfer {} {
    set FWHM 1.5
    set pos 5.33
    set height 700
    set stddev [expr $FWHM/2.354]
    set ftmp [expr ([SICSValue a3] - $pos)/$stddev]
    set count [expr 10 + $height*0.4*exp(-.5*$ftmp*$ftmp)]
    set counti [expr int($count)]
    append res [SICSValue "lieselotte gettime"] " "
    append res $counti " "
    for {set i 1} {$i < 7} {incr i} {
	append res [SICSValue "lieselotte getmonitor $i"] " "
    } 
    return $res
}
scancter transferscript scantransfer

MakeScanCommand xxxscan scancter $home/dmc.hdd $home/recover.bin
MakePeakCenter xxxscan

#-------------------- initialize scripted commands
source $home/vdmccom.tcl
#-------------------- configure commandlog
commandlog auto
commandlog intervall 5

#==================== install Hipadaba
proc hdbReadOnly {} {
	error "Parameter is READ ONLY"
} 
#------------------------------------
proc maketwotheta {} {
	set txt [TwoThetaD]
	set l [split $txt =]
	set start [string trim [lindex $l 1]]
	for {set i 0} {$i < 400} {incr i } {
		append result [expr $start + $i * .2] " "
	}
	return $result
}
#-------------------------------------
InstallHdb
MakeStateMon
MakeHdbQueue hdbqueue HdbQueue
hmake /instrument spy none
hsetprop /instrument type instrument
#-------- experiment
hmake /instrument/experiment spy none
hattach /instrument/experiment title title
hattach /instrument/experiment starttime starttime
hattach /instrument/experiment user user
hattach /instrument/experiment/user adress address
hattach /instrument/experiment/user phone phone
hattach /instrument/experiment/user email email
hattach /instrument/experiment comment1 comment1
hattach /instrument/experiment comment2 comment2
hattach /instrument/experiment comment3 comment3
#-------  SINQ
hmake /instrument/sinq spy none
hmake /instrument/sinq/proton_monitor internal int
hattach /instrument/sinq/proton_monitor  counter 4  
#-------- monochromator
hmake /instrument/monochromator spy none    
hattach /instrument/monochromator lambda wavelength
hsetprop /instrument/monochromator/wavelength priv user
hattach /instrument/monochromator OmegaM theta
hattach /instrument/monochromator TwoThetaM two_theta
hchain /instrument/monochromator/wavelength /instrument/monochromator/two_theta
hattach /instrument/monochromator MonoX x_translation
hattach /instrument/monochromator MonoY y_translation
hattach /instrument/monochromator MonoChi chi
hattach /instrument/monochromator MonoPhi phi
hattach /instrument/monochromator CurveM vertical_focusing
hmakescript /instrument/monochromator/d_value "mono dd" "mono dd" float
hsetprop /instrument/monochromator/d_value priv manager
hmakescript /instrument/monochromator/scattering_sense  "mono ss" "mono ss" int
hsetprop /instrument/monochromator/scattering_sense priv manager

#----------- sample
hmake /instrument/sample spy none
hmakescript /instrument/sample/name sample sample Text 
hattach /instrument/sample Table rotation
hmake /instrument/sample/monitor internal int
hattach /instrument/sample/monitor counter 1
hsetprop /instrument/sample/monitor priv internal
hsetprop /instrument/sample/monitor sicsdev histogrammemory
#---------- detector
hmake /instrument/detector spy none
hattach /instrument/detector TwoThetaD two_theta   
hmakescript /instrument/detector/preset "banana preset" hdbReadOnly float
hsetprop /instrument/detector/preset priv internal
hmakescript /instrument/detector/countmode "banana countmode" hdbReadOnly  text
hsetprop /instrument/detector/countmode priv internal
sicspoll add /instrument/detector/preset hdb 30
sicspoll add /instrument/detector/countmode hdb 30
hmake /instrument/detector/count_time internal float
hattach /instrument/detector/count_time counter -1
#------------ commands
hmake /instrument/commands spy none
hcommand /instrument/commands/count count
hsetprop /instrument/commands/count type command
hsetprop /instrument/commands/count priv user
hmake /instrument/commands/count/mode user text
hsetprop /instrument/commands/count/mode values "monitor,timer"
hmake /instrument/commands/count/preset user float
hset /instrument/commands/count/preset 60000
hset /instrument/commands/count/mode monitor
hcommand /instrument/commands/killfile killfile
hsetprop /instrument/commands/killfile type command
hsetprop /instrument/commands/killfile priv manager

#------------- scan command
hcommand /instrument/commands/scan hdbscan
hsetprop /instrument/commands/scan type command
hsetprop /instrument/commands/scan priv user
hsetprop /instrument/commands/scan viewer mountaingumui.ScanEditor
hmake /instrument/commands/scan/scan_variables user text
hsetprop /instrument/commands/scan/scan_variables argtype drivable
hmake /instrument/commands/scan/scan_start user text
hmake /instrument/commands/scan/scan_increments user text
hmake /instrument/commands/scan/NP user int
hmake /instrument/commands/scan/mode user text
hsetprop /instrument/commands/scan/mode values "timer,monitor"
hmake /instrument/commands/scan/preset user float
hset /instrument/commands/scan/mode timer
hset /instrument/commands/scan/scan_start 2.
hset /instrument/commands/scan/scan_increments .3
hset /instrument/commands/scan/NP 25
hset /instrument/commands/scan/preset 2

hcommand /instrument/commands/wait wait
hsetprop /instrument/commands/wait type command
hsetprop /instrument/commands/wait priv user
hmake /instrument/commands/wait/time user int

#---------------- graphics
hmake /graphics spy none
hmake /graphics/powder_diagram spy none
hattach /graphics/powder_diagram title title
hsetprop /graphics/powder_diagram type graphdata
hsetprop /graphics/powder_diagram viewer default
hmake /graphics/powder_diagram/rank internal int
hset /graphics/powder_diagram/rank 1
hmake /graphics/powder_diagram/dim internal intar 1
hset /graphics/powder_diagram/dim 400
hmakescript /graphics/powder_diagram/two_theta maketwotheta hdbReadOnly floatar 400
hchain /graphics/powder_diagram/two_theta /instrument/detector/two_theta
hsetprop /graphics/powder_diagram/two_theta type axis
hsetprop /graphics/powder_diagram/two_theta transfer zip
hsetprop /graphics/powder_diagram/two_theta dim 0
hattach /graphics/powder_diagram banana counts
hsetprop /graphics/powder_diagram/counts type data
hsetprop /graphics/powder_diagram/counts transfer zip
hsetprop /graphics/powder_diagram/counts priv internal
sicspoll add /graphics/powder_diagram/counts hdb 60

hmake /graphics/scan_data spy none
hsetprop /graphics/scan_data type graphdata
hsetprop /graphics/scan_data viewer default
hmake /graphics/scan_data/rank mugger int
hset /graphics/scan_data/rank 1
hsetprop /graphics/scan_data/rank priv internal
hmakescript /graphics/scan_data/dim "xxxscan np" hdbReadOnly intar 1
hsetprop /graphics/scan_data/dim priv internal
hmakescript /graphics/scan_data/scan_variable "gethdbscanvardata 0" hdbReadOnly floatvarar 1
hsetprop /graphics/scan_data/scan_variable type axis
hsetprop /graphics/scan_data/scan_variable dim 0
hsetprop /graphics/scan_data/scan_variable transfer zip
hsetprop /graphics/scan_data/scan_variable priv internal
hmakescript /graphics/scan_data/counts "gethdbscancounts" hdbReadOnly intvarar 1
hsetprop /graphics/scan_data/counts type data
hsetprop /graphics/scan_data/counts transfer zip
hsetprop /graphics/scan_data/counts priv internal

hmake /graphics/samenv spy none
hsetprop /graphics/samenv type graphdata
hsetprop /graphics/samenv viewer mountaingumui.TimeSeries
hmake /graphics/samenv/vars user text
hset /graphics/samenv/vars tomato
hmake /graphics/samenv/rank user int
hset /graphics/samenv/rank 1
hmake /graphics/samenv/dim user intar 1
hset /graphics/samenv/dim 300
hmake /graphics/samenv/getdata user text
hsetprop /graphics/samenv/getdata type logcommand
hsetprop /graphics/samenv/getdata datacom true
hmake /graphics/samenv/getdata/starttime spy text
hmake /graphics/samenv/getdata/endtime spy text

hmake /batch spy none
hmakescript /batch/bufferlist listbatchfiles hdbReadOnly text
sicspoll add /batch/bufferlist hdb 30
hmake /batch/commandtext spy text
hsetprop /batch/commandtext viewer mountaingumui.TextEdit
hsetprop /batch/commandtext commandtext true


hmake /gui spy none
hmake /gui/status internal text
status hdbinterest /gui/status

proc makeQuickPar {name path} {
    hmake /quickview/$name mugger text
    hset /quickview/$name $path
}

hmake /quickview spy none
makeQuickPar title /instrument/experiment/title
makeQuickPar sample /instrument/sample/name
makeQuickPar lambda /instrument/monochromator/wavelength
makeQuickPar two-theta /instrument/detector/two_theta
makeQuickPar preset /instrument/detector/preset
makeQuickPar monitor /instrument/sample/monitor

restore

