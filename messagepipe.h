/**
 * An implementation for a message pipe system as described in
 * 
 * http://eventuallyconsistent.net/2013/08/14/messaging-as-a-programming-model-part-2/
 *
 * A message is passed through a series of filter routines which do things.
 *
 * copyright: GPL Copyleft
 *
 * Mark Koennecke, November 2013
 */
#ifndef __MESSAGEPIPE
#define __MESSAGEPIPE

#define MPCONTINUE 1
#define MPSTOP 0 

/**
 * The messagePipe data structure defined in messagepipe.c
 */
typedef struct __MessagePipe *pMP;

/**
 * Protoype of the message filter function.
 * @param message The message to process
 * @param Optional private data for this message filter function
 * @return MPSTOP for stopping further processing of the filter pipe 
 * or MPCONTINUE to continue processing
 */
typedef int (*mpFilterFunc)(void *message, void *userData);

/**
 * A function to free the userData when it is used
 * @param userData The data structure passed in for userData to a filter function.
 */ 
typedef void (*mpUserKill)(void *userData);


/**
 * Create an empty message pipe
 * @return A message pipe structure or NULL when out of memory
 */
pMP MakeMP();

/**
 * Delete a message pipe 
 * @param self The message pipe to delete
 */
void KillMP(pMP self);

/**
 * Append a filter function to the list of message filters to process
 * @param self The message queue to append too
 * @param func The filter function to append
 * @param Optional userData to pass through to the filter function
 * @param usKill An optional function to free the userData when the message pipe gets deleted
 * @return 0 on success, 1 when out of memory
 */ 
int AppendMPFilter(pMP self, mpFilterFunc func, void *userData, mpUserKill usKill);

/**
 * Preppend a filter function to the list of message filters to process
 * @param self The message queue to append too
 * @param func The filter function to append
 * @param Optional userData to pass through to the filter function
 * @param usKill An optional function to free the userData when the message pipe gets deleted
 * @return An updated pointer to the message pipe.
 */ 
pMP PrependMPFilter(pMP self, mpFilterFunc func, void *userData, mpUserKill usKill);


/**
 * Process the message pipe with the given message
 * @param self The messae pipe to process
 * @param message The message to process.
 * @return MPCONTINUE on successfull completion, MPABORT when the chain was 
 * aborted for some reason.
 */
int MPprocess(pMP self, void *message);


#endif
