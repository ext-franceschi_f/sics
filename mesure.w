\subsection{Four Circle Single Counter Measurement Object}
This object implements a single counter reflection measurement routine
for four circle diffractometers. This object is able to read a reflection
listing, drive to each reflection in the list, do a scan on it, integrate
the scan results and write the results to ASCII files. 
Three files will be created as output: one file with the ending
.rfl which contains the reflection profiles for each reflection, a file .asc
which contains a summary in form of HKL, I, sigma(I) for each reflection
and a file ending .err which contains all the error messages obtained during
the run.

This module has evolved to support the following mode advanced feautures:
\begin{itemize}
\item Vary step width and scanned variable in dependence on two theta
\item Calculation mode in order to determine the number of reflections which
can actually be measured from a given list.
\item Remeasuring of weak reflections.
\item Fast scans.
\end{itemize}

The interface to this object consists of these functions:

@d mesureint @{
 typedef struct __Mesure *pMesure;
/*--------------------- live & death --------------------------------------*/
  pMesure CreateMesure(pHKL pCryst, pScanData pScanner, 
                       pMotor pOmega, char *pom,
		       pMotor p2Theta, char *p2t,
                       char *pFileRoot,pDataNumber pDanu, char *headerTemplate);
  void    DeleteMesure(void *pData);

  int MesureFactory(SConnection *pCon, SicsInterp *pSics, void *pData,
                    int argc, char *argv[]);
/*------------------- object functions -----------------------------------*/
  int MesureReflection(pMesure self, float fHKL[3], float fPsi,
	SConnection *pCon);
  int MesureGenReflection(pMesure self, float fHKL[3],float fSet[4], SConnection *pCon);
  int MesureStart(pMesure self, SConnection *pCon);
  int MesureReopen(pMesure self, char *filename, SConnection *pCon);
  int MesureClose(pMesure self);
  int MesureFile(pMesure self, char *pFile, int iSkip, SConnection *pCon);
  int MesureGenFile(pMesure self, char *pFile, int iSkip, SConnection *pCon);
  int MesureSetPar(pMesure self, char *name, float fVal);
  int MesureGetPar(pMesure self, char *name, float *fVal);

  int MesureAction(SConnection *pCon, SicsInterp *pSics, void *pData,
                    int argc, char *argv[]);
  
@}
All functions return 1 on success, 0 on failure.
\begin{description}
\item[CreateMesure] creates a new pMesure object. Parameters are the HKL
object to use for cystallographic conversions and the scan object to use for
doing the scans. This is followd by the motor for running omega, its name
and the name of the motor for driving omega 2 theta. Finnaly the path to the
data directory and the data number object for automatic filename creation is
specified.
\item[DeleteMesure] wipes the mesure object pData from memory.
\item[MesureFactory] is the factory function for mesure which will be used
by the interpreter to  install mesure into the system.
\item[MesureReflection] measures the single reflection fHKL. It drives
there, calculates scan borders and performs the scan.
\item[MesureGenReflection] measures the single reflection fHKL. It drives
there, calculates scan borders and performs the scan. This version uses
supplied values for the instrument settings.
\item[MesureStart] sets everything up for a new measurement with new file
names.
\item[MesureReopen] reopens the files belonging to the file name given as
base for appending. This is a recovery feature.
\item[MesureFile] opens the file specified as second parameter and reads
each line. Expected is a reflection to measure. Each reflection is then
measured and output written. The last parameter iSkip allows to skip iSkip
lines of the reflection file. This facility exists in order to restart work
on an reflection file which had been interrupted for some reason.
\item[MesureGenFile] opens the file specified as second parameter and reads
each line. Expected is a reflection to measure. Each reflection is then
measured and output written. The last parameter iSkip allows to skip iSkip
lines of the reflection file. This facility exists in order to restart work
on an reflection file which had been interrupted for some reason. This
version acts upon files created by the program HKLGEN.
\item[MesureSetPar] sets the parameter name of Mesure to fVal. A listing of
possible parameters is given below.
\item[MesureGetPar] returns the value of the parameter name in fVal.
\item[MesureAction] implements the interpreter interface to the mesure
object. 
\end{description}
Mesure supports the following parameters:
\begin{description}
\item[np] the number of points per scan.
\item[preset] The preset value for counting.
\item[countmode] the counting mode, can be 0 for timer or 1 for monitor
mode. 
\end{description}


\subsubsection{Four Circle Table}
In order to support the variation of scan parameters with two theta a table is needed which
holds the necessary parameters. This table is implemented in a separate module. The 
interface to this module is:
@d fourtableint @{
	int MakeFourCircleTable();
	void DeleteFourCircleTable(int handle);
	int HandleFourCircleCommands(int *handle, SConnection *pCon, 
		int argc, char *argv[], int *err);
	char  *GetFourCircleScanVar(int handle, double two_theta);
	double GetFourCircleStep(int handle, double two_theta);
	int  SaveFourCircleTable(int handle, char *objName, FILE *fd);
	float GetFourCirclePreset(int handle, double twoTheta);
	int GetFourCircleScanNP(int handle, double twoTheta);
@}
Many functions takes as the first argument a handle to the four circle table as
 created by MakeFourCircleTable.
\begin{description}
\item[MakeFourCircleTable] creats a four circle table. Returns a handle with which
the table can be referred to later.
\item[DeleteFourCircleTable] removes a four circle table. The single argument is
 a handle to a four circle tbale as created by MakeFourCircle.
\item[HandleFourCircleCommands] handles interpreter commands which allow to
 edit or list the table described by handle. pCon is used for output. argc, argv are
 the arguments to the command. The returns 1 when the command was handled, 0 else.
 If there was an error, err is set to 0. 
\item[GetFourCircleScanVar] retrieves the scan variable to use for the two theta 
 value given.
\item[GetFourCircleStep] retrieves the step width to use for the two theta 
 value given.
\item[SaveFourCircleTable] saves the configuration of the table into fd. objName is the 
name of the object to which this table belongs.
\end{description}


@o mesure.h @{
/*----------------------------------------------------------------------------
                         M E S U R E

 A SICS object for doing four circle measurements with a single
 counter. 

 copyright: see copyright.h

 Mark Koennecke, April 1998
 Heavily reworked: Mark Koennecke, February-March 2005 
---------------------------------------------------------------------------*/
#ifndef SICSMESURE
#define SICSMESURE
@<mesureint@>
#endif
@}

@o fourtable.h @{
/*---------------------------------------------------------------------------
                  F O U R T A B L E

 A SICS object which holds the variation of scan parameters for four circle
 reflection list measurements.
 copyright: see copyright.h

 Mark Koennecke, February 2005
---------------------------------------------------------------------------*/
#ifndef FOURTABLE
#define FOURTABLE
@<fourtableint@>
#endif
@}