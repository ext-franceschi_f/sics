/*--------------------------------------------------------------------------
	SICS needs to run motors. This is where this happens.
	
	Mark Koennecke, November 1996
	
	copyright: see implementation file
	
----------------------------------------------------------------------------*/
#ifndef SICSMOTOR
#define SICSMOTOR
#include "Scommon.h"
#include "obpar.h"
#include "modriv.h"
#include "obdes.h"
#include "interface.h"
#include "hipadaba.h"

#define MOTOBPARLENGTH 12
typedef struct __Motor {
  pObjectDescriptor pDescriptor;
  pHdb objectNode;
  pIDrivable pDrivInt;
  pICallBack pCall;
  int (*MotorSetPar) (struct __Motor * self, SConnection * pCon,
                      char *name, float fVal);
  int (*MotorGetPar) (struct __Motor * self, char *name, float *fVal);
  int (*MotorGetHardPosition) (struct __Motor * self, SConnection * pCon,
                               float *fVal);
  char *drivername;
  char *name;
  char *error;
  MotorDriver *pDriver;
  float fTarget;
  float fPosition;
  long endScriptID;
  int posCount;                 /* counter for calling the
                                   motor callback */
  int retryCount;               /* for retries in status */
  int posFaultCount;
  int stopped;
  int stopReported;
  int errorCount;
  int running;
  int moving;
  double last_report_time;
  ObjectFunc pActionRoutine;
  ObPar *ParArray;
  void *pPrivate;
  void (*KillPrivate) (void *);
} Motor;
typedef Motor *pMotor;
/*------------------------------------------------------------------------
   a tiny structure used in CallBack work
--------------------------------------------------------------------------*/
typedef struct {
  float fVal;
  char *pName;
} MotCallback;
/*-------------------------------------------------------------------------*/

/* parameter management */
int MotorGetPar(pMotor self, char *name, float *fVal);
int MotorSetPar(pMotor self, SConnection * pCon, char *name, float fVal);

/* driving */
long MotorRun(void *self, SConnection * pCon, float fNew);
int MotorCheckBoundary(pMotor self, float fVal, float *fHard,
                       char *error, int iErrLen);

/* Where are we ? */
int MotorGetSoftPosition(pMotor self, SConnection * pCon, float *fVal);
int MotorGetHardPosition(pMotor self, SConnection * pCon, float *fVal);

/* creation */
int MotorCreate(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);
pMotor MotorInit(char *drivername, char *name, MotorDriver * pDriv);
void MotorKill(void *self);

/* interface to interpreter */
int MotorAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);
pMotor FindMotor(SicsInterp * pSics, char *name);

/* Made available to oscillate.c to generate MOTEND event. Ferdi */
void finishDriving(pMotor self, SConnection *pCon);
#endif
