
/*----------------------------------------------------------------------
  Support module which manages a list of motors and their target values
  when running complex movements. See accompanying tex file for
  more info.

  copyright: see file COPYRIGHT

  Mark Koennecke, September 2005
-----------------------------------------------------------------------*/
#ifndef SICSMOTLIST
#define SICSMOTLIST
#include "sics.h"

typedef struct {
  char name[80];
  float target;
  float position;
  pIDrivable pDriv;
  void *data;
  int running;
  TaskTaskID taskID;
} MotControl, *pMotControl;

/*======================================================================*/

pIDrivable makeMotListInterface();
int addMotorToList(int listHandle, char *name, float targetValue);
int setNewMotorTarget(int listHandle, char *name, float value);
int getMotorFromList(int listHandle, char *name, pMotControl tuk);
float getListMotorPosition(int listHandle, char *name);
void printMotorList(int listHandle, SConnection * pCon);

#endif
