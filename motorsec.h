/**
 * This is the header file for the second generation motor object. 
 * More details in the header of the implementation file.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, December 2008
 */
#ifndef MOTORSEC_H_
#define MOTORSEC_H_
int SecMotorFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
                    int argc, char *argv[]);

#endif                          /*MOTORSEC_H_ */
