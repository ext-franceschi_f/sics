
/*-------------------------------------------------------------------------
                        R e g M o t

  This is  a helper module for the Anti Collider. It handles all the
  stuff necessary for dealing with a single motor. For more
  information see the file anticollider.tex.

  copyright: see file copyright
 
  Mark Koennecke, August 2002
-----------------------------------------------------------------------*/
#ifndef REGMOT
#define REGMOT
#include "sics.h"



typedef struct __MOTREG {
  void *motorData;
  char *motorName;
  float targetPosition;
  long (*originalSetValue) (void *motorData,
                            SConnection * pCon, float fTarget);
  int (*originalCheckStatus) (void *motorData, SConnection * pCon);
  int iActive;
} MotReg, *pMotReg;


/*----------------------------------------------------------------------*/

pMotReg RegisterMotor(char *name, SicsInterp * pSics,
                      long (*SetValue) (void *pData, SConnection * pCon,
                                        float fTarget),
                      int (*CheckStatus) (void *pData,
                                          SConnection * pCon));
void KillRegMot(void *self);

void SetRegMotTarget(pMotReg self, float target);
void CreateTargetString(pMotReg self, char pBueffel[80]);

int RegMotMatch(pMotReg self, char *name);

int StartRegMot(pMotReg self, SConnection * pCon, float fValue);

int CheckRegMot(pMotReg self, SConnection * pCon);



#endif
