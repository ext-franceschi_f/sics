
/*-----------------------------------------------------------------------
 A couple of utility functions for handling a list of MotReg
 structures . This is a helper module for the anticollider collision
 control system. See anticollider.tex for more details.

 copyright: see file copyright

 Mark Koennecke, August 2002
-------------------------------------------------------------------------*/
#ifndef MOTREGLIST
#define MOTREGLIST

#include "motreg.h"


int MakeMotList();
pMotReg FindMotEntry(int iList, char *name);
pMotReg FindMotFromDataStructure(int iList, void *pData);
int CheckAllMotors(int iList, SConnection * pCon);
void KillMotList(int iList);
void StopAllMotors(int iList);
void DeactivateAllMotors(int iList);




#endif
