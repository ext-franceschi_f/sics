/*--------------------------------------------------------------------------

			Networking for SICS
			
	This module implements some networking functionality based on 
	TCP/IP sockets.
	
	
	Mark Koennecke, October 1996
	
	Free for non-commercial use, no warranties taken!

	Added functions:
	    NETavailable
            NETReadTillTerm
        in order to support RS-232 connection through a terminal server.

	Mark Koennecke, October 2001
	
	Changed all timeout units from microseconds to milliseconds
	Markus Zolliker August 2004
----------------------------------------------------------------------------*/
#ifndef NNNET
#define NNNET
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

typedef struct __MKCHANNEL {
  int sockid;
  int iType;
  struct sockaddr_in adresse;
  long lMagic;
} mkChannel;

#define NETMAGIC 29121993

/*========================= exported functions ============================*/

/*               C O N N E C T I O N    O R I E N T E D                    */

/**********************   OPENING  **************************************  */

mkChannel *NETOpenPort(int iPort);
        /* opens a ServerPort for listening, returns NULL if failure,
           else a valid mkChannel structure for the port
         */

mkChannel *NETAccept(mkChannel * self, long timeout);
        /* tries to accept a new connection on the Channel self
           until timeout. If a connection can be built a new mkChannel
           structure is returned, else NULL. With a negative value or 0 for
           timeout this function blocks for an accept.
         */

mkChannel *NETConnect(char *name, int port);
        /* tries to open a client connection to the server specified by name 
           and port. Returns NULL on failure, a struct else
         */

mkChannel *NETConnectWithFlags(char *name, int port, int flags);
        /* the same as NETConnect, but with flags:
           if (flags & 1): do not block on connect (use NETConnectFinished
           to check success)
           if (flags & 2): wait 1000 ms after last close (workaround for
           a bug in the Lantronix terminal server)
         */

int NETConnectFinished(mkChannel * self);
        /*  returns 0 if in progress, 1 on success, a negative value on error
         */

int NETInfo(mkChannel * self, char *pComposter, int iBufLen);
        /* Once a socket is connected it is possible to figure out
           which host the connection came from. Maximum iBufLen characters
           of hostname are copied to pComposter
         */

int NETReconnect(mkChannel * self);
        /* If a connection has been lost, try to reconnect using the same
         * socket id if possible. Blocks for up to one second.
         *  returns 0 if in progress, 1 on success, a negative value on error
         */

int NETReconnectWithFlags(mkChannel * self, int flags);
        /* If a connection has been lost, try to reconnect using the same
         * socket id if possible. If (flags & 1) do not block, use
         * NETConnectFinished to check success.
         *  returns 0 if in progress, 1 on success, a negative value on error
         */
/* *********************** DATA TRANSFER ******************************** */

int NETWrite(mkChannel * self, char *buffer, long lLen);
        /* writes data to socket self, returns 1 if success,
           0 otherwise.
         */

long NETRead(mkChannel * self, char *buffer, long lLen, long timeout);
        /* reads data from socket self into buffer with max length lLen
           waits maximum timeout for data. Returns -1 on error, 0 on no
           data, and else the length of the data read. With a negative value
           or 0 for timeout this function blocks for the read.
         */
int NETAvailable(mkChannel * self, long timeout);
        /* returns 1 if data is pending on the port, 0 if none is
           pending.
         */
int NETReadTillTerm(mkChannel * self, long timeout,
                    char *pTerm, char *pBuffer, int iBufLen);
        /*
           reads data until one of the terminators defined in pTerm has
           been found. The data is copied into the buffer pBuffer. A
           maximum length of iBufLen characters is observed. The timeout
           parameter defines a maximum time to wait for a terminator to
           appear. NETReadTillTerm returns the number of characters read
           (including terminator) on success, 0 on a timeout,
           and a negative value if a network error occurred. Beware that 
           this may not work correctly if the wrong terminator is given.
           The last one is really needed.
           In the new version, timeout is in MILLIseconds (10 -3 sec).
           However, the accuracy is machine dependent (for Linux 10 ms, for Tru64 1 ms)
           If no terminator is given, the routine waits for iBufLen characters
           or timeout.
         */
int NETReadRemob(mkChannel *self, long timeout, long timeout2,
                 char term, char *pBuffer, int iBufLen);
    /* special version for remob */
/*  ********************* KILLING FIELD ******************************** */
int NETClosePort(mkChannel * self);
        /* closes a port, do not forget to free the channel data-
           structure afterwards, returns True on success, False else
         */

/*################## ConnectionLess functions ##########################*/
mkChannel *UDPOpen(int iPort);
       /* opens a port connectionless communications.
        */
mkChannel *UDPConnect(char *name, int iPort);
       /* connects a client for connectionless communication
        */

       /* can use NETClosePort */

long UDPRead(mkChannel * self, char *buffer, long lLen, int timeout);
int UDPWrite(mkChannel * self, char *buffer, long lLen);

#endif
