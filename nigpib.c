/*
  This is an implementation of a GPIB driver based on the drivers
  provided by National Instruments. The driver has been tested 
  with the ENET-100 GPIB-TCP/IP bridge but should also work with
  NI GPIB boards plugged into the composter.

  copright: see file COPYRIGHT
 
  Mark Koennecke, January 2002
*/

#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <tcl.h>
#include "fortify.h"
#include "sics.h"
#include <ugpib.h>
#include "gpibcontroller.i"

typedef struct __GPIB *pGPIB;
/*--------------------------------------------------------------------------*/
static int NIattach(int boardNo, int address, int secondaryAddress,
                    int tmo, int eot, int eos)
{
  int devID;

  devID = ibdev(boardNo, address, secondaryAddress, tmo, eot, eos);
  if (devID < 0) {
    return -iberr;
  } else {
    return devID;
  }
}

/*---------------------------------------------------------------------*/
static int NIdetach(int devID)
{
  int status;

  status = ibonl(devID, 0);
  if (status & ERR) {
    return -iberr;
  } else {
    return 1;
  }
}

/*-------------------------------------------------------------------*/
static int NIwrite(int devID, void *buffer, int bytesToWrite)
{
  int status;

  status = ibwrt(devID, buffer, bytesToWrite);
  if (status & ERR) {
    return -iberr;
  } else {
    return 1;
  }
}

/*-------------------------------------------------------------------*/
static int NIread(int devID, void *buffer, int bytesToRead)
{
  int status;

  status = ibrd(devID, buffer, bytesToRead);
  if (status & ERR) {
    return -iberr;
  } else {
    return 1;
  }
}

/*--------------------------------------------------------------------*/
static int NIclear(int devID)
{
  ibclr(devID);
  return 1;
}

/*-----------------------------------------------------------------*/
static void NIerror(int code, char *buffer, int maxBuffer)
{
  int flag = -code;

  switch (flag) {
  case EDVR:
  case ENEB:
  case ECIC:
    strlcpy(buffer, "NI-GPIB not correctly installed or bad address",
            maxBuffer);
    return;
  case EABO:
    strlcpy(buffer, "Timeout on data transfer", maxBuffer);
    return;
  case EBUS:
    strlcpy(buffer, "No device connected to GPIB or address errror",
            maxBuffer);
    return;
  case ENOL:
    strlcpy(buffer, "No listeners on bus. Perhaps address error?",
            maxBuffer);
    return;
  default:
    strlcpy(buffer, "Unrecognised error code, fix nigpib.c", maxBuffer);
    break;
  }
}

/*------------------------------------------------------------------*/
void NIassign(pGPIB self)
{
  self->attach = NIattach;
  self->detach = NIdetach;
  self->send = NIwrite;
  self->read = NIread;
  self->getErrorDescription = NIerror;
  self->clear = NIclear;
}
