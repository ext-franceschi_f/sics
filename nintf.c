/*----------------------------------------------------------------------
  Some systems, such as linux, miss nintf in the math library. This
  is an implementation.

  Mark Koennecke, February 2000
  
  Added double version, Mark Koennecke, August 2008
  ---------------------------------------------------------------------------*/

#include <math.h>
float nintf(float f)
{
  double ip, rm, dVal;
  float fRes;

  dVal = (double) f;
  rm = modf(dVal, &ip);
  if (rm < .0)
    rm = -rm;
  if (rm > .5) {
    if (ip < .0)
      ip -= 1.;
    else
      ip += 1.;

  }
  return (float) ip;
}

/*-------------------------------------------------------------------*/
double nintd(double f)
{
  double ip, rm, dVal;
  double fRes;

  dVal = (double) f;
  rm = modf(dVal, &ip);
  if (rm < .0)
    rm = -rm;
  if (rm > .5) {
    if (ip < .0)
      ip -= 1.;
    else
      ip += 1.;

  }
  return (double) ip;
}
