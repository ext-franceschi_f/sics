
/*------------------------------------------------------------------------
                    N E T   R E A D E R

  This module will check for pending requests to the SICS server and
  initiate apropriate actions.

  Mark Koenencke, September 1997

  copyright: see copyright.h

----------------------------------------------------------------------------*/
#ifndef SICSNETREADER
#define SICSNETREADER

/*--------------------------------------------------------------------------*/
typedef struct __netreader *pNetRead;
typedef enum { naccept, command, udp, user, taccept, tcommand } eNRType;
/*--------------------------------------------------------------------------*/
pNetRead CreateNetReader(pServer pServ, int iPasswdTimeout,
                         int iReadTimeout);
void DeleteNetReader(void *pData);
/*--------------------------------------------------------------------------*/
int NetReadRegister(pNetRead self, mkChannel * pSock, eNRType eType,
                    SConnection * pCon);
int NetReadRegisterUserSocket(pNetRead self, int iSocket);
int NetReadRemove(pNetRead self, mkChannel * pSock);
int NetReadRemoveUserSocket(pNetRead self, int iSocket);
/*-------------------------------------------------------------------------*/
int NetReaderTask(void *pReader);
void NetReaderSignal(void *pUser, int iSignal, void *pSigData);
int NetReadWait4Data(pNetRead self, int iSocket);
int NetReadReadable(pNetRead self, int iSocket);
int NetReadResetUser(pNetRead self, int iSocket);
/*--------------------------------------------------------------------------*/
int NetReadInstallANETPort(pNetRead self, eNRType eType, int iPort);

#endif
