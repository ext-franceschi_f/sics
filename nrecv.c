#include "nrecv.h"
#include "future.h"
#include "macro.h"
#include "nwatch.h"
#include "perfmon.h"
#include "sics.h"
#include "sicszmq.h"
#include "task.h"
#include <json.h>

void json_print(json_object * jobj, int indent);
void json_print_value(json_object * jobj, int indent);
void json_print_array(json_object * jobj, char *key, int indent);

/*--------------------------------------------------------------------------*/
extern pPerfMon pMonHR;
extern pPerfMon pMonLR;
/*--------------------------------------------------------------------------*/
typedef struct __task {
  TaskTaskID taskID;
  pTaskQueue queue;
} TQ;
static TQ jsnTask = {0};
static TQ scsTask = {0};
static TQ tclTask = {0};
static TQ datTask = {0};
static TQ devTask = {0};
static TQ srcTask = {0};

/**
 * ZeroMQ ROUTER socket context
 */
typedef struct zsock_context {
  HANDLE_T zhandle;       /**< ZMQ ROUTER socket handle */
  pNWContext nhandle;     /**< NetWatch handle */
} ZSOCK_CONTEXT;

/**
 * Task Message Command Payload
 */
typedef struct command_context {
  ZSOCK_CONTEXT *pRtr;    /**< ZMQ ROUTER socket context */
  void *pMsg;             /**< ZMQ received multipart message */
  json_object *msg_json;  /**< JSON message object */
  pTaskPromise promise;   /**< Promise to fulfil */
} COMMAND_CONTEXT;

/*--------------------------------------------------------------------------*/
static void process_deferred(pTaskQueue queue,
                             pTaskMessage pTMsg,
                             COMMAND_CONTEXT *pCtx,
                             json_object *msg_json);
static void process_immediate(void (*func)(void *),
                             pTaskMessage pTMsg,
                             COMMAND_CONTEXT *pCtx,
                             json_object *msg_json);
/*--------------------------------------------------------------------------*/
/**
 * Example JSON immediate responder command
 *
 * Also provides a ping-pong facility for connection testing through to
 * the JSON message handling.
 */
static void PingPongCommand(void *data)
{
  COMMAND_CONTEXT *self = (COMMAND_CONTEXT *) data;
  json_object *node;
  const char *json_text;
  extern double DoubleTime(void);
  const char *pFlag = "OK";
  const char *pResp = "JSON error";
  char text[80];
  if (self == NULL)
    return;
  /* interpret the command again */
  if (json_object_object_get_ex(self->msg_json, "cmd", &node)) {
    const char *pCmd = json_object_get_string(node);
    if (strcasecmp("PING", pCmd) == 0) {
      /* PING=>PONG */
      pResp = "PONG";
    } else if (strcasecmp("PERF", pCmd) == 0) {
      /* PERF=> High Resolution Performance Counter */
      double perf = 0.0;
      if (pMonHR)
        perf = GetPerformance(pMonHR);
      snprintf(text, sizeof(text), "%.2f", perf);
      pResp = text;
    } else {
      /* Send it back */
      pResp = pCmd;
    }
  }
  json_object_object_add(self->msg_json, "final",
                         json_object_new_boolean(true));
  json_object_object_add(self->msg_json, "flag",
                         json_object_new_string(pFlag));
  json_object_object_add(self->msg_json, "reply",
                         json_object_new_string(pResp));
  json_object_object_add(self->msg_json, "ts",
                         json_object_new_double(DoubleTime()));
  json_text = json_object_to_json_string(self->msg_json);

  sics_zmq_router_reply(self->pRtr->zhandle, self->pMsg, json_text);
  json_object_put(self->msg_json);
  sics_zmq_router_free(self->pMsg);
}

/*--------------------------------------------------------------------------*/
/**
 * Default future response handler
 *
 * Generate a response based on the future state.
 */
static void deferredResponse(pTaskFuture fut)
{
  void *expn = NULL;
  void *rslt = NULL;
  char *pFlag = "OK";
  char *pResp = NULL;
  bool final = false;
  const char *json_text;
  COMMAND_CONTEXT *self = FutureGetContext(fut);

  if (FutureIsRunning(fut)) {
    final = false;
    pFlag = "OK";
    pResp = "RUNNING";
  } else if (FutureIsCancelled(fut)) {
    final = true;
    pFlag = "OK";
    pResp = "CANCELLED";
  } else {
    if (FutureIsDone(fut)) {
      final = true;
      rslt = FutureGetResult(fut, 0.0);
      if (rslt) {
        pFlag = "OK";
        pResp = DynStringGetArray((pDynString) rslt);
      } else {
        expn = FutureGetException(fut, 0.0);
        if (expn) {
          pFlag = "ERROR";
          pResp = DynStringGetArray((pDynString) expn);
        }
      }
    } else {
      final = false;
      pFlag = "ERROR";
      pResp = "BROKEN";
      Log(ERROR, "sys", "Unexpected callback");
    }
  }
  json_object_object_add(self->msg_json, "final",
                         json_object_new_boolean(final));
  json_object_object_add(self->msg_json, "flag",
                         json_object_new_string(pFlag));
  json_object_object_add(self->msg_json, "reply",
                         json_object_new_string(pResp));
  json_object_object_add(self->msg_json, "ts",
                         json_object_new_double(DoubleTime()));
  json_text = json_object_to_json_string(self->msg_json);
  sics_zmq_router_reply(self->pRtr->zhandle, self->pMsg, json_text);

  if (final) {
    json_object_put(self->msg_json);
    sics_zmq_router_free(self->pMsg);
    if (rslt)
      DynStringDelete((pDynString) rslt);
    if (expn)
      DynStringDelete((pDynString) expn);
    FutureRelease(fut);
  }
}

/*--------------------------------------------------------------------------*/
/**
 * Drive future response handler
 *
 * Generate a response based on the future state.
 */
static void driveResponse(pTaskFuture fut)
{
  pTaskMessage pTMsg = FutureGetContext(fut);
  COMMAND_CONTEXT *self = TaskMessageGetData(pTMsg);
  /* TODO */
  FutureRelease(fut);
}

static void *fail()
{
  return NULL;
}
/*--------------------------------------------------------------------------*/
static pTaskFuture driveCommand(pTaskMessage pTMsg)
{
  /* Receive ownership of pTmsg */
  COMMAND_CONTEXT *self = (COMMAND_CONTEXT *) TaskMessageGetData(pTMsg);
  const char *pCmd = NULL;
  const char *pTxt = NULL;
  json_object *node;
  enum json_type jtype;
  json_print(self->msg_json, 0);
  if (json_object_object_get_ex(self->msg_json, "cmd", &node))
    pCmd = json_object_get_string(node);
  if (json_object_object_get_ex(self->msg_json, "text", &node))
    pTxt = json_object_get_string(node);
  if (json_object_object_get_ex(self->msg_json, "device", &node)) {
    /* Single device */
    const char *dev_name = NULL;
    int seq_num;
    double tgt_pos;
    json_object *device = node;
    if (!json_object_object_get_ex(device, "dev", &node))
      return fail("No dev");
    dev_name = json_object_get_string(node);
    if (!json_object_object_get_ex(device, "seq", &node))
      return fail("No seq");
    seq_num = json_object_get_int(node);
    if (!json_object_object_get_ex(device, "pos", &node))
      return fail("No pos");
    tgt_pos = json_object_get_double(node);

  } else if (json_object_object_get_ex(self->msg_json, "devices", &node)) {
    json_object *device = node;
    if (json_object_get_type(node) == json_type_array) {
      int i;
      int arraylen = json_object_array_length(node);
    }
  } else if (json_object_object_get_ex(self->msg_json, "table", &node)) {
    if (json_object_get_type(node) == json_type_array) {
      int i;
      int arraylen = json_object_array_length(node);
    }
  }
  if (json_object_object_get_ex(self->msg_json, "device", &node)) {
    json_object *device = node;
  }
  pTaskPromise prom = PromiseCreate(GetTasker());
  PromiseSetContext(prom, pTMsg, NULL);
  pTaskFuture fut = FutureFromPromise(prom);
  FutureAddCallback(fut, driveResponse);
  /* TODO Execute */
  return fut;
}

/*--------------------------------------------------------------------------*/
static int DriveTaskQ(void *data)
{
  pTaskMessage pTMsg;

  while ((pTMsg = TaskQueueRecvMine(GetTasker()))) {
    int mType = TaskMessageGetType(pTMsg);
    COMMAND_CONTEXT *self= (COMMAND_CONTEXT *) TaskMessageGetData(pTMsg);
    if (self->promise && !PromiseSetRunning(self->promise)) {
    }
  }
  return 1;
}

/*--------------------------------------------------------------------------*/
static int InterpSicsTask(void *data)
{
  json_object *node;
  const char *json_text;
  extern double DoubleTime(void);
  const char *pFlag = "ERROR";
  const char *pResp = NULL;
  pTaskMessage pTMsg;
  COMMAND_CONTEXT *self;
  pDynString msg = NULL;

  while ((pTMsg = TaskQueueRecvMine(GetTasker()))) {
    self = (COMMAND_CONTEXT *) TaskMessageGetData(pTMsg);
    if (self->promise && !PromiseSetRunning(self->promise)) {
      /* cancelled */
      pFlag = "ERROR";
      pResp = "Cancelled";
    } else if (json_object_object_get_ex(self->msg_json, "text", &node)) {
      int iRet, inMacro;
      const char *pText = NULL;
      SConnection *pCon = SCCreateDummyConnection(pServ->pSics);

      pText = json_object_get_string(node);
      SCStartBuffering(pCon);
      inMacro = SCinMacro(pCon);
      SCsetMacro(pCon, 0);
      iRet = InterpExecute(pServ->pSics, pCon, (char *) pText);
      if (iRet == 1) {
        pFlag = "OK";
      } else {
        pFlag = "ERROR";
      }
      msg = SCEndBuffering(pCon);
      /* Hold ownership after connection deleted */
      DynStringHold(msg);
      pResp = DynStringGetArray(msg);
      SCDeleteConnection(pCon);
    } else {
      pFlag = "ERROR";
      pResp = "No text";
    }
    if (self->promise) {
      if (msg == NULL) {
        msg = DynStringCreate(100, 100);
        DynStringConcat(msg, pResp);
      }
      /* Pass ownership of msg to future */
      if (*pFlag == 'E') {
        PromiseSetException(self->promise, msg);
      } else {
        PromiseSetResult(self->promise, msg);
      }
      PromiseRelease(self->promise);
      msg = NULL;
    } else {
      json_object_object_add(self->msg_json, "final",
                             json_object_new_boolean(true));
      json_object_object_add(self->msg_json, "flag",
                             json_object_new_string(pFlag));
      json_object_object_add(self->msg_json, "reply",
                             json_object_new_string(pResp));
      json_object_object_add(self->msg_json, "ts",
                             json_object_new_double(DoubleTime()));
      json_text = json_object_to_json_string(self->msg_json);


      sics_zmq_router_reply(self->pRtr->zhandle, self->pMsg, json_text);

      json_object_put(self->msg_json);
      sics_zmq_router_free(self->pMsg);
      if (msg) {
        DynStringDelete(msg);
        msg = NULL;
      }
    }
    TaskMessageRelease(pTMsg);
  }

  return 1;
}

/*--------------------------------------------------------------------------*/
static int InterpTclTask(void *data)
{
  json_object *node;
  const char *json_text;
  extern double DoubleTime(void);
  const char *pFlag = "ERROR";
  const char *pResp = NULL;
  pTaskMessage pTMsg;
  COMMAND_CONTEXT *self;

  while ((pTMsg = TaskQueueRecvMine(GetTasker()))) {
    self = (COMMAND_CONTEXT *) TaskMessageGetData(pTMsg);
    if (self->promise && !PromiseSetRunning(self->promise)) {
      /* cancelled */
      pFlag = "ERROR";
      pResp = "Cancelled";
    } else if (json_object_object_get_ex(self->msg_json, "text", &node)) {
      int iRet;
      const char *pText = NULL;
      pText = json_object_get_string(node);
      iRet = MacroEvalString(pServ->dummyCon, pServ->pSics, pText);
      if (iRet == MACRO_OK) {
        pFlag = "OK";
      } else {
        pFlag = "ERROR";
      }
      pResp = MacroGetResult(pServ->dummyCon, pServ->pSics);
    } else {
      pResp = "No text";
      pFlag = "ERROR";
    }
    if (self->promise) {
      pDynString msg = DynStringCreate(100, 100);
      DynStringConcat(msg, pResp);
      /* Pass ownership of msg to future */
      if (*pFlag == 'E') {
        PromiseSetException(self->promise, msg);
      } else {
        PromiseSetResult(self->promise, msg);
      }
      PromiseRelease(self->promise);
    } else {
      json_object_object_add(self->msg_json, "final",
                             json_object_new_boolean(true));
      json_object_object_add(self->msg_json, "flag",
                             json_object_new_string(pFlag));
      json_object_object_add(self->msg_json, "reply",
                             json_object_new_string(pResp));
      json_object_object_add(self->msg_json, "ts",
                             json_object_new_double(DoubleTime()));
      json_text = json_object_to_json_string(self->msg_json);


      sics_zmq_router_reply(self->pRtr->zhandle, self->pMsg, json_text);
      json_object_put(self->msg_json);
      sics_zmq_router_free(self->pMsg);
    }
    TaskMessageRelease(pTMsg);
  }

  return 1;
}

/*--------------------------------------------------------------------------*/
static int DataTask(void *data)
{
  json_object *node;
  const char *json_text;
  extern double DoubleTime(void);
  const char *pFlag = "ERROR";
  const char *pResp = NULL;
  pTaskMessage pTMsg;
  COMMAND_CONTEXT *self;

  while ((pTMsg = TaskQueueRecvMine(GetTasker()))) {
    char text[1024];
    self = (COMMAND_CONTEXT *) TaskMessageGetData(pTMsg);
    if (self->promise && !PromiseSetRunning(self->promise)) {
      /* cancelled */
      pFlag = "ERROR";
      pResp = "Cancelled";
    } else if (json_object_object_get_ex(self->msg_json, "text", &node)) {
      int iRet;
      const char *node_name = NULL;
      const char *prop_name = NULL;
      const char *value = NULL;
      const char *pCmd = json_object_get_string(node);
      if (json_object_object_get_ex(self->msg_json, "node", &node)) {
        node_name = json_object_get_string(node);
      }
      if (json_object_object_get_ex(self->msg_json, "property", &node)) {
        prop_name = json_object_get_string(node);
      }
      if (json_object_object_get_ex(self->msg_json, "value", &node)) {
        value = json_object_get_string(node);
      }
      if (strcasecmp(pCmd, "hget") == 0) {
        snprintf(text, sizeof(text)-1, "HVALI %s", node_name);
      } else if (strcasecmp(pCmd, "hset") == 0) {
        snprintf(text, sizeof(text)-1, "HSET %s {%s}", node_name, value);
      } else if (strcasecmp(pCmd, "hgetprop") == 0) {
        snprintf(text, sizeof(text)-1, "HGETPROPVAL %s %s",
                 node_name,
                 prop_name);
      } else if (strcasecmp(pCmd, "hsetprop") == 0) {
        snprintf(text, sizeof(text)-1, "HSETPROP %s %s {%s}",
                 node_name,
                 prop_name,
                 value);
      } else {
        /* TODO set command */
        text[0] = '\0';
        pFlag = "ERROR";
        pResp = "Unknown DATA subcommand";
      }
      if (text[0]) {
        iRet = MacroEvalString(pServ->dummyCon, pServ->pSics, text);
        if (iRet == MACRO_OK) {
          pFlag = "OK";
        } else {
          pFlag = "ERROR";
        }
        pResp = MacroGetResult(pServ->dummyCon, pServ->pSics);
      }
    } else {
      pResp = "No text";
      pFlag = "ERROR";
    }
    if (self->promise) {
      pDynString msg = DynStringCreate(100, 100);
      DynStringConcat(msg, pResp);
      /* Pass ownership of msg to future */
      if (*pFlag == 'E') {
        PromiseSetException(self->promise, msg);
      } else {
        PromiseSetResult(self->promise, msg);
      }
      PromiseRelease(self->promise);
    } else {
      json_object_object_add(self->msg_json, "final",
                             json_object_new_boolean(true));
      json_object_object_add(self->msg_json, "flag",
                             json_object_new_string(pFlag));
      json_object_object_add(self->msg_json, "reply",
                             json_object_new_string(pResp));
      json_object_object_add(self->msg_json, "ts",
                             json_object_new_double(DoubleTime()));
      json_text = json_object_to_json_string(self->msg_json);


      sics_zmq_router_reply(self->pRtr->zhandle, self->pMsg, json_text);
      json_object_put(self->msg_json);
      sics_zmq_router_free(self->pMsg);
    }
    TaskMessageRelease(pTMsg);
  }

  return 1;
}

/*--------------------------------------------------------------------------*/
static int DeviceTask(void *data)
{
  json_object *node;
  const char *json_text;
  extern double DoubleTime(void);
  const char *pFlag = "ERROR";
  const char *pResp = NULL;
  pTaskMessage pTMsg;
  COMMAND_CONTEXT *self;

  while ((pTMsg = TaskQueueRecvMine(GetTasker()))) {
    self = (COMMAND_CONTEXT *) TaskMessageGetData(pTMsg);
    if (self->promise && !PromiseSetRunning(self->promise)) {
      /* cancelled */
      pFlag = "ERROR";
      pResp = "Cancelled";
    } else if (json_object_object_get_ex(self->msg_json, "text", &node)) {
      const char *pCmd = NULL;
      pCmd = json_object_get_string(node);
      json_print(self->msg_json, 0);
      if (strcasecmp(pCmd, "RUN") == 0 ||
          strcasecmp(pCmd, "DRIVE") == 0 ||
          strcasecmp(pCmd, "MOVE") == 0) {
        /* Pass ownership of pTmsg to command */
        (void) driveCommand(pTMsg);
        continue;
      }
      pFlag = "ERROR";
      pResp = "Done";
    } else {
      pResp = "No text";
      pFlag = "ERROR";
    }
    if (self->promise) {
      pDynString msg = DynStringCreate(100, 100);
      DynStringConcat(msg, pResp);
      /* Pass ownership of msg to future */
      if (*pFlag == 'E') {
        PromiseSetException(self->promise, msg);
      } else {
        PromiseSetResult(self->promise, msg);
      }
      PromiseRelease(self->promise);
    } else {
      json_object_object_add(self->msg_json, "final",
                             json_object_new_boolean(true));
      json_object_object_add(self->msg_json, "flag",
                             json_object_new_string(pFlag));
      json_object_object_add(self->msg_json, "reply",
                             json_object_new_string(pResp));
      json_object_object_add(self->msg_json, "ts",
                             json_object_new_double(DoubleTime()));
      json_text = json_object_to_json_string(self->msg_json);


      sics_zmq_router_reply(self->pRtr->zhandle, self->pMsg, json_text);
      json_object_put(self->msg_json);
      sics_zmq_router_free(self->pMsg);
    }
    TaskMessageRelease(pTMsg);
  }

  return 1;
}

/*--------------------------------------------------------------------------*/
/**
 * Parse and dispatch the JSON
 *
 * Receive a message, parse it, and dispatch it. If we dispatch it then
 * ownership transfers downstream. Otherwise we must respond and release it.
 */
static int JSONTask(void *data)
{
  struct json_tokener *jtok;
  json_object *msg_json = NULL, *node = NULL;
  enum json_tokener_error tokerr;
  const char *pResp;
  const char *pFlag;
  pTaskMessage pTMsg;

  while ((pTMsg = TaskQueueRecvMine(GetTasker()))) {
    COMMAND_CONTEXT *pCtx = (COMMAND_CONTEXT *) TaskMessageGetData(pTMsg);
    ZSOCK_CONTEXT *self = pCtx->pRtr;
    pROUTER_MESSAGE pMsg = pCtx->pMsg;
    int len = sics_zmq_router_size(pMsg);
    const char *txt = sics_zmq_router_data(pMsg);

    jtok = json_tokener_new();
    json_tokener_reset(jtok);
    msg_json = json_tokener_parse_ex(jtok, txt, len);
    tokerr = json_tokener_get_error(jtok);
    pFlag = "ERROR"; /* Prepare for the worst */
    if (tokerr != json_tokener_success) {
      pResp = "JSON parsing error";
      Log(ERROR,"sys", "JSON parsing error %s on %s at %d",
          json_tokener_error_desc(tokerr),
          txt,
          jtok->char_offset);
    } else if (json_object_get_type(msg_json) != json_type_object) {
      pResp = "Received JSON of bad type";
      Log(ERROR,"sys", "Received JSON of bad type in %s",
          txt);
    } else {
      /* Valid JSON */
      if (json_object_object_get_ex(msg_json, "cmd", &node)) {
        /* valid cmd message - deferred commands */
        const char *pCmd = json_object_get_string(node);
        if (strcasecmp(pCmd, "SICS") == 0) {
          process_deferred(scsTask.queue, pTMsg, pCtx, msg_json);
          json_tokener_free(jtok);
          continue;
        }
        if (strcasecmp(pCmd, "TCL") == 0) {
          process_deferred(tclTask.queue, pTMsg, pCtx, msg_json);
          json_tokener_free(jtok);
          continue;
        }
        if (strcasecmp(pCmd, "DATA") == 0) {
          process_deferred(datTask.queue, pTMsg, pCtx, msg_json);
          json_tokener_free(jtok);
          continue;
        }
        if (strcasecmp(pCmd, "DEVICE") == 0) {
          process_deferred(devTask.queue, pTMsg, pCtx, msg_json);
          json_tokener_free(jtok);
          continue;
        }
        /* immediate commands */
        if (strcasecmp(pCmd, "POCH") == 0
            || strcasecmp(pCmd, "PING") == 0
            || strcasecmp(pCmd, "PERF") == 0) {
          process_immediate(PingPongCommand, pTMsg, pCtx, msg_json);
          json_tokener_free(jtok);
          continue;
        } else {
          /* pCmd not recognized */
          pFlag = "ERROR";
          pResp = "Received JSON cmd not recognized";
          Log(ERROR,"sys", "Received JSON with '%s' in %s", pCmd, txt);
        }
      } else if (json_object_object_get_ex(msg_json, "command", &node)) {
        /* Gumtree */
        const char *pCmd = json_object_get_string(node);
        if (memcmp(pCmd, "INT1712", 7) == 0) {
          /* TODO: INT1712 */
        } else if (strncasecmp(pCmd, "login ", 6) == 0) {
          /* TODO: login */
        } else {
          /* pCmd not recognized */
          pFlag = "ERROR";
          pResp = "Received JSON command not recognized";
          Log(ERROR,"sys", "Received JSON with '%s' in %s", pCmd, txt);
        }
      } else {
        pResp = "Received JSON without cmd";
        Log(ERROR,"sys", "Received JSON without 'cmd' in %s", txt);
      }
      /* JSON command not recognized */
      json_object_object_add(msg_json, "final",
                             json_object_new_boolean(true));
      json_object_object_add(msg_json, "flag",
                             json_object_new_string(pFlag));
      json_object_object_add(msg_json, "reply",
                             json_object_new_string(pResp));
      json_object_object_add(msg_json, "ts",
                             json_object_new_double(DoubleTime()));
      pResp = json_object_to_json_string(msg_json);
    }

    sics_zmq_router_reply(self->zhandle, pMsg, pResp);
    json_object_put(msg_json);
    sics_zmq_router_free(pMsg);
    json_tokener_free(jtok);
    TaskMessageRelease(pTMsg);
  }
  return 1;
}

/*--------------------------------------------------------------------------*/
static int SourceTask(void *data)
{
  json_object *node;
  const char *json_text;
  extern double DoubleTime(void);
  const char *pFlag = "ERROR";
  const char *pResp = NULL;
  pTaskMessage pTMsg;
  COMMAND_CONTEXT *self;

  while ((pTMsg = TaskQueueRecvMine(GetTasker()))) {
    self = (COMMAND_CONTEXT *) TaskMessageGetData(pTMsg);
    if (json_object_object_get_ex(self->msg_json, "value", &node)) {
      int iRet;
      const char *pText = NULL;
      pText = json_object_get_string(node);
      if (pText) {
        struct json_tokener *jtok;
        json_object *msg_json = NULL, *data = NULL;
        enum json_tokener_error tokerr;

        jtok = json_tokener_new();
        json_tokener_reset(jtok);
        msg_json = json_tokener_parse_ex(jtok, pText, strlen(pText));
        tokerr = json_tokener_get_error(jtok);
        if (tokerr != json_tokener_success) {
          Log(ERROR,"sys", "JSON parsing error %s on %s at %d",
              json_tokener_error_desc(tokerr),
              jtok->char_offset);
        } else if (json_object_get_type(msg_json) != json_type_object) {
          Log(ERROR,"sys", "Received JSON of bad type in %s", pText);
        } else {
          double timestamp = 0.0;
          int level;
          const char *timeText = NULL;
          const char *sub = NULL;
          const char *short_message = NULL;
          const char *version = NULL;
          const char *host = NULL;
          if (json_object_object_get_ex(msg_json, "timestamp", &data)) {
            timestamp = json_object_get_double(data);
          }
          if (json_object_object_get_ex(msg_json, "level", &data)) {
            level = json_object_get_int(data);
          }
          if (json_object_object_get_ex(msg_json, "_timetext", &data)) {
            timeText = json_object_get_string(data);
          }
          if (json_object_object_get_ex(msg_json, "_sub", &data)) {
            sub = json_object_get_string(data);
          }
          if (json_object_object_get_ex(msg_json, "short_message", &data)) {
            short_message = json_object_get_string(data);
          }
          if (json_object_object_get_ex(msg_json, "version", &data)) {
            version = json_object_get_string(data);
          }
          if (json_object_object_get_ex(msg_json, "host", &data)) {
            host = json_object_get_string(data);
          }
          LogInject(level, timeText, sub, short_message, NULL);
        }
      }
    }
    json_object_put(self->msg_json);
    sics_zmq_subscribe_free(self->pMsg);
    TaskMessageRelease(pTMsg);
  }

  return 1;
}

/*--------------------------------------------------------------------------*/
static void process_deferred(pTaskQueue queue, pTaskMessage pTMsg, COMMAND_CONTEXT *pCtx, json_object *msg_json)
{
  pCtx->msg_json = msg_json;
  pCtx->promise = PromiseCreate(GetTasker());
  PromiseSetContext(pCtx->promise, pCtx, NULL);
  FutureAddCallback(FutureFromPromise(pCtx->promise), deferredResponse);
  TaskQueueSend(queue, pTMsg);

  if (true) {
    const char *json_text;
    json_object_object_add(pCtx->msg_json, "final",
                           json_object_new_boolean(false));
    json_object_object_add(pCtx->msg_json, "flag",
                           json_object_new_string("OK"));
    json_object_object_add(pCtx->msg_json, "reply",
                           json_object_new_string("DEFERRED"));
    json_object_object_add(pCtx->msg_json, "ts",
                           json_object_new_double(DoubleTime()));
    json_text = json_object_to_json_string(pCtx->msg_json);
    sics_zmq_router_reply(pCtx->pRtr->zhandle, pCtx->pMsg, json_text);
  }
}

/*--------------------------------------------------------------------------*/
static void process_immediate(void (*func)(void *),
                             pTaskMessage pTMsg,
                             COMMAND_CONTEXT *pCtx,
                             json_object *msg_json)
{
  pCtx->msg_json = msg_json;
  func(pCtx);
  TaskMessageRelease(pTMsg);
}

/*--------------------------------------------------------------------------*/
int source_dispatch(void *context, int mode)
{
  ZSOCK_CONTEXT *self = (ZSOCK_CONTEXT *) context;
  pSUBSCRIBE_MESSAGE pMsg;
  while ((pMsg = sics_zmq_subscribe_recv(self->zhandle, 20))) {
    int len = sics_zmq_subscribe_size(pMsg);
    pDynString str = DynStringCreate(len + 10, len + 10);
    DynStringConcatBytes(str, sics_zmq_subscribe_data(pMsg), len);
    if (*DynStringGetArray(str) == '{') {
      struct json_tokener *jtok;
      json_object *msg_json = NULL, *data = NULL;
      enum json_tokener_error tokerr;

      jtok = json_tokener_new();
      json_tokener_reset(jtok);
      msg_json = json_tokener_parse_ex(jtok, DynStringGetArray(str), len);
      tokerr = json_tokener_get_error(jtok);
      if (tokerr != json_tokener_success) {
        Log(ERROR,"sys", "JSON parsing error %s on %s at %d",
                      json_tokener_error_desc(tokerr),
                      DynStringGetArray(str),
                      jtok->char_offset);
      } else if (json_object_get_type(msg_json) != json_type_object) {
        Log(ERROR,"sys", "Received JSON of bad type in %s",
                     DynStringGetArray(str));
      } else if (!json_object_object_get_ex(msg_json, "type", &data)) {
        Log(ERROR,"sys", "Received JSON without 'type' in %s",
                     DynStringGetArray(str));
      } else {
        /* valid message */
        const char *pCmd = json_object_get_string(data);
        if (strcasecmp(pCmd, "Log") == 0) {
          pTaskMessage pTMsg = TaskMessageAlloc(sizeof(COMMAND_CONTEXT), 0);
          COMMAND_CONTEXT *pCtx = (COMMAND_CONTEXT *) TaskMessageGetData(pTMsg);
          pCtx->pMsg = pMsg;
          pCtx->msg_json = msg_json;
          TaskQueueSend(srcTask.queue, pTMsg);
          json_tokener_free(jtok);
          DynStringDelete(str);
          continue;
        }
        /* pCmd not recognized */
        Log(ERROR,"sys", "Received JSON with 'type' as '%s' in %s",
                     pCmd, DynStringGetArray(str));
      }
      /* JSON error or pCmd not recognized */
      json_object_put(msg_json);
      json_tokener_free(jtok);
    }
    sics_zmq_subscribe_free(pMsg);
    DynStringDelete(str);
  }
  return 0;
}

/*--------------------------------------------------------------------------*/
/**
 * The interrupt service routine for the ZMQ ROUTER socket.
 *
 * Called from nwatch when a message is available. Job is to take the
 * message from the ZMQ ROUTER socket and dispatch it to the proper
 * message handler task. There could be more than one.
 */
int router_dispatch(void *context, int mode)
{
  ZSOCK_CONTEXT *self = (ZSOCK_CONTEXT *) context;
  pROUTER_MESSAGE pMsg;
  const char *pFlag, *pResp;
  while ((pMsg = sics_zmq_router_recv(self->zhandle, 20))) {
    int len = sics_zmq_router_size(pMsg);
    const char *txt = sics_zmq_router_data(pMsg);
    pResp = txt;
    if (strcasecmp(txt, "Hello") == 0) {
      pResp = "Hello  Sailor!";
    } else if (*txt == '{') {
      pTaskMessage pTMsg = TaskMessageAlloc(sizeof(COMMAND_CONTEXT), 0);
      COMMAND_CONTEXT *pCtx = (COMMAND_CONTEXT *) TaskMessageGetData(pTMsg);
      pCtx->pRtr = self;
      pCtx->pMsg = pMsg;
      TaskQueueSend(jsnTask.queue, pTMsg);
      continue;
    }
    sics_zmq_router_reply(self->zhandle, pMsg, pResp);
    sics_zmq_router_free(pMsg);
  }
  return 0;
}
/*--------------------------------------------------------------------------*/
int SICSZRouter(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[])
{
  ZSOCK_CONTEXT *zsock_context;
  bool doLogSource = false;

  if (argc < 2) {
    SCPrintf(pCon, eError, "Insufficient arguments - usage:%s <zmq-address>", argv[0]);
    return 0;
  }

  zsock_context = (ZSOCK_CONTEXT *) calloc(1, sizeof(ZSOCK_CONTEXT));
  if (!zsock_context) {
    SCPrintf(pCon, eError, "Insufficient memory for:%s %s", argv[0], argv[1]);
    return 0;
  }

  if (strcasecmp(argv[0], "source") == 0) {
    doLogSource = true;
    zsock_context->zhandle = sics_zmq_subscribe(argv[1]);
    if (zsock_context->zhandle) {
      if (!NetWatchRegisterZMQCallback(&zsock_context->nhandle,
                                       sics_zmq_subscribe_zsock(zsock_context->zhandle),
                                       source_dispatch, zsock_context)) {
        SCPrintf(pCon, eLog, "source netwatch fail:%s", argv[1]);
        sics_zmq_unsubscribe(&zsock_context->zhandle);
        free(zsock_context);
        return 0;
      }
      SCPrintf(pCon, eLog, "source established to:%s", argv[1]);
    } else {
      SCPrintf(pCon, eLog, "source establish fail:%s", argv[1]);
      return 0;
    }
    srcTask.taskID = TaskRegisterQ(GetTasker(), "zSOURCE", SourceTask,
                                   NULL, NULL, NULL, TASK_PRIO_LOW);
    srcTask.queue = TaskQueueGet(GetTasker(), srcTask.taskID);
    return 1;
  } else {
    zsock_context->zhandle = sics_zmq_router(argv[1]);
    if (zsock_context->zhandle) {
      if (!NetWatchRegisterZMQCallback(&zsock_context->nhandle,
                                       sics_zmq_router_zsock(zsock_context->zhandle),
                                       router_dispatch, zsock_context)) {
        SCPrintf(pCon, eLog, "zrouter netwatch fail:%s", argv[1]);
        sics_zmq_unrouter(&zsock_context->zhandle);
        free(zsock_context);
        return 0;
      }
      SCPrintf(pCon, eLog, "zrouter established to:%s", argv[1]);
    } else {
      SCPrintf(pCon, eLog, "zrouter establish fail:%s", argv[1]);
      return 0;
    }
    if (jsnTask.queue == NULL) {
      jsnTask.taskID = TaskRegisterQ(GetTasker(), "zJSON", JSONTask,
                                  NULL, NULL, NULL, TASK_PRIO_LOW);
      jsnTask.queue = TaskQueueGet(GetTasker(), jsnTask.taskID);
    }
    if (scsTask.queue == NULL) {
      scsTask.taskID = TaskRegisterPoolQ(GetTasker(), "zSICS", InterpSicsTask,
                                  NULL, NULL, NULL, TASK_PRIO_LOW, 10);
      scsTask.queue = TaskQueueGet(GetTasker(), scsTask.taskID);
    }
    if (tclTask.queue == NULL) {
      tclTask.taskID = TaskRegisterPoolQ(GetTasker(), "zTCL", InterpTclTask,
                                  NULL, NULL, NULL, TASK_PRIO_LOW, 10);
      tclTask.queue = TaskQueueGet(GetTasker(), tclTask.taskID);
    }
    if (datTask.queue == NULL) {
      datTask.taskID = TaskRegisterPoolQ(GetTasker(), "zDATA", DataTask,
                                  NULL, NULL, NULL, TASK_PRIO_LOW, 10);
      datTask.queue = TaskQueueGet(GetTasker(), datTask.taskID);
    }
    if (devTask.queue == NULL) {
      devTask.taskID = TaskRegisterPoolQ(GetTasker(), "zDEVICE", DeviceTask,
                                  NULL, NULL, NULL, TASK_PRIO_LOW, 10);
      devTask.queue = TaskQueueGet(GetTasker(), devTask.taskID);
    }
    return 1;
  }
  return 0;
}


/*--------------------------------------------------------------------------*/
/*printing the value corresponding to boolean, double, integer and strings*/
void json_print_value(json_object * jobj, int indent)
{
  enum json_type type;
  type = json_object_get_type(jobj);	/*Getting the type of the json object */
  switch (type) {
  case json_type_boolean:
    printf("%*stype: json_type_boolean", indent, "");
    printf("  value: %s\n", json_object_get_boolean(jobj) ? "true" : "false");
    break;
  case json_type_double:
    printf("%*stype: json_type_double", indent, "");
    printf("  value: %lf\n", json_object_get_double(jobj));
    break;
  case json_type_int:
    printf("%*stype: json_type_int   ", indent, "");
    printf("  value: %d\n", json_object_get_int(jobj));
    break;
  case json_type_string:
    printf("%*stype: json_type_string", indent, "");
    printf("  value: %s\n", json_object_get_string(jobj));
    break;
  }
}

/*--------------------------------------------------------------------------*/
void json_print_array(json_object * jobj, char *key, int indent)
{
  enum json_type type;

  json_object *jarray = jobj;	/*Simply get the array */
  if (key) {
    json_object_object_get_ex(jobj, key, &jarray);	/*Getting the array if it is a key value pair */
  }

  int arraylen = json_object_array_length(jarray);	/*Getting the length of the array */
  printf("Array Length: %d\n", arraylen);
  int i;
  json_object *jvalue;

  for (i = 0; i < arraylen; i++) {
    jvalue = json_object_array_get_idx(jarray, i);	/*Getting the array element at position i */
    type = json_object_get_type(jvalue);
    if (type == json_type_array) {
      json_print_array(jvalue, NULL, indent + 2);
    } else if (type != json_type_object) {
      printf("%*svalue[%d]: ", indent, "", i);
      json_print_value(jvalue, 0);
    } else {
      printf("%*svalue[%d]:\n", indent, "", i);
      json_print(jvalue, indent + 2);
    }
  }
}

/*--------------------------------------------------------------------------*/
/* Printing the json object*/
void json_print(json_object * jobj, int indent)
{
  enum json_type type;
  json_object_object_foreach(jobj, key, val) {	/*Passing through every array element */
    printf("%*sname: %s\n", indent, "", key);
    type = json_object_get_type(val);
    switch (type) {
    case json_type_boolean:
    case json_type_double:
    case json_type_int:
    case json_type_string:
      json_print_value(val, indent + 2);
      break;
    case json_type_object:
      printf("%*s  type: json_type_object\n", indent, "");
      json_object_object_get_ex(jobj, key, &jobj);
      json_print(jobj, indent + 2);
      break;
    case json_type_array:
      printf("%*s  type: json_type_array, ", indent, "");
      json_print_array(jobj, key, indent + 2);
      break;
    }
  }
  if (indent == 0)
    fflush(stdout);
}
