
/*--------------------------------------------------------------------------
                        N S E R V E R

  The SICS server main data structure and functions.

  Restructured: September 1997

  Mark Koennecke.
  
  copyright: see copyright.h
----------------------------------------------------------------------------*/
#ifndef SICSNSERVER
#define SICSNSERVER
#include "conman.h"
#include "SCinter.h"
#include "emon.h"
#include "devexec.h"
#include "task.h"
#include "network.h"

typedef struct __SicsServer *pServer;

#include "nread.h"


typedef struct __SicsServer {
  SicsInterp *pSics;
  pTaskMan pTasker;
  pExeList pExecutor;
  pEnvMon pMonitor;
  mkChannel *pServerPort;
  pNetRead pReader;
  int simMode;
  SConnection *dummyCon;
} SicsServer;


/*----------------------------------------------------------------------*/
int InitServer(char *file, pServer * pServ);
void RunServer(pServer self);
void StopServer(pServer self);
/*----------------------------------------------------------------------*/
SicsInterp *GetInterpreter(void);
pExeList GetExecutor(void);
pTaskMan GetTasker(void);
void ServerWriteGlobal(char *pMessage, int iCode);

int UserWait(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[]);

int SicsWait(long lTime);
int ServerIsStarting(pServer self);

#endif
