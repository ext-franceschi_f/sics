/*
 * N E T   W A T C H
 *
 * This module watches network connections for sockets becoming readable or
 * writeable and invokes callbacks. It also provides a timer mechanism.
 *
 * Douglas Clowes, February 2007
 *
 */
#ifndef NETWATCH_H
#define NETWATCH_H
#include <sys/time.h>

#define nwatch_read 1
#define nwatch_write 2
#define nwatch_except 4
/**
 * \brief network or timer callback function
 *
 * \param context from the network/timer registration
 * \param mode
 *    for network, nwatch_read or nwatch_write
 *    for timer, zero, reserved for future use
 *
 * \return normally zero, for future use
 */
typedef int (*pNWCallback) (void *context, int mode);

/* the abstract timer object handle */
typedef struct __netwatchtimer *pNWTimer;

/**
 * \brief register a one-shot timer event
 *
 * \param handle pointer to location to receive the timer object handle
 * \param mSec milliseconds after which the timer should expire
 * \param callback function when timer expires
 * \param context abstract context passed to callback function
 * \return success=1, failure=0
 */
int NetWatchRegisterTimer(pNWTimer * handle, int mSec,
                          pNWCallback callback, void *context);

/**
 * \brief register a periodic timer
 *
 * \param handle pointer to location to receive the timer object handle
 * \param mSec milliseconds after which the timer should expire
 * \param mSecPeriod milliseconds after which the timer should repeat
 * \param callback function when timer expires
 * \param context abstract context passed to callback function
 * \return success=1, failure=0
 */
int NetWatchRegisterTimerPeriodic(pNWTimer * handle, int mSecInitial,
                                  int mSecPeriod, pNWCallback callback,
                                  void *context);

pNWTimer NetWatchGetActiveTimer(void);

int NetWatchGetTimerDelay(pNWTimer handle);
int NetWatchGetTimerInitial(pNWTimer handle);
int NetWatchGetTimerPeriod(pNWTimer handle);
int NetWatchSetTimerPeriod(pNWTimer handle, int mSecPeriod);
/**
 * \brief remove a registered timer event
 *
 * \param handle from the timer registration
 * \return success=1, failure=0
 */
int NetWatchRemoveTimer(pNWTimer handle);

/* the abstract socket object handle */
typedef struct __netwatchcontext *pNWContext;

/**
 * \brief register a socket to be watched in read mode
 *
 * \param handle pointer to location to receive the socket object handle
 * \param iSocket file descriptor number of the socket to watch
 * \param callback function when socket readable/writeable
 * \param context abstract context passed to callback function
 * \return success=1, failure=0
 */
int NetWatchRegisterCallback(pNWContext * handle, int iSocket,
                             pNWCallback callback, void *context);

/**
 * \brief register a queue to be watched in read mode
 *
 * \param handle pointer to location to receive the socket object handle
 * \param zqueue handle of the zmq queue to watch
 * \param callback function when queue readable/writeable
 * \param context abstract context passed to callback function
 * \return success=1, failure=0
 */
int NetWatchRegisterZMQCallback(pNWContext * handle, void *zqueue,
                             pNWCallback callback, void *context);

/**
 * \brief remove a socket callback registration
 *
 * \param handle from the socket registration
 * \return success=1, failure=0
 */
int NetWatchRemoveCallback(pNWContext handle);

/**
 * \brief retrieve the mode of a socket callback registration
 *
 * \param handle from the socket registration
 * \return 0=failure else the mode (read and/or write)
 */
int NetWatchGetMode(pNWContext handle);

/**
 * \brief set the mode of a socket callback registration
 *
 * \param handle from the socket registration
 * \param mode read and/or write 
 * \return 0=failure, 1=success
 */
int NetWatchSetMode(pNWContext handle, int mode);

/**
 * \brief Event Handler Idle Loop
 *
 * \param wait_time Time in seconds to wait for events
 * \return number of events dispatched or -1 on error
 */
int NetWatchHandleEvents(double wait_time);

#endif                          /* NETWATCH_H */
