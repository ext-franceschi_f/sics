/*-------------------------------------------------------------------------
                      N X U T I L

  Prototypes for some more useful functions for writing NeXus data from
  SICS.


  Mark Koennecke, April 1997
---------------------------------------------------------------------------*/
#ifndef NXUTIL
#define NXUTIL
int SNXSPutMotor(SicsInterp * pSics, SConnection * pCon, NXhandle hFil,
                 NXdict pDict, char *pAlias, char *pName);
int SNXSPutMotorNull(SicsInterp * pSics, SConnection * pCon, NXhandle hFil,
                     NXdict pDict, char *pAlias, char *pName);
int SNXSPutVariable(SicsInterp * pSics, SConnection * pCon,
                    NXhandle hFil, NXdict pDict, char *pAlias,
                    char *pName);
int SNXSPutEVVar(NXhandle hfil, NXdict pDict,
                 char *pName, SConnection * pCon,
                 char *pValAlias, char *pStdDevAlias);
int SNXSPutGlobals(NXhandle hfil, char *pFile, char *pInst,
                   SConnection * pCon);
void SNXFormatTime(char *pBuffer, int iLen);

char *SNXMakeFileName(SicsInterp * pSics, SConnection * pCon);
   /* 
      coded in nxdata.c
    */
int SNXSPutDrivable(SicsInterp * pSics, SConnection * pCon, NXhandle hFil,
                    NXdict pDict, char *pAlias, char *pName);


#endif
