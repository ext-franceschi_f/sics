/*--------------------------------------------------------------------------
		     Omega 2 Theta
		     
  variable for reflectometers and TOPSI.
  
  Mark Koennecke, February 1997
  
  copyright: see implementation file
---------------------------------------------------------------------------*/
#ifndef SICSO2T
#define SICSO2T

typedef struct __SicsO2T *pSicsO2T;

pSicsO2T MakeO2T(char *omega, char *theta, SicsInterp * pSics);
void DeleteO2T(void *pData);

int CreateO2T(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[]);

#endif
