
#line 398 "interface.w"


#line 29 "interface.w"

/*--------------------------------------------------------------------------
        In SICS there is the to find out what an
        object is capable of at runtime. If this has been done a general
        way to access those capabilities is needed. In order to do all
        this each SICS-object is required to carry an object descriptor
        struct as first parameter in its class/object struct. Additionslly
        it is required to initialize this struct to something sensible.
        
        This file defines this struct. Additionally a few functions of
        general use are prototyped.
        
        Mark Koennecke, June, 1997
        
        copyrigth: see implementation file 
----------------------------------------------------------------------------*/
#ifndef SICSDESCRIPTOR
#define SICSDESCRIPTOR
#include <stdio.h>
#include "ifile.h"
#include "hipadaba.h"

typedef struct {
  char *name;
  int (*SaveStatus) (void *self, char *name, FILE * fd);
  void *(*GetInterface) (void *self, int iInterfaceID);
  IPair *pKeys;
  pHdb parNode;
} ObjectDescriptor, *pObjectDescriptor;

 /*---------------------------------------------------------------------------*/
pObjectDescriptor CreateDescriptor(char *name);
void DeleteDescriptor(pObjectDescriptor self);
pObjectDescriptor FindDescriptor(void *pData);

/*============================================================================
        Objects which do not carry data need a dummy descriptor. Otherwise
        drive or scan will protection fault when trying to drive something
        which should not be driven. This is defined below.
*/

typedef struct {
  pObjectDescriptor pDescriptor;
} Dummy, *pDummy;


pDummy CreateDummy(char *name);
void KillDummy(void *pData);

int iHasType(void *pData, char *Type);

#endif

#line 399 "interface.w"

/*--------------------------------------------------------------------------*/
/* Additional properties used by the ANSTO site to provide more information
 * about each object instance, especially devices.
 */
void SetDescriptorKey(pObjectDescriptor self, char *keyName, char *value);
void SetDescriptorGroup(pObjectDescriptor self, char *group);
void SetDescriptorDescription(pObjectDescriptor self, char *description);
char *GetDescriptorKey(pObjectDescriptor self, char *keyName);
char *GetDescriptorGroup(pObjectDescriptor self);
char *GetDescriptorDescription(pObjectDescriptor self);
