/*---------------------------------------------------------------------------
	                     A R P A R

     Some of the SICS objects hold a long list of parameters. For
     instance motors. In order to facilitate access to these parameters
     this helper class has been defined. In Sics-objects parameters
     will be held in an array of par-structs. This class now defines
     a few functions for operating on this array. This is specially
     important for the textual wrapper function.
     
     The objects themselves will have suitable defines, which allows them
     to access the parameters with ease. 
     
     As the number of parameters is known and static at definition time 
     of the object this will be implemented on arrays.
     
     
     Mark Koenencke, November 1996
     
     copyright: see implementation file
----------------------------------------------------------------------------*/
#ifndef SICSARPAR
#define SICSARPAR

typedef struct {
  char *name;
  float fVal;
  int iCode;
} ObPar;

/* quick access internally, self is the ObPar array, i is the parameter
   number, nice to have defines for some, better than handling ints
 */
float ObVal(ObPar * self, int i);

int ObParLength(ObPar * self);
  /*
     finds the length of an ObPar array 
   */

ObPar *ObParFind(ObPar * self, char *name);
  /* 
     finds a ObPar struct for a name, return NULL if none 
   */
int ObParIndex(ObPar * self, char *name);
  /*
     finds an index for name in self. Returns -1 on failure
   */

int ObParInit(ObPar * self, int i, char *name, float fVal, int iCode);
  /* 
     sets a ObPar entry. self is a pointer to the array
   */
int ObParSet(ObPar * self, char *obname, char *name, float fVal,
             SConnection * pCon);
  /*
     checks if the connections permissions are alright and changes value
     if so. Returns 1 on success, 0 on failure. Prints errors directly to
     pCon. The parameter obmane is the name of the object the parameters belong
     to. Needed for error printing. name is the parameter name, fVal the new
     value.
   */

void ObParDelete(ObPar * self);
  /*
     Deletes an ObPar array 
   */

ObPar *ObParCreate(int iArrayLong);
  /*
     creates an array with iArrayLong entries
   */
#endif
