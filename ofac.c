/**
 * Startup commands and definitions
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke 1996 - ?
 * heavy modifications to separate PSI specific commands into a 
 * separate library. Mark Koennecke, June 2003
 *
 * moved some functions to sicsutil.c
 * Markus Zolliker Jan 2010
 */

#include "ofac.h"
#include "exeman.h"
#include "statusfile.h"
#include "site.h"
#include "sicshipadaba.h"
#include "sicsglobal.h"

static unsigned int killStartupCommands = 1;
int isDuringInitialization;


extern void DevExecInit(void); /* devexec.c */
/*--------------------------------------------------------------------------*/
static void InitGeneral(void)
{
#define INIT(F) { void F(void); F(); }

/*  insert here initialization routines ... */

  INIT(StatisticsInit);
  INIT(InitializerInit);
  INIT(SaveHdbInit);            /* must be after InitializerInit */
  INIT(DefineAliasInit);
  INIT(SctInit);
  INIT(SctDriveAdapterInit);
  INIT(SctDriveObjInit);
  INIT(SctDriveAdapterInit);
  INIT(SICSGetInit);
  INIT(LogReaderInit);
  INIT(LogSetupInit);
  INIT(InstallBackground);
  INIT(MakeProtocol);
  INIT(UdpInit);
  INIT(HelpInit);
  INIT(AddTestProt);
  INIT(AddGenBinProtocoll);
  INIT(AddSyncedProt);
  INIT(AddBinProtocol);
  INIT(AddProcProtocol);
  INIT(AddPMACProtocoll);
  INIT(AddZMQDProtocol);
  INIT(AddCharByCharProtocoll);
  INIT(AddLscProtocol);
  INIT(AddSecopProtocol);
  INIT(MakeTrace);
  INIT(InitTaskOBJ);
  INIT(RemoteObjectInit);
  INIT(Logv2Init);
  INIT(LogListenInit);
  INIT(SiteInit);               /* site specific initializations */
}

static void InitIniCommands(SicsInterp * pInter)
{

/* declare and add permanent command */
#define PCMD(NAME, FUN) { \
     int FUN(SConnection * pCon, SicsInterp * pSics, void *pData, \
             int argc, char *argv[]); \
     AddCommandWithFlag(pInter, NAME, FUN, NULL, NULL, 0); \
   }
   
/* declare and add startup command */
#define SCMD(NAME, FUN) { \
     int FUN(SConnection * pCon, SicsInterp * pSics, void *pData, \
             int argc, char *argv[]); \
     AddCommandWithFlag(pInter, NAME, FUN, NULL, NULL, 1); \
   }

  /* permanent commands in alphabetic order */
  PCMD("alias",                 MakeAlias);
  PCMD("broadcast",             Broadcast);
  PCMD("ClientPut",             ClientPut);
  PCMD("ClientLog",             ClientLog);
  PCMD("config",                ConfigCon);
  PCMD("db",                    SICSDebug);
  PCMD("Dir",                   ListObjects);
  PCMD("dolater",               MakeCron);
  PCMD("DrivableInvoke",        TclDrivableInvoke);
  PCMD("DynSicsObj",            InstallSICSOBJ);
  PCMD("EVFactory",             EVControlFactory);
  PCMD("FileEval",              MacroFileEval);
  PCMD("findalias",             LocateAliasAction);
  PCMD("fulltransact",          TransactAction);
  PCMD("GetInt",                GetSICSInterrupt);
  PCMD("GetLog",                LogCapture);
  PCMD("GumPut",                GumPut);
  PCMD("InternEval",            InternalFileEval);
  PCMD("kill_command",          SICSKill);
  PCMD("list",                  SicsList);
  PCMD("MakeAsyncProtocol",     AsyncProtocolFactory);
  PCMD("MakeAsyncQueue",        AsyncQueueFactory);
  PCMD("MakeMcStasController",  McStasControllerFactory);
  PCMD("MakeMulti",             MakeMulti);
  PCMD("MakeOptimise",          MakeOptimiser);
  PCMD("MakeSecMotor",          SecMotorFactory);
  PCMD("Publish",               TclPublish);
  PCMD("Remob",                 RemobCreate);
  PCMD("ResetServer",           ResetStatus);
  PCMD("scriptcallback",        CallbackScript);
  PCMD("SetInt",                SetSICSInterrupt);
  PCMD("SetStatus",             SetSICSStatus);
  PCMD("SICSBounds",            SICSBounds);
  PCMD("sicscron",              MakeCron);
  PCMD("sicsdatafactory",       SICSDataFactory);
  PCMD("sicsdescriptor",        SICSDescriptor);
  PCMD("SICSLog",               LogOutput);
  PCMD("sicsprompt",            SicsPrompt);
  PCMD("SICSStatus",            SICSStatus);
  PCMD("sicstime",              SICSTime);
  PCMD("doubletime",            SICSDoubleTime);
  PCMD("SICSType",              SICSType);
  PCMD("Sics_Exitus",           SicsExit);
  PCMD("silent",                SICSSilent);
  PCMD("json2tcl",              SICSjson2tcl);
  PCMD("status",                UserStatus);
  PCMD("TclReplaceDrivable",    TclReplaceDrivable);
  PCMD("transact",              TransactAction);
  PCMD("wait",                  UserWait);
  PCMD("yield",                 UserYield);
  PCMD("checksum",              CheckSum);
  PCMD("loguserinfo",           LogUserInfoWrapper);
  PCMD("zdealer",               SICSZDealer);
  PCMD("zrouter",               SICSZRouter);

  /* startup commands in alphabetic order */
  SCMD("allowexec",             AllowExec);
  SCMD("AntiCollisionInstall",  AntiColliderFactory);
  SCMD("ChopperAdapter",        CHAdapterFactory);
  SCMD("MakeBatchManager",      MakeExeManager);
  SCMD("MakeChopper",           ChocoFactory);
  SCMD("MakeCone",              MakeCone);
  SCMD("MakeConfigurableMotor", MakeConfigurableVirtualMotor);
  SCMD("MakeCounter",           MakeCounter);
  SCMD("MakeDataNumber",        DNFactory);
  SCMD("MakeDiffScan",          MakeDiffScan);
  SCMD("MakeDrive",             MakeDrive);
  SCMD("MakeEnergy",            MakeEnergyVar);
  SCMD("MakeGPIB",              MakeGPIB);
  SCMD("MakeHdbTable",          HdbTableFactory);
  SCMD("MakeHKL",               HKLFactory);
  SCMD("MakeHKLMot",            HKLMotInstall);
  SCMD("MakeHM",                MakeHistMemory);
  SCMD("MakeHMControl",         MakeHMControl);
  SCMD("MakeLin2Ang",           MakeLin2Ang);
  SCMD("MakeMaxDetector",       LoMaxFactory);
  SCMD("MakeMaximize",          MaximizeFactory);
  SCMD("MakeMcStasReader",      McStasReaderFactory);
  SCMD("MakeMono",              MonoInit);
  SCMD("MakeMultiCounter",      MakeMultiCounter);
  SCMD("MakeMultiSec",          MakeMultiSec);
  SCMD("MakeNXScript",          MakeNXScript);
  SCMD("MakeO2T",               CreateO2T);
  SCMD("MakeOscillator",        MakeOscillator);
  SCMD("MakePeakCenter",        FitFactory);
  SCMD("MakeProxy",             ProxyFactory);
  SCMD("MakeRefList",           MakeReflectionList);
  SCMD("MakeRS232Controller",   RS232Factory);
  SCMD("MakeSansBC",            SansBCFactory);
  SCMD("MakeScanCommand",       ScanFactory);
  SCMD("MakeSecCounter",        MakeSecCter);
  SCMD("MakeSecHM",             MakeSecHM);
  SCMD("MakeSecNVS",            MakeSecNVS);
  SCMD("MakeSicsObj",           InstallSICSOBJ);
  SCMD("MakeSicsPoll",          InstallSICSPoll);
  SCMD("MakeSingleX",           MakeSingleX);
  SCMD("MakeStateMon",          StateMonFactory);
  SCMD("MakeSync",              MakeSync);
  SCMD("MakeTasScan",           TASUBScanFactory);
  SCMD("MakeTasUB",             TasUBFactory);
  SCMD("MakeTclInt",            MakeTclInt);
  SCMD("MakeUBCalc",            MakeUBCalc);
  SCMD("MakeWaveLength",        MakeWaveLengthVar);
  SCMD("MakeXYTable",           XYFactory);
  SCMD("Motor",                 MotorCreate);
  SCMD("ServerOption",          IFServerOption);
  SCMD("SicsAlias",             SicsAlias);
  SCMD("SicsUser",              PWSicsUser);
  SCMD("TokenInit",             TokenInit);
  SCMD("UpdateFactory",         UpdateFactory);
  SCMD("VarMake",               VarFactory);
  SCMD("VelocitySelector",      VelSelFactory);
  
}

/*--------------------------------------------------------------------------*/
int InitObjectCommands(pServer pServ, char *file)
{
  SConnection *pCon = NULL;
  char pBueffel[1024];
  int iRet;
  SicsInterp *pSics;
  pExeList pExe;
  pEnvMon pEnv = NULL;
  pSite site = NULL;

  pSics = pServ->pSics;
  assert(pSics);

  InitGeneral();

  /* general initialization */
  /* create a connection  */
  pCon = SCCreateDummyConnection(pSics);
  if (!pCon) {
    return 0;
  }
  MakeExeManager(pCon, pSics, NULL, 1, NULL);

  pExe = CreateExeList(pServ->pTasker);
  pServ->pExecutor = pExe;
  pEnv = CreateEnvMon();
  DevExecInit();

  assert(pExe);
  assert(pEnv);

  InitIniCommands(pSics);
  /*
     install site specific commands
   */
  site = getSite();
  if (site != NULL) {
    site->AddSiteCommands(pServ->pSics);
  }

  InstallBckRestore(pCon, pSics);
  InstallSICSHipadaba(pCon, pSics,NULL,0,NULL);

  /* evaluate the file */
  snprintf(pBueffel,sizeof(pBueffel)-1, "fileeval %s", file);
  iRet = InterpExecute(pSics, pCon, pBueffel);

  /* done */
  SCDeleteConnection(pCon);
  if (site != NULL && site->RemoveSiteCommands != NULL) {
    site->RemoveSiteCommands(pSics);
  }
  if(killStartupCommands){
    RemoveStartupCommands();
  }
  isDuringInitialization = 0;
  return 1;
}
/*---------------------------------------------------------------------------------*/
void KeepStartupCommands()
{
  killStartupCommands = 0;
}
