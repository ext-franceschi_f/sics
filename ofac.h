/**
 * Startup commands and definitions
 * 
 * copyright: see file COPYRIGHT
 * 
 *  SICS is a highly configurable system. This function implements
 *  the configurable. Configuration is meant to happen via a Tcl-
 *  configuration script. This module will initialize commands which
 *  create SICS-objects. Than an initialization file is evaluated
 *  via the macro facility. As most of the initialization commands
 *  will no longer be needed after this, they will be deleted.
 *  All this will be run with a higly privileged connection which 
 *  prints to stdout/stdin.
 *
 * Mark Koennecke 1996 - ?
 * Markus Zolliker Jan 2010
 */

#ifndef OBJECTFACTORY
#define OBJECTFACTORY
#include "sics.h"

int InitObjectCommands(pServer pServ, char *file);
#endif
