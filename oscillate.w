\subsection{Motor Oscillation}
This module allows to oscillate a motor, i.e. drive between two
positions back and  forth automatically. This is required for instance
in order to control a radial collimator or in order to prevent
preferred orientation effects in powder measurements. The oscialltion
can be started and stoped through commands. When starting, this module
takes over the motor in order to prevent it being driven by a
user. The limits of the oscillation are given through the current
software limits. When running, a special SICS task watches the motor
and makes it run the other way when it has arrived at one of its
boundaries. When oscillation is stopped, the motor is stopped, the
task stopped and the control of the motor is returned to the user.

In order to this, a data structure the following data structure is
required:
@d oscdat @{
typedef struct {
	pObjectDescriptor pDes;
	pMotor pMot;
	float upperLimit;
	float lowerLimit;
	int nextTargetFlag;
	long taskID;
	int stopFlag;
	SConnection *pCon;
	int errorCount;
        int debug;
	} Oscillator, *pOscillator;
@} 
The fields:
\begin{description}
\item[pDes] The SICS object descriptor.
\item[pMot] The motor controlled through this module.
\item[upperLimit] The uper limit of the oscillation.
\item[lowerLimit] the lower limits of the oscillation.
\item[nextTargetFlag] A flag which decides which limit is the next one
to drive to.
\item[taskID] The ID of the control task.
\item[stopFlag] A flag to signal the control task to stop.
\item[pCon] A dummy connection object to use for writing. Is
configured to write to log files.
\item[debug] A debug flag causing more messages to be printed.
\end{description}

The interface to this module is just the interpreter interface. The
rest is module local.

@o oscillate.h @{
/*-----------------------------------------------------------------------
  Oscillator runs a motor back and forth between its software limits.
  
  copyright: see file COPYRIGHT

  Mark Koennecke, November 2004
------------------------------------------------------------------------*/
#ifndef SICSOSCILLATOR
#define SICSOSCILLATOR
#include "motor.h"

@<oscdat@>
/*---------------------------------------------------------------------*/
int MakeOscillator(SConnection *pCon, SicsInterp *pSics, void *pData,
	int argc, char *argv[]);
int OscillatorWrapper(SConnection *pCon, SicsInterp *pSics, void *pData,
	int argc, char *argv[]);

#endif

@}
