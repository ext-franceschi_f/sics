/* ------------------------------------------------------------------------
  The OutCode's for SICS have to be translated from text at several
  places in SICS. Wherever necessary such code should include this file.
  This restricts changes to Scommon.h and this file
  
  Mark Koennecke, November 1996
----------------------------------------------------------------------------*/
#ifndef POUTCODE
#define POUTCODE

static char *pCode[] = {
  "internal",
  "command",
  "hwerror",
  "inerror",
  "status",
  "value",
  "start",
  "finish",
  "event",
  "warning",
  "error",
  "hdbvalue",
  "hdbevent",
  "log",
  "logerror",
  NULL
};
static int iNoCodes = 15;

const char* OutCodeToTxt(OutCode eOut);

int OutCodeFromText(const char *text, OutCode *outcode);

#endif
