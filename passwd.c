
/*--------------------------------------------------------------------------

	A little bit of password management
	


	Mark Koennecke,    October 1996

	Copyright:

	Labor fuer Neutronenstreuung
	Paul Scherrer Institut
	CH-5423 Villigen-PSI


        The authors hereby grant permission to use, copy, modify, distribute,
        and license this software and its documentation for any purpose, provided
        that existing copyright notices are retained in all copies and that this
        notice is included verbatim in any distributions. No written agreement,
        license, or royalty fee is required for any of the authorized uses.
        Modifications to this software may be copyrighted by their authors
        and need not follow the licensing terms described here, provided that
        the new terms are clearly indicated on the first page of each file where
        they apply.

        IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
        FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
        ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
        DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
        POSSIBILITY OF SUCH DAMAGE.

        THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
        INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
        IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
        NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
        MODIFICATIONS.
-----------------------------------------------------------------------------*/
#include "fortify.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "passwd.h"
#include "splitter.h"
#include "sics.h"

typedef struct __PENTRY {
  char *name;
  char *passwd;
  int iCode;
  struct __PENTRY *pNext;
} Pentry;

/* -------------------------- THE DATABASE --------------------------------*/
static Pentry *pPasswords = NULL;

/*=========================================================================*/
void AddUser(char *name, char *passwd, int iCode)
{
  Pentry *pNew = NULL;

  assert(name);
  assert(passwd);

  pNew = (Pentry *) malloc(sizeof(Pentry));
  assert(pNew);

  pNew->name = NULL;
  pNew->passwd = NULL;
  pNew->name = strdup(name);
  pNew->passwd = strdup(passwd);
  pNew->iCode = iCode;
  pNew->pNext = pPasswords;
  pPasswords = pNew;
}

/*--------------------------------------------------------------------------*/
int IsValidUser(char *name, char *password)
{
  int iRes = -10;
  Pentry *pCurrent = NULL;

  assert(pPasswords);

  if ((name == NULL) || (password == NULL)) {
    return iRes;
  }
  pCurrent = pPasswords;
  while (pCurrent) {
    if (strcmp(pCurrent->name, name) == 0) {
      if (strcmp(pCurrent->passwd, password) == 0) {
        return pCurrent->iCode;
      }
    }
    pCurrent = pCurrent->pNext;
  }
  return iRes;
}

/* --------------------------------------------------------------------------*/
void KillPasswd(void)
{
  Pentry *pCurrent, *pTemp;

  pCurrent = pPasswords;
  while (pCurrent) {
    pTemp = pCurrent->pNext;
    if (pCurrent->name)
      free(pCurrent->name);
    if (pCurrent->passwd)
      free(pCurrent->passwd);
    free(pCurrent);
    pCurrent = pTemp;
  }
  pPasswords = NULL;
}

/*-------------------------------------------------------------------------*/
int InitPasswd(char *filename)
{
  FILE *fp = NULL;
  char pLine[256];
  TokenList *pList = NULL;
  int iRes = 0;

  assert(filename);
  fp = fopen(filename, "r");
  if (!fp)
    return 0;

  while ((long)fgets(pLine, 255, fp) != EOF) {
    if (feof(fp))
      break;

    pList = SplitText(pLine);
    if ((!pList) || (!pList->pNext) || (!pList->pNext->pNext)) {
      printf("Invalid Passwd Entry ::\n %s \n", pLine);
      continue;
    } else {
      if (pList->pNext->pNext->Type != eInt) {
        continue;
      } else {
        AddUser(pList->text, pList->pNext->text,
                pList->pNext->pNext->iVal);
        iRes = 1;
      }
    }
    DeleteTokenList(pList);
    pList = NULL;
  }
  fclose(fp);
  return iRes;
}
/*----------------------- Password database update -------------------------*/
int PWSicsUser(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[])
{
  char pBueffel[512];
  TokenList *pList = NULL;

  assert(pSics);
  assert(pCon);

  /* check authorisation */
  if (SCGetRights(pCon) > usMugger) {
    SCWrite(pCon, "Insufficient privilege to set users", eError);
    return 0;
  }

  /* analyse commandlist */
  pList = SplitArguments(argc - 1, &argv[1]);
  if ((!pList) || (!pList->pNext) || (!pList->pNext->pNext)) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "Invalid Passwd Entry ::\n %s %s %s\n", argv[1],
            argv[2], argv[3]);
    SCWrite(pCon, pBueffel, eError);
    DeleteTokenList(pList);
    return 0;
  } else {
    if (pList->pNext->pNext->Type != eInt) {
      SCWrite(pCon, "Need integer rights code", eError);
      DeleteTokenList(pList);
      return 0;
    } else {
      AddUser(pList->text, pList->pNext->text, pList->pNext->pNext->iVal);
    }
  }
  DeleteTokenList(pList);
  return 1;
}
