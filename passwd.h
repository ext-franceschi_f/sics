/*---------------------------------------------------------------------------

	A little password database for SICS, is meant to handle something
	like 4 users.
	
	Mark Koenneck, October 1996
	
	copyright: sees implementation file
---------------------------------------------------------------------------*/
#ifndef MKPASSWD
#define MKPASSWD

int InitPasswd(char *filename);
void AddUser(char *name, char *passwd, int iCode);
int IsValidUser(char *name, char *passwd);
        /* 
           returns negative number if not found, iCode else
           iCode is the integer rights code the user is configured
           with.
         */
void KillPasswd(void);
#endif
