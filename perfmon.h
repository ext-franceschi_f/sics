
#line 54 "perfmon.w"

/*-------------------------------------------------------------------------
                            P E R F M O N
  A performance monitor for SICS. Maintains and calculates a value for
  cycles per seconds. 

  Mark Koennecke, Juli 1997

  copyright: see implementation file
---------------------------------------------------------------------------*/
#ifndef SICSPERFMON
#define SICSPERFMON


#line 35 "perfmon.w"

typedef struct __PerfMon *pPerfMon;
/*---------------------- live and death ----------------------------------*/
pPerfMon CreatePerfMon(int iInteg);
void DeletePerfMon(void *pData);
/*---------------------- increment ---------------------------------------*/
int IncrementPerfMon(pPerfMon self);
/*------------------------------------------------------------------------*/
float GetPerformance(pPerfMon self);
/*------------------------------------------------------------------------*/
int PerfMonWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[]);
/*-----------------------------------------------------------------------*/
int PerfMonTask(void *pPerf);
void PerfMonSignal(void *pPerf, int iSignal, void *pSigData);

#line 67 "perfmon.w"

#endif
