
#line 71 "perfmon.w"

/*--------------------------------------------------------------------------
   The             P E R F M O N           datastructure.

   Mark Koennecke, Juli 1997
---------------------------------------------------------------------------*/

#line 19 "perfmon.w"

    typedef struct __PerfMon {
                                pObjectDescriptor pDes;
                                pICallBack        pCall;
                                int               iLog; /* flag for
                                                  serverlog writing */       
                                float fCPS; /* cycles per seconds */
                                int iInteg; /* integration time */
                                int iCount;
                                time_t tLast; /* last time calculated */
                                time_t tTarget; /* next target time */
                                int iEnd;
                             }PerfMon;

#line 77 "perfmon.w"

