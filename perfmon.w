\subsection{Performance Monitor}
This object implements a performance monitor for SICS. Please note that SICS
performance is determined not only by processor speed but by a whole range
of factors:
\begin{itemize}
\item Processor speed and memory.
\item Speed of network connctions to hardware devices.
\item Number of clients connected.
\end{itemize}

 The PerfMonTask is called from the SICS task modules main loop. It will then 
 increment a  cycle counter and check if a predefined intergration time has 
 passed. If so a new 
value for cycle  per seconds will be calculated. This is also the main
output of PerfMon. PerfMon implements a callback interface in order to allow
automatical notification about system performance.

PerfMon has a little datastructure:
@d pdata @{
    typedef struct __PerfMon {
                                pObjectDescriptor pDes;
                                pICallBack        pCall;
                                int               iLog; /* flag for
                                                  serverlog writing */       
                                float fCPS; /* cycles per seconds */
                                int iInteg; /* integration time */
                                int iCount;
                                time_t tLast; /* last time calculated */
                                time_t tTarget; /* next target time */
                                int iEnd;
                             }PerfMon;
@}  

The interface consists of the following functions:
@d pInter @{
  typedef struct __PerfMon *pPerfMon;
/*---------------------- live and death ----------------------------------*/
  pPerfMon CreatePerfMon(int iInteg);
  void     DeletePerfMon(void *pData);
/*---------------------- increment ---------------------------------------*/
  int IncrementPerfMon(pPerfMon self);
/*------------------------------------------------------------------------*/
  float GetPerformance(pPerfMon self);
/*------------------------------------------------------------------------*/
  int PerfMonWrapper(SConnection *pCon, SicsInterp *pSics, void *pData,
                     int argc, char *argv[]);
/*-----------------------------------------------------------------------*/
  int PerfMonTask(void *pPerf);
  void PerfMonSignal(void *pPerf, int iSignal, void *pSigData);   
@}

There is really not much to this.

@o perfmon.h -d @{
/*-------------------------------------------------------------------------
                            P E R F M O N
  A performance monitor for SICS. Maintains and calculates a value for
  cycles per seconds. 

  Mark Koennecke, Juli 1997

  copyright: see implementation file
---------------------------------------------------------------------------*/
#ifndef SICSPERFMON
#define SICSPERFMON

@<pInter@>
#endif
@}

@o perfmon.i -d @{
/*--------------------------------------------------------------------------
   The             P E R F M O N           datastructure.

   Mark Koennecke, Juli 1997
---------------------------------------------------------------------------*/
@<pdata@>
@}
