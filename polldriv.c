/**
 * This is the sister module to sicspoll which defines the drivers for the 
 * various modes of polling SICS objects. 
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, November-December 2006
 */

#include <stdlib.h>
#include <assert.h>
#include <sics.h>
#include <splitter.h>

#include "polldriv.h"
#include "macro.h"
#include "sicshipadaba.h"
#include <tcl.h>

/*================ actual driver implementation =========================*/
static int timeDue(struct __POLLDRIV *self, double now, SConnection * pCon)
{
  if (now > self->nextPoll) {
    return 1;
  } else {
    return 0;
  }
}

/*------------------ HDB Driver -----------------------------------------*/
static int pollHdb(struct __POLLDRIV *self, SConnection * pCon)
{
  hdbValue newVal;
  pHdb node = NULL;

  memset(&newVal, 0, sizeof(hdbValue));
  node = (pHdb) self->objPointer;

  assert(node != NULL);

  if (GetHipadabaPar(node, &newVal, pCon) == 1) {
	ReleaseHdbValue(&newVal);
    return 1;
  } else {
    return 0;
  }
}

/*-----------------------------------------------------------------------*/
static pPollDriv makeHdbDriver(SConnection * pCon, char *objectIdentifier,
                               int argc, char *argv[])
{
  pHdb node = NULL;
  pPollDriv pNew = NULL;

  node = FindHdbNode(NULL, objectIdentifier, pCon);
  if (node == NULL) {
    SCWrite(pCon, "ERROR: object to poll not found", eError);
    return 0;
  }
  pNew = malloc(sizeof(PollDriv));
  if (pNew == NULL) {
    return NULL;
  }
  memset(pNew, 0, sizeof(PollDriv));


  pNew->objectIdentifier = strdup(objectIdentifier);
  pNew->objPointer = node;
  pNew->isDue = timeDue;
  pNew->poll = pollHdb;

  if (argc > 0) {
    pNew->pollIntervall = strtod(argv[0], NULL);
  } else {
    pNew->pollIntervall = 10.0;
  }

  return pNew;
}

/*==================== script poll driver ========================*/
static int pollScript(struct __POLLDRIV *self, SConnection * pCon)
{
  int status;
  Tcl_Interp *pTcl = InterpGetTcl(pServ->pSics);


  MacroPush(pCon);
  status = Tcl_Eval(pTcl, (char *) self->objPointer);
  MacroPop();
  if (status == 0) {
    return 1;
  } else {
    return 0;
  }
}

/*-----------------------------------------------------------------------*/
static void killScriptObj(void *data)
{
  if (data != NULL) {
    free(data);
  }
}

/*-----------------------------------------------------------------------*/
static pPollDriv makeScriptDriver(SConnection * pCon,
                                  char *objectIdentifier, int argc,
                                  char *argv[])
{
  pPollDriv pNew = NULL;
  char scriptBuffer[512];

  if (argc < 2) {
    SCWrite(pCon,
            "ERROR: need intervall and script parameter for script polling driver",
            eError);
    return NULL;
  }

  pNew = malloc(sizeof(PollDriv));
  if (pNew == NULL) {
    return NULL;
  }
  memset(pNew, 0, sizeof(PollDriv));

  pNew->pollIntervall = strtod(argv[0], NULL);
  memset(scriptBuffer, 0, 512);
  Arg2Text(argc - 1, &argv[1], scriptBuffer, 511);

  pNew->objectIdentifier = strdup(objectIdentifier);
  pNew->objPointer = strdup(scriptBuffer);
  pNew->isDue = timeDue;
  pNew->poll = pollScript;
  pNew->killObjPointer = killScriptObj;

  return pNew;
}

/*================ external interface ====================================*/
pPollDriv makePollDriver(SConnection * pCon, char *driver,
                         char *objectIdentifier, int argc, char *argv[])
{

  strtolower(driver);
  if (strcmp(driver, "hdb") == 0) {
    return makeHdbDriver(pCon, objectIdentifier, argc, argv);
  } else if (strcmp(driver, "script") == 0) {
    return makeScriptDriver(pCon, objectIdentifier, argc, argv);
  } else {
    SCWrite(pCon, "ERROR: polling driver type unknown", eError);
    return NULL;
  }
}

/*------------------------------------------------------------------------*/
void deletePollDriv(pPollDriv self)
{
  if (self->objectIdentifier != NULL) {
    free(self->objectIdentifier);
  }
  if (self->objPointer != NULL && self->killObjPointer != NULL) {
    self->killObjPointer(self->objPointer);
  }
  free(self);
}
