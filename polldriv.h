/**
 * This is the sister module to sicspoll which defines the drivers for the 
 * various modes of polling SICS objects. 
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, November-December 2006
 */
#ifndef POLLDRIV_H_
#define POLLDRIV_H_
#include <time.h>
#include "sics.h"
/*==================== a data structure ===================================*/
typedef struct __POLLDRIV {
  char *objectIdentifier;       /* the object identifier */
  void *objPointer;             /* a pointer to the object */
  double nextPoll;              /* next polling time */
  double pollIntervall;            /* poll intervall */
  double actualPollIntervall;      /* for supporting exponential backoff */
  int (*isDue) (struct __POLLDRIV * self, double now, SConnection * pCon);
  /* function called to determine if this object must be polled */
  int (*poll) (struct __POLLDRIV * self, SConnection * pCon);
  /* the actual polling function */
  void (*killObjPointer) (void *data);
  /* a function to possibly kill the objPointer. Can be NULL */
} PollDriv, *pPollDriv;
/*==================== the interface =====================================*/
/*
 * make a poll driver
 * @param pCon A connection to report errors too
 * @param driver the driver type to generate
 * @param objectIdentifier An identifer for the object to poll
 * @param argc number of additional parameter
 * @param *argv[] Additional parameters.
 * @return NULL on failure or a PollDriv strucure else.
 */
pPollDriv makePollDriver(SConnection * pCon, char *driver,
                         char *objectIdentifier, int argc, char *argv[]);
/**
 * free all memory associated with this poll driver
 * @param self The structure to delete
 */
void deletePollDriv(pPollDriv self);

#endif                          /*POLLDRIV_H_ */
