/**
 * This is the implementation of a SICS object which really is a placeholder
 * for another one. It shall be used in Hipadaba for sample enviornment 
 * devices. This is also the reason why the objectNode is supposed to be 
 * double. 
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, March 2008
 * 
 */
#ifndef PROXY_H_
#define PROXY_H_
int ProxyFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[]);

#endif                          /*PROXY_H_ */
