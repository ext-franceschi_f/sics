/**
 * Reflection list: a configurable list of reflections and 
 * their setting angles. To be used for four circle and possibly
 * TAS diffraction.
 * 
 * copyright: see File COPYRIGHT
 * 
 * Mark Koennecke, July 2008
 */
#ifndef REFLIST_H_
#define REFLIST_H_
#include "sics.h"
#include "sicsobj.h"
/**
 * This is an internal creation command: it only creates the data
 * structure but does not add the commands
 * \param name The name of the reflection list
 * \return A SICSOBJ representing a new reflection list
 */
pSICSOBJ MakeReflectionListInt(char *name);
/**
 * This is the standard SICS creation function for reflection lists
 */
int MakeReflectionList(SConnection * pCon, SicsInterp * pSics,
                       void *data, int argc, char *argv[]);
/**
 * This creates a full reflection list with commands and adds it to the 
 * interpreter. 
 * \param pCon for error messages
 * \param pSics the interpreter to add the command to
 * \param The name of the reflection list
 * \return A SICSOBJ representing the reflection list
 */
pSICSOBJ CreateReflectionList(SConnection * pCon, SicsInterp * pSics,
                              char *name);

void ConfigureReflectionListIndex(pSICSOBJ refl, char *header);
void ConfigureReflectionListSettings(pSICSOBJ refl, char *header);

void ClearReflectionList(pSICSOBJ refl);
int NewListReflection(pSICSOBJ refl);
void DelListReflection(pSICSOBJ refl, int idx);
int ReflectionListCount(pSICSOBJ refl);

void AddRefIdx(pSICSOBJ refl, double hkl[]);
void AddRefIdxAng(pSICSOBJ refl, double hkl[], double ang[]);

int SetRefIndex(pSICSOBJ refl, int idx, double hkl[]);
int SetRefAngles(pSICSOBJ refl, int idx, double ang[]);
int SetRefFlag(pSICSOBJ refl, int idx, int val);

int GetRefIndex(pSICSOBJ refl, int idx, double hkl[]);
int GetRefIndexID(pSICSOBJ refl, char *id, double hkl[]);
int GetRefAngles(pSICSOBJ refl, int idx, double ang[]);
int GetRefAnglesID(pSICSOBJ refl, char *id, double ang[]);
int GetRefFlag(pSICSOBJ refl, int idx);

char *GetRefName(pSICSOBJ refl, int idx);

const char *FindHKL(pSICSOBJ self, double h, double k, double l);

#endif                          /*REFLIST_H_ */
