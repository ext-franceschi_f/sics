/*--------------------------------------------------------------------------
remote driveable objects

Markus Zolliker July 04
	
copyright: see implementation file
	
----------------------------------------------------------------------------*/
#ifndef SICSREM
#define SICSREM
#include "Scommon.h"

/*-------------------------------------------------------------------------*/

int RemobCreate(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);

#endif
