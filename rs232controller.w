\subsection{RS232 Controller}
This class provides the basic communication facilities for an arbitrary
 controller which is connected through a RS-232 cable to a terminal 
 server. This class bypasses David Maden's SerPortServer program. Also this 
 code may be useful for any other controller connected to a TCP/IP
 network.  Basic facilities are provided for writing data to such a device 
 and to read from 
 it. Morevoer there are utility functions for the common case when the 
 device is to send some data terminated with a specified terminator.
 Timeouts are also observed on reading operations. It is required that this
 controller accesses a binary port and not a port which uses some kind of
 telnet negotiation.  


 This classes data structure:

@d rs232dat @{
	typedef struct{
			pObjectDescriptor pDes;
			char *sendTerminator;
			char *replyTerminator;
			int timeout;
			mkChannel *pSock;
			char *pHost;
			int iPort;
                        int debug;
			int registered;
                      } rs232, *prs232;

@}
The fields are:
\begin{description}
\item[pDes] The standard object descriptor.
\item[sendTerminator] The terminator with which to terminate any command.
\item[replyTerminator] The terminator expected to end a transmission from the
 device.
\item[timeout] A timeout for reading in microseconds.
\item[mkChannel] Our very own structure for a network connection.
\item[pHost]The host (mostly the terminal server) to connect to.
\item[iPort] The port at host to which to connect.
\item[debug] a flag which switches logging of the communication to
stdout on. 
\item[registered] a flag which is set if the registration of the rs232
controllers network port has succeeded.  
\end{description}

The following interface functions are provided: 

@d rs232int @{
	int RS232Action(SConnection *pCon, SicsInterp *pSics,
			void *pData, int argc, char *argv[]);
	int RS232Factory(SConnection *pCon, SicsInterp *pSics,
			void *pData, int argc, char *argv[]);

	void setRS232SendTerminator(prs232 self, char *term);
	void setRS232ReplyTerminator(prs232 self, char *term);
        void setRS232Timeout(prs232 self, int timeout);
	void setRS232Debug(prs232 self, int deb);

	int writeRS232(prs232 self, void *data, int dataLen);
	int readRS232(prs232 self, void *data, int *dataLen);
	int readRS232TillTerm(prs232 self, void *data, int *datalen);
	int availableRS232(prs232 self);
        int availableNetRS232(prs232 self);
	int transactRS232(prs232 self, void *send, int sendLen,
				void *reply, int replylen);

	void getRS232Error(int iCode, char *errorBuffer, 
                                             int errorBufferLen);
	int getRS232Timeout(prs232 self);
        int initRS232(prs232 self);
        int initRS232WithFlags(prs232 self, int flags);
        int initRS232Finished(prs232 self);
	void closeRS232(prs232 self);
        prs232 createRS232(char *host, int iPort);
	void KillRS232(void *pData);
@}

All functions take a pointer to their daat structure as a parameter.
When the functions return an integer, 1 measn successful completion, 
 anything else is an error code if not stated otherwise. 
The functions have the following meanings:
\begin{description}
\item[RS232Action] The interpreter interface functon for the controller.
\item[RS232Factory] The factory function for configuring an interpreter.
\item[setRS232SendTerm] sets the terminator with which each command is
  terminated.
\item[setRS232ReplyTerminator] sets the expected terminator in a reply from
 the device.
\item[setRS232Timeout] sets the timeout for reading from the device. The value
 is in microseconds.
\item[writeRS232] writes dataLen bytes from data to the device.
\item[readRS232] reads at max dataLen bytes from the device. dataLen is set
 to the actual number of bytes read.
\item[transactRS232] sends sendLen bytes from send to the device and then 
 reads from the device until the reply terminator has been found. At max
 replyLen bytes of reply are copied to reply.
\item[availableRS232] returns 1 if data is available, o if none is available 
 and a negative value if an error occurs.
\item[availableNetRS232] returns 1 when data is pending at the network
reader for this port, else 0. This function also resets the network reader for 
further use. This could lead to trouble if pending data is not
directly followed by a read.  
\item[getRS232Error] gets a string representation for the error code 
 iCode.
\item[initRS232] tries to close and reopen the RS232 connection. This is 
 useful for the automatic fixing of communication problems encountered.
\item[initRS232WithFlags] the same as initRS232, but with flags. flags = 1:
 do not block on connect (use initRS232Finished to check success).
 flags=2: wait 1000 ms after last close or restart (workaround for a bug
 in Lantronix terminal server). flags=3: both options. flags=0: no opitons.
\item[initRS232Finished] returns 0 when connect in progress, 1 when finished,
 FAILEDCONNECT on failure.
\item[closeRS232] closes a network connection but does not delete the datastructure.
\item[createRS232] creates a new rs232 data structure with all
parameters at  default values. The connection is NOT opened. 
\end{description}

@o rs232controller.h @{
/*---------------------------------------------------------------------
	         R S 2 3 2 C o n t r o l l e r
  
  A general object which represents a controller connected to the network
  via a terminal server. This bypasses David Maden's SerPortServer software.
  Basic facilities are provided for writinga nd reading to and from the 
  device. For more information see the rs232controller.tex file.

  copyright: see copyright.h

  Mark Koennecke, October 2001
-----------------------------------------------------------------------*/
#ifndef RS232CONTROLLER
#define RS232CONTROLLER
#include "network.h"
/*
  own error codes
*/
#define NOTCONNECTED  -2700
#define BADMEMORY     -2701
#define TIMEOUT       -2702
#define FAILEDCONNECT -2703
#define INCOMPLETE    -2704
#define BADREAD       -2705
#define BADSEND       -2706

/*----------------------- a data structure ----------------------------*/
@<rs232dat@>

/*----------------------- the interface functions --------------------*/
@<rs232int@>

#endif
@}







