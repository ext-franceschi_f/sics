/**
 * This is a buffer to store bytes for reading and writing.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, January 2009
 *
 * added resizing option and MakeBigRWPuffer in order to support transfer 
 * of large amounts of image data on few connections
 *
 * Mark Koennecke, August 2014
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "rwpuffer.h"
/*----------------------------------------------------------------------*/
typedef struct __RWBuffer {
  char *data;
  int length;
  int startPtr;
  int endPtr;
  int maxSize;
} RWBuffer;
/*----------------------------------------------------------------------*/
prwBuffer MakeRWPuffer(int size)
{
  prwBuffer self = NULL;

  self = malloc(sizeof(RWBuffer));
  if (self == NULL) {
    return NULL;
  }
  self->data = calloc(size, sizeof(char));
  if (self->data == NULL) {
    return NULL;
  }
  self->length = size;
  self->startPtr = 0;
  self->endPtr = 0;
  self->maxSize = size;
  return self;
}
/*------------------------------------------------------------------------*/
prwBuffer MakeBigRWPuffer(int size, int maxSize)
{
  prwBuffer result = MakeRWPuffer(size);
  if(result != NULL){
    result->maxSize = maxSize;
  }
  return result;
}
/*------------------------------------------------------------------------*/
void KillRWBuffer(prwBuffer self)
{
  if (self == NULL) {
    return;
  }
  if (self->data != NULL) {
    free(self->data);
  }
  free(self);
}
/*------------------------------------------------------------------------*/
int CanStoreRWBuffer(prwBuffer self, void *data, int count)
{
  int length;
  char *ptr;

  length = self->endPtr - self->startPtr;
  if (count + length >= self->length ) {
    if(self->length < self->maxSize){
      ptr = calloc(self->maxSize,sizeof(char));
      if(ptr == NULL) {
	  return 0;
      }
      memcpy(ptr,self->data, length*sizeof(char));
      free(self->data);
      self->data = ptr;
      self->length = self->maxSize;
    } else {
      return 0;
    }
  }
  return 1;
}
/*------------------------------------------------------------------------*/
int StoreRWBuffer(prwBuffer self, void *data, int count)
{
  int length;
  char *ptr;

  length = self->endPtr - self->startPtr;
  if (count + length >= self->length ) {
    if(self->length < self->maxSize){
      ptr = calloc(self->maxSize,sizeof(char));
      if(ptr == NULL) {
	  printf("HELP: RWBuffer overrun!!!!\n");
	  return 0;
      }
      memcpy(ptr,self->data, length*sizeof(char));
      free(self->data);
      self->data = ptr;
      self->length = self->maxSize;
    } else {
	  printf("HELP: RWBuffer overrun!!!!\n");
	  return 0;
    }
  }
  if (count + self->endPtr > self->length) {
    memmove(self->data, self->data + self->startPtr, length);
    self->startPtr = 0;
    self->endPtr = length;
  }
  memcpy(self->data + self->endPtr, data, count);
  self->endPtr += count;
  return 1;
}

/*------------------------------------------------------------------------*/
void *GetRWBufferData(prwBuffer self, int *length)
{
  *length = self->endPtr - self->startPtr;
  return (void *) self->data + self->startPtr;
}

/*-------------------------------------------------------------------------*/
void RemoveRWBufferData(prwBuffer self, int count)
{
  self->startPtr += count;
  if (self->startPtr >= self->endPtr) {
    self->startPtr = 0;
    self->endPtr = 0;
    memset(self->data,0,self->length*sizeof(char));
  }
}
