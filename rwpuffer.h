/**
 * This is a buffer to store bytes for reading and writing.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, January 2009
 */
#ifndef RWPUFFER_H_
#define RWPUFFER_H_

typedef struct __RWBuffer *prwBuffer;

/**
 * \brief create a RW buffer.
 * \param size The size of the buffer.
 * \return NULL on success, else a pointer to t a new rwPuffer
 */
prwBuffer MakeRWPuffer(int size);
/**
 * \brief create a RW buffer which can grow.
 * \param size The size of the buffer.
 * \param maxSize The maximum size of the buffer. 
 * \return NULL on success, else a pointer to t a new rwPuffer
 */
prwBuffer MakeBigRWPuffer(int size, int maxSize);
/**
 * \brief delete a rw buffer.
 * \param self The rwPuffer to delete.
 */
void KillRWBuffer(prwBuffer self);
/**
 * \brief store some data in the RW buffer
 * \param self The rw buffer to store the data in
 * \param data pointer to the data to store
 * \param count The number of bytes to store
 * \return 1 on success, 0 on failure
 */
int StoreRWBuffer(prwBuffer self, void *data, int count);
/**
 * \brief Test if the data can be stored in the rwBuffer
 * \param self The rw buffer to store the data in
 * \param data pointer to the data to store
 * \param count The number of bytes to store
 * \return 1 when OK, 0 when buffer full
 */
int CanStoreRWBuffer(prwBuffer self, void *data, int count);
/**
 * \brief Get a pointer to the current buffer data
 * \param self the buffer to get the data from
 * \param length Will be set to the number of available bytes.
 * \return A pointer to the data
 */
void *GetRWBufferData(prwBuffer self, int *length);
/**
 * \brief remove data from the buffer
 * \param self the buffer to remove data from
 * \param count The number of bytes to remove
 */
void RemoveRWBufferData(prwBuffer self, int count);

#endif                          /*RWPUFFER_H_ */
