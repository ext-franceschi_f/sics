/**
 * This is a little routine to calculate the beam center from SANS data 
 * in the detector. This is basically a COG or COC (center of contour) 
 * calculation above a  given threshold. Where we make an effor to 
 * find the threshold ourselves.
 * 
 * copyright: GPL
 * 
 * Mark Koennecke, July 2009
 */
#ifndef SANSBC_H_
#define SANSBC_H_
int SansBCFactory(SConnection *pCon, SicsInterp *pSics, void *pData, 
		int argc, char *argv[]);

#endif /*SANSBC_H_*/
