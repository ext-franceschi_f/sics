#include <stdio.h>
#include "dynstring.h"
#include "statusfile.h"
#include "sicshipadaba.h"

/* this file is obsolete (M.Z. 9.2011) */

static void SaveHdbBranch(pHdb node, FILE * fil)
{
  pHdb child;
  char prop[16];
  pDynString dyn;
  char path[1024];

  if (GetHdbProperty(node, "save", prop, sizeof prop)) {
    if (strcmp(prop, "me") == 0) {
      dyn = formatValue(node->value, node);
      GetHdbPath(node, path, sizeof path);
      fprintf(fil, "hupdate %s %s\n", path, GetCharArray(dyn));
      DeleteDynString(dyn);
    }
    for (child = node->child; child != NULL; child = child->next) {
      SaveHdbBranch(child, fil);
    }
  }
}

static int SaveHdbTree(void *object, char *name, FILE * fil)
{
  pHdb node;

  SaveHdbBranch(GetHipadabaRoot(), fil);
  return 1;
}

static hdbCallbackReturn SaveHdbCallback(pHdb node, void *userData,
                                         pHdbMessage message)
{
  pHdbDataMessage mm = NULL;

  if ((mm = GetHdbUpdateMessage(message)) == NULL) {
    return hdbContinue;
  }

  StatusFileDirty();
  return hdbContinue;;
}

static int SaveHdbEnable(SConnection * con, SicsInterp * sics,
                         void *data, int argc, char *argv[])
{
  pHdb node;
  char prop[16];
  pHdbCallback cb;

  if (argc < 2) {
    SCPrintf(con, eError, "ERROR: should be: %s <path>", argv[0]);
    return 0;
  }

  node = FindHdbNode(NULL, argv[1], con);
  if (!node) {
    SCPrintf(con, eError, "ERROR: %s not found", argv[1]);
    return 0;
  }
  cb = MakeHipadabaCallback(SaveHdbCallback, NULL, NULL);
  assert(cb);
  AppendHipadabaCallback(node, cb);

  SetHdbProperty(node, "save", "me");
  for (node = node->mama; node != NULL; node = node->mama) {
    if (!GetHdbProperty(node, "save", prop, sizeof prop)) {
      SetHdbProperty(node, "save", "kids");
    }
  }
  StatusFileDirty();
  return 1;
}

void SaveHdbInit(void)
{
  pDummy hs = NULL;

  hs = CreateDummy("hdb saver");
  hs->pDescriptor->SaveStatus = SaveHdbTree;
  AddCommandWithFlag(pServ->pSics, "hsave", SaveHdbEnable, KillDummy, hs,
                     0);
}
