
/*--------------------------------------------------------------------------
   Internal header file holding the definition of the scan objects data
   structure.

   Mark Koennecke, October 1997
----------------------------------------------------------------------------*/
#include "sdynar.h"
#include "scanvar.h"
#include "stringdict.h"

/*--------------------------------------------------------------------------*/
  typedef struct {
                   int i;
                   long lCount;
                   long Monitors[10];
                   float fTime;
                 } CountEntry, *pCountEntry;
/*---------------------------------------------------------------------------*/ 
  typedef struct __ScanData {
                             pObjectDescriptor pDes;
                             pICallBack        pCall;
                             pDynar            pScanVar;
                             char              objectName[132];
                             int               iScanVar;
                             int               iNP;
                             int               iMode;
                             float             fPreset;
                             char              pFile[1024];
                             FILE              *fd;
                             SicsInterp        *pSics;
                             SConnection       *pCon;
                             char              pRecover[1024];
                             char              pHeaderFile[1024];
                             int               (*PrepareScan)(pScanData self);
                             int               (*WriteHeader)(pScanData self);
                             int               (*WriteScanPoints)
                                                      (pScanData self, 
                                                       int iPoint);
                             int               (*ScanDrive)(pScanData self, 
                                                            int i);
                             int               (*ScanCount)(pScanData self,
                                                            int i);
                             int               (*CollectScanData)
                                                           (pScanData self,
                                                            int iP);
                             pStringDict       scanFunctions;
                             long              lPos;
                             void              *pCounterData;
                             char              pCounterName[512];
                             int               iChannel;
                             pDynar            pCounts;
                             int               iCounts;
                             int               iActive;
                             int               iWindow;
                             char              *pCommand;
                             void              *pSpecial;
                             } ScanData;


/*
  internal helper functions for scan implementations
*/

/*
  CollectCounterData collects all the data from the configured counter
  ans stows it away
*/
CountEntry CollectCounterData(pScanData self);
void InitCountEntry(pCountEntry pCount);
