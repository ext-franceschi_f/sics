/*-----------------------------------------------------------------------
  Implementation file for the SICS ScanVariable. This is a support module 
  for the SICS scan system.

  Evolved during refactoring scan in November 2004
  
  copyright: see file COPYRIGHT

  Mark Koennecke, November 2004
-------------------------------------------------------------------------*/
#include <stdio.h>
#include <assert.h>
#include "fortify.h"
#include "sics.h"
#include "scanvar.h"
#include "lld.h"
#include "devexec.h"
/*----------------------------------------------------------------------*/
pVarEntry MakeScanVar(SicsInterp * pSics, SConnection * pCon, char
                      *name, float start, float step)
{
  CommandList *pCom = NULL;
  pIDrivable pDriv = NULL;
  pDummy pData = NULL;
  pVarEntry pVar = NULL;
  char pBueffel[512];

  /*
     allocate space
   */
  pVar = (pVarEntry) malloc(sizeof(VarEntry));
  if (pVar == NULL) {
    SCWrite(pCon, "ERROR: out of memory allocating scan variable", eError);
    return NULL;
  }
  memset(pVar, 0, sizeof(VarEntry));

  /* find the thing */
  pCom = FindCommand(pSics, name);
  if (!pCom) {
    snprintf(pBueffel, 511, "ERROR: Cannot find variable %s to scan",
             name);
    SCWrite(pCon, pBueffel, eError);
    return NULL;
  }
  pData = (pDummy) pCom->pData;
  if (!pData) {
    snprintf(pBueffel, 511, "ERROR: Cannot find data for variable %s",
             name);
    SCWrite(pCon, pBueffel, eError);
    return NULL;
  }
  pDriv = pData->pDescriptor->GetInterface(pData, DRIVEID);
  if (!pDriv) {
    snprintf(pBueffel, 511,
             "ERROR: variable %s is NOT driveable and cannot be scanned",
             name);
    SCWrite(pCon, pBueffel, eError);
    return NULL;
  }

  /* got everything, fill in the VarEntry structure */
  strlcpy(pVar->Name, name,131);
  pVar->pInter = pDriv;
  pVar->pObject = pData;
  pVar->fStart = start;
  pVar->fStep = step;
  pVar->dataList = LLDcreate(sizeof(float));

  return pVar;
}

/*----------------------------------------------------------------------*/
pVarEntry MakeLogVar(SicsInterp * pSics, SConnection * pCon, char *name)
{
  CommandList *pCom = NULL;
  pIDrivable pDriv = NULL;
  pDummy pData = NULL;
  pVarEntry pVar = NULL;
  char pBueffel[512];

  /*
     allocate space
   */
  pVar = (pVarEntry) malloc(sizeof(VarEntry));
  if (pVar == NULL) {
    SCWrite(pCon, "ERROR: out of memory allocating scan variable", eError);
    return NULL;
  }
  memset(pVar, 0, sizeof(VarEntry));

  /* find the thing */
  pCom = FindCommand(pSics, name);
  if (!pCom) {
    snprintf(pBueffel, 511, "ERROR: Cannot find variable %s to log", name);
    SCWrite(pCon, pBueffel, eError);
    return NULL;
  }
  pData = (pDummy) pCom->pData;
  if (!pData) {
    snprintf(pBueffel, 511, "ERROR: Cannot find data for variable %s",
             name);
    SCWrite(pCon, pBueffel, eError);
    return NULL;
  }
  pDriv = pData->pDescriptor->GetInterface(pData, DRIVEID);
  if (!pDriv) {
    snprintf(pBueffel, 511,
             "ERROR: variable %s is NOT driveable and cannot be logged",
             name);
    SCWrite(pCon, pBueffel, eError);
    return NULL;
  }

  /* got everything, fill in the VarEntry structure */
  strlcpy(pVar->Name, name,131);
  pVar->pInter = pDriv;
  pVar->pObject = pData;
  pVar->logVar = 1;
  pVar->dataList = LLDcreate(sizeof(float));

  return pVar;
}

/*------------------------------------------------------------------*/
void InitScanVar(pVarEntry pVar)
{
  LLDdelete(pVar->dataList);
  pVar->dataList = LLDcreate(sizeof(float));
}

/*--------------------------------------------------------------------*/
void DeleteVarEntry(void *pData)
{
  pVarEntry pVar = NULL;

  pVar = (pVarEntry) pData;

  if (pVar == NULL) {
    return;
  }

  if (pVar->fData) {
    free(pVar->fData);
  }
  LLDdelete(pVar->dataList);
  free(pVar);
}

/*------------------------------------------------------------------------*/
char *ScanVarName(pVarEntry pVar)
{
  return pVar->Name;
}

/*------------------------------------------------------------------------*/
float ScanVarStart(pVarEntry pVar)
{
  return pVar->fStart;
}

/*-------------------------------------------------------------------------*/
float ScanVarStep(pVarEntry pVar)
{
  return pVar->fStep;
}

/*------------------------------------------------------------------------*/
int StartScanVar(pVarEntry pVar, SConnection * pCon, int i)
{
  float fVal;
  pDummy pDum;
  char pBueffel[512];
  int status;

   /**
    * logged variables are not started
    */
  if (pVar->logVar == 1) {
    return 1;
  }

  pDum = (pDummy) pVar->pObject;
  fVal = pVar->fStart + i * pVar->fStep;
  status = StartDevice(pServ->pExecutor,
                       pVar->Name,
                       pDum->pDescriptor, pVar->pObject, pCon, 
                       RUNDRIVE, fVal);
  if (!status) {
    snprintf(pBueffel, 511, "ERROR: Failed to start %s", pVar->Name);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  return 1;
}

/*-------------------------------------------------------------------------*/
int CheckScanVar(pVarEntry pVar, SConnection * pCon, int np)
{
  int status;
  char pError[132], pBueffel[512];

  if (pVar->logVar == 1) {
    return 1;
  }

  status = pVar->pInter->CheckLimits(pVar->pObject,
                                     pVar->fStart, pError, 131);
  if (status != 1) {
    snprintf(pBueffel, 511, "ERROR: %s, scan aborted", pError);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
  status = pVar->pInter->CheckLimits(pVar->pObject,
                                     pVar->fStart + np * pVar->fStep,
                                     pError, 131);
  if (status != 1) {
    snprintf(pBueffel, 511, "ERROR: %s, scan aborted", pError);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
  return 1;
}

/*-------------------------------------------------------------------------*/
void AppendScanVar(pVarEntry pVar, float pos)
{
  float fVal = pos;
  LLDnodeAppendFrom(pVar->dataList, &fVal);
}

/*------------------------------------------------------------------------*/
float GetScanVarPos(pVarEntry pVar, int i)
{
  int count = 0, status;

  status = LLDnodePtr2First(pVar->dataList);
  while (count < i && (status = LLDnodePtr2Next(pVar->dataList)) != 0) {
    count++;
  }
  if (count == i) {
    return LLDnodeFloat(pVar->dataList);
  } else {
    return -99999.99;
  }
}

/*------------------------------------------------------------------------*/
void CopyScanVar(pVarEntry pVar, float *fData, int np)
{
  int i, count = 0, status;

  status = LLDnodePtr2First(pVar->dataList);
  while (status > 0 && count < np) {
    fData[count] = LLDnodeFloat(pVar->dataList);
    count++;
    status = LLDnodePtr2Next(pVar->dataList);
  }
}

/*-------------------------------------------------------------------------*/
int isLogVar(pVarEntry pVar)
{
  return pVar->logVar;
}
