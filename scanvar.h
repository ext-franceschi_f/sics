
/*-----------------------------------------------------------------------
  Header file for the SICS ScanVariable. This is a support module for
  the SICS scan system.

  Evolved during refactoring scan in November 2004
  
  copyright: see file COPYRIGHT

  Mark Koennecke, November 2004
-------------------------------------------------------------------------*/
#ifndef SICSSCANVAR
#define SICSSCANVAR
#include "sics.h"

typedef struct {
  char Name[132];
  pIDrivable pInter;
  pDummy pObject;
  float fStart;
  float fStep;
  float *fData;
  int dataList;
  int logVar;
} VarEntry, *pVarEntry;

/*---------------------------------------------------------------------*/

/**
  * MakeScanVar creates a scan variable. All the necessary checks are 
  * performed
  * @param pSics The interpreter in order to locate the variable.
  * @param pCon A connection object for error reporting
  * @param name The name of the variable to scan
  * @param start The start position from which to scan
  * @param step The step width with which to scan.
  * @return A pointer to a new scan variable object on success, NULL
  * else
  */
pVarEntry MakeScanVar(SicsInterp * pSics, SConnection * pCon, char
                      *name, float start, float step);
  /**
    * make a variable which is logged during the scan but not driven.
  * @param pSics The interpreter in order to locate the variable.
  * @param pCon A connection object for error reporting
  * @param name The name of the variable to log
  */
pVarEntry MakeLogVar(SicsInterp * pSics, SConnection * pCon, char *name);
  /**
    * InitScanVar clears the list of scan points
    * @param pvar The scna variable to clear
    */
void InitScanVar(pVarEntry pVar);
  /**
    * DeleteVarEntry deletes a scan variable.
    * @param pData The scan variable entry to delete.
    */
void DeleteVarEntry(void *pData);
  /**
    * ScanVarName returns the name of the scan variable
    * @param pVar The scan variable to query.
    * @return The name of the scan variable. Do not delete pointer.
    */
char *ScanVarName(pVarEntry pVar);
  /**
    * ScanVarStart returns the start value for the scan 
    * @param pVar The scan variable to query.
    * @return The start point for the scan.
    */
float ScanVarStart(pVarEntry pVar);
  /**
    * ScanVarStep returns the start value for the scan  
    * @param pVar The scan variable to query.
    * @return The step width for the scan.
    */
float ScanVarStep(pVarEntry pVar);
  /**
    * StartScanVar starts the scan variable to drive to the next
    * position.
    * @param pVar The scan variable to start.
    * @param pCon The connection to report errors to.
    * @param i The position number to drive to
    * @return 1 on success, 0 on failure
   */
int StartScanVar(pVarEntry pVar, SConnection * pCon, int i);
  /**
    * AppendScanVar appends a position to the list of positions
    * reached while scanning this variable.
    * @param pVar The scan variable to append to.
    * @param pos The position to append.
    */
void AppendScanVar(pVarEntry pVar, float pos);
  /**
    * GetScanVarPos returns a position for an index.
    * @param pVar The scan variable to append to.
    * @param i The position number to retrieve
    * @return  The positiopn or -99999.99 for an error
    */
float GetScanVarPos(pVarEntry pVar, int i);
  /**
    * CopyScanVar copies the scan positions to the array given.
    * @param pVar The scan variable to copy from
    * @param fData The array to copy to.
    * @param np The number of slots in fData.
    */
void CopyScanVar(pVarEntry pVar, float *fData, int np);
  /**
    * CheckScanVar checks if the scan variable can be driven through the
    * whole range.
    * @param pVar The scan variable to check
    * @param pCon The connection object to which to report errors.
    * @param np The number of  points to check for.
    * @return 0 on failuyre, 1 on success
    */
int CheckScanVar(pVarEntry pVar, SConnection * pCon, int np);
  /**
    * queries if the variable is alogged variable or a drive one.
    * @param pVar The variable to query.
    * @return 1 if log var, 0 else
    */
int isLogVar(pVarEntry pVar);

#endif
