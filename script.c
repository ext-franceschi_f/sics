/*-------------------------------------------------------------------------
				S C R I P T

       A few utility commands which permit more control about Sics from
       within the macro language.
       
       Mark Koennecke, February 1997
       
       added sicsprompt, Mark Koennecke, November 1999

       Copyright:

       Labor fuer Neutronenstreuung
       Paul Scherrer Institut
       CH-5423 Villigen-PSI


      The authors hereby grant permission to use, copy, modify, distribute,
      and license this software and its documentation for any purpose, provided
      that existing copyright notices are retained in all copies and that this
      notice is included verbatim in any distributions. No written agreement,
      license, or royalty fee is required for any of the authorized uses.
      Modifications to this software may be copyrighted by their authors
      and need not follow the licensing terms described here, provided that
      the new terms are clearly indicated on the first page of each file where
      they apply.

      IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
      FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
      ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
      DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
      POSSIBILITY OF SUCH DAMAGE.

      THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
      FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
      IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
      NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
      MODIFICATIONS.
----------------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>
#include <math.h>
#include "fortify.h"
#include "sics.h"
#include "fupa.h"
#include "motor.h"
#include "countdriv.h"
#include "counter.h"
#include "interrupt.h"
#include "splitter.h"
#include "status.h"
#include "script.h"
#include "devexec.h"
#include <tcl.h>

extern void SNXFormatTime(char *pBueffel, int iLen);    /* in nxdata.c */

/* -------------------------- Interrupts -----------------------------------*/
int GetSICSInterrupt(SConnection * pCon, SicsInterp * pSics, void *pData,
                     int argc, char *argv[])
{
  char pBueffel[132];
  SConnection *pOwner = NULL;
  pExeList pDev = NULL;

  assert(pCon);
  assert(pSics);

  /* we are interested in the interrupt pending in the executor. If
     not specified, use our very own
   */
  pDev = GetExecutor();
  assert(pDev);
  pOwner = GetExeOwner(pDev);
  if (pOwner) {
    Interrupt2Text(SCGetInterrupt(pOwner), pBueffel, 131);
    SCWrite(pCon, pBueffel, eValue);
  } else {
    Interrupt2Text(SCGetInterrupt(pCon), pBueffel, 131);
    SCWrite(pCon, pBueffel, eValue);
  }
  return 1;
}

/*-------------------------------------------------------------------------*/
int SetSICSInterrupt(SConnection * pCon, SicsInterp * pSics, void *pData,
                     int argc, char *argv[])
{
  int iInt;
  char pBueffel[132];
  SConnection *pOwner = NULL;
  pExeList pDev = NULL;

  assert(pCon);
  assert(pSics);

  /* minimum  user privelege */
  if (!SCMatchRights(pCon, usUser)) {
    SCWrite(pCon, "ERROR: you are not priviledged to set interrupts",
            eError);
    return 0;
  }

  /* only in script processing */
  if (!SCinMacro(pCon)) {
    SCWrite(pCon, "ERROR: Interrupt manipulation only permitted in Macros",
            eError);
    return 0;
  }

  /* is there a value ? */
  if (argc < 2) {
    SCWrite(pCon, "ERROR: missing parameter for SetInterrupt", eError);
    return 0;
  }

  /* actually do a job */
  strtolower(argv[1]);
  iInt = Text2Interrupt(argv[1]);
  if (iInt < 0) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s not recognized as Interrupt code",
            argv[1]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  /* we want to change the interrupt pending in the executor. If none
     there, charnge our own.
   */
  pDev = GetExecutor();
  assert(pDev);
  pOwner = GetExeOwner(pDev);
  if (pOwner) {
    SCSetInterrupt(pOwner, iInt);
  } else {
    SCSetInterrupt(pCon, iInt);
  }
  SCSendOK(pCon);
  return 1;
}

/*-------------------------------------------------------------------------
This method does nothing anymore. Status is now calculated from the state 
of SICS and no longer explicitly set. I left the code in because I am to 
lazy to find all use cases and eliminate them

M.K. 07/2014
--------------------------------------------------------------------------*/
int SetSICSStatus(SConnection * pCon, SicsInterp * pSics, void *pData,
                  int argc, char *argv[])
{
  int iInt = 0;
  char pBueffel[132];

  assert(pCon);
  assert(pSics);

  /* minimum  user privelege */
  if (!SCMatchRights(pCon, usUser)) {
    SCWrite(pCon, "ERROR: you are not priviledged to set status", eError);
    return 0;
  }

  /* only in script processing */
  if (!SCinMacro(pCon)) {
    SCWrite(pCon, "ERROR: status manipulation only permitted in Macros",
            eError);
    return 0;
  }

  SCWrite(pCon,"Status is calculated, cannot be set manually any longer", eValue);
  return 1;
  
  /*
    The code below is defunct and just here for documentary reasons
  */

  /* is there a value ? */
  if (argc < 2) {
    SCWrite(pCon, "ERROR: missing parameter for SetStatus", eError);
    return 0;
  }

  /* actually do a job */
  strtolower(argv[1]);
  /* iInt = SetStatusFromText(argv[1]); */
  if (iInt) {
    SCSendOK(pCon);
    return 1;
  } else {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s not recognized as status code", argv[1]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
}

/*-----------------------------------------------------------------------*/
static int isNum(char *pText)
{
  int i;
  int iRet = 1;

  for (i = 0; i < strlen(pText); i++) {
    if (!isdigit(pText[i])) {
      if (!((pText[i] == '+') || (pText[i] == '-') || (pText[i] == '.') || (tolower(pText[i]) == 'e' ))) {
        iRet = 0;
        break;
      }
    }
  }
  return iRet;
}

/*--------------------------- Type Checking -------------------------------
  classifies a given string: numeric, countable, drivable, sicsobject, text
*/

int SICSType(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[])
{
  CommandList *pCom = NULL;
  Dummy *pDum = NULL;
  char pBueffel[132];


  assert(pCon);
  assert(pSics);

  /* is there a parameter anyway */
  if (argc < 2) {
    SCWrite(pCon, "ERROR: no object to test specified!", eError);
    return 0;
  }

  /* test, one by one */
  strtolower(argv[1]);
  if (isNum(argv[1])) {
    SCWrite(pCon, "NUM", eValue);
    return 1;
  }

  pCom = FindCommand(pSics, argv[1]);
  if (pCom) {
    pDum = (Dummy *) pCom->pData;
    if (pDum) {
      if (pDum->pDescriptor->GetInterface(pDum, DRIVEID)) {
        SCWrite(pCon, "DRIV", eValue);
        return 1;
      }
      if (pDum->pDescriptor->GetInterface(pDum, COUNTID)) {
        SCWrite(pCon, "COUNT", eValue);
        return 1;
      }
    }
    SCWrite(pCon, "COM", eValue);
    return 1;
  }

  SCWrite(pCon, "TEXT", eValue);
  return 1;
}

/*----------------- Bounds checking for all driveables ---------------------*/
int SICSBounds(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[])
{
  CommandList *pCom = NULL;
  pIDrivable pInt = NULL;
  Dummy *pDum = NULL;
  char pBueffel[132];
  float fVal;
  int iRet;

  assert(pCon);
  assert(pSics);

  if (argc < 3) {
    SCWrite(pCon, "ERROR: not enough parameters specified", eError);
    return 0;
  }

  /* argv[1] should be drivable */
  pCom = FindCommand(pSics, argv[1]);
  if (!pCom) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s is no Sics Object", argv[1]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  pDum = (Dummy *) pCom->pData;
  if (!pDum) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s is no valid Sics Object", argv[1]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  pInt = pDum->pDescriptor->GetInterface(pDum, DRIVEID);
  if (!pInt) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s is not drivable!", argv[1]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  /* argv[2] should be a numeric */
  if (!isNum(argv[2])) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s is not a number!", argv[2]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  /* do a job */
  fVal = atof(argv[2]);
  iRet = pInt->CheckLimits(pCom->pData, fVal, pBueffel, 131);
  if (iRet) {
    SCWrite(pCon, "OK", eValue);
    return 1;
  } else {
    SCWrite(pCon, pBueffel, eWarning);
    return 0;
  }
  return 0;                     /* not reached */
}

/*----------------- Status checking for Sics Objects   ---------------------*/
int SICSStatus(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[])
{
  CommandList *pCom = NULL;
  Dummy *pDum = NULL;
  pIDrivable pDInt = NULL;
  pICountable pCInt = NULL;
  char pBueffel[132];
  float fVal;
  int iRet;

  assert(pCon);
  assert(pSics);

  if (argc < 2) {
    SCWrite(pCon, "ERROR: not enough parameters specified", eError);
    return 0;
  }

  /* argv[1] should be drivable */
  pCom = FindCommand(pSics, argv[1]);
  if (!pCom) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s is no Sics Object", argv[1]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  pDum = (Dummy *) pCom->pData;
  if (!pDum) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s is no valid Sics Object", argv[1]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  pDInt = pDum->pDescriptor->GetInterface(pDum, DRIVEID);
  pCInt = pDum->pDescriptor->GetInterface(pDum, COUNTID);
  if (pDInt) {
    iRet = pDInt->CheckStatus(pDum, pCon);
    snprintf(pBueffel,sizeof(pBueffel)-1, "%d", iRet);
    SCWrite(pCon, pBueffel, eValue);
    return 1;
  } else if (pCInt) {
    iRet = pCInt->CheckCountStatus(pDum, pCon);
    snprintf(pBueffel,sizeof(pBueffel)-1, "%d", iRet);
    SCWrite(pCon, pBueffel, eValue);
    return 1;
  } else {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s is neither drivable nor countable",
            argv[1]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
  /* not reached */
  return 0;

}

/*--------------------------------------------------------------------------*/
int SICSDebug(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[])
{
  char pBueffel[256];
  Tcl_Interp *pTcl = NULL;
  int iRet;
  char *cmd;

  assert(pCon);
  assert(pSics);

  if (!SCMatchRights(pCon, usInternal)) {
    SCWrite(pCon, "ERROR: no privilege to interact with Tcl for you",
            eError);
    return 0;
  }

  pTcl = InterpGetTcl(pSics);
  assert(pTcl);

  cmd = Arg2Tcl(argc - 1, &argv[1], pBueffel, sizeof pBueffel);
  if (!cmd) {
    SCWrite(pCon, "ERROR: no more memory", eError);
    return 0;
  }
  iRet = Tcl_Eval(pTcl, cmd);
  if (cmd != pBueffel)
    free(cmd);
  if (strlen(Tcl_GetStringResult(pTcl)) > 1) {
    SCPrintf(pCon, eValue, "%s", Tcl_GetStringResult(pTcl));
  }
  return iRet;
}

/*--------------------------------------------------------------------------*/
int SICSTime(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[])
{
  char pBueffel[256];

  SNXFormatTime(pBueffel, 255);
  SCWrite(pCon, pBueffel, eValue);
  return 1;
}
/*-------------------------------------------------------------------------*/
int SICSDoubleTime(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[])
{
  double dtime;

  dtime = DoubleTime();
  SCPrintf(pCon,eValue,"%lf",dtime);
  return 1;
}
/*--------------------------------------------------------------------------
  Kill a command from SICS
*/
int SICSKill(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[])
{
  CommandList *pCom = NULL, *pCurrent = NULL;
  void *pDat = NULL;
  const char *pPtr = NULL;

  if (!SCMatchRights(pCon, usMugger)) {
    SCWrite(pCon, "ERROR: you may not kill commands here, no privilege",
            eError);
    return 0;
  }

  if (argc < 2) {
    SCWrite(pCon, "ERROR: need name of command to kill", eError);
    return 0;
  }

  /* memorise data for alias search */
  pCom = FindCommand(pSics, argv[1]);
  if (pCom) {
    pDat = pCom->pData;
  } else {
    pDat = NULL;
  }
  RemoveCommand(pSics, argv[1]);
  if (!pDat) {                  /* no data, no alias */
    SCSendOK(pCon);
    return 0;
  }

  /* kill aliases */
  pPtr = FindAlias(pSics, pDat);
  while (pPtr) {
    RemoveCommand(pSics, pPtr);
    pPtr = FindAlias(pSics, pDat);
  }
  SCSendOK(pCon);
  return 1;
}

/*------------------------------------------------------------------------*/
int SicsPrompt(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[])
{
  char pBueffel[512];
  int iRet;

  /* all arguments are optional */
  if (argc < 1) {
    iRet = SCPrompt(pCon, "SICS> ", pBueffel, 511);
  } else {
    iRet = SCPrompt(pCon, argv[1], pBueffel, 511);
  }
  if (iRet == 1) {
    SCWrite(pCon, pBueffel, eValue);
    return 1;
  } else {
    SCWrite(pCon, "ERROR: Interrupted while waiting for data", eError);
    return 0;
  }
}

/*----------------------- get object descriptor name -------------------------------
  get the name of the object descriptor
*/
int SICSDescriptor(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[])
{
  CommandList *pCom = NULL;
  Dummy *pDum = NULL;
  char pBueffel[132];


  assert(pCon);
  assert(pSics);

  /* is there a parameter anyway */
  if (argc < 2) {
    SCWrite(pCon, "ERROR: no object specified!", eError);
    return 0;
  }

  pCom = FindCommand(pSics, argv[1]);
  if (pCom) {
    pDum = (Dummy *) pCom->pData;
    if (pDum) {
      SCWrite(pCon, pDum->pDescriptor->name, eValue);
      return 1;
    }
    SCWrite(pCon, "empty", eValue);
    return 1;
  }

  SCWrite(pCon, "notfound", eValue);
  return 1;
}
/*------------------------------------------------------------------------*/
int SICSSilent(SConnection *pCon, SicsInterp *pSics, void *pData,
                  int argc, char *argv[]) {
  Tcl_Interp *pTcl = NULL;
  int iRet;
  char *cmd;
  char *result;
  writeFunc oldWrite;
  
  assert(pCon);
  assert(pSics);
  
  if (!SCinMacro(pCon)) {
    SCPrintf(pCon, eError,
      "ERROR: %s may only be called in scripts", argv[0]);
    return 0;
  }
  pTcl = InterpGetTcl(pSics);
  assert(pTcl);
  if (argc < 3) {
    SCWrite(pCon, "ERROR:  should be:  silent <error-value> <tcl-command>", eError);
    return 0;
  }
  cmd = Arg2Tcl(argc-2,&argv[2],NULL,0);
  assert(cmd);
  
  oldWrite = SCGetWriteFunc(pCon);
  SCSetWriteFunc(pCon,SCNotWrite);
  iRet = Tcl_EvalEx(pTcl, cmd, strlen(cmd), 0);
  SCSetWriteFunc(pCon,oldWrite);

  if (iRet == TCL_OK) {
    result = strdup(Tcl_GetStringResult(pTcl));
    SCWrite(pCon, result, eValue);
    free(result);
  } else {
    SCWrite(pCon, argv[1], eValue);
  }
  free(cmd);
  return 1;
}
/*------------------------------------------------------------------------*/
Tcl_Obj *json2tcl(struct json_object *jobj) {
  char buf[100];
  int i, len;
  Tcl_Obj **list;
  Tcl_Obj *dict, *kobj, *vobj;
  Tcl_Obj *val;
  const char *key;
  const char *str;

  switch (json_object_get_type(jobj)) {
  case json_type_boolean:
    return Tcl_NewBooleanObj(json_object_get_boolean(jobj));
  case json_type_int:
    return Tcl_NewLongObj(json_object_get_int(jobj));
  case json_type_double:
    return Tcl_NewDoubleObj(json_object_get_double(jobj));
  case json_type_string:
    str = json_object_get_string(jobj);
    return Tcl_NewStringObj((char *)str, json_object_get_string_len(jobj));
  case json_type_array:
    len = json_object_array_length(jobj);
    list = calloc(sizeof *list, len);
    for (i=0; i<len; i++) {
      list[i] = json2tcl(json_object_array_get_idx(jobj, i));
      if (!list[i]) return NULL;
    }
    return Tcl_NewListObj(len, list);
  case json_type_object:
    dict = Tcl_NewDictObj();
    if (!dict) return NULL;
    json_object_object_foreach(jobj, key, val) {
      kobj = Tcl_NewStringObj(key, strlen(key));
      vobj = json2tcl(val);
      if (kobj && vobj) {
        Tcl_DictObjPut(NULL, dict, kobj, vobj);
      }
    }
    return dict;
  }
  return Tcl_NewStringObj("None", 4);
}
/*------------------------------------------------------------------------*/
int SICSjson2tcl(SConnection *pCon, SicsInterp *pSics, void *pData,
                  int argc, char *argv[]) {
  json_object *jobj;
  assert(pCon);
  assert(pSics);
  
  if (argc != 2) {
    SCWrite(pCon, "ERROR:  should be:  json2tcl <string>", eError);
    return 0;
  }
  jobj = json_tokener_parse(argv[1]);
  if (!jobj) {
    SCWrite(pCon, "ERROR:  no valid JSON", eError);
    return 0;
  }
  /* the result is written only to the interpreter, no output to the client */
  Tcl_SetObjResult(pSics->pTcl, json2tcl(jobj));
  json_object_put(jobj);
  return 1;
}
