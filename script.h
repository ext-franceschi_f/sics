/*--------------------------------------------------------------------------
				S C R I P T
  This is a header file for a set of SICS-commands used to query
  and modify internal status information from within scripts.
  
  Mark Koennecke, February 1997
  
  copyright: see implementation file
-----------------------------------------------------------------------------*/
#ifndef SICSSCRIPT
#define SICSSCRIPT

#include <tcl.h>
#include <json-c/json.h>

/* -------------------------- Interrupts -----------------------------------*/
int GetSICSInterrupt(SConnection * pCon, SicsInterp * pSics, void *pData,
                     int argc, char *argv[]);

int SetSICSInterrupt(SConnection * pCon, SicsInterp * pSics, void *pData,
                     int argc, char *argv[]);

/*---------------------------- Status -------------------------------------*/
int SetSICSStatus(SConnection * pCon, SicsInterp * pSics, void *pData,
                  int argc, char *argv[]);

/*--------------------------- Type Checking -------------------------------*/
int SICSType(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[]);

/*----------------- Bounds checking for all driveables ---------------------*/
int SICSBounds(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[]);

/*------------------ Status checking for SicsObjects  ----------------------- */
int SICSStatus(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[]);

/*------------------ Type directly to Tcl  ----------------------- */
int SICSDebug(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[]);
/*-----------------------------------------------------------------------*/
int SICSTime(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[]);
/*----------------------------------------------------------------------*/
int SICSKill(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[]);
/*----------------------------------------------------------------------*/
int SicsPrompt(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[]);
/*----------------------------------------------------------------------*/
int SICSDescriptor(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[]);
/*----------------------------------------------------------------------*/
int SICSSilent(SConnection *pCon, SicsInterp *pSics, void *pData,
               int argc, char *argv[]);
/*----------------------------------------------------------------------*/
Tcl_Obj *json2tcl(struct json_object *jobj);
/*----------------------------------------------------------------------*/
int SICSjson2tcl(SConnection *pCon, SicsInterp *pSics, void *pData,
                  int argc, char *argv[]);
#endif
