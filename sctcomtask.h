/**
 * This is another approach at using the scriptcontext system. It introduces 
 * the concept of a communication task which is executed against a device serializer, 
 * accessed via a given ScriptContext. Clients can:
 * - Create CommTasks
 * - Figure out their state
 * - Retrieve reply data 
 * - wait for a ComTask to finish.
 *
 * The the purpose sctcomtask will keep a cache of pending and finished communications. 
 * Old runs will be deleted periodically. Nevertheless the cache can be listed in order 
 * to figure out what happened.
 *
 * The intended use is for C-code or scripts to interact in a serial manner with 
 * the asynchronous communication system implemented in devser and ascon. As a 
 * standalone implementation would share tons of code with scriptcontext, this has 
 * been implemented as an add on module to scriptcontext. 
 *
 * copyright: see file COPYRIGHT
 *
 * Mark Koennecke, December 2012
 */
#ifndef __SCTCOMTASK
#define __SCTCOMTASK
#include "sics.h"

typedef struct ComTaskManager ComTaskManager;

typedef enum {
  eWaiting,
  eRunning,
  eFinished,
  eUnknown
} ComTaskState;


/**
 * Standard SIS installation function
 */
int SctMakeComTask(SConnection * con, SicsInterp * sics,
                   void *object, int argc, char *argv[]);
/*============================ client interface section ===========================*/
/**
 * Start a communication task
 * @param manager the communication task manager to start the communication with
 * @param priority The priority of the task
 * @param sendData The data to send to the device
 * @return A positive com task ID on success, -1 on failure.
 */
int StartComTask(ComTaskManager *manager, char *priority, char *sendData);
/**
 * Retrive the state of a communication task
 * @param manager the communication task manager
 * @param comTaskID The ID of the communication task to test
 * @return The state of the task
 */
ComTaskState GetComTaskState(ComTaskManager *manager, int comTaskID);
/**
 * Retrive the reply of a communication task
 * @param manager the communication task manager
 * @param comTaskID The ID of the communication task to test
 * @param length An output parameter set to the length of the data returned
 * @return NULL when this is called erroneously, the reply data else. The client is 
 * not supposed to delete the reply data. On the other hand, the client cannot rely 
 * on the data to be available for ever. Make a copy if long term storage of the reply 
 * data is necessary.    
 */
const char *GetComTaskReply(ComTaskManager *manager, int comTaskID, int *length);
/**
 * Wait for a communication task to finish
 * @param manager the communication task manager
 * @param comTaskID The ID of the communication task to test
 * Please note that eventual timeouts are handled by devser and ascon. 
 */
void ComTaskWait(ComTaskManager *manager, int comTaskID);


#endif
