/*
 * Script Context to Process Adapter
 *
 * Douglas Clowes (dcl@ansto.gov.au) September 2016
 *
 * The purpose of this module is to provide the interface glue to allow
 * Script Context drivers to use a sub-process.
 *
 * The approach taken is to provide an Ascon device interface and state machine
 * on top of an exec service.
 */
#include "ascon.h"
#include "ascon.i"
#include "dynstring.h"
#include "splitter.h"
#include "logv2.h"
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>

struct pid_chain_link {
  pid_t pid;
  double read_start;
  struct pid_chain_link *next;
};

struct private_data {
  int cmd_len;
  int child_pid;
  struct pid_chain_link *pid_chain_head;
  char *queue_name;
  int pipefd[2];
  double read_start;
  double time_out;
};

static void push_child(struct private_data *pp)
{
  struct pid_chain_link *self = calloc(1, sizeof(struct pid_chain_link));
  if (self) {
    self->pid = pp->child_pid;
    self->read_start = pp->read_start;
    self->next = pp->pid_chain_head;
    pp->pid_chain_head = self;
  }
}

static void wait_kids(struct private_data *pp)
{
  struct pid_chain_link *self = pp->pid_chain_head;
  double now = DoubleTime();
  /* try waiting for each pending pid */
  for (self = pp->pid_chain_head;
       self;
       self = self->next) {
    pid_t child;
    /* skip the empties */
    if (self->pid == 0)
      continue;
    child = waitpid(self->pid, NULL, WNOHANG);
    if (child == self->pid) {
      Log(INFO, "sys", "Waited stale %.3f for recalcitrant PID: %d",
                    now - self->read_start,
                    (int) child);
      /* mark it as done */
      self->pid = 0;
    }
    if (child < 0 && errno == ECHILD) {
      Log(INFO, "sys", "Waited stale %.3f for nonexistent PID: %d",
                    now - self->read_start,
                    (int) child);
      /* mark it as done */
      self->pid = 0;
    }
  }
  /* remove empties at the queue head */
  while (pp->pid_chain_head && pp->pid_chain_head->pid == 0) {
    struct pid_chain_link *victim = pp->pid_chain_head;
    pp->pid_chain_head = pp->pid_chain_head->next;
    free(victim);
  }
  /* remove empties not at the queue head */
  self = pp->pid_chain_head;
  while (self) {
    if (self->next && self->next->pid == 0) {
      struct pid_chain_link *victim = self->next;
      self->next = self->next->next;
      free(victim);
    } else {
      self = self->next;
    }
  }
}


static char *state_name(Ascon *a)
{
  switch (a->state) {
  case AsconNotConnected:
    return "AsconNotConnected";
    break;
  case AsconConnectStart:
    return "AsconConnectStart";
    break;
  case AsconConnecting:
    return "AsconConnecting";
    break;
  case AsconConnectDone:
    return "AsconConnectDone";
    break;
  case AsconWriteStart:
    return "AsconWriteStart";
    break;
  case AsconWriting:
    return "AsconWriting";
    break;
  case AsconWriteDone:
    return "AsconWriteDone";
    break;
  case AsconReadStart:
    return "AsconReadStart";
    break;
  case AsconReading:
    return "AsconReading";
    break;
  case AsconReadDone:
    return "AsconReadDone";
    break;
  case AsconIdle:
    return "AsconIdle";
    break;
  case AsconFailed:
    return "AsconFailed";
    break;
  case AsconTimeout:
    return "AsconTimeout";
    break;
  case AsconMaxState:
    return "AsconMaxState";
    break;
  default:
    return "Unknown Ascon State";
    break;
  }
}

static int scaqaProtHandler(Ascon *a)
{
  int rval;
  pid_t child;
  char buff[100];
  struct private_data *pp = (struct private_data *) a->private;
  switch (a->state) {
  case AsconNotConnected:
    return 0;
  case AsconConnectStart:
    a->state = AsconConnecting;
    return 0;
  case AsconConnecting:
    a->state = AsconConnectDone;
    return 0;
  case AsconConnectDone:
    /* should not get here */
    a->state = AsconIdle;
    return 0;
  case AsconWriteStart:
    a->state = AsconWriting;
    return 0;
  case AsconWriting:
    /*
     * This waits for ANY pending children
     */
    wait_kids(pp);

    pp->cmd_len = GetDynStringLength(a->wrBuffer);
    if (pp->cmd_len > 0) {
      int argc;
      char **argv;
      if (pipe(pp->pipefd) == -1) {
        perror("pipe");
        AsconError(a, "Pipe failed", errno);
        return 0;
      }
      pp->child_pid = fork();
      if (pp->child_pid == -1) {
        perror("fork");
        abort();
      }
      if (!pp->child_pid) {     /* child process */
        close(pp->pipefd[0]);
        dup2(pp->pipefd[1], 1); /* redirect stdout to pipe */
        dup2(pp->pipefd[1], 2); /* redirect stderr to pipe */
        Text2Arg(GetCharArray(a->wrBuffer), &argc, &argv);
        execvp(*argv, argv);
        /* should not get here */
        perror("exec");
        abort();
      } else {                  /* parent process */
        int flags;
        close(pp->pipefd[1]);
        flags = fcntl(pp->pipefd[0], F_GETFL, 0);
        fcntl(pp->pipefd[0], F_SETFL, flags | O_NONBLOCK);
        Log(INFO, "sys", "SCT Proc exec %d:%s", (int) pp->child_pid,
                      GetCharArray(a->wrBuffer));
      }
    }
    a->state = AsconWriteDone;
    return 0;
  case AsconWriteDone:
    /* should not get here */
    a->state = AsconReadStart;
    return 0;
  case AsconReadStart:
    DynStringClear(a->rdBuffer);
    pp->read_start = DoubleTime();
    a->state = AsconReading;
    return 0;
  case AsconReading:
    while ((rval = read(pp->pipefd[0], &buff, 100)) > 0) {
      DynStringConcatBytes(a->rdBuffer, buff, rval);
    }
    if (rval < 0) {
      if (errno != EAGAIN) {    /* failed */
        AsconError(a, "Disconnected", errno);
        a->state = AsconFailed;
        close(pp->pipefd[0]);
        child = waitpid(pp->child_pid, NULL, WNOHANG);
        Log(INFO, "sys",
                      "Waited error (%s) %.3f for expected PID: %d, got %d",
                      strerror(errno), (DoubleTime() - pp->read_start),
                      (int) pp->child_pid, (int) child);
        if (child != pp->child_pid)
          push_child(pp);
        return 0;
      } else {
        if (pp->time_out > 0.0
            && (DoubleTime() - pp->read_start) > pp->time_out) {
          if (GetDynStringLength(a->rdBuffer) == 0) {
            a->state = AsconTimeout;
            close(pp->pipefd[0]);
            child = waitpid(pp->child_pid, NULL, WNOHANG);
            Log(INFO, "sys",
                          "Waited noresponse %.3f for expected PID: %d, got %d",
                          (DoubleTime() - pp->read_start),
                          (int) pp->child_pid, (int) child);
            if (child != pp->child_pid)
              push_child(pp);
          } else {
            a->state = AsconReadDone;
            close(pp->pipefd[0]);
            child = waitpid(pp->child_pid, NULL, WNOHANG);
            Log(INFO, "sys",
                          "Waited timeout %.3f for expected PID: %d, got %d",
                          (DoubleTime() - pp->read_start),
                          (int) pp->child_pid, (int) child);
            if (child != pp->child_pid)
              push_child(pp);
          }
        }
      }
    } else if (rval == 0) {     /* complete */
      child = waitpid(pp->child_pid, NULL, WNOHANG);
      if (child == pp->child_pid) {
        a->state = AsconReadDone;
        close(pp->pipefd[0]);
        Log(INFO, "sys",
                      "Waited complete %.3f for expected PID: %d, got %d",
                      (DoubleTime() - pp->read_start), (int) pp->child_pid,
                      (int) child);
      } else if (child < 0) {
        a->state = AsconReadDone;
        close(pp->pipefd[0]);
        Log(INFO, "sys",
                      "Waited aborted %.3f for expected PID: %d, got %d.%d:%s",
                      (DoubleTime() - pp->read_start), (int) pp->child_pid,
                      (int) child, errno, strerror(errno));
      } else {
        Log(INFO, "sys",
                      "Waited incomplete %.3f for expected PID: %d, got %d",
                      (DoubleTime() - pp->read_start), (int) pp->child_pid,
                      (int) child);
      }
    }
    return 0;
  case AsconReadDone:
    /* should not get here */
    return 0;
  case AsconIdle:
    return 0;
  case AsconFailed:
    return 0;
  case AsconTimeout:
    /* should not get here */
    a->state = AsconIdle;
    return 0;
  case AsconMaxState:
    return 0;
  default:
    return 0;
  }
}

/*
 * Kill the private storage
 *
 * Clean up and release all resources associated with the private object
 */
static void SCAQ_KillPrivate(void *vp)
{
  struct private_data *pp = (struct private_data *) vp;
  if (pp) {
    if (pp->queue_name)
      free(pp->queue_name);
    free(pp);
  }
}

/*
 * Initialize the Ascon object for this device, the async queue argument.
 */
static int scaqaAsconInit(Ascon *a, SConnection *pCon, int argc,
                          char *argv[])
{
  int i;
  struct private_data *pp;
  for (i = 0; i < argc; ++i) {
    SCPrintf(pCon, eStatus, "scaqaAsconInit: arg[%d] = %s\n", i, argv[i]);
  }
  if (argc < 1) {
    SCPrintf(pCon, eError,
             "Insufficient arguments to scaqaAsconInit: %d\n", argc);
    return 0;
  }
  pp = (struct private_data *) calloc(sizeof(struct private_data), 1);
  pp->queue_name = strdup(argv[1]);
  pp->time_out = 60.0;
  a->private = pp;
  a->hostport = strdup(argv[1]);
  a->killPrivate = SCAQ_KillPrivate;
  return 1;
}

/*
 * This procedure creates, initializes and registers the "scaqa" protocol
 * with the Ascon infrastructure
 */
void AddProcProtocol(void)
{
  AsconProtocol *prot = NULL;
  printf("AddProcProtocol\n");
  prot = calloc(sizeof(AsconProtocol), 1);
  prot->name = strdup("procadapter");
  prot->init = scaqaAsconInit;
  prot->handler = scaqaProtHandler;
  AsconInsertProtocol(prot);
}
