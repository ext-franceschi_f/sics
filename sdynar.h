
/*---------------------------------------------------------------------------
                       D Y N A M I C    A R R A Y

  This file describes the interface to a dynamic array module for pointers.
  This sort of array resizes dynamically.
 
  Mark Koennecke, September 1997

  copyright: see copyright.h

-----------------------------------------------------------------------------*/
#ifndef SICSDYNAR
#define SICSDYNAR

typedef struct __SDynar *pDynar;
/*-------------------------------------------------------------------------*/
pDynar CreateDynar(int iStart, int iEnd, int iGrain,
                   void (*DataFree) (void *pData));
void DeleteDynar(pDynar self);
/*------------------------------------------------------------------------*/
int DynarPut(pDynar self, int iIndex, void *pData);
int DynarPutCopy(pDynar self, int iIndex, void *pData, int iDataLen);
int DynarReplace(pDynar self, int iIndex, void *pData, int iDatLen);

int DynarGet(pDynar self, int iIndex, void **pData);
int DynarGetCopy(pDynar self, int iIndex, void *pData, int iDataLen);


#endif
