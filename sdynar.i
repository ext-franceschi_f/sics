
/*----------------------------------------------------------------------------
                       D Y N A M I C    A R R A Y

      This file defines the internal data structure used by the dynamic
      array module.
  
      More info: see sdynar.h
-----------------------------------------------------------------------------*/

   typedef struct __SDynar {
                           int iStart;
                           int iEnd;
                           int iGrain;
                           void **pArray;
                           void (*DataFree)(void *pData);
                           } Dynar;
                           
                            

