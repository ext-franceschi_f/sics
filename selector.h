/*--------------------------------------------------------------------------

			S E L E C T O R
			
	This is an object to support a Crystal-Monochromator with 
	possibly two bending dimensions. 

        The monochromator can be used in two modes:  to adjust wavelength
        or to adjust energy for those triple axis people. This is reflected
        in a code structure like this: There is the actual device, the 
        monochromator, as given here. This mono supports some configuration
        commands. And provides basic routines to run the monochromator 
        common to the second part.
        
        The second part are two variables, wavelength and energy, which
        are configured with a monochromator to act upon. See file selvar.* for
        more details. 
	
	Mark Koennecke, January 1996
	
	copyright: see implementation file.
	
----------------------------------------------------------------------------*/

#ifndef SICSMONO
#define SICSMONO
#include "motor.h"

typedef struct __SicsSelector *pSicsSelector;


  /* birth and death: The Selector himself */
pSicsSelector CreateSelector(char *name, pMotor pTheta, pMotor pTwoTheta,
                             pMotor pBend1, pMotor pBend2);

int MonoInit(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[]);

void DeleteSelector(void *self);


 /* Action!  */

int MonoAction(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[]);

int MonoRun(pSicsSelector self, SConnection * pCon, float fWaveLength);
struct __ComEntry *GetSelectorMotors(pSicsSelector self,
                                     SConnection * pCon, float fNew);
int MonoLimits(pSicsSelector self, float fWaveLength, char *error,
               int iErrLen);
int MonoCheck(pSicsSelector self, SConnection * pCon);
int MonoHalt(pSicsSelector self);
float GetMonoPosition(pSicsSelector self, SConnection * pCon);
char *MonoGetType(pSicsSelector self);
int GetMonoPositions(pSicsSelector self, SConnection * pCon,
                     float *fTheta, float *fTwtTh, float *fB1, float *fB2);

#endif
