/*--------------------------------------------------------------------------

				S E L V A R

  Implementation file for wavelength and energy variables using a crystal
  monochromator.
  
	Mark Koennecke,    January 1997

	Copyright:

	Labor fuer Neutronenstreuung
	Paul Scherrer Institut
	CH-5423 Villigen-PSI


        The authors hereby grant permission to use, copy, modify, distribute,
        and license this software and its documentation for any purpose, provided
        that existing copyright notices are retained in all copies and that this
        notice is included verbatim in any distributions. No written agreement,
        license, or royalty fee is required for any of the authorized uses.
        Modifications to this software may be copyrighted by their authors
        and need not follow the licensing terms described here, provided that
        the new terms are clearly indicated on the first page of each file where
        they apply.

        IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
        FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
        ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
        DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
        POSSIBILITY OF SUCH DAMAGE.

        THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
        INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
        IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
        NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
        MODIFICATIONS.
	 			
-----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <tcl.h>
#include <math.h>
#include "fortify.h"
#include "sics.h"
#include "motor.h"
#include "splitter.h"
#include "devexec.h"
#include "selector.h"
#include "selvar.h"
#define DRIVE "drive"

#include "selvar.i"

/* -------------------------------------------------------------------------
  lots of static functions necessary to initialize the ObjectDescriptor
  structures of our variables.
----------------------------------------------------------------------------*/
static void *SelVarGetInterface(void *pData, int iID)
{
  pSelVar self = NULL;

  self = (pSelVar) pData;
  assert(self);

  if (iID == DRIVEID) {
    return self->pDrivInt;
  } else if (iID == CALLBACKINTERFACE) {
    return self->pCall;
  }
  return NULL;
}

/*--------------------------------------------------------------------------*/
static float Energy2Wave(float fVal, SConnection * pCon)
{
  float fWave;
  char pBueffel[132];

  if (fVal < .0) {
    snprintf(pBueffel,sizeof(pBueffel)-1,
            "ERROR: Invalid energy %f specified, defaulted to 20.", fVal);
    SCWrite(pCon, pBueffel, eError);
    fWave = 20. / 2.07;
  }
  fWave = fVal / 1000.;
  fWave = sqrt(fWave);
  fWave = 0.286 / fWave;
  return fWave;
}

/*--------------------------------------------------------------------------*/
static int CheckWLLimits(void *pSelf, float fNew, char *error, int iErrLen)
{
  pSelVar self = NULL;

  self = (pSelVar) pSelf;
  assert(self);
  assert(self->pDes);
  assert(strcmp(self->pDes->name, "SicsSelVar") == 0);
  assert(self->pSel);

  return MonoLimits(self->pSel, fNew, error, iErrLen);
}

/*--------------------------------------------------------------------------*/
static int CheckELimits(void *pSelf, float fNew, char *error, int iErrLen)
{
  pSelVar self = NULL;
  float fWave;
  char pBueffel[132];

  self = (pSelVar) pSelf;
  assert(self);
  assert(self->pDes);
  assert(strcmp(self->pDes->name, "SicsSelVar") == 0);
  assert(self->pSel);

  fWave = fNew / 2.07;
  if (fWave < .0) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: Invalid energy %f specified", fNew);
    strlcpy(error, pBueffel, iErrLen);
    return 0;
  }
  fWave = sqrt(fWave);

  return MonoLimits(self->pSel, fWave, error, iErrLen);
}

/*-------------------------------------------------------------------------*/
static long SetWL(void *pSelf, SConnection * pCon, float fNew)
{
  pSelVar self = NULL;

  self = (pSelVar) pSelf;
  assert(self);
  assert(self->pDes);
  assert(strcmp(self->pDes->name, "SicsSelVar") == 0);
  assert(self->pSel);

  return MonoRun(self->pSel, pCon, fNew);
}

/*-------------------------------------------------------------------------*/
static long SetEnergy(void *pSelf, SConnection * pCon, float fNew)
{
  pSelVar self = NULL;

  self = (pSelVar) pSelf;
  assert(self);
  assert(self->pDes);
  assert(strcmp(self->pDes->name, "SicsSelVar") == 0);
  assert(self->pSel);

  return MonoRun(self->pSel, pCon, Energy2Wave(fNew, pCon));
}

/*------------------------------------------------------------------------*/
static int CheckVal(void *pSelf, SConnection * pCon)
{
  pSelVar self = NULL;
  int status;

  self = (pSelVar) pSelf;
  assert(self);
  assert(self->pDes);
  assert(strcmp(self->pDes->name, "SicsSelVar") == 0);
  assert(self->pSel);
  assert(pCon);

  self->pCon = pCon;
  InvokeCallBack(self->pCall, WLCHANGE, self);
  status = MonoCheck(self->pSel, pCon);
  if (status != HWBusy) {
    InvokeCallBack(self->pCall, WLCHANGE, self);
  }
  return status;
}

/*------------------------------------------------------------------------*/
static int HaltSelVar(void *pSelf)
{
  pSelVar self = NULL;

  self = (pSelVar) pSelf;
  assert(self);
  assert(self->pDes);
  assert(strcmp(self->pDes->name, "SicsSelVar") == 0);
  assert(self->pSel);

  return MonoHalt(self->pSel);
}

/*-------------------------------------------------------------------------*/
static float GetWL(void *pData, SConnection * pCon)
{
  pSelVar pVar;

  pVar = (pSelVar) pData;
  assert(pVar);
  return GetMonoPosition(pVar->pSel, pCon);
}

/*-------------------------------------------------------------------------*/
static float GetEE(void *pData, SConnection * pCon)
{
  float fWave = 0;
  pSelVar pVar;

  pVar = (pSelVar) pData;
  assert(pVar);

  fWave = GetMonoPosition(pVar->pSel, pCon);
  fWave = 0.286 / fWave;
  fWave = fWave * fWave * 1000.;
  return fWave;
}

/*------------------------------------------------------------------------*/
pSelVar CreateWLVar(char *name, pSicsSelector pSel)
{
  pSelVar pNew = NULL;

  assert(pSel);
  assert(name);

  /* allocate memory */
  pNew = (pSelVar) malloc(sizeof(SelVar));
  if (!pNew) {
    return NULL;
  }
  pNew->pDes = CreateDescriptor("SicsSelVar");
  if (!pNew->pDes) {
    free(pNew);
    return NULL;
  }

  pNew->pCall = CreateCallBackInterface();
  if (!pNew->pCall) {
    free(pNew);
    return NULL;
  }
  /* initialize Descriptor */
  pNew->pDes->GetInterface = SelVarGetInterface;


  /* initialise Drivable interface */
  pNew->pDrivInt = CreateDrivableInterface();
  if (!pNew->pDrivInt) {
    DeleteDescriptor(pNew->pDes);
    free(pNew);
    return NULL;
  }
  pNew->pDrivInt->SetValue = SetWL;
  pNew->pDrivInt->CheckStatus = CheckVal;
  pNew->pDrivInt->GetValue = GetWL;
  pNew->pDrivInt->CheckLimits = CheckWLLimits;
  pNew->pDrivInt->Halt = HaltSelVar;

  /* intialize Rest */
  pNew->pSel = pSel;
  pNew->name = strdup(name);

  return pNew;
}

/*------------------------------------------------------------------------*/
pSelVar CreateEnergy(char *name, pSicsSelector pSel)
{
  pSelVar pNew = NULL;

  assert(pSel);
  assert(name);

  /* allocate memory */
  pNew = (pSelVar) malloc(sizeof(SelVar));
  if (!pNew) {
    return NULL;
  }
  pNew->pDes = CreateDescriptor("SicsSelVar");
  if (!pNew->pDes) {
    free(pNew);
    return NULL;
  }

  /* create call back interface */
  pNew->pCall = CreateCallBackInterface();
  if (!pNew->pCall) {
    free(pNew);
    return NULL;
  }

  /* initialize Descriptor */
  pNew->pDes->GetInterface = SelVarGetInterface;

  /* initialise Drivable interface */
  pNew->pDrivInt = CreateDrivableInterface();
  if (!pNew->pDrivInt) {
    DeleteDescriptor(pNew->pDes);
    free(pNew);
    return NULL;
  }
  pNew->pDrivInt->Halt = HaltSelVar;
  pNew->pDrivInt->CheckLimits = CheckELimits;
  pNew->pDrivInt->SetValue = SetEnergy;
  pNew->pDrivInt->CheckStatus = CheckVal;
  pNew->pDrivInt->GetValue = GetEE;
  /* intialize Rest */
  pNew->pSel = pSel;
  pNew->name = strdup(name);

  return pNew;
}

/*------------------------------------------------------------------------*/
void DeleteSelVar(void *pSelf)
{
  pSelVar self = NULL;

  assert(pSelf);
  self = (pSelVar) pSelf;
  assert(self->pDes);
  assert(strcmp(self->pDes->name, "SicsSelVar") == 0);

  if (self->pDes) {
    DeleteDescriptor(self->pDes);
  }
  if (self->pDrivInt) {
    free(self->pDrivInt);
  }
  if (self->pCall) {
    DeleteCallBackInterface(self->pCall);
  }
  if (self->name) {
    free(self->name);
  }
  free(self);
}

/*------------------------------------------------------------------------*/
float GetSelValue(pSelVar self, SConnection * pCon)
{
  assert(self);
  return self->pDrivInt->GetValue(self, pCon);
}

/*-------------------------------------------------------------------------
   Syntax:
    MakeWL name monochromatorname AccesCode 
*/


int MakeWaveLengthVar(SConnection * pCon, SicsInterp * pSics, void *pData,
                      int argc, char *argv[])
{
  int iRet;
  char pBueffel[132];
  pSicsSelector pMono = NULL;
  pSelVar pNeu = NULL;
  CommandList *pCom = NULL;

  /* 99.99999999999 % of all code is argument checking! */
  argtolower(argc, argv);
  if (argc < 3) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: Insufficient number of arguments to %s",
            argv[0]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  /* argv[1] == name, argv[2] should be a monochromator, find it */
  pCom = FindCommand(pSics, argv[2]);
  if (!pCom) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s not found", argv[2]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
  pMono = (pSicsSelector) pCom->pData;
  if (!pMono) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s is no monochromator", argv[2]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
  if (!iHasType(pMono, "CrystalSelector")) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s is no monochromator", argv[2]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  /* got everything we need to set things up */
  pNeu = CreateWLVar(argv[1], pMono);
  if (!pNeu) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: Out of memory creating %s found", argv[1]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
  iRet = AddCommand(pSics, argv[1], WaveLengthAction, DeleteSelVar, pNeu);
  if (!iRet) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: duplicate command %s not created", argv[1]);
    DeleteSelVar((void *) pNeu);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
  return 1;
}

/*--------------------------------------------------------------------------*/
int MakeEnergyVar(SConnection * pCon, SicsInterp * pSics, void *pData,
                  int argc, char *argv[])
{
  int iRet;
  char pBueffel[132];
  pSicsSelector pMono = NULL;
  pSelVar pNeu = NULL;
  CommandList *pCom = NULL;

  /* 99.99999999999 % of all code is argument checking! */
  argtolower(argc, argv);
  if (argc < 3) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: Insufficient number of arguments to %s",
            argv[0]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  /* argv[1] == name, argv[2] should be a monochromator, find it */
  pCom = FindCommand(pSics, argv[2]);
  if (!pCom) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s not found", argv[2]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
  pMono = (pSicsSelector) pCom->pData;
  if (!pMono) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s is no monochromator", argv[2]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
  if (!iHasType(pMono, "CrystalSelector")) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s is no monochromator", argv[2]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  /* got everything we need to set things up */
  pNeu = CreateEnergy(argv[1], pMono);
  if (!pNeu) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: Out of memory creating %s found", argv[1]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
  iRet = AddCommand(pSics, argv[1], EnergyAction, DeleteSelVar, pNeu);
  if (!iRet) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: duplicate command %s not created", argv[1]);
    SCWrite(pCon, pBueffel, eError);
    DeleteSelVar((void *) pNeu);
    return 0;
  }
  return 1;
}

/*------------------------------------------------------------------------*/
static int WaveLengthCallBack(int iEvent, void *pEvent, void *pUser)
{
  SConnection *pCon = NULL;
  pSelVar self = NULL;
  float fVal;
  char pBueffel[512];

  pCon = (SConnection *) pUser;
  self = (pSelVar) pEvent;
  assert(pCon);
  assert(self);

  if (pCon == NULL || !SCisConnected(pCon)) {
    return -1;
  }

  fVal = GetSelValue(self, pCon);
  snprintf(pBueffel,sizeof(pBueffel)-1, "%s.value = %f", self->name, fVal);
  SCWrite(pCon, pBueffel, eValue);
  return 1;
}

/*--------------------------------------------------------------------------
  very simple syntax:
  name       - return current value
  name val   - drives to new value. For driving the drive command is used.
               no new code is needed to do that
  interest   - enables automatic printing when changes.
  uninterest -  disables printing of value changes            
*/
int WaveLengthAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                     int argc, char *argv[])
{
  char pBueffel[132];
  float fWave;
  pSelVar self;
  int iRet;
  double dVal;
  long lID;

  assert(pCon);
  assert(pSics);
  assert(pData);
  self = (pSelVar) pData;
  assert(self->pSel);

  if (argc > 1) {               /* set case or interest handling */
    strtolower(argv[1]);
    if (strcmp(argv[1], "interest") == 0) {
      lID = RegisterCallback(self->pCall,
                             WLCHANGE, WaveLengthCallBack,
                             SCCopyConnection(pCon), SCDeleteConnection);
      SCSendOK(pCon);
      return 1;
    } else if (strcmp(argv[1], "uninterest") == 0) {
      RemoveCallback2(self->pCall, pCon);
      SCSendOK(pCon);
      return 1;
    }

    /* verify that argv[1] is a valid number */
    iRet = Tcl_GetDouble(pSics->pTcl, argv[1], &dVal);
    if (iRet != TCL_OK) {
      snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s is NO valid number ", argv[1]);
      SCWrite(pCon, pBueffel, eError);
      return 0;
    }
    if (dVal < 0) {
      snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s cannnot be a valid wavelength",
              argv[1]);
      SCWrite(pCon, pBueffel, eError);
      return 0;
    }

    /*
       Removed, on demand of Lukas Keller, DMC
       snprintf(pBueffel,sizeof(pBueffel)-1,"%s %s %s",DRIVE,argv[0],argv[1]);
       return InterpExecute(pSics,pCon,pBueffel);
     */
    snprintf(pBueffel, 131, "ERROR: subcommand %s to %s not understood",
             argv[1], argv[0]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  } else {                      /* get case */

    fWave = GetMonoPosition(self->pSel, pCon);
    snprintf(pBueffel,sizeof(pBueffel)-1, "%s = %f", argv[0], fWave);
    SCWrite(pCon, pBueffel, eValue);
    return 1;
  }
  return 0;
}

/*------------------------------------------------------------------------*/
int EnergyAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[])
{
  char pBueffel[132];
  float fWave;
  pSelVar self;
  int iRet;
  double dVal;
  long lID;

  assert(pCon);
  assert(pSics);
  assert(pData);
  self = (pSelVar) pData;
  assert(self->pSel);

  if (argc > 1) {               /* set case or interest/uninterest */
    strtolower(argv[1]);
    if (strcmp(argv[1], "interest") == 0) {
      lID = RegisterCallback(self->pCall,
                             WLCHANGE, WaveLengthCallBack, pCon, NULL);
      SCSendOK(pCon);
      return 1;
    } else if (strcmp(argv[1], "uninterest") == 0) {
      RemoveCallback2(self->pCall, pCon);
      SCSendOK(pCon);
      return 1;
    }

    /* verify that argv[1] is a valid number */
    iRet = Tcl_GetDouble(pSics->pTcl, argv[1], &dVal);
    if (iRet != TCL_OK) {
      snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s is NO valid number ", argv[1]);
      SCWrite(pCon, pBueffel, eError);
      return 0;
    }
    if (dVal < 0) {
      snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s cannnot be a valid energy", argv[1]);
      SCWrite(pCon, pBueffel, eError);
      return 0;
    }


    snprintf(pBueffel,sizeof(pBueffel)-1, "%s %s %s", DRIVE, argv[0], argv[1]);
    return InterpExecute(pSics, pCon, pBueffel);
  } else {                      /* get case */

    fWave = GetMonoPosition(self->pSel, pCon);
    /* convert to energy */
    if (fWave > 0.001) {
      fWave = 0.286 / fWave;
      fWave = fWave * fWave * 1000.;
    } else {
      fWave = 777.77;
    }
    snprintf(pBueffel,sizeof(pBueffel)-1, "%s = %f", argv[0], fWave);
    SCWrite(pCon, pBueffel, eValue);
    return 1;
  }
  return 0;
}
