
/*--------------------------------------------------------------------------
                          S E R I A L W A I T

  Executes a command on a serial port and waits for replies coming
  along by polling with null commands.

  copyright: see copyright.h

  Mark Koennecke, June 1998
---------------------------------------------------------------------------*/
#ifndef SERIALSICSWAIT
#define SERIALSICSWAIT
#include "sics.h"
#include "psi/hardsup/serialsinq.h"
int SerialSicsExecute(void **pData, char *pCommand, char *pReply,
                      int iBufLen);

#endif
