\subsection{Serial Wait}
This module is a very SINQ specific function. At SINQ, serial devices are
controlled through a serial port server program running on a Macintosh PC.
This program blocks, if the serial device is very slow. This blocks the
Macintosh as well as the SICS server. This module solves this problem. It
does so using the following logic:

The command to execute is sent with a zero timeout.

A special sics task, the SerialPoller, is started to monitor the device.

The routine issuing the command waits for the SerialPoller to finish. If
somebody issues new commands while this is going on, an error must be
returned.

The SerialPoller sends commands of zero length and with a zero timeout to
the Macintosh. Usually the reply will be a ?TMO because no data is
available. If data becomes available, two situations have to be handled:
In the first case all data was read including the terminator. Then a reply
without a ?TMO will be returned. If there is a slight delay in getting all
the data, a ?TMO followed by data bytes is returned. In that case the data
has to be appended to the data buffer. Then a null command with a sensible
timeout is sent. If a new timeout comes in on that onew, there is an error.
Else the data is appended to the data already read and we are finished. 

It has been choosen to implement this logic separatly from the serialsinq
stuff as it interacts very much with rest of SICS.

Just one function is exported:
@o serialwait.h @{
/*--------------------------------------------------------------------------
                          S E R I A L W A I T

  Executes a command on a serial port and waits for replies coming
  along by polling with null commands.

  copyright: see copyright.h

  Mark Koennecke, June 1998
---------------------------------------------------------------------------*/
#ifndef SERIALSICSWAIT
#define SERIALSICSWAIT
#include "sics.h"
#include "psi/hardsup/serialsinq.h"
  int SerialSicsExecute(void **pData, char *pCommand, char *pReply, 
                        int iBufLen);

#endif
@}   

Parameters and return values are the same as defined for the
SerialWriteRead function.

I am not sure if this facility works as expected. I think there was a
problem with crosstalk on the serial line.  

