/*--------------------------------------------------------------------------
                             S I C S
                         
       A common header file which includes the SICS kernel header files
       in the right order.
       
       Mark Koennecke, October 1997
       
-----------------------------------------------------------------------------*/
#ifndef SICSSICS
#define SICSSICS

typedef enum {
  eTimer,
  ePreset
} CounterMode;

/* the following line suppresses const declarations in tcl.h.
   WARNING: including sics.h must always be done before tcl.h
   -> makes the compiler happy M.Z. */
#define NO_CONST

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <strlutil.h>

#include "Scommon.h"
#include "event.h"
#include "obdes.h"
#include "interrupt.h"
#include "task.h"
#include "conman.h"
#include "interface.h"
#include "devexec.h"
#include "emon.h"
#include "nserver.h"
#include "sicsutil.h"
#include "logv2.h"
#include "trace.h"

extern pServer pServ;


#endif
