/*
 * SICS interface to asyncdealer
 *
 * Douglas Clowes, May 2018
 *
 */
#include "sicsasyncdealer.h"

#include "asyncdealer.h"
#include "dynstring.h"
#include "logv2.h"
#include "macro.h"
#include "nserver.h"
#include "nwatch.h"
#include "obdes.h"
#include "rbtree.h"
#include "task.h"

#include <stdlib.h>
#include <string.h>

extern pServer pServ;
pTaskMan GetTasker(void);
static int myTask(void *data);

static int keycmp(const void *left_key, const void* right_key);

/*--------------------------------------------------------------------------*/
/**
 * DEALER connection SICS object
 *
 */
typedef struct __Dealer {
  pObjectDescriptor pDes;       /* required as first field */
  char *name;                   /* dealer socket name */
  char *addr;                   /* target address */
  int iTrans;
  bool isFinal;
  pAsyncDealer dealer;
  struct rbtree_t rb_pending;
  TaskTaskID taskID;
  pTaskQueue taskQueue;
  pDynString cmd;
  pDynString request;
  pDynString response;
  pDynString replyHandlerDev;
  pDynString unsolHandlerDev;
} Dealer, *pDealer;

/*--------------------------------------------------------------------------*/
/**
 * The transaction control structure
 */
typedef struct {
    struct rbtree_node_t rbnode;
    int iTrans;
    pDynString cmd;
    pDynString request;
    pDynString response;
    pDynString replyHandlerTxn;
} data_node;

/*--------------------------------------------------------------------------*/
/**
 * The Task Message payload
 */
typedef struct command_context {
  pDealer dealer;
  struct json_object *msg_json;
  int iTrans;
  bool isFinal;
} COMMAND_CONTEXT;

/*--------------------------------------------------------------------------*/
static void dealerDelete(void *data)
{
  pDealer self = (pDealer) data;
  if (self->pDes)
    DeleteDescriptor(self->pDes);
  if (self->name)
    free(self->name);
  if (self->addr)
    free(self->addr);

  /* TODO: delete the asyncdealer */

  free(self);
}

/*--------------------------------------------------------------------------*/
static void dealerDeferred(pDealer self,
                              struct json_object *msg_json,
                              int iTrans,
                              bool isFinal)
{
  data_node *dnode = NULL;
  pDynString script = NULL;
  json_object *node;
  pDynString response;
  const char *reply = NULL;

  /* capture the response */
  if (json_object_object_get_ex(msg_json, "reply", &node)) {
    if (json_object_get_type(node) == json_type_string) {
      reply = json_object_get_string(node);
    } else {
      /* TODO logging */
    }
  }

  response = DynStringFromC(reply);

  /* retrieve the context */
  dnode = (data_node *) rbtree_node_lookup(&self->rb_pending, &iTrans);
  if (dnode) {
    if (dnode->response)
      DynStringDelete(dnode->response);
    dnode->response = DynStringHold(response);
    if (dnode->replyHandlerTxn)
      script = dnode->replyHandlerTxn;
  } else {
    if (self->unsolHandlerDev)
      script = self->unsolHandlerDev;
  }
  /* save the response - transfer ownership of response */
  self->iTrans = iTrans;
  self->isFinal = isFinal;
  if (self->response)
    DynStringDelete(self->response);
  self->response = response;

  /* run the script */
  if (script) {
    SConnection *pCon = SCCreateDummyConnection(pServ->pSics);
    SCStartBuffering(pCon);
    MacroEvalString(pCon, pServ->pSics, DynStringGetArray(script));
    SCEndBuffering(pCon);
    SCDeleteConnection(pCon);
  }

  /* if final then delete */
  if (dnode && isFinal) {
    rbtree_node_delete(&self->rb_pending, &dnode->rbnode);
    if (dnode->cmd)
      DynStringDelete(dnode->cmd);
    if (dnode->request)
      DynStringDelete(dnode->request);
    if (dnode->response)
      DynStringDelete(dnode->response);
    if (dnode->replyHandlerTxn)
      DynStringDelete(dnode->replyHandlerTxn);
    free(dnode);
  }
}

/*--------------------------------------------------------------------------*/
/**
 * Callback from AsyncDealer for all messages
 *
 *
 */
static void dealerCallback(void *pSelf,
                              struct json_object *msg_json,
                              int iTrans,
                              bool isFinal)
{
  pDealer dealer = pSelf;
  pTaskMessage pTMsg;     /**< Tasker message envelope*/
  COMMAND_CONTEXT *pCtx;  /**< Tasker message body */

  pTMsg = TaskMessageAlloc(sizeof(COMMAND_CONTEXT), 0);
  pCtx = (COMMAND_CONTEXT *) TaskMessageGetData(pTMsg);
  pCtx->dealer = dealer;
  pCtx->msg_json = json_object_get(msg_json); /* shared ownership */
  pCtx->iTrans = iTrans;
  pCtx->isFinal = isFinal;

  /* Handle it in the background */
  TaskQueueSend(dealer->taskQueue, pTMsg);
}

/*--------------------------------------------------------------------------*/
static data_node *dealerSend(pDealer self,
                      pDynString request,
                      pDynString cmd,
                      pDynString replyHandler)
{
    rbtree_node rnode = NULL;
    data_node *dnode = NULL;
    int trans;

    trans = AsyncDealerSend4(self->dealer,
                             DynStringGetArray(request),
                             DynStringGetArray(cmd),
                             dealerCallback);
    /* Create and set up the outstanding transaction entity */
    dnode = calloc(1, sizeof(data_node));
    rnode = &dnode->rbnode;
    rnode->key = &dnode->iTrans;
    rnode->value = dnode;
    dnode->iTrans = trans;
    dnode->cmd = DynStringHold(cmd);
    dnode->request = DynStringHold(request);
    dnode->replyHandlerTxn = DynStringHold(replyHandler);

    /* save it in the cache and return it */
    rbtree_insert(&self->rb_pending, rnode);
    return dnode;
}

/*--------------------------------------------------------------------------*/
/**
 * SICS command to manipulate a ZMQ DEALER socket object
 *
 */
static int DealerCommand(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[])
{
  pDealer self = (pDealer) pData;

  if (argc < 2) {
    SCPrintf(pCon, eValue, "Enter '%s help' for help", self->name);
    return 0;
  }

  /* One word commands */
  if (argc == 2) {
    if (strcasecmp(argv[1], "list") == 0) {
      pDynString str = DynStringCreate(256, 256);
      DynStringPrintf(str, "%s.handler = %s\n", self->name,
                      self->replyHandlerDev ? DynStringGetArray(self->replyHandlerDev) : "None");
      DynStringPrintf(str, "%s.unsolicited = %s\n", self->name,
                      self->unsolHandlerDev ? DynStringGetArray(self->unsolHandlerDev) : "None");
      DynStringPrintf(str, "%s.request = %s\n", self->name,
                      self->request ? DynStringGetArray(self->request) : "None");
      DynStringPrintf(str, "%s.response = %s\n", self->name,
                      self->response ? DynStringGetArray(self->response) : "None");
      DynStringPrintf(str, "%s.cmd = %s\n", self->name,
                      self->cmd ? DynStringGetArray(self->cmd) : "None");
      DynStringPrintf(str, "%s.trans = %d\n", self->name, self->iTrans);
      DynStringPrintf(str, "%s.final = %d\n", self->name, self->isFinal ? 1 : 0);
      SCWrite(pCon, DynStringGetArray(str), eValue);
      DynStringDelete(str);
      return 1;
    }

    if (strcasecmp(argv[1], "help") == 0) {
      /* TODO */
      return 1;
    }

    if (strcasecmp(argv[1], "handler") == 0) {
      SCPrintf(pCon, eValue, "%s.handler = %s", self->name,
               self->replyHandlerDev ? DynStringGetArray(self->replyHandlerDev) : "None");
      return 1;
    }

    if (strcasecmp(argv[1], "unsolicited") == 0) {
      SCPrintf(pCon, eValue, "%s.unsolicited = %s", self->name,
               self->unsolHandlerDev ? DynStringGetArray(self->unsolHandlerDev) : "None");
      return 1;
    }

    if (strcasecmp(argv[1], "request") == 0) {
      SCPrintf(pCon, eValue, "%s.request = %s", self->name,
               self->request ? DynStringGetArray(self->request) : "None");
      return 1;
    }

    if (strcasecmp(argv[1], "response") == 0) {
      SCPrintf(pCon, eValue, "%s.response = %s", self->name,
               self->response ? DynStringGetArray(self->response) : "None");
      return 1;
    }

    if (strcasecmp(argv[1], "cmd") == 0) {
      SCPrintf(pCon, eValue, "%s.cmd = %s", self->name,
               self->cmd ? DynStringGetArray(self->cmd) : "None");
      return 1;
    }

    if (strcasecmp(argv[1], "trans") == 0) {
      SCPrintf(pCon, eValue, "%s.trans = %d", self->name, self->iTrans);
      return 1;
    }

    if (strcasecmp(argv[1], "final") == 0) {
      SCPrintf(pCon, eValue, "%s.final = %d", self->name, self->isFinal ? 1 : 0);
      return 1;
    }
  }

  /* multi word commands */
  if (strcasecmp(argv[1], "request") == 0) {
    int i = 2;
    if (argc > i) {
      pDynString str = DynStringCreate(100, 100);
      for (; i < argc; ++i) {
        DynStringPrintf(str, "%s%s", i == 2 ? "" : " ", argv[i]);
      }
      if (self->request)
        DynStringDelete(self->request);
      self->request = str;
    }
    return 1;
  }

  if (strcasecmp(argv[1], "handler") == 0) {
    int i = 2;
    if (argc > i) {
      pDynString str = DynStringCreate(100, 100);
      for (; i < argc; ++i) {
        DynStringPrintf(str, "%s%s", i == 2 ? "" : " ", argv[i]);
      }
      if (self->replyHandlerDev)
        DynStringDelete(self->replyHandlerDev);
      self->replyHandlerDev = str;
    }
    return 1;
  }

  if (strcasecmp(argv[1], "unsolicited") == 0) {
    int i = 2;
    if (argc > i) {
      pDynString str = DynStringCreate(100, 100);
      for (; i < argc; ++i) {
        DynStringPrintf(str, "%s%s", i == 2 ? "" : " ", argv[i]);
      }
      if (self->unsolHandlerDev)
        DynStringDelete(self->unsolHandlerDev);
      self->unsolHandlerDev = str;
    }
    return 1;
  }

  if (strcasecmp(argv[1], "cmd") == 0) {
    int i = 2;
    if (argc > i) {
      pDynString str = DynStringCreate(100, 100);
      for (; i < argc; ++i) {
        DynStringPrintf(str, "%s%s", i == 2 ? "" : " ", argv[i]);
      }
      if (self->cmd)
        DynStringDelete(self->cmd);
      self->cmd = str;
    }
    return 1;
  }

  if (strcasecmp(argv[1], "trans") == 0) {
    data_node *dnode = NULL;
    int trans = atoi(argv[2]);

    dnode = (data_node *) rbtree_node_lookup(&self->rb_pending, &trans);
    if (dnode) {
      if (argc > 3) {
        if (strcasecmp(argv[3], "request") == 0) {
          SCPrintf(pCon, eValue, "%s.request = %s", self->name,
                   dnode->request ? DynStringGetArray(dnode->request) : "<None>");
        } else if (strcasecmp(argv[3], "response") == 0) {
          SCPrintf(pCon, eValue, "%s.response = %s", self->name,
                   dnode->response ? DynStringGetArray(dnode->response) : "<None>");
        } else if (strcasecmp(argv[3], "handler") == 0) {
          SCPrintf(pCon, eValue, "%s.handler = %s", self->name,
                   dnode->replyHandlerTxn ? DynStringGetArray(dnode->replyHandlerTxn) : "<None>");
        } else {
          SCPrintf(pCon, eError, "%s unknown '%s'", self->name, argv[3]);
          return 0;
        }
      } else {
        SCPrintf(pCon, eValue, "%s.response = %s", self->name,
                 dnode->response ? DynStringGetArray(dnode->response) : "<None>");
      }
    } else {
      SCPrintf(pCon, eError, "%s unknown trans '%s'", self->name, argv[2]);
      return 0;
    }
    return 1;
  }

  if (strcasecmp(argv[1], "send") == 0) {
    const char *cmd;
    data_node *dnode = NULL;
    int trans;
    int i = 2;
    cmd = DynStringGetArray(self->cmd);
    if (argc > i) {
      pDynString str = DynStringCreate(100, 100);
      for (; i < argc; ++i) {
        DynStringPrintf(str, "%s%s", i == 2 ? "" : " ", argv[i]);
      }
      if (self->request)
        DynStringDelete(self->request);
      self->request = str;
    }
    dnode = dealerSend(self,
                       self->request,
                       self->cmd,
                       self->replyHandlerDev);

    SCPrintf(pCon, eValue, "%s.trans = %d", self->name, dnode->iTrans);
    return 1;
  }

  if (strcasecmp(argv[1], "transact") == 0) {
    data_node *dnode = NULL;
    pDynString cmd = NULL;
    pDynString handler = NULL;
    pDynString request = NULL;
    int trans;
    int i;

    for (i = 2; i < argc - 1; i += 2) {
      if (argc > i + 1 && strcasecmp(argv[i], "cmd") == 0) {
        if (cmd)
          DynStringDelete(cmd);
        cmd = DynStringFromC(argv[i + 1]);
        continue;
      }
      if (argc > i + 1 && strcasecmp(argv[i], "handler") == 0) {
        if (handler)
          DynStringDelete(handler);
        handler = DynStringFromC(argv[i + 1]);
        continue;
      }
      if (argc > i + 1 && strcasecmp(argv[i], "request") == 0) {
        if (request)
          DynStringDelete(request);
        request = DynStringFromC(argv[i + 1]);
        continue;
      }
    }

    dnode = dealerSend(self,
                       request ? request : self->request,
                       cmd ? cmd : self->cmd,
                       handler ? handler : self->replyHandlerDev);

    if (cmd)
      DynStringDelete(cmd);
    if (handler)
      DynStringDelete(handler);
    if (request)
      DynStringDelete(request);

    SCPrintf(pCon, eValue, "%s.trans = %d", self->name, dnode->iTrans);
    return 1;
  }
  return 0;
}
/*--------------------------------------------------------------------------*/
/**
 * SICS command ZDEALER
 *
 * create a named ZMQ DEALER socket
 */
int SICSZDealer(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[])
{
  const char *name, *addr, *cmd;
  char taskName[132];
  pDealer self;
  pAsyncDealer pNew;

  if (argc < 3) {
    SCPrintf(pCon, eError,
             "usage:%s <name> <zmq-address> [<cmd>]",
             argv[0]);
    return 0;
  }

  name = argv[1];
  addr = argv[2];
  if (argc >= 3)
    cmd = argv[3];
  else
    cmd = "None";

  self = calloc(1, sizeof(Dealer));
  if (self == NULL) {
    SCPrintf(pCon, eError, "create %s to %s failed alloc", name, addr);
    return 0;
  }
  self->pDes = CreateDescriptor("DealerSocket");
  if (self->pDes == NULL) {
    SCPrintf(pCon, eError, "create %s to %s failed desc", name, addr);
    dealerDelete(self);
    return 0;
  }
  self->name = strdup(name);
  self->addr = strdup(addr);
  pNew = AsyncDealerCreate(addr, cmd, dealerCallback, self);
  if (pNew == NULL) {
    SCPrintf(pCon, eError, "create %s to %s failed connect", name, addr);
    dealerDelete(self);
    return 0;
  }
  self->dealer = pNew;
  rbtree_init(&self->rb_pending, (rbtree_compare_func) keycmp);
  AddCommand(pSics, (char *)name, DealerCommand, dealerDelete, self);

  snprintf(taskName, sizeof(taskName) - 1, "DEALER:%s", addr);
  self->taskID = TaskRegisterQ(GetTasker(), taskName, myTask,
                                  NULL, NULL,
                                  self, TASK_PRIO_LOW);
  self->taskQueue = TaskQueueGet(GetTasker(), self->taskID);

  SCSendOK(pCon);
  return 1;
}

/**
 * rbtree integer key comparisson routine
 */
static int keycmp(const void *left_key, const void* right_key)
{
  int *left = (int *)left_key;
  int *right = (int *)right_key;
  if (*left < *right)
    return -1;
  if (*left > *right)
    return 1;
  return 0;
}

/**
 * Handle the replies in this background task
 */
static int myTask(void *data)
{
  /* TODO */
  const char *pResp;
  const char *pFlag;
  pTaskMessage pTMsg;
  data_node *dnode = NULL;

  while ((pTMsg = TaskQueueRecvMine(GetTasker()))) {
    COMMAND_CONTEXT *pCtx = (COMMAND_CONTEXT *) TaskMessageGetData(pTMsg);
    pDealer self = pCtx->dealer;
    struct json_object *msg_json = pCtx->msg_json;
    int iTrans = pCtx->iTrans;
    bool isFinal = pCtx->isFinal;

    dealerDeferred(self, msg_json, iTrans, isFinal);

    TaskMessageRelease(pTMsg);
    json_object_put(msg_json);
  }
  return 1;
}
