/*-------------------------------------------------------------------------
                      S I C S C R O N

  Periodically repeated task in SICS.

  Mark Koennecke, November 1999
  --------------------------------------------------------------------------*/
#ifndef SICSCRON
#define SICSCRON

#include "SCinter.h"
#include "conman.h"

int MakeCron(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[]);

#endif
