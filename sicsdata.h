
/*---------------------------------------------------------------------
                           S I C S D A T A 

  An attempt to a generic interface to SICS data for all sorts of SICS
  clients.

  copyright: see file COPYRIGHT

  Mark Koennecke, June 2003
----------------------------------------------------------------------*/
#ifndef SICSDATA
#define SICSDATA
#define INTTYPE 0
#define FLOATTYPE 1

#include "SCinter.h"
#include "conman.h"
#include "obdes.h"

        typedef struct { 
                        pObjectDescriptor pDes;
                        int *data;
                        char *dataType;
                        int dataUsed;
                        int currentDataSize;
        }SICSData, *pSICSData;
                        

/*------------------------------------------------------------------*/

 int *getSICSDataPointer(pSICSData self, int start, int end);
 
 pSICSData createSICSData(char *name);

 void assignSICSType(pSICSData self, int start, int end, int type);

 int SICSDataFactory(SConnection *pCon, SicsInterp *pSics, 
        void    *pData,
        int argc, char *argv[]);

 int SICSDataAction(SConnection *pCon, SicsInterp *pSics, 
        void    *pData,
        int argc, char *argv[]);
        
 void clearSICSData(pSICSData self);    
 int getSICSDataInt(pSICSData self, int pos, int *value);
 int getSICSDataFloat(pSICSData self, int pos, float *value);
 int setSICSDataInt(pSICSData self, int pos, int value);
 int setSICSDataFloat(pSICSData self, int pos, float value);
        

#endif
