/*---------------------------------------------------------------------------
  Sometimes we want to close the server correctly. This header and 
  its implementation file define an apropriate command.
  
  Mark Koennecke, November 1996
  
  copyright: see implementation file
----------------------------------------------------------------------------*/
#ifndef SICSEXIT
#define SICSEXIT
#include "conman.h"

int SicsExit(SConnection * pCon, SicsInterp * pInterp, void *pData,
             int argc, char *argv[]);

#endif
