/**
 * This is a generalized SICS get/put routine. It tries to hard to match a name 
 * with the data. 
 * 
 * copyright: see file COPYRIGHT
 *
 */
#ifndef __SICSGET
#define __SICSGET
#include "hipadaba.h"

/**
 * Get a SICS value
 * @param name The name of the value to get
 * @param v The output hdbValue 
 * @return 1 on success, 0 on failure
 */
int sget(char *name, hdbValue *v);
/**
 * Put a SICS value
 * @param name The name of the value to write to
 * @param v The  hdbValue to write 
 * @return 1 on success, 0 on failure
 */
int sput(char *name, hdbValue v);



#endif

