/*
 * Experience has shown that integrating existing SICS objects into the
 * Hierarchical Parameter Database (Hdb) is a difficult task without reworking 
 * the complete SICS object model. Rather, it seems easier to adapt some 
 * critical objects to the Hdb with some glue code. Following the facade or 
 * adapter design pattern. This is the purpose of this module. For the moment
 * the external interface is only an interpreter function which will be used to 
 * install suitable SICS objects into the Hdb tree and generates the necessary 
 * adapters internally. This code can be used to adapt to:
 * - motors
 * - the data segment of histogram memories
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, November 2006
 */
#ifndef SICSHDBADAPTER_H_
#define SICSHDBADAPTER_H_

#include "SCinter.h"
#include "conman.h"

int SICSHdbAdapter(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[]);
int HdbSubSample(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[]);

#endif                          /*SICSHDBADAPTER_H_ */
