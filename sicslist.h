/*
  A module for displaying various bits of information about SICS
  internals and objects.
  
  copyright: see file COPYRIGHT
  
  Mark Koennecke, January 2006 
 */
#ifndef SICSLIST_H_
#define SICSLIST_H_
#include "sics.h"
int SicsList(SConnection * pCon, SicsInterp * pSics,
             void *pData, int argc, char *argv[]);

#endif                          /*SICSLIST_H_ */
