/**
 * This is the header file for the new (as of 2007) style SICS objects 
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, July 2007
 */
#ifndef SICSOBJ2_H_
#define SICSOBJ2_H_
#include "stdio.h"
#include "hipadaba.h"
#include "sics.h"
/*======================================================================
 * Be careful when changing this data structure. It has to be compatible 
 * in its first fields with the SICS object descriptor as defined in 
 * obdes.h in order to achieve backwards compatibility with old style 
 * SICS objects.
 * =====================================================================*/
typedef struct {
  pObjectDescriptor pDes;
  pHdb objectNode;
  void *pPrivate;
  void (*KillPrivate) (void *pPrivate);
} SICSOBJ, *pSICSOBJ;
/*-----------------------------------------------------------------------*/
typedef int (*SICSOBJFunc) (pSICSOBJ self, SConnection * pCon,
                            pHdb commandNode, pHdb par[], int nPar);
/*======================= Live & Death  =================================*/
pSICSOBJ MakeSICSOBJ(char *name, char *class);
pSICSOBJ MakeSICSOBJv(char *name, char *class, int type, int priv);
void KillSICSOBJ(void *data);
void DefaultKill(void *data);
void DefaultFree(void *data);

/* register a callback for killing the command when the node is killed */
void RegisterSICSOBJKillCmd(pSICSOBJ sicsobj, char *name);

int SaveSICSOBJ(void *data, char *name, FILE * fd);

/**
 * This creates a new SICS object and installs it in the interpreter. It returns 
 * the newly created SICS object such that the caller can continue 
 * configuring it.
 */
pSICSOBJ SetupSICSOBJ(SConnection * pCon, SicsInterp * pSics, void *pData,
                      int argc, char *argv[]);
/**
 * runObjectFunction is an internal function for running an existing object 
 * command. The user has to search the node first, prime the parameters, 
 * and then call this command to actually execute.
 * @param object The object on which the command is executed 
 * @param pCon a connection to use for error reporting
 * @param commandNode The node to invoke.
 */
int runObjFunction(pSICSOBJ object, SConnection *pCon, pHdb commandNode);

/*====================== Interpreter Interface ===========================
 * InvokeSICSObj is special in that it returns -1 if it cannot handle 
 * the command. This leaves calling code the opportunity to process
 * further commands.It returns 1 on success and 0 on failures though.
 * ------------------------------------------------------------------------*/
int InvokeSICSOBJ(SConnection * pCon, SicsInterp * pSics, void *pData,
                  int argc, char *argv[]);
int InterInvokeSICSOBJ(SConnection * pCon, SicsInterp * pSics, void *pData,
                       int argc, char *argv[]);
int InstallSICSOBJ(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[]);


#endif                          /*SICSOBJ2_H_ */
