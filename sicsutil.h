/**
 * Some general functions for SICS.
 * 
 * copyright: see file COPYRIGHT
 * 
 *
 * Decode privilege text. moved from access.c by M. Zolliker Jan 2010
 * @param the text to decode
 * @return -1 if code invalid, else the privilege code
 */
#ifndef SICSUTIL_H
#define SICSUTIL_H

#include <stdlib.h>

int decodeSICSPriv(char *privText);

double DoubleTime(void);
/* returns time since epoch in seconds
   the resultion is machine dependent, but mostly better than msec */

/**
 * returns a checksum for data
 *  /param data pointer to the data to checksum
 *  /param len The length of data
 *  /return a checksum
 */
unsigned short fletcher16( char *data, size_t len);



void LogUserInfo(void *pCon);
#endif
