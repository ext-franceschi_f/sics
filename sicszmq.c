#include "sicszmq.h"
#include "nserver.h"
#include "logv2.h"
#include <stdlib.h>
#include <json.h>
#include <zmq.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

typedef struct {
  void *zsock;
  unsigned int counter;
  const char *addr;
} ZSOCK;

static pHNDBAG dealerBag = NULL;
static pHNDBAG publishBag = NULL;
static pHNDBAG routerBag = NULL;
static pHNDBAG subscribeBag = NULL;
static void *pZMQcontext = NULL;

static void free_func(void *ptr);
static int find_func(const void *left, const void *right);

/*-------------------------------------------------------------------------*/
void *GetZMQcontext(void)
{
  if (dealerBag == NULL) {
    if (hndbag_create(&dealerBag, 32, 16536)) {
      Log(ERROR,"sys", "Failed to create ZMQ dealer handbag");
      assert(dealerBag);
      return NULL;
    }
    hndbag_set_item_destructor(dealerBag, free_func);
    (void) hndbag_set_unique(dealerBag, true);
  }
  if (publishBag == NULL) {
    if (hndbag_create(&publishBag, 32, 16536)) {
      Log(ERROR, "sys", "Failed to create ZMQ publish handbag");
      assert(publishBag);
      return NULL;
    }
    hndbag_set_item_destructor(publishBag, free_func);
    (void) hndbag_set_unique(publishBag, true);
  }
  if (routerBag == NULL) {
    if (hndbag_create(&routerBag, 32, 16536)) {
      Log(ERROR,"sys", "Failed to create ZMQ router handbag");
      assert(routerBag);
      return NULL;
    }
    hndbag_set_item_destructor(routerBag, free_func);
    (void) hndbag_set_unique(routerBag, true);
  }
  if (subscribeBag == NULL) {
    if (hndbag_create(&subscribeBag, 32, 16536)) {
      Log(ERROR,"sys", "Failed to create ZMQ subscribe handbag");
      assert(subscribeBag);
      return NULL;
    }
    hndbag_set_item_destructor(subscribeBag, free_func);
    (void) hndbag_set_unique(subscribeBag, true);
  }
  if (pZMQcontext == NULL) {
    pZMQcontext = zmq_ctx_new();
    if (!pZMQcontext) {
      Log(ERROR,"sys", "Failed to create ZMQ context");
      assert(pZMQcontext);
      return NULL;
    }
  }
  return pZMQcontext;
}

/*
 * ZMQ DEALER Sockets
 */
HANDLE_T sics_zmq_dealer(const char *address)
{
  HANDLE_T handle = 0;
  void *this_socket = NULL;
  ZSOCK temp = {0};
  int rc;
  temp.addr = address;
  if (pZMQcontext == NULL)
    GetZMQcontext();

  if (hndbag_find_handle(dealerBag, find_func, &temp, &handle) == 0) {
    /* already exists - hold it */
    hndbag_item_retain(dealerBag, handle);
    return handle;
  }
  this_socket = zmq_socket(GetZMQcontext(), ZMQ_DEALER);
  if (this_socket) {
    int value = 2000;
    (void) zmq_setsockopt(this_socket, ZMQ_LINGER, &value, sizeof(value));
    rc = zmq_connect(this_socket, address);
    if (rc == 0) {
      ZSOCK *p = calloc(1, sizeof(ZSOCK));
      if (!p) {
        Log(INFO,"sys", "Failed sics_zmq_dealer alloc:%s", address);
        return 0;
      }
      if (hndbag_item_insert(dealerBag, p, &handle)) {
          Log(ERROR,"sys", "Failed sics_zmq_dealer insert:%s", address);
          free(p);
      } else {
        Log(INFO,"sys", "Bound ZEROMQ DEALER socket:%s", address);
        p->zsock = this_socket;
        p->counter = 0;
        p->addr = strdup(address);
        return handle;
      }
    }
    Log(ERROR,"sys", "Failed to connect ZEROMQ DEALER socket:%s %s",
        address,
        strerror(errno));
    zmq_close(this_socket);
    this_socket = NULL;
    return 0;
  }
  Log(ERROR,"sys", "Failed to open ZEROMQ DEALER socket:%s", address);
  return 0;
}

void sics_zmq_undealer(HANDLE_T *hand)
{
  void *v = NULL;
  ZSOCK *p;
  if (hndbag_get_pointer(dealerBag, *hand, &v)) {
    Log(ERROR,"sys", "Closed ZEROMQ DEALER socket:unknown");
    return;
  }
  p = v;
  hndbag_item_release(dealerBag, hand);
}

typedef struct dealer_message DEALER_MESSAGE;
struct dealer_message {
  int max_parts;
  int num_parts;
  zmq_msg_t *parts;
};

int sics_zmq_dealer_size(pDEALER_MESSAGE msg)
{
  if (msg && msg->num_parts > 0) {
    return zmq_msg_size(&msg->parts[msg->num_parts - 1]);
  }
  return -1;
}

const void *sics_zmq_dealer_data(pDEALER_MESSAGE msg)
{
  if (msg && msg->num_parts > 0) {
    return zmq_msg_data(&msg->parts[msg->num_parts - 1]);
  }
  return NULL;
}

void sics_zmq_dealer_free(pDEALER_MESSAGE msg)
{
  if (msg) {
    if (msg->parts) {
      if (msg->num_parts > 0) {
        int rc, idx;
        for (idx = 0; idx < msg->num_parts; ++idx) {
          rc = zmq_msg_close(&msg->parts[idx]);
        }
      }
      free(msg->parts);
    }
    free(msg);
  }
}

DEALER_MESSAGE *sics_zmq_dealer_recv(HANDLE_T hand, int max_parts)
{
  int32_t more = 0;
  size_t more_size = sizeof more;
  DEALER_MESSAGE *rm;
  void *v = NULL;
  ZSOCK *p;
  if (hndbag_get_pointer(dealerBag, hand, &v) < 0) {
    return NULL;
  }
  p = v;
  if (p && p->zsock) {
    zmq_pollitem_t item = {0};
    item.socket = p->zsock;
    item.events = ZMQ_POLLIN;
    if (zmq_poll(&item, 1, 0) <= 0)
      return NULL;
    rm = (DEALER_MESSAGE *) calloc(1, sizeof(DEALER_MESSAGE));
    if (!rm) {
      /* TODO */
      return NULL;
    }
    rm->max_parts = max_parts;
    rm->num_parts = 0;
    rm->parts = (zmq_msg_t *) calloc(max_parts, sizeof(zmq_msg_t));
    if (!rm->parts) {
      /* TODO */
      free(rm);
      return NULL;
    }
    do {
      int rc;
      if (rm->num_parts >= rm->max_parts) {
        /* TODO */
      }
      rc = zmq_msg_init(&rm->parts[rm->num_parts]);
      rc = zmq_recvmsg(p->zsock, &rm->parts[rm->num_parts++], ZMQ_DONTWAIT);
      if (rc < 0 && errno == EAGAIN) {
        sics_zmq_dealer_free(rm);
        return NULL;
      }
      rc = zmq_getsockopt(p->zsock, ZMQ_RCVMORE, &more, &more_size);
    } while (more);
    return rm;
  }
  return NULL;
}

int sics_zmq_dealer_send(HANDLE_T hand,
                          const char *request)
{
  void *v = NULL;
  ZSOCK *p;
  if (hndbag_get_pointer(dealerBag, hand, &v) < 0) {
    return -1;
  }
  p = v;
  if (p && p->zsock) {
    int rc, idx;
    zmq_msg_t zmsg, null_part;
    size_t len = strlen(request);
    rc = zmq_msg_init_size(&null_part, 0);
    rc = zmq_msg_init_size(&zmsg, len);
    memcpy(zmq_msg_data(&zmsg), request, len);
    rc = zmq_sendmsg(p->zsock, &null_part, ZMQ_SNDMORE);
    rc = zmq_sendmsg(p->zsock, &zmsg, 0);
    zmq_msg_close(&null_part);
    zmq_msg_close(&zmsg);
    if (rc == len) {
      return len;
    }
  }
  return -1;
}

void *sics_zmq_dealer_zsock(HANDLE_T hand)
{
  void *v = NULL;
  ZSOCK *p;
  if (hndbag_get_pointer(dealerBag, hand, &v) < 0) {
    return NULL;
  }
  p = v;
  return p->zsock;
}

/*
 * ZMQ PUBLISH Sockets
 */
HANDLE_T sics_zmq_publish(const char *address)
{
  HANDLE_T handle = 0;
  void *this_socket = NULL;
  ZSOCK temp = {0};
  int rc;
  temp.addr = address;
  if (pZMQcontext == NULL)
    GetZMQcontext();

  if (hndbag_find_handle(publishBag, find_func, &temp, &handle) == 0) {
    /* already exists - hold it */
    hndbag_item_retain(publishBag, handle);
    return handle;
  }
  this_socket = zmq_socket(GetZMQcontext(), ZMQ_PUB);
  if (this_socket) {
    int value = 2000;
    (void) zmq_setsockopt(this_socket, ZMQ_LINGER, &value, sizeof(value));
    rc = zmq_bind(this_socket, address);
    if (rc == 0) {
      ZSOCK *p = calloc(1, sizeof(ZSOCK));
      if (!p) {
        Log(ERROR,"sys", "Failed sics_zmq_publish alloc:%s", address);
        return 0;
      }
      if (hndbag_item_insert(publishBag, p, &handle)) {
          Log(ERROR,"sys", "Failed sics_zmq_publish insert:%s", address);
          free(p);
      } else {
        Log(INFO,"sys", "Bound ZEROMQ PUB socket:%s", address);
        p->zsock = this_socket;
        p->counter = 0;
        p->addr = strdup(address);
        return handle;
      }
    }
    Log(ERROR,"sys", "Failed to bind ZEROMQ PUB socket:%s", address);
    zmq_close(this_socket);
    this_socket = NULL;
    return 0;
  }
  Log(ERROR,"sys", "Failed to open ZEROMQ PUB socket:%s", address);
  return 0;
}

void sics_zmq_unpublish(HANDLE_T *hand)
{
  void *v = NULL;
  ZSOCK *p;
  if (hndbag_get_pointer(publishBag, *hand, &v)) {
    Log(ERROR,"sys", "Closed ZEROMQ PUB socket:unknown");
    return;
  }
  p = v;
  hndbag_item_release(publishBag, hand);
}

int sics_zmq_pubsend(HANDLE_T hand,
                     const char *status_type,
                     const char *status_name,
                     const char *status_valu)
{
  void *v = NULL;
  ZSOCK *p;
  if (hndbag_get_pointer(publishBag, hand, &v) < 0) {
    return -1;
  }
  p = v;
  if (p && p->zsock) {
    struct json_object *msg_json;
    msg_json = json_object_new_object();
    if (msg_json) {
      const char *json_text;
      zmq_msg_t zmsg;
      int len, rc;
      extern double DoubleTime(void);
      double double_time = DoubleTime();
      json_object_object_add(msg_json, "type",
                             json_object_new_string(status_type));
      json_object_object_add(msg_json, "name",
                             json_object_new_string(status_name));
      json_object_object_add(msg_json, "value",
                             json_object_new_string(status_valu));
      json_object_object_add(msg_json, "seq",
                             json_object_new_int(p->counter));
      json_object_object_add(msg_json, "ts",
                             json_object_new_double(double_time));
      if (++p->counter > 999999)
        p->counter = 0;
      json_text = json_object_to_json_string(msg_json);
      len = strlen(json_text);
      rc = zmq_msg_init_size (&zmsg, len);
      if (rc == 0) {
        memcpy(zmq_msg_data(&zmsg), json_text, len);
        rc = zmq_sendmsg(p->zsock, &zmsg, 0);
        if (rc == len) {
          json_object_put(msg_json);
          return len;
        }
      }
      json_object_put(msg_json);
      return 0;
    }
  }
  return 0;
}

/*
 * ZMQ ROUTER Sockets
 */
HANDLE_T sics_zmq_router(const char *address)
{
  HANDLE_T handle = 0;
  void *this_socket = NULL;
  ZSOCK temp = {0};
  int rc;
  temp.addr = address;
  if (pZMQcontext == NULL)
    GetZMQcontext();

  if (hndbag_find_handle(routerBag, find_func, &temp, &handle) == 0) {
    /* already exists - hold it */
    hndbag_item_retain(routerBag, handle);
    return handle;
  }
  this_socket = zmq_socket(GetZMQcontext(), ZMQ_ROUTER);
  if (this_socket) {
    int value = 2000;
    (void) zmq_setsockopt(this_socket, ZMQ_LINGER, &value, sizeof(value));
    value = 10000;
    (void) zmq_setsockopt(this_socket, ZMQ_RCVHWM, &value, sizeof(value));
    (void) zmq_setsockopt(this_socket, ZMQ_SNDHWM, &value, sizeof(value));
    rc = zmq_bind(this_socket, address);
    if (rc == 0) {
      ZSOCK *p = calloc(1, sizeof(ZSOCK));
      if (!p) {
        Log(INFO,"sys", "Failed sics_zmq_router alloc:%s", address);
        return 0;
      }
      if (hndbag_item_insert(routerBag, p, &handle)) {
          Log(ERROR,"sys", "Failed sics_zmq_router insert:%s", address);
          free(p);
      } else {
        Log(INFO,"sys", "Bound ZEROMQ ROUTER socket:%s", address);
        p->zsock = this_socket;
        p->counter = 0;
        p->addr = strdup(address);
        return handle;
      }
    }
    Log(ERROR,"sys", "Failed to bind ZEROMQ ROUTER socket:%s", address);
    zmq_close(this_socket);
    this_socket = NULL;
    return 0;
  }
  Log(ERROR,"sys", "Failed to open ZEROMQ ROUTER socket:%s", address);
  return 0;
}

void sics_zmq_unrouter(HANDLE_T *hand)
{
  void *v = NULL;
  ZSOCK *p;
  if (hndbag_get_pointer(routerBag, *hand, &v)) {
    Log(ERROR,"sys", "Closed ZEROMQ ROUTER socket:unknown");
    return;
  }
  p = v;
  hndbag_item_release(routerBag, hand);
}

typedef struct router_message ROUTER_MESSAGE;
struct router_message {
  int max_parts;
  int num_parts;
  zmq_msg_t *parts;
};

int sics_zmq_router_size(pROUTER_MESSAGE msg)
{
  if (msg && msg->num_parts > 0) {
    return zmq_msg_size(&msg->parts[msg->num_parts - 1]);
  }
  return -1;
}

const void *sics_zmq_router_data(pROUTER_MESSAGE msg)
{
  if (msg && msg->num_parts > 0) {
    return zmq_msg_data(&msg->parts[msg->num_parts - 1]);
  }
  return NULL;
}

void sics_zmq_router_free(pROUTER_MESSAGE msg)
{
  if (msg) {
    if (msg->parts) {
      if (msg->num_parts > 0) {
        int rc, idx;
        for (idx = 0; idx < msg->num_parts; ++idx) {
          rc = zmq_msg_close(&msg->parts[idx]);
        }
      }
      free(msg->parts);
    }
    free(msg);
  }
}

ROUTER_MESSAGE *sics_zmq_router_recv(HANDLE_T hand, int max_parts)
{
  int32_t more = 0;
  size_t more_size = sizeof more;
  ROUTER_MESSAGE *rm;
  void *v = NULL;
  ZSOCK *p;
  if (hndbag_get_pointer(routerBag, hand, &v) < 0) {
    return NULL;
  }
  p = v;
  if (p && p->zsock) {
    zmq_pollitem_t item = {0};
    item.socket = p->zsock;
    item.events = ZMQ_POLLIN;
    if (zmq_poll(&item, 1, 0) <= 0)
      return NULL;
    rm = (ROUTER_MESSAGE *) calloc(1, sizeof(ROUTER_MESSAGE));
    if (!rm) {
      /* TODO */
      return NULL;
    }
    rm->max_parts = max_parts;
    rm->num_parts = 0;
    rm->parts = (zmq_msg_t *) calloc(max_parts, sizeof(zmq_msg_t));
    if (!rm->parts) {
      /* TODO */
      free(rm);
      return NULL;
    }
    do {
      int rc;
      if (rm->num_parts >= rm->max_parts) {
        /* TODO */
      }
      rc = zmq_msg_init(&rm->parts[rm->num_parts]);
      rc = zmq_recvmsg(p->zsock, &rm->parts[rm->num_parts++], ZMQ_DONTWAIT);
      if (rc < 0 && errno == EAGAIN) {
        sics_zmq_router_free(rm);
        return NULL;
      }
      rc = zmq_getsockopt(p->zsock, ZMQ_RCVMORE, &more, &more_size);
    } while (more);
    return rm;
  }
  return NULL;
}

int sics_zmq_router_reply(HANDLE_T hand,
                          pROUTER_MESSAGE msg,
                          const char *reply)
{
  void *v = NULL;
  ZSOCK *p;
  if (!msg || msg->num_parts <= 0 || msg->num_parts > msg->max_parts) {
    /* TODO */
    return -1;
  }
  if (hndbag_get_pointer(routerBag, hand, &v) < 0) {
    return -1;
  }
  p = v;
  if (p && p->zsock) {
    int rc, idx;
    zmq_msg_t zmsg;
    size_t len = strlen(reply);
    for (idx = 0; idx < msg->num_parts - 1; ++idx) {
      rc = zmq_msg_init(&zmsg);
      rc = zmq_msg_copy(&zmsg, &msg->parts[idx]);
      rc = zmq_sendmsg(p->zsock, &zmsg, ZMQ_SNDMORE);
    }
    rc = zmq_msg_init_size(&zmsg, len);
    memcpy(zmq_msg_data(&zmsg), reply, len);
    rc = zmq_sendmsg(p->zsock, &zmsg, 0);
    zmq_msg_close(&zmsg);
    if (rc == len) {
      return len;
    }
  }
  return -1;
}

void *sics_zmq_router_zsock(HANDLE_T hand)
{
  void *v = NULL;
  ZSOCK *p;
  if (hndbag_get_pointer(routerBag, hand, &v) < 0) {
    return NULL;
  }
  p = v;
  return p->zsock;
}

/*
 * ZMQ SUBSCRIBE Sockets
 */
HANDLE_T sics_zmq_subscribe(const char *address)
{
  HANDLE_T handle = 0;
  void *this_socket = NULL;
  ZSOCK temp = {0};
  int rc;
  temp.addr = address;
  if (pZMQcontext == NULL)
    GetZMQcontext();

  if (hndbag_find_handle(subscribeBag, find_func, &temp, &handle) == 0) {
    /* already exists - hold it */
    hndbag_item_retain(subscribeBag, handle);
    return handle;
  }
  this_socket = zmq_socket(GetZMQcontext(), ZMQ_SUB);
  if (this_socket) {
    rc = zmq_connect(this_socket, address);
    if (rc == 0) {
      const char *value = "";
      ZSOCK *p = calloc(1, sizeof(ZSOCK));
      if (!p) {
        Log(INFO,"sys", "Failed sics_zmq_subscribe alloc:%s", address);
        return 0;
      }
      (void) zmq_setsockopt(this_socket, ZMQ_SUBSCRIBE, value, 0);
      if (hndbag_item_insert(subscribeBag, p, &handle)) {
          Log(ERROR,"sys", "Failed sics_zmq_subscribe insert:%s", address);
          free(p);
      } else {
        Log(INFO,"sys", "Bound ZEROMQ SUBSCRIBE socket:%s", address);
        p->zsock = this_socket;
        p->counter = 0;
        p->addr = strdup(address);
        return handle;
      }
    }
    Log(ERROR,"sys", "Failed to bind ZEROMQ SUBSCRIBE socket:%s", address);
    zmq_close(this_socket);
    this_socket = NULL;
    return 0;
  }
  Log(ERROR,"sys", "Failed to open ZEROMQ SUBSCRIBE socket:%s", address);
  return 0;
}

void sics_zmq_unsubscribe(HANDLE_T *hand)
{
  void *v = NULL;
  ZSOCK *p;
  if (hndbag_get_pointer(subscribeBag, *hand, &v)) {
    Log(ERROR,"sys", "Closed ZEROMQ SUBSCRIBE socket:unknown");
    return;
  }
  p = v;
  hndbag_item_release(subscribeBag, hand);
}

typedef struct subscribe_message SUBSCRIBE_MESSAGE;
struct subscribe_message {
  int max_parts;
  int num_parts;
  zmq_msg_t *parts;
};

int sics_zmq_subscribe_size(pSUBSCRIBE_MESSAGE msg)
{
  if (msg && msg->num_parts > 0) {
    return zmq_msg_size(&msg->parts[msg->num_parts - 1]);
  }
  return -1;
}

const void *sics_zmq_subscribe_data(pSUBSCRIBE_MESSAGE msg)
{
  if (msg && msg->num_parts > 0) {
    return zmq_msg_data(&msg->parts[msg->num_parts - 1]);
  }
  return NULL;
}

void sics_zmq_subscribe_free(pSUBSCRIBE_MESSAGE msg)
{
  if (msg) {
    if (msg->parts) {
      if (msg->num_parts > 0) {
        int rc, idx;
        for (idx = 0; idx < msg->num_parts; ++idx) {
          rc = zmq_msg_close(&msg->parts[idx]);
        }
      }
      free(msg->parts);
    }
    free(msg);
  }
}

SUBSCRIBE_MESSAGE *sics_zmq_subscribe_recv(HANDLE_T hand, int max_parts)
{
  int32_t more = 0;
  size_t more_size = sizeof more;
  SUBSCRIBE_MESSAGE *sm;
  void *v = NULL;
  ZSOCK *p;
  if (hndbag_get_pointer(subscribeBag, hand, &v) < 0) {
    return NULL;
  }
  p = v;
  if (p && p->zsock) {
    zmq_pollitem_t item = {0};
    item.socket = p->zsock;
    item.events = ZMQ_POLLIN;
    if (zmq_poll(&item, 1, 0) <= 0)
      return NULL;
    sm = (SUBSCRIBE_MESSAGE *) calloc(1, sizeof(SUBSCRIBE_MESSAGE));
    if (!sm) {
      /* TODO */
      return NULL;
    }
    sm->max_parts = max_parts;
    sm->num_parts = 0;
    sm->parts = (zmq_msg_t *) calloc(max_parts, sizeof(zmq_msg_t));
    if (!sm->parts) {
      /* TODO */
      free(sm);
      return NULL;
    }
    do {
      int rc;
      if (sm->num_parts >= sm->max_parts) {
        /* TODO */
      }
      rc = zmq_msg_init(&sm->parts[sm->num_parts]);
      rc = zmq_recvmsg(p->zsock, &sm->parts[sm->num_parts++], ZMQ_DONTWAIT);
      if (rc < 0 && errno == EAGAIN) {
        sics_zmq_subscribe_free(sm);
        return NULL;
      }
      rc = zmq_getsockopt(p->zsock, ZMQ_RCVMORE, &more, &more_size);
    } while (more);
    return sm;
  }
  return NULL;
}

void *sics_zmq_subscribe_zsock(HANDLE_T hand)
{
  void *v = NULL;
  ZSOCK *p;
  if (hndbag_get_pointer(subscribeBag, hand, &v) < 0) {
    return NULL;
  }
  p = v;
  return p->zsock;
}

void sics_zmq_terminate(void)
{
  if (publishBag)
    hndbag_release(&publishBag);
  if (routerBag)
    hndbag_release(&routerBag);
  if (subscribeBag)
    hndbag_release(&subscribeBag);
  if (pZMQcontext) {
    zmq_ctx_destroy(pZMQcontext);
    pZMQcontext = NULL;
  }
}

/* static functions */
static void free_func(void *ptr) {
  ZSOCK *p = (ZSOCK *) ptr;
  if (p->zsock) {
    zmq_close(p->zsock);
    /* give ZMQ a chance to close it in another thread */
    usleep(500);
  }
  if (p->addr) {
    free((void *)p->addr);
    p->addr = NULL;
  }
  free(p);
}

static int find_func(const void *left, const void *right)
{
  return strcasecmp(((ZSOCK *)left)->addr, ((ZSOCK *)right)->addr);
}

