#include "hndbag.h"
#include <stdbool.h>
#ifndef SICSZMQ_H
#define SICSZMQ_H

void *GetZMQcontext(void);

HANDLE_T sics_zmq_dealer(const char *address);
void sics_zmq_undealer(HANDLE_T *hand);

void *sics_zmq_dealer_zsock(HANDLE_T hand);
typedef struct dealer_message *pDEALER_MESSAGE;
pDEALER_MESSAGE sics_zmq_dealer_recv(HANDLE_T hand, int max_parts);
int sics_zmq_dealer_send(HANDLE_T hand, const char* reply);
int sics_zmq_dealer_size(pDEALER_MESSAGE msg);
const void *sics_zmq_dealer_data(pDEALER_MESSAGE msg);
void sics_zmq_dealer_free(pDEALER_MESSAGE msg);

HANDLE_T sics_zmq_publish(const char *address);
void sics_zmq_unpublish(HANDLE_T *hand);

int sics_zmq_pubsend(HANDLE_T hand,
                     const char *status_type,
                     const char *status_name,
                     const char *status_valu);

HANDLE_T sics_zmq_router(const char *address);
void sics_zmq_unrouter(HANDLE_T *hand);

void *sics_zmq_router_zsock(HANDLE_T hand);
typedef struct router_message *pROUTER_MESSAGE;
pROUTER_MESSAGE sics_zmq_router_recv(HANDLE_T hand, int max_parts);
int sics_zmq_router_reply(HANDLE_T hand, pROUTER_MESSAGE msg, const char* reply);
int sics_zmq_router_size(pROUTER_MESSAGE msg);
const void *sics_zmq_router_data(pROUTER_MESSAGE msg);
void sics_zmq_router_free(pROUTER_MESSAGE msg);

HANDLE_T sics_zmq_subscribe(const char *address);
void sics_zmq_unsubscribe(HANDLE_T *hand);

void *sics_zmq_subscribe_zsock(HANDLE_T hand);
typedef struct subscribe_message *pSUBSCRIBE_MESSAGE;
pSUBSCRIBE_MESSAGE sics_zmq_subscribe_recv(HANDLE_T hand, int max_parts);
int sics_zmq_subscribe_size(pSUBSCRIBE_MESSAGE msg);
const void *sics_zmq_subscribe_data(pSUBSCRIBE_MESSAGE msg);
void sics_zmq_subscribe_free(pSUBSCRIBE_MESSAGE msg);

void sics_zmq_terminate(void);
#endif /* SICSZMQ_H */
