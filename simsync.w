\subsection{Synchronizing with other SICS server}
A common demand is to have a simulation server for testing command files or
checking complex instrument movements. For such uses the simulation SICS 
server must be synchrnized at times with the actual SICS server running the
 instrument. This is the purpose of this module. It works by sending a 
 backup command to to the actual SICS server and then  runs the restore 
 command. In order for this scheme to work properly, both the simulation
 and the real SICS server must be configured to read the same status file.
 The installation of this module also sets a flag in the SICS servers 
 general data structure which can be used by other system components in order
 to modify behaviour in simulation mode. For instance the writing of data
 files can be inhibited.

 As the simulation server must be able to run independently from the actual
 SICS server, it was considered sufficient  to update the state of the
 simulation server manually. As this may break other simulations currently
 running a broadcast message about the action is sent to all connected
 clients.

 The interface to this module is just the interpreter function doing
 the sync and the object creation function.

@d syncint @{
	int MakeSync(SConnection *pCon, SicsInterp *pSics, void *pData,
                    int argc, char *argv[]);
	int Synchronize(SConnection *pCon, SicsInterp *pSics, void *pData,
                    int argc, char *argv[]);

@}  

@o synchronize.h @{
/*
                 S y n c h r o n i z e

 Synchronize parameters in this server with parameters from another
 SICS server.

 Mark Koennecke, March 2001
*/
#ifndef SICSSYNC
#define SICSSYNC
@<syncint@>
#endif
@}

