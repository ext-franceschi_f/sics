/**
 * This is an implementation  of the polymorphic single crystal calculation
 * system defined in singlediff.h for a bisecting four circle diffractometer with
 * an eulerian cradle.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, August 2008
 */
#ifndef SINGELBI_H_
#define SINGELBI_H_
#include "singlediff.h"

void initializeSingleBisecting(pSingleDiff diff);
void initializeSingleBisectingOrion(pSingleDiff diff);
#endif                          /*SINGELBI_H_ */
