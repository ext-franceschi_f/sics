/**
 * This header file defines the polymorphic functions required for 
 * a single crystal diffractmeter, regardless of the geometry used. 
 * This exists in order to support the multiple modes of the TRICS
 * instrument. But can be useful elsewhere. This code assumes that
 * all motor driving happens via a motorlist as defined in 
 * motorlist.*
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, August 2008
 */
#ifndef SINGLEDIFF_H_
#define SINGLEDIFF_H_
#include "matrix/matrix.h"

#define REFERR -17001

/**
 * A polymorphic structure for single crystal calculations.
 */
typedef struct __SingleDiff {
  int motList;       /** A list of associated real motors */
  void *userData;       /** a pointer to a user data structure */
  MATRIX UB;       /** The UB matrix */
  double lambda;       /** The wavelength to use */
  double cell[6];       /** The unit cell */
        /**
	 * \brief calculates the settings for an input reciprocal space 
	 * position hkl
	 * \param self A pointer to this data structure
	 * \param hkl The input reciprocal space position
	 * \param settings The angles calculated.
	 * \return 1 on success, 0 on error
	 */
  int (*calculateSettings) (struct __SingleDiff * self,
                            double *hkl, double *settings);
        /**
	 * \brief copy setting angles into motor list
	 * \param self A pointer to this data structure
	 * \param settings The angles to prime the motor list with
	 * \return 1 on success, 0 on error
	 */
  int (*settingsToList) (struct __SingleDiff * self, double *settings);
        /**
	 * \brief read all angles and convert to reciprocal space
	 * \param self A pointer to this data structure
	 * \param Result hkl values 
	 * \return 1 on success, 0 on error
	 */
  int (*hklFromAngles) (struct __SingleDiff * self, double *hkl);
        /**
	 * \brief Take angles given and convert to reciprocal space
	 * \param self A pointer to this data structure
	 * \param Result hkl values 
	 * \return 1 on success, 0 on error
	 */
  int (*hklFromAnglesGiven) (struct __SingleDiff * self, double *settings,
                             double *hkl);
        /**
	 * \brief calculate a UB matrix from two reflections (and the cell)
	 * \param self A pointer to this data structure
	 * \param refid1 first reflection ID
	 * \param refid2 second reflection ID
	 * \return NULL on error, a matrix else
	 */
   MATRIX(*calcUBFromTwo) (struct __SingleDiff * self,
                           char *refid1, char *refid2, int *err);
        /**
	 * \brief calculate a UB matrix from three reflections 
	 * \param self A pointer to this data structure
	 * \param refid1 first reflection ID
	 * \param refid2 second reflection ID
	 * \param refid3 third reflection ID
	 * \return NULL on error, a matrix else
	 */
   MATRIX(*calcUBFromThree) (struct __SingleDiff * self,
                             char *refid1, char *refid2, char *refid3,
                             int *err);
        /**
	 * \brief calculate the scattering vector from the angles.
	 * \param refid The id of the reflection to calculate the
	 *  scattering vector for
	 * \param z1 output scattering vector
	 * \return 1 on success, o on fail
	 */
  int (*calcZ1) (struct __SingleDiff * self, char *refid, double z1[3]);
} SingleDiff, *pSingleDiff;

MATRIX calculateScatteringVector(pSingleDiff pSingle, double hkl[3]);
#endif                          /*SINGLEDIFF_H_ */
