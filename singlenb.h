/**
 * This is an implementation  of the polymorphic single crystal calculation
 * system defined in singlediff.h for a diifractometer in normal beam geometry. 
 * This means the detector tilts out of the instrument plane upwards or 
 * downwards.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, August 2008
 */
#ifndef SINGLENB_H_
#define SINGLENB_H_

void initializeNormalBeam(pSingleDiff diff);

#endif                          /*SINGLENB_H_ */
