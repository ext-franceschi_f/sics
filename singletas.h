/**
 * This is an implementation of the polymorphic single crystal calculation module 
 * for the triple axis mode. In this mode the tilt angles of a standard sample 
 * cradle are used to orient the sample. The math behind this has been decribed by 
 * Mark Lumsden in his paper.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, November 2008
 */
#ifndef SINGLETAS_H_
#define SINGLETAS_H_
#include "singlediff.h"

void initializeSingleTas(pSingleDiff diff);


#endif                          /*SINGLETAS_H_ */
