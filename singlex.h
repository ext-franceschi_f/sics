/**
 * This is the header file for the single crystal diffraction module within SICS. 
 * This is a master module which initializes all the other ones and holds 
 * data structures and entities particular to single crystal diffraction. 
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, July 2008
 */
#ifndef SINGLEX_H_
#define SINGLEX_H_
#include "sics.h"
#include "motor.h"
#include "reflist.h"
#include "sginfo.h"
#include "singlediff.h"

typedef enum {
  TwoTheta, Omega, Chi, Phi, Nu, Sgu, Sgl, None
} MotorFunction;
pMotor SXGetMotor(MotorFunction m);
const char *SXGetMotorName(MotorFunction m);

const double *SXGetUB();
void SXSetUB(double ub[9]);

double *SXGetPlanenormal();
void SXSetPlanenormal(double planeNormal[3]);

const double *SXGetCell();
void SXSetCell(double c[6]);

double SXGetLambda();

pSICSOBJ SXGetReflectionList();

typedef enum {
  Bisecting, NB, Tas, BiNB, BiO, 
} SingleXModes;

SingleXModes SXGetMode();
void SXSetMode(SingleXModes m);

pSingleDiff SXGetDiffractometer();

T_SgInfo *SXGetSpaceGroup();


int MakeSingleX(SConnection * pCon, SicsInterp * pSics,
                void *data, int argc, char *argv[]);

#endif                          /*SINGLEX_H_ */
