\subsubsection{Site Abstraction Layer}
With ANSTO using SICS as well it became necessary to separate the
general parts  of SICS from the installation specific components. Each
installation will have a separate set of drivers and, to some
extent,  instrument specific commands. Such code has to be in a
separate library. Access to this library is through an interface which
consists of a structure containing pointers to functions which allow
for the creation of site specific drivers and commands. Moreover, the
site specific library has to implement a function,  getSite, which
returns the appropriate data structure for the site for which SICS is
being compiled. This data structure looks like this:

@d sitedata @{
	typedef struct {
		void (*AddSiteCommands)(SicsInterp *pSics);
		void (*RemoveSiteCommands)(SicsInterp *pSics);
	        pMotor (*CreateMotor)(SConnection *pCon,
					    int argc, char *argv[]);
		pCounterDriver (*CreateCounterDriver)(
						SConnection *pCon,
						int argc, 
						char *argv[]);
                HistDriver *(*CreateHistogramMemoryDriver)(
				char *name, pStringDict pOption);
		pVelSelDriv (*CreateVelocitySelector)(char *name, 
				char *array, Tcl_Interp *pTcl);
                pCodri      (*CreateControllerDriver)(SConnection *pCon,
							int argc,
							char *argv[]);
		pEVControl            (*InstallEnvironmentController)(
					SicsInterp *pSics,
					SConnection *pCon,
					int argc,
					char *argv[]);
	        int                     (*ConfigureScan)(pScanData self,
						char *option);
                void (*KillSite)(void *pData);
}Site, *pSite;
@}
\begin{description}
\item[AddSiteCommands] adds site specific object creation and
instrument specific commands to the SICS interpreter, pSics.
\item[RemoveSiteCommands] will be called to remove surplus object
creation commands after the SICS interpreter has processed the
initialization files. Please note, that SICS does not support the
removal of objects at runtime in general. This is due to the fact that
any possible object may be used by or linked to others and and it
would be a bookeeping nightmare to keep track of all those relations.
\item[CreateMotor] creates a motor using the arguments in argc and
argv. It returns a pointer to the new motor structure on success or
NULL in case of a failure. This function has to return a complete
motor in order to allow for special configurations of the motor to
take place in its initialization.
\item[CreateCounterDriver] returns a driver for a new counter box
driver if the parameters are valid or NULL if not. Driver arguments
are in the argc, argv pair.  
\item[CreateHistogramMemoryDriver] creates a driver for a histogram
memory. The driver type is specified through name. 
Driver options are in pOptions. 
\item[CreateVelocitySelector] create a driver for a velocity selector.
The parameter name is the name of the driver, array is the name of a
Tcl array holding configuration parameters for the driver and pTcl is
the Tcl interpreter in which array lives. 
\item[CreateControllerDriver] creates a driver for the general
controller module within SICS. argc and argv hold the parameters,
starting with the name of the driver to create.  
\item[InstallEnvironmentController] installs a a sample
environment device such as a temperature controller or magnet
controller etc. into the interpreter pSics. pCon is a connection
object to  which errors can be
reported, argc and argv are the controller parameters starting with
the driver name. This method does not get away with creating a driver
but must install the command into SICS because  some environment
devices overload the standard Wrapper function with special ones. The
newly created object is still returned for further processing. In the
case of failure NULL is returned. Errors will have been printed to
pCon. 
\item[ConfigureScan] allows for modules which configure the scan
object. option is the option to xxscan configure to process, the scan
object to configure is passed in in self. This returns 1 on success
and 0 on failures or options which are not recognized. 
\item[KillSite] is a function to remove the site data structure when
SICS is done with it. pData must point to the site data structure.
KillSite's  purpose is to free all memory associated with
the site data structure. This is mostly a cleanup thing, to keep the
fortify logs clear off inconsequential and confusing data.
\end{description} 


@o site.h @{
/*-----------------------------------------------------------------------
	S i t e  A b s t r a c t  i o n  L a y e r

With ANSTO using SICS as well it became necessary to separate the
general parts  of SICS from the installation specific components. Each
installation will have a separate set of drivers and, to some
extent,  instrument specific commands. Such code has to be in a
separate library. Access to this library is through an interface which
consists of a structure containing pointers to functions which allow
for the creation of site specific drivers and commands. Moreover, the
site specific library has to implement a function,  getSite, which
returns the appropriate data structure for the site for which SICS is
being compiled. 
------------------------------------------------------------------------*/
#ifndef SICSSITE
#define SICSSITE
#include <sics.h>
#include <motor.h>
#include <countdriv.h>
#include <HistDriv.i>
#include <stringdict.h>
#include <velo.h>
#include <tcl.h>
#include <codri.h>
#include <evcontroller.h>
#include <scan.h>
@<sitedata@>
/*-------------------------------------------------------------------*/
pSite getSite(void);
#endif
@}



