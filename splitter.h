/*-------------------------------------------------------------------------

	Splitter. A module to break a line of text input into single
	words. Thereby determining type of input and convert if 
	necessary. The result will be stored in a doubly linked
	list.
	
	Mark Koennecke October 1996
--------------------------------------------------------------------------*/

#ifndef MKSPLITTER
#define MKSPLITTER

#include <stdio.h>

typedef enum { eText, eInt, eFloat, eUndefined } eType;

typedef struct _TokenEntry {
  eType Type;
  char *text;
  long iVal;
  float fVal;
  struct _TokenEntry *pNext;
  struct _TokenEntry *pPrevious;
} TokenList;

/* --------------------- exported functions ------------------------------*/

TokenList *SplitText(char *pLine);
        /*!
           Splits a line of text into a list of tokens as defined above.
           Any item in the line which is separated from others by spaces
           is considered a token. The type of the token will be determined
           as well. Possible types recognized are: int, float and text.
           ! */

TokenList *SplitArguments(int argc, char *argv[]);
        /*!
           Very much like SplitText, but uses an argc, argv[]  pair as input.
           ! */

void DeleteTokenList(TokenList * pList);
        /*!
           Deletes a TokenList as returned by SplitText or 
           SplitArguments. Nver forget to call this once you are done
           with the token list.
           ! */

int Text2Arg(char *pLine, int *argc, char **argv[]);
        /*!
           Text2Arg converts a line of text given in pLine to an 
           argc, argv[] pair. Note that the caller is responsible
           for freeing the memory occupied by argv[]. This implies,
           that Text2Arg allocates the necessary space for the argv[]
           array.
           ! */

int Arg2Text(int argc, char *argv[], char *buf, int iBuflen);
        /*!
           Arg2Text converts an argc, argv[] pair into a line of
           text. The result will be stored in buf, allocated by the 
           caller. The parameter iBufLen specifies the length of buf,
           in order to prevent memory overwrite. Returns 1 on success,
           0 on failure.
           ! */

int GetShellLine(FILE * fd, char *Buffer, int iBufLen);
        /*!
           GetShellLine reads the next line from the file fd, which does
           not start with the comment character "#". Returns 1 for success,
           or EOF when the end of file has been reached.
           ! */
int isNumeric(char *pText);
         /*!
            isNumeric test if pText is a number
            ! */
char *Arg2Tcl(int argc, char *argv[], char *buffer, int buffersize);
        /*!
           Arg2Tcl converts an argc, argv[] pair into a line of
           text. Args are quoted if needed, in order to be interpreted as
           proper tcl command. If buffer is NULL or the result longer than
           buffersize, the result is allocated by Arg2Tcl.
           If the results fits the buffer, buffer is returned.
           If no memory is available or any element of argv is NULL, NULL is
           returned.
           The result has to be freed by the caller after use be something like:

           if (result != NULL && result != buffer) free(result);

           ! */
char *Arg2Tcl0(int argc, char *argv[], char *buffer, int buffersize,
               char *prepend);
        /*!
           This function is added for convenience, and acts similar to Arg2Tcl.
           If prepend is not NULL, its contents appear untreated before the args.
           A space is used as separator.
           ! */
char *sicsNextNumber(char *pStart, char pNumber[80]);
         /*!
            This function reads the next number from the string in pStart.
            The number is put into pNumber, a pointer to the string after 
            the number is returned or NULL whne the string is exhausted.
            ! */
#endif
