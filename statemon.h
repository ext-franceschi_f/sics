/**
 * This is a state monitor. It collects all the start and stop messages 
 * from the device executor and from scan and batch commands. Clients can 
 * listen to this in order to figure out what is actually going on in a
 * given SICS installation. This might in the end supersede the status code 
 * managment in status.c
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, January 2007
 */
#ifndef STATEMON_H_
#define STATEMON_H_
typedef struct __STATEMON *pStateMon;

/*===================== The interpreter interface ===========================*/
int StateMonFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
                    int argc, char *argv[]);
int StateMonAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[]);
#endif                          /*STATEMON_H_ */
