/* statistics.h

   statistics on the time spend for commands/tasks

*/

#ifndef STATISTICS_H
#define STATISTICS_H

typedef struct Statistics Statistics;

Statistics *StatisticsNew(char *name);
/* create a new Statistics item */

void StatisticsKill(Statistics * s);
/* kill item */

Statistics *StatisticsBegin(Statistics * s);
void StatisticsEnd(Statistics * s);
/*
  Switch statistics to item s.

  Typical usage:

  old = StatisticsBegin(thisItem);

  ...do work related with thisItem...

  StatisticsEnd(old);
*/

void StatisticsInit(void);

/* initialize module and activate statistics command */

#endif
