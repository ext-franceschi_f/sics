/*--------------------------------------------------------------------------

	The Status object. Holds the current status of the SICS server.
	
	Mark Koennecke, November 1996
        copyright: see implementation file
----------------------------------------------------------------------------*/
#ifndef SICSSTATUS
#define SICSSTATUS

typedef enum {
  eEager,
  eUserWait,
  eCounting,
  eOutOfBeam,
  ePaused,
  eDriving,
  eRunning,
  eScanning,
  eWriting,
  eBatch,
  eHalted,
  eDead,
  eInput,
  eCountDrive,
  eWorking
} Status;

   /* for status display */
void KillStatus(void *pData);

Status GetStatus(void);
void GetStatusText(char *buf, int iBufLen);

void StatusIncr(int mode);
void StatusDecr(int mode);

int UserStatus(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[]);
int ResetStatus(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);
void InitStatus(void);

#endif
