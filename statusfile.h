/*--------------------------------------------------------------------------
	Status files
	
	Mark Koennecke, November 1996
        copyright: see implementation file
----------------------------------------------------------------------------*/
#ifndef STATUSFILE_H
#define STATUSFILE_H

#include "sics.h"

int InstallBckRestore(SConnection * pCon, SicsInterp * pSics);
int BackupStatus(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[]);

int RestoreStatus(SConnection * pCon, SicsInterp * pSics, void *pData,
                  int argc, char *argv[]);
int hasRestored();

int StatusFileTask(void *data); /* saves status file if parameters have changed */

void StatusFileDirty(void);     /* indicate that the status file has to be rewritten */
#endif
