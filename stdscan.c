/*--------------------------------------------------------------------------
                   S T A N D A R D S C A N
                    
  This is a library of scan functions for the SICS standard scan.           

  copyright: see copyright.h

  Extracted from scan.c: Mark Koennecke, November 2004 
----------------------------------------------------------------------------*/
#include "sics.h"
#include <stdio.h>
#include <arpa/inet.h>
#include <tcl.h>
#include <math.h>
#include "fortify.h"
#include "sdynar.h"
#include "dynstring.h"
#include "stringdict.h"
#include "status.h"
#include "sicsvar.h"
#include "counter.h"
#include "scan.h"
#include "scan.i"
#include "splitter.h"
#include "danu.h"
#include "userscan.h"
#include "motor.h"
#include "nxscript.h"
#include "nxutil.h"
#include "site.h"
#include "lld.h"

/*-----------------------------------------------------------------------*/
static char *fixExtension(char *filename)
{
  if (strstr(filename, ".hdf") != NULL) {
    changeExtension(filename, "dat");
  }
  return filename;
}

/*------------------------------------------------------------------------*/
char *ScanMakeFileName(SicsInterp * pSics, SConnection * pCon)
{
  pSicsVariable pPath = NULL, pPref = NULL, pEnd = NULL;
  char *pRes = NULL;
  int iLen, iNum, iYear;
  char pNumText[10];
  CommandList *pCom = NULL;
  char simName[255];

  /*
     make a simulated filename if in simulation mode
   */
  if (pServ->simMode) {
    snprintf(simName, 255, "%s/tmp/sim001001901.sim", getenv("HOME"));
    return strdup(simName);
  }

  pRes = makeFilename(pSics, pCon);
  if (pRes == NULL) {
    pRes = strdup("emergency.scn");
  }
  return fixExtension(pRes);
}

/*-------------------------------------------------------------------------*/
void WriteTemplate(FILE * fd, FILE * temp, char *filename, pScanData pScan,
                   SConnection * pCon, SicsInterp * pSics)
{
  char pBuffer[512], pError[512], *pPtr, *pName;
  CommandList *pCom = NULL;
  pSicsVariable pVar = NULL;
  pDummy pDum = NULL;
  pIDrivable pDriv = NULL;
  pMotor pMot = NULL;
  pVarEntry pScanVar = NULL;
  void *pVoid = NULL;
  float fVal;
  int iRet;

  /* loop through description file and act along the way */
  while (fgets(pBuffer, 511, temp) != NULL) {
    pPtr = strstr(pBuffer, "!!VAR(");
    if (pPtr) {                 /* handle a Sics variable */
      /* extract the name */
      pName = pPtr + 6;         /* first char of name */
      *pPtr = '\0';             /* this is the starter of the line */
      pPtr = pName;
      while ((*pPtr != '\0') && (*pPtr != ')')) {
        pPtr++;
      }
      *pPtr = '\0';
      /* find the variable */
      pCom = FindCommand(pSics, pName);
      if (!pCom) {
        snprintf(pError,sizeof(pError)-1, "ERROR: variable %s NOT found", pName);
        SCWrite(pCon, pError, eLogError);
        continue;
      }
      pVar = (pSicsVariable) pCom->pData;
      if (!pVar) {
        snprintf(pError,sizeof(pError)-1, "ERROR: variable %s NOT found", pName);
        SCWrite(pCon, pError, eLogError);
        continue;
      }
      switch (pVar->eType) {
      case veFloat:
        snprintf(pError,sizeof(pError)-1, "%f", pVar->fVal);
        break;
      case veInt:
        snprintf(pError,sizeof(pError)-1, "%d", pVar->iVal);
        break;
      case veText:
        snprintf(pError,sizeof(pError)-1, "%s", pVar->text);
        break;
      }
      /* finally write  */
      fprintf(fd, "%s %s\n", pBuffer, pError);
      continue;
    }
    /* end variable */
 /*------- Drivable */
    pPtr = strstr(pBuffer, "!!DRIV(");
    if (pPtr) {
      /* extract the name */
      pName = pPtr + 7;         /* first char of name */
      *pPtr = '\0';             /* this is the starter of the line */
      pPtr = pName;
      while ((*pPtr != '\0') && (*pPtr != ')')) {
        pPtr++;
      }
      *pPtr = '\0';
      /* find the variable */
      pCom = FindCommand(pSics, pName);
      if (!pCom) {
        snprintf(pError,sizeof(pError)-1, "ERROR: variable %s NOT found", pName);
        SCWrite(pCon, pError, eLogError);
        continue;
      }
      pDum = (pDummy) pCom->pData;
      if (!pDum) {
        snprintf(pError,sizeof(pError)-1, "ERROR: variable %s is NOT drivable", pName);
        SCWrite(pCon, pError, eLogError);
        continue;
      }
      pDriv = (pIDrivable) pDum->pDescriptor->GetInterface(pDum, DRIVEID);
      if (!pDriv) {
        snprintf(pError,sizeof(pError)-1, "ERROR: variable %s is NOT drivable", pName);
        SCWrite(pCon, pError, eLogError);
        continue;
      }
      fVal = pDriv->GetValue(pDum, pCon);
      fprintf(fd, "%s %f\n", pBuffer, fVal);
      continue;
    }
    /* end drive */
 /*------- zero point */
    pPtr = strstr(pBuffer, "!!ZERO(");
    if (pPtr) {
      /* extract the name */
      pName = pPtr + 7;         /* first char of name */
      *pPtr = '\0';             /* this is the starter of the line */
      pPtr = pName;
      while ((*pPtr != '\0') && (*pPtr != ')')) {
        pPtr++;
      }
      *pPtr = '\0';
      /* find the motor */
      pMot = FindMotor(pSics, pName);
      if (!pMot) {
        snprintf(pError,sizeof(pError)-1, "ERROR: motor %s NOT found", pName);
        SCWrite(pCon, pError, eLogError);
        continue;
      }
      iRet = MotorGetPar(pMot, "softzero", &fVal);
      if (!iRet) {
        SCWrite(pCon, "ERROR: failed to read zero point", eLogError);
        continue;
      }
      fprintf(fd, "%s %f\n", pBuffer, fVal);
      continue;
    }
    /* end zero point */
 /*---------- scripted info */
    pPtr = strstr(pBuffer, "!!SCRIPT(");
    if (pPtr) {
      /* extract the name */
      pName = pPtr + 9;         /* first char of name */
      *pPtr = '\0';             /* this is the starter of the line */
      pPtr = pName;
      while ((*pPtr != '\0') && (*pPtr != ')')) {
        pPtr++;
      }
      *pPtr = '\0';
      if (Tcl_Eval(InterpGetTcl(pSics), pName) == TCL_OK) {
        fprintf(fd, "%s\n", Tcl_GetStringResult(InterpGetTcl(pSics)));
      } else {
        SCWrite(pCon, "ERROR: failed to execute Tcl command", eError);
        strlcpy(pBuffer, Tcl_GetStringResult(InterpGetTcl(pSics)), 511);
        SCWrite(pCon, pBuffer, eLogError);
        continue;
      }
    }
/* ------- date */
    pPtr = strstr(pBuffer, "!!DATE!!");
    if (pPtr) {
      *pPtr = '\0';
      SNXFormatTime(pError, 511);
      fprintf(fd, "%s %s\n", pBuffer, pError);
      continue;
    }
/*-------- filename */
    pPtr = strstr(pBuffer, "!!FILE!!");
    if (pPtr) {
      *pPtr = '\0';
      fprintf(fd, "%s %s\n", pBuffer, filename);
      continue;
    }
/*------------ scanzero */
    pPtr = strstr(pBuffer, "!!SCANZERO!!");
    if (pPtr && pScan != NULL) {
      *pPtr = '\0';
      /* write zero point of first scan variable if motor */
      DynarGet(pScan->pScanVar, 0, &pVoid);
      pScanVar = (pVarEntry) pVoid;
      if (pScanVar) {
        pMot = NULL;
        pMot = FindMotor(pSics, ScanVarName(pScanVar));
        if (pMot != NULL) {
          MotorGetPar(pMot, "softzero", &fVal);
          fprintf(fd, "%s zero = %8.3f\n", ScanVarName(pScanVar), fVal);
        }
      }
    }
/* --------- plain text */
    fprintf(fd, "%s", pBuffer);
  }                             /* end while */
}

/*---------------------------------------------------------------------------*/
int WriteHeader(pScanData self)
{
  int i, iRet;
  FILE *fd;

  assert(self->pSics);
  assert(self->pCon);

  /* open data file */
  self->fd = fopen(self->pFile, "w");
  if (!self->fd) {
    SCWrite(self->pCon, "ERROR: cannot write data file", eLogError);
    return 0;
  }

  /* open header description file */
  fd = fopen(self->pHeaderFile, "r");
  if (!fd) {
    SCWrite(self->pCon, "ERROR: cannot open header description file",
            eLogError);
    return 0;
  }

  WriteTemplate(self->fd, fd, self->pFile, self, self->pCon, self->pSics);

  /* remember position for seeking to it for writing data */
  self->lPos = ftell(self->fd);

  fclose(fd);
  fclose(self->fd);
  self->fd = NULL;
  return 1;
}

/*---------------------------------------------------------------------------*/
int WriteHeaderOld(pScanData self)
{
  int i, iRet;
  FILE *fd;
  char pBuffer[512], pError[512], *pPtr, *pName;
  CommandList *pCom = NULL;
  pSicsVariable pVar = NULL;
  pDummy pDum = NULL;
  pIDrivable pDriv = NULL;
  float fVal;
  pMotor pMot = NULL;
  pVarEntry pScanVar = NULL;
  void *pVoid = NULL;

  assert(self->pSics);
  assert(self->pCon);

  /* open data file */
  self->fd = fopen(self->pFile, "w");
  if (!self->fd) {
    SCWrite(self->pCon, "ERROR: cannot write data file", eError);
    return 0;
  }

  /* open header description file */
  fd = fopen(self->pHeaderFile, "r");
  if (!fd) {
    SCWrite(self->pCon, "ERROR: cannot open header description file",
            eError);
    return 0;
  }

  /* loop through description file and act along the way */
  while (fgets(pBuffer, 511, fd) != NULL) {
    pPtr = strstr(pBuffer, "!!VAR(");
    if (pPtr) {                 /* handle a Sics variable */
      /* extract the name */
      pName = pPtr + 6;         /* first char of name */
      *pPtr = '\0';             /* this is the starter of the line */
      pPtr = pName;
      while ((*pPtr != '\0') && (*pPtr != ')')) {
        pPtr++;
      }
      *pPtr = '\0';
      /* find the variable */
      pCom = FindCommand(self->pSics, pName);
      if (!pCom) {
        snprintf(pError,sizeof(pError)-1, "ERROR: variable %s NOT found", pName);
        SCWrite(self->pCon, pError, eError);
        continue;
      }
      pVar = (pSicsVariable) pCom->pData;
      if (!pVar) {
        snprintf(pError,sizeof(pError)-1, "ERROR: variable %s NOT found", pName);
        SCWrite(self->pCon, pError, eError);
        continue;
      }
      switch (pVar->eType) {
      case veFloat:
        snprintf(pError,sizeof(pError)-1, "%f", pVar->fVal);
        break;
      case veInt:
        snprintf(pError,sizeof(pError)-1, "%d", pVar->iVal);
        break;
      case veText:
        snprintf(pError,sizeof(pError)-1, "%s", pVar->text);
        break;
      }
      /* finally write  */
      fprintf(self->fd, "%s %s\n", pBuffer, pError);
      continue;
    }
    /* end variable */
 /*------- Drivable */
    pPtr = strstr(pBuffer, "!!DRIV(");
    if (pPtr) {
      /* extract the name */
      pName = pPtr + 7;         /* first char of name */
      *pPtr = '\0';             /* this is the starter of the line */
      pPtr = pName;
      while ((*pPtr != '\0') && (*pPtr != ')')) {
        pPtr++;
      }
      *pPtr = '\0';
      /* find the variable */
      pCom = FindCommand(self->pSics, pName);
      if (!pCom) {
        snprintf(pError,sizeof(pError)-1, "ERROR: variable %s NOT found", pName);
        SCWrite(self->pCon, pError, eError);
        continue;
      }
      pDum = (pDummy) pCom->pData;
      if (!pDum) {
        snprintf(pError,sizeof(pError)-1, "ERROR: variable %s is NOT drivable", pName);
        SCWrite(self->pCon, pError, eError);
        continue;
      }
      pDriv = (pIDrivable) pDum->pDescriptor->GetInterface(pDum, DRIVEID);
      if (!pDriv) {
        snprintf(pError,sizeof(pError)-1, "ERROR: variable %s is NOT drivable", pName);
        SCWrite(self->pCon, pError, eError);
        continue;
      }
      fVal = pDriv->GetValue(pDum, self->pCon);
      fprintf(self->fd, "%s %f\n", pBuffer, fVal);
      continue;
    }
    /* end drive */
 /*------- zero point */
    pPtr = strstr(pBuffer, "!!ZERO(");
    if (pPtr) {
      /* extract the name */
      pName = pPtr + 7;         /* first char of name */
      *pPtr = '\0';             /* this is the starter of the line */
      pPtr = pName;
      while ((*pPtr != '\0') && (*pPtr != ')')) {
        pPtr++;
      }
      *pPtr = '\0';
      /* find the motor */
      pMot = FindMotor(self->pSics, pName);
      if (!pMot) {
        snprintf(pError,sizeof(pError)-1, "ERROR: motor %s NOT found", pName);
        SCWrite(self->pCon, pError, eError);
        continue;
      }
      iRet = MotorGetPar(pMot, "softzero", &fVal);
      if (!iRet) {
        SCWrite(self->pCon, "ERROR: failed to read zero point", eError);
        continue;
      }
      fprintf(self->fd, "%s %f\n", pBuffer, fVal);
      continue;
    }
    /* end zero point */
    /* ------- date */
    pPtr = strstr(pBuffer, "!!DATE!!");
    if (pPtr) {
      *pPtr = '\0';
      SNXFormatTime(pError, 511);
      fprintf(self->fd, "%s %s\n", pBuffer, pError);
      continue;
    }
/*-------- filename */
    pPtr = strstr(pBuffer, "!!FILE!!");
    if (pPtr) {
      *pPtr = '\0';
      fprintf(self->fd, "%s %s\n", pBuffer, self->pFile);
      continue;
    }
/*------------ scanzero */
    pPtr = strstr(pBuffer, "!!SCANZERO!!");
    if (pPtr) {
      *pPtr = '\0';
      /* write zero point of first scan variable if motor */
      DynarGet(self->pScanVar, 0, &pVoid);
      pScanVar = (pVarEntry) pVoid;
      if (pScanVar) {
        pMot = NULL;
        pMot = FindMotor(self->pSics, ScanVarName(pScanVar));
        if (pMot != NULL) {
          MotorGetPar(pMot, "softzero", &fVal);
          fprintf(self->fd, "%s zero = %8.3f\n", ScanVarName(pScanVar),
                  fVal);
        }
      }
    }
/* --------- plain text */
    fprintf(self->fd, "%s", pBuffer);
  }                             /* end while */


  /* remember position for seeking to it for writing data */
  self->lPos = ftell(self->fd);

  fclose(fd);
  fclose(self->fd);
  self->fd = NULL;
  return 1;
}

/*--------------------------------------------------------------------------*/
int WriteScanPoints(pScanData self, int iPoint)
{
  int i, i2;
  char pLine[512], pItem[30], pInfo[1024], pSteps[256];
  pVarEntry pVar = NULL;
  pCountEntry pData = NULL;
  void *pPtr = NULL;

  assert(self->pCon);
  assert(self->pSics);

  /* reopen file */
  self->fd = fopen(self->pFile, "r+");
  if (!self->fd) {
    SCWrite(self->pCon,
            "ERROR: Failed to reopen scan file, aborting scan", eLogError);
    return 0;
  }


  /* jump to end of header */
  fseek(self->fd, self->lPos, SEEK_SET);
  if (self->iChannel != 0) {
    fprintf(self->fd, "WARNING: Scanning monitor %d\n", self->iChannel);
  }

  /* make the data header */
  snprintf(pLine,sizeof(pLine)-1, "%-4s ", "NP");
  strcpy(pInfo, "Scanning Variables: ");
  strcpy(pSteps, "Steps: ");
  for (i = 0; i < self->iScanVar; i++) {
    DynarGet(self->pScanVar, i, &pPtr);
    pVar = (pVarEntry) pPtr;
    if (pVar) {
      snprintf(pItem,sizeof(pItem)-1, "%-9.9s ", ScanVarName(pVar));
      strlcat(pLine, pItem,511);
      snprintf(pItem,30, "%s, ", ScanVarName(pVar));
      strlcat(pInfo, pItem,1024);
      snprintf(pItem,30, "%f ", ScanVarStep(pVar));
      strlcat(pSteps, pItem,255);
    }
  }
  strlcat(pLine, " Counts      ",sizeof(pLine));
  strlcat(pLine, "Monitor1    ",sizeof(pLine));
  strlcat(pLine, "Monitor2    ",sizeof(pLine));
  strlcat(pLine, "Monitor3    ",sizeof(pLine));
  strlcat(pLine, "Time  ",sizeof(pLine));
  strlcat(pInfo, pSteps,sizeof(pLine));
  snprintf(pItem,sizeof(pItem)-1, "\n%d Points,", self->iNP);
  strlcat(pInfo, pItem,sizeof(pLine));
  if (self->iMode == eTimer) {
    strlcat(pInfo, " Mode: Timer,",sizeof(pLine));
  } else {
    strlcat(pInfo, " Mode: Monitor,",sizeof(pLine));
  }
  snprintf(pItem,sizeof(pItem)-1, " Preset %f", self->fPreset);
  strlcat(pInfo, pItem,sizeof(pLine));
  fprintf(self->fd, "%s\n", pInfo);
  fprintf(self->fd, "%s\n", pLine);

  /* now the scan points */
  for (i = 0; i < self->iCounts; i++) {
    snprintf(pLine,sizeof(pLine)-1, "%-4d ", i);
    /* print vars */
    for (i2 = 0; i2 < self->iScanVar; i2++) {
      DynarGet(self->pScanVar, i2, &pPtr);
      pVar = (pVarEntry) pPtr;
      if (pVar) {
        snprintf(pItem,sizeof(pItem)-1, "%-9.4f ", GetScanVarPos(pVar, i));
        strlcat(pLine, pItem,sizeof(pLine));
      }
    }
    /* print Counts & Monitor */
    DynarGet(self->pCounts, i, &pPtr);
    pData = (pCountEntry) pPtr;
    if (pData) {
      snprintf(pItem,sizeof(pItem)-1, " %-11ld ", pData->lCount);
      strlcat(pLine, pItem,sizeof(pLine));
      snprintf(pItem,sizeof(pItem)-1, "%-11ld ", pData->Monitors[0]);
      strlcat(pLine, pItem,sizeof(pLine));
      snprintf(pItem,sizeof(pItem)-1, "%-11ld ", pData->Monitors[1]);
      strlcat(pLine, pItem,sizeof(pLine));
      snprintf(pItem,sizeof(pItem)-1, "%-11ld ", pData->Monitors[2]);
      strlcat(pLine, pItem,sizeof(pLine));
      snprintf(pItem,sizeof(pItem)-1, "%-8.3f ", pData->fTime);
      strlcat(pLine, pItem,sizeof(pLine));
    }
    fprintf(self->fd, "%s\n", pLine);
  }

  /* done */
  fprintf(self->fd, "END-OF-DATA\n");
  fclose(self->fd);
  self->fd = NULL;
  return 1;
}

/*------------------------------------------------------------------------*/
int prepareDataFile(pScanData self)
{
  char *pPtr = NULL;
  char pBueffel[512];
  Tcl_Interp *pTcl;
  const char *val = NULL;

  pTcl = InterpGetTcl(pServ->pSics);
  val = Tcl_GetVar(pTcl,"simMode",TCL_GLOBAL_ONLY);
  if(val != NULL && strstr(val,"2") != NULL){
         pPtr = strdup("sicssim.scn");
  } else {
	  /* allocate a new data file */
	  pPtr = ScanMakeFileName(pServ->pSics, self->pCon);
	  if (!pPtr) {
		  SCWrite(self->pCon,
				  "ERROR: cannot allocate new data filename, Scan aborted",
				  eLogError);
		  self->pCon = NULL;
		  self->pSics = NULL;
		  return 0;
	  }
  }
  snprintf(pBueffel, 511, "Writing data file: %s ...", pPtr);
  SCWrite(self->pCon, pBueffel, eLog);
  LogUserInfo(self->pCon);
  strcpy(self->pFile, pPtr);
  free(pPtr);
  return 1;
}

/*--------------------------------------------------------------------------*/
int PrepareScan(pScanData self)
{
  pVarEntry pVar = NULL;
  void *pDings;
  int i, iRet;
  float fVal;
  char pBueffel[512];
  char pMessage[1024];

  assert(self);
  assert(self->pCon);

  /* check boundaries of scan variables and allocate storage */
  for (i = 0; i < self->iScanVar; i++) {
    DynarGet(self->pScanVar, i, &pDings);
    pVar = (pVarEntry) pDings;
    if (pVar) {
      iRet = CheckScanVar(pVar, self->pCon, self->iNP - 1);
      if (!iRet) {
        return 0;
      }
      InitScanVar(pVar);
    } else {
      SCWrite(self->pCon,
              "WARNING: Internal error, no scan variable, I try to continue",
              eLog);
    }
    pVar = NULL;
  }                             /* end for */

  /* configure counter */
  SetCounterMode((pCounter) self->pCounterData, self->iMode);
  SetCounterPreset((pCounter) self->pCounterData, self->fPreset);
  self->iCounts = 0;

  if (!prepareDataFile(self)) {
    return 0;
  }

  return 1;
}

/*--------------------------------------------------------------------------*/
int SilentPrepare(pScanData self)
{
  pVarEntry pVar = NULL;
  void *pDings;
  int i, iRet;
  float fVal;
  char pBueffel[512];
  char pMessage[1024];

  assert(self);
  assert(self->iNP > 0);
  assert(self->pCon);

  /* check boundaries of scan variables and allocate storage */
  for (i = 0; i < self->iScanVar; i++) {
    DynarGet(self->pScanVar, i, &pDings);
    pVar = (pVarEntry) pDings;
    if (pVar) {
      iRet = CheckScanVar(pVar, self->pCon, self->iNP);
      if (!iRet) {
        return 0;
      }
      InitScanVar(pVar);
    } else {
      SCWrite(self->pCon,
              "WARNING: Internal error, no scan variable, I try to continue",
              eLog);
    }
    pVar = NULL;
  }                             /* end for */

  /* configure counter */
  SetCounterMode((pCounter) self->pCounterData, self->iMode);
  SetCounterPreset((pCounter) self->pCounterData, self->fPreset);
  self->iCounts = 0;

  return 1;
}

/*--------------------------------------------------------------------------*/
int NonCheckPrepare(pScanData self)
{
  pVarEntry pVar = NULL;
  void *pDings;
  int i, iRet;
  char pBueffel[512];
  char pMessage[1024];

  assert(self);
  assert(self->iNP > 0);
  assert(self->pCon);

  /* allocate storage for scan variables */
  for (i = 0; i < self->iScanVar; i++) {
    DynarGet(self->pScanVar, i, &pDings);
    pVar = (pVarEntry) pDings;
    if (pVar) {
      InitScanVar(pVar);
    } else {
      SCWrite(self->pCon,
              "WARNING: Internal error, no scan variable, I try to continue",
              eLog);
    }
    pVar = NULL;
  }                             /* end for */

  /* configure counter */
  SetCounterMode((pCounter) self->pCounterData, self->iMode);
  SetCounterPreset((pCounter) self->pCounterData, self->fPreset);
  self->iCounts = 0;

  if (!prepareDataFile(self)) {
    return 0;
  }

  return 1;
}

/*------------------------------------------------------------------------*/
static int CollectScanDataIntern(pScanData self, int iPoint,
                                 int jochenFlag)
{
  pVarEntry pVar = NULL;
  void *pDings;
  int i, iRet, status;
  char pStatus[2024], pItem[20];
  char pHead[2024];
  float fVal;
  CountEntry sCount;

  assert(self);
  assert(self->pCon);
  InitCountEntry(&sCount);

  /* prepare output header */
  snprintf(pHead,sizeof(pHead)-1, "%-4.4s ", "NP");
  snprintf(pStatus,sizeof(pStatus)-1, "%-4d ", iPoint);

  /* loop over all scan variables */
  status = 1;
  for (i = 0; i < self->iScanVar; i++) {
    DynarGet(self->pScanVar, i, &pDings);
    pVar = (pVarEntry) pDings;
    if (pVar) {
      fVal = pVar->pInter->GetValue(pVar->pObject, self->pCon);
      AppendScanVar(pVar, fVal);
      snprintf(pItem,sizeof(pItem)-1, "%-9.9s ", ScanVarName(pVar));
      strlcat(pHead, pItem,2024);
      snprintf(pItem,sizeof(pItem)-1, "%-9.3f ", fVal);
      strlcat(pStatus, pItem,2024);
    }
  }

  /* store counter data */
  sCount = CollectCounterData(self);

  /* 
     format header
   */
  strlcat(pHead, "Counts         ",2024);

  snprintf(pItem,sizeof(pItem)-1, "%-14ld ", sCount.lCount);
  strlcat(pStatus, pItem,2024);

  strlcat(pHead, "Monitor1    ",2024);
  snprintf(pItem,sizeof(pItem)-1, "%-11ld ", sCount.Monitors[0]);
  strlcat(pStatus, pItem,2024);
  strlcat(pHead, "Monitor2    ",2024);
  snprintf(pItem,sizeof(pItem)-1, "%-11ld ", sCount.Monitors[1]);
  strlcat(pStatus, pItem,2024);
  strlcat(pHead, "Monitor3    ",2024);
  snprintf(pItem,sizeof(pItem)-1, "%-11ld ", sCount.Monitors[2]);
  strlcat(pStatus, pItem,2024);
  strlcat(pHead, "Time  ",2024);
  snprintf(pItem,sizeof(pItem)-1, "%-6.1f", sCount.fTime);
  strlcat(pStatus, pItem,2024);

  /* write progress */
  /*
     strcat(pHead,"\r\n");
     strcat(pStatus,"\r\n");
   */
  SCWrite(self->pCon, pHead, eLog);
  SCWrite(self->pCon, pStatus, eLog);

  return 1;
}

/*---------------------------------------------------------------------------*/
int CollectScanData(pScanData self, int iPoint)
{
  return CollectScanDataIntern(self, iPoint, 0);
}

/*------------------------------------------------------------------------*/
int CollectSilent(pScanData self, int iPoint)
{
  pVarEntry pVar = NULL;
  void *pDings;
  int i, iRet, status, jochenFlag = 1;
  float fVal;
  CountEntry sCount;

  assert(self);
  assert(self->pCon);
  InitCountEntry(&sCount);

  /* loop over all scan variables */
  status = 1;
  for (i = 0; i < self->iScanVar; i++) {
    DynarGet(self->pScanVar, i, &pDings);
    pVar = (pVarEntry) pDings;
    if (pVar) {
      fVal = pVar->pInter->GetValue(pVar->pObject, self->pCon);
      AppendScanVar(pVar, fVal);
    }
  }

  /* store counter data */
  sCount = CollectCounterData(self);

  return 1;
}

/*--------------------------------------------------------------------------*/
static int StartToDrive(pScanData self, int iPoint)
{
  pVarEntry pVar = NULL;
  void *pDings;
  int i, iRet, status;

  assert(self);
  assert(self->pCon);


  /* loop over all scan variables */
  status = 1;
  for (i = 0; i < self->iScanVar; i++) {
    DynarGet(self->pScanVar, i, &pDings);
    pVar = (pVarEntry) pDings;
    if (pVar) {
      iRet = StartScanVar(pVar, self->pCon, iPoint);
      if (!iRet) {
        status = 0;
        break;
      }
    }
  }
  return status;
}

/*------------------------------------------------------------------------*/
int ScanDrive(pScanData self, int iPoint)
{
  int iRet;
  long lTask;
  int status;

  iRet = StartToDrive(self, iPoint);
  if (!iRet) {
    SCWrite(self->pCon, "ERROR: Cannot Drive, Scan aborted", eLogError);
    status = 0;
  } else {
    status = 1;
  }
  /* wait for finish */
  while(DevExecLevelRunning(pServ->pExecutor, RUNDRIVE)){
	  TaskYield(pServ->pTasker);
  }
  return status;
}

/*------------------------------------------------------------------------*/
int ScanFastDrive(pScanData self, int iPoint)
{
  int iRet;
  long lTask;
  int status;

  /*
   * drive the first point normally, otherwise there is to much intensity
   * in the first data point from the long time driving to the start
   * position.
   */
  if (iPoint == 0) {
    return ScanDrive(self, iPoint);
  }

  iRet = StartToDrive(self, iPoint);
  if (!iRet) {
    SCWrite(self->pCon, "ERROR: Cannot Drive, Scan aborted", eLogError);
    status = 0;
  } else {
    status = 1;
  }
  return status;
}

/*--------------------------------------------------------------------------*/
int ScanCount(pScanData self, int iPoint)
{
  pDummy pDum;
  int iRet;
  long lTask;

  pDum = (pDummy) self->pCounterData;
  iRet = StartDevice(pServ->pExecutor,
                     "ScanCounter",
                     pDum->pDescriptor,
                     self->pCounterData, self->pCon, RUNDRIVE,
                     self->fPreset);
  if (!iRet) {
    SCWrite(self->pCon, "ERROR: Cannot Count, Scan aborted", eLogError);
    return 0;
  }
  /* wait for finish */
  while(DevExecLevelRunning(pServ->pExecutor, RUNDRIVE)){
	  TaskYield(pServ->pTasker);
  }
  return 1;
}

/*====================== script invocation functions ====================*/
static pDynString GetStandardInvocation(pScanData self, char *function)
{
  pDynString result = NULL;
  char value[132];

  result = CreateDynString(80, 80);
  if (result == NULL) {
    SCWrite(self->pCon, "ERROR: out of memory in scan invocation",
            eLogError);
    return NULL;
  }
  if (StringDictGet(self->scanFunctions, function, value, 131) != 1) {
    snprintf(value, 131, "ERROR: scan function %s not found", function);
    SCWrite(self->pCon, value, eLogError);
    DeleteDynString(result);
    return NULL;
  }
  DynStringCopy(result, value);
  DynStringConcatChar(result, ' ');
  DynStringConcat(result, self->objectName);
  DynStringConcatChar(result, ' ');
  value[0] = '\0';
  StringDictGet(self->scanFunctions, "userdata", value, 131);
  DynStringConcat(result, value);
  DynStringConcatChar(result, ' ');
  return result;
}

/*-----------------------------------------------------------------------*/
static int StandardScriptInvoke(pScanData self, char *function)
{
  pDynString command = NULL;
  int status;

  command = GetStandardInvocation(self, function);
  if (command == NULL) {
    return 0;
  }
  if(self->pCon != NULL){
    status = InterpExecute(pServ->pSics, self->pCon, GetCharArray(command));
  } else {
    status = InterpExecute(pServ->pSics, pServ->dummyCon, GetCharArray(command));
  }
  DeleteDynString(command);
  if (status != 1) {
    return 0;
  }
  return status;
}

/*-----------------------------------------------------------------------*/
static int StandardScriptInvokeWithPoint(pScanData self,
                                         char *function, int iPoint)
{
  pDynString command = NULL;
  int status;
  char pNumber[50];

  command = GetStandardInvocation(self, function);
  if (command == NULL) {
    return 0;
  }
  snprintf(pNumber, 49, "%d", iPoint);
  DynStringConcat(command, pNumber);
  status = InterpExecute(self->pSics, self->pCon, GetCharArray(command));
  DeleteDynString(command);
  if (status != 1) {
    return 0;
  }
  return status;
}

/*------------------------------------------------------------------------*/
int ScriptWriteHeader(pScanData self)
{
  return StandardScriptInvoke(self, "writeheader");
}

/*---------------------------------------------------------------------*/
int ScriptPrepareScan(pScanData self)
{
  /* configure counter */
  SetCounterMode((pCounter) self->pCounterData, self->iMode);
  SetCounterPreset((pCounter) self->pCounterData, self->fPreset);
  self->iCounts = 0;
  return StandardScriptInvoke(self, "prepare");
}

/*-----------------------------------------------------------------------*/
int ScriptScanDrive(pScanData self, int iPoint)
{
  return StandardScriptInvokeWithPoint(self, "drive", iPoint);
}

/*------------------------------------------------------------------------*/
int ScriptScanCount(pScanData self, int iPoint)
{
  pDynString command = NULL;
  int status;
  char pNumber[50];

  command = GetStandardInvocation(self, "count");
  if (command == NULL) {
    return 0;
  }
  snprintf(pNumber, 49, "%d", iPoint);
  DynStringConcat(command, pNumber);
  if (self->iMode == eTimer) {
    DynStringConcat(command, " timer ");
  } else {
    DynStringConcat(command, " monitor ");
  }
  snprintf(pNumber, 49, " %f ", self->fPreset);
  DynStringConcat(command, pNumber);

  status = InterpExecute(self->pSics, self->pCon, GetCharArray(command));
  DeleteDynString(command);
  if (status != 1) {
    return 0;
  }
  return status;
}

/*-------------------------------------------------------------------------*/
int ScriptScanCollect(pScanData self, int iPoint)
{
  return StandardScriptInvokeWithPoint(self, "collect", iPoint);
}

/*----------------------------------------------------------------------*/
int ScriptWriteScanPoints(pScanData self, int iPoint)
{
  return StandardScriptInvokeWithPoint(self, "writepoint", iPoint);
}

/*--------------------------------------------------------------------*/
int ScriptScanFinish(pScanData self)
{
  return StandardScriptInvoke(self, "finish");
}

/*---------------------------------------------------------------------*/
void ConfigureScript(pScanData self)
{
  assert(self);

  self->PrepareScan = ScriptPrepareScan;
  self->WriteHeader = ScriptWriteHeader;
  self->WriteScanPoints = ScriptWriteScanPoints;
  self->ScanDrive = ScriptScanDrive;
  self->ScanCount = ScriptScanCount;
  self->CollectScanData = ScriptScanCollect;
}

/*======================================================================*/
int StandardScanWrapper(SConnection * pCon, SicsInterp * pSics,
                        void *pData, int argc, char *argv[])
{
  pScanData self = NULL;
  int iPoint;
  char pError[132];

  if (argc < 4) {
    SCWrite(pCon, "ERROR: not enough arguments to stdscan", eError);
    return 0;
  }
  strtolower(argv[1]);
  self = (pScanData) FindCommandData(pSics, argv[2], "ScanObject");
  if (self == NULL) {
    SCWrite(pCon, "ERROR: scan object not found", eError);
    return 0;
  }

  if (self->pCon == NULL) {
    self->pCon = pCon;
  }

  if (strcmp(argv[1], "writeheader") == 0) {
    return WriteHeader(self);
  } else if (strcmp(argv[1], "prepare") == 0) {
    return PrepareScan(self);
  } else if (strcmp(argv[1], "noncheckprepare") == 0) {
    return NonCheckPrepare(self);
  } else if (strcmp(argv[1], "finish") == 0) {
    return 1;
  }

  /*
     from here on we need a scan point
   */
  if (argc < 5) {
    SCWrite(pCon, "ERROR: not enough arguments to stdscan", eError);
    return 0;
  }
  iPoint = atoi(argv[4]);

  if (strcmp(argv[1], "drive") == 0) {
    return ScanDrive(self, iPoint);
  } else if (strcmp(argv[1], "fastdrive") == 0) {
    return ScanFastDrive(self, iPoint);
  } else if (strcmp(argv[1], "count") == 0) {
    return ScanCount(self, iPoint);
  } else if (strcmp(argv[1], "collect") == 0) {
    return CollectScanData(self, iPoint);
  } else if (strcmp(argv[1], "silentcollect") == 0) {
    return CollectSilent(self, iPoint);
  } else if (strcmp(argv[1], "writepoint") == 0) {
    return WriteScanPoints(self, iPoint);
  } else {
    snprintf(pError, 131, "ERROR: subcommand %s to stdscan not found",
             argv[1]);
    SCWrite(pCon, pError, eError);
    return 0;
  }
  return 0;
}
