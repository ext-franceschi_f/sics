/* -----------------------------------------------------------------------

	a replacement for strdup to make memdebug happy
	
	Mark Koennecke, November 1996
	
-----------------------------------------------------------------------------*/
#include "fortify.h"
#include <string.h>


char *Fortify_STRDUP(const char *in, char *file, unsigned long lLine)
{
  char *pResult = NULL;

  if (!in)
    return NULL;

  pResult =
      (char *) Fortify_malloc((strlen(in) + 2) * sizeof(char), file,
                              lLine);
  if (!pResult) {
    return NULL;
  } else {
    strcpy(pResult, in);
  }
  return pResult;
}
