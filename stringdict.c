/*-------------------------------------------------------------------------

			S T R I N G D I C T

        Reimplementation using red-black tree.

        Douglas Clowes (dcl@ansto.gov.au), April 2018

	Implementation file for a simple list based string dictionary
	of name value pairs.

       Mark Koennecke, April 1997

       Copyright:

       Labor fuer Neutronenstreuung
       Paul Scherrer Institut
       CH-5423 Villigen-PSI


      The authors hereby grant permission to use, copy, modify, distribute,
      and license this software and its documentation for any purpose, provided
      that existing copyright notices are retained in all copies and that this
      notice is included verbatim in any distributions. No written agreement,
      license, or royalty fee is required for any of the authorized uses.
      Modifications to this software may be copyrighted by their authors
      and need not follow the licensing terms described here, provided that
      the new terms are clearly indicated on the first page of each file where
      they apply.

      IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
      FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
      ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
      DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
      POSSIBILITY OF SUCH DAMAGE.

      THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
      FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
      IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
      NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
      MODIFICATIONS.
----------------------------------------------------------------------------*/
#include "fortify.h"
#include "stringdict.h"
#include "rbtree.h"
#include "strlutil.h"
#include <string.h>
#include <stdlib.h>
//#include <stdio.h>

#include "fortify.h"
#include "lld.h"
#include "stringdict.h"

/*-------------------------------------------------------------------------*/
typedef struct __StringDict {
  struct rbtree_t rb_tree;
  rbtree_node pTraverse;
} StringDict;

/*---------------------------------------------------------------------------*/
pStringDict CreateStringDict(void)
{
  pStringDict self = calloc(1, sizeof(*self));
  if (!self)
    return self;
  rbtree_init(&self->rb_tree, (rbtree_compare_func) strcmp);
  return self;
}

/*---------------------------------------------------------------------------*/
void DeleteStringDict(pStringDict self)
{
  rbtree_node node;
  while ((node = rbtree_node_first(&self->rb_tree))) {
    node = rbtree_node_delete(&self->rb_tree, node);
    if (node) {
      free(node);
    }
  }
  free(self);
}

/*---------------------------------------------------------------------------*/
int StringDictAddPair(pStringDict self, const char *name, const char *value)
{
  rbtree_node node;
  int klen, vlen = 0;
  klen = strlen(name) + 1;
  if (value)
    vlen = strlen(value) + 1;
  node = calloc(1, sizeof(*node) + (klen + vlen) * sizeof(char));
  if (!node)
    return 0;
  node->key = &node[1];
  strcpy(node->key, name);
  if (value) {
    char *k = node->key;
    node->value = &k[klen];
    strcpy(node->value, value);
  }
  node = rbtree_insert(&self->rb_tree, node);
  if (node) {
    /* replace existing */
    free(node);
  }
  return 1;
}
/*---------------------------------------------------------------------------*/
int StringDictExists(pStringDict self, const char *name)
{
  if (rbtree_lookup(&self->rb_tree, name))
    return 1;
  return 0;
}
/*---------------------------------------------------------------------------*/
int StringDictUpdate(pStringDict self, const char *name, const char *value)
{
  rbtree_node node = rbtree_node_lookup(&self->rb_tree, name);
  if (node) {
    StringDictAddPair(self, name, value);
    return 1;
  }
  return 0;
}
/*---------------------------------------------------------------------------*/
int StringDictGet(pStringDict self, const char *name, char *pResult, int iLen)
{
  rbtree_node node = rbtree_node_lookup(&self->rb_tree, name);
  if (node) {
    if (pResult == NULL) {
      if (node->value)
        return strlen(node->value) + 1;  /* for \0 */
      return 1; /* for \0 */
    } else {
      if (node->value)
        strlcpy(pResult, node->value, iLen);
      else
        *pResult = '\0';
      return 1;
    }
  }
  return 0;
}

/*---------------------------------------------------------------------------*/
const char *StringDictGetShort(pStringDict self, const char *name)
{
  rbtree_node node = rbtree_node_lookup(&self->rb_tree, name);
  if (node) {
    return node->value;
  }
  return NULL;
}

/*---------------------------------------------------------------------------*/
int StringDictGetAsNumber(pStringDict self, const char *name, float *fVal)
{
  rbtree_node node = rbtree_node_lookup(&self->rb_tree, name);
  if (node) {
    if (fVal) {
      if (node->value) {
        char *endptr;
        double f = strtod(node->value, &endptr);
        if (endptr == node->value)
          return 0;
        *fVal = f;
      } else {
        *fVal = 0.0;
      }
      return 1;
    }
  }
  return 0;
}
/*---------------------------------------------------------------------------*/
int StringDictDelete(pStringDict self, const char *name)
{
  rbtree_node node = rbtree_delete(&self->rb_tree, name);
  if (node) {
    free(node);
    return 1;
  }
  return 0;
}

/*---------------------------------------------------------------------------*/
const char *StringDictGetNext(pStringDict self, char *pValue, int iValLen)
{
  /* Get the first or next node */
  if (self->pTraverse) {
    self->pTraverse = rbtree_node_next(&self->rb_tree, self->pTraverse);
  } else {
    self->pTraverse = rbtree_node_first(&self->rb_tree);
  }
  /* If empty or finished */
  if (self->pTraverse == NULL)
    return NULL;
  /* else copy value and return key */
  if (self->pTraverse->value)
    strlcpy(pValue, self->pTraverse->value, iValLen);
  else
    *pValue = '\0';
  return self->pTraverse->key;
}

/*---------------------------------------------------------------------------*/
void StringDictKillScan(pStringDict self)
{
  self->pTraverse = NULL;
}
