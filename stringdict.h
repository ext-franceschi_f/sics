
#line 53 "stringdict.w"

/*--------------------------------------------------------------------------
                        S T R I N G D I C T

 A module which implements a general purpose string dictionary.

 copyright: see implementation file

 Mark Koennecke, April 1997
---------------------------------------------------------------------------*/
#ifndef SICSSTRINGDICT
#define SICSSTRINGDICT

typedef struct __StringDict *pStringDict;


#line 6 "stringdict.w"

pStringDict CreateStringDict(void);
void DeleteStringDict(pStringDict self);

int StringDictAddPair(pStringDict self, const char *name, const char *value);
int StringDictExists(pStringDict self, const char *name);
int StringDictUpdate(pStringDict self, const char *name, const char *value);
int StringDictGet(pStringDict self, const char *name, char *pResult, int iLen);

  /* the result of StringDictGetShort is only valid as long that the entry is not changed */
const char *StringDictGetShort(pStringDict self, const char *name);

int StringDictGetAsNumber(pStringDict self, const char *name, float *fVal);
int StringDictDelete(pStringDict self, const char *name);

const char *StringDictGetNext(pStringDict self, char *pValue, int iValLen);
void StringDictKillScan(pStringDict self);

#line 68 "stringdict.w"


#endif
