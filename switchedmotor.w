\subsection{Switched Motors}
At TOPSI there is a set of motors which share a single EL734 motor slot. Which
 motor is adressed is switched via a SPS. The idea was to save money but really
 this is a pain in the ass. Especially as the motors do not have own encoders.
 This module implements sofwtare support for this setup. Virtual motors are
 defined for the slave motors. The algorithm implemented works as follows:

There is always an active motor which is currently adressed. When
{\bf reading } the motor either a stored value is returned (when the
motor is not adressed) or the real value is read from the motor. When
driving a motor the following things happen:
\begin{itemize}
\item If the motor is active the motor is directly driven.
\item If the motor is not active:
\begin{itemize}
\item The correct motor is switched to in the SPS.
\item A reference run is made.
\item The motor is finally driven.
\end{itemize}
\end{itemize}


In order to do all this we need yet another data structure:

@d swdata @{
	typedef struct __SWMOT {
	                       pObjectDescriptor pDes;
	                       pIDrivable pDriv;
	                       pMotor pMaster;
	                       int *selectedMotor;
	                       int myNumber;
	                       float positions[3];
	                       char slaves[3][80];
	                       char *switchFunc;
                               int errCode;
                               } SWmot, *pSWmot;
@}

The fields are:
\begin{description}
\item[pDes] The standard SICS object descriptor.
\item[pDriv] The drivable interface.
\item[pMaster] The EL734 motor used as the master motor for driving
 the slaves.
\item[myNumber] The number of the motor implemented by this object.
\item[selectedMotor] The number of the selected motor. This must be a
pointer in order to be shared among multiple copies of this.
\item[slaves] The names of the slave motors.  
\item[switchFunc] is the name of a interpreter function which does 
 the switching to another motor. Usually this will be implemented in
Tcl. This gives some level of portability to other setups. The syntax
shall be : status = switchFun motNumber. status will either hold OK on
 return or the error encountered. 
\item[errCode] is an internal error code.
\end{description}

Of course there will be three copies of this structure for the three
slave motors. 

Most feautures of this module will be implemented in the functions of
the drivable interface or internal functions. The only additional
functions needed provide the interface for the interpreter:

@d swint @{
  int MakeSWMotor(SConnection *pCon, SicsInterp *pSics, void *pData,
                   int argc, char *argv[]);

  int SWMotorAction(SConnection *pCon, SicsInterp *pSics, void *pData,
                   int argc, char *argv[]);
@}

@o swmotor.h @{
/*----------------------------------------------------------------------
  TOPSI switched motors. Three motors share a EL734 motor. Between is
  switched through a SPS. 

  Mark Koennecke, May 2001
-----------------------------------------------------------------------*/
#ifndef SWMOTOR
#define SWMOTOR
#include "obdes.h"
#include "interface.h"
#include "motor.h"
@<swint@>
#endif 
@}

@o swmotor.i @{
/* ----------------------------------------------------------------------
 Internal data structure for the switched motor module. For
 documentation see swmotor.tex.

 Mark Koennecke, May 2001
----------------------------------------------------------------------*/
@<swdata@>
 
@}