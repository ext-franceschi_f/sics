#ifndef SYNCEDPROT_H
#define SYNCEDPROT_H

/** synchronized execution of scriptcontext and other operations
 *
 * see sctsyncprot.c for a description of the 'protocol'
 *
 * M. Zolliker Aug 2012
 */

typedef enum {
  SYNCED_NO_ID = -1,
  SYNCED_NO_MEMORY = -2,
  SYNCED_STACK_OVERFLOW = -3,
  SYNCED_STACK_UNDERFLOW = -4,
  SYNCED_COUNT_UNDERFLOW = -5,
  SYNCED_ID_MISMATCH = -6,
  SYNCED_NOT_FOUND = -7
} Synced_Error_Message;

/** \brief set the actual sync id 
 * \param syncid the sync id or 0 to reate a new sync id
 * \return the created or given sync id or < 0 on error
 */
long SyncedBegin(long syncid);

/** \brief set the actual sync id back to value before the last call to SyncedBegin
 * \param syncid the sync id (for checking) or 0 (for lazy programmers)
 * \return the sync id on success or a negative value on failure (Synced_Error_Message)
 */
long SyncedEnd(long syncid);

/** \brief increment the counter of syncid
 * \parameter syncid the syncid to increment
 * \return the sync id on success or a negative value on failure (Synced_Error_Message)
 */
long SyncedIncr(long syncid);

/** \brief decrement the counter of syncid
 * \parameter syncid the sync id to decrement (or 0 for using the actual sync id)
 * \return the sync id on success, 0 if syncid was 0 and no actual sync id was defined
 *         or a negative value on failure (Synced_Error_Message)
 */
long SyncedDecr(long syncid);

/** \brief get the actual syncid
 * \return 1 the actual sync id or 0
 */
long SyncedGet(void);

/**
 * \brief check if an action created with this sync id is pending
 * \param syncid the sync id
 * \return 1 for pending, 0 for not pending
 */
int SyncedPending(long syncid);

#endif

