
/*
                 S y n c h r o n i z e

 Synchronize parameters in this server with parameters from another
 SICS server.

 Mark Koennecke, March 2001
*/
#ifndef SICSSYNC
#define SICSSYNC

int MakeSync(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[]);
int Synchronize(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);


#endif
