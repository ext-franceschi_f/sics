
/*---------------------------------------------------------------------
 SICS interface to the triple axis drivable motors.

 copyright: see file COPYRIGHT

 Mark Koennecke, May 2005
--------------------------------------------------------------------*/
#ifndef TASDRIV
#define TASDRIV
#include "tasub.h"

int InstallTasMotor(SicsInterp *pSics, ptasUB self, int tasVar, char *name);
int InstallTasQMMotor(SicsInterp *pSics, ptasUB self);
int TasMot(SConnection *pCon,SicsInterp *pSics, void *pData,                
                int argc, char *argv[]);

int readTASMotAngles(ptasUB self, SConnection *pCon, ptasAngles ang);
#endif
