/*----------------------------------------------------------------------
  This is the header file for the TAS scan code. It mainly implements 
  writing of the ILL file format, which the TAS'lers adore.

  Mark Koennecke, May 2005, using code from an earlier TASAMAD emulation
  core.
------------------------------------------------------------------------*/
#ifndef SICSUBTAS
#define SICSUBTAS
#include "sicsvar.h"
#include "tasub.h"
/* --------------------- data structure -------------------------------*/
#define MAXADD 20
typedef struct {
  pObjectDescriptor pDes;
  pCounter counter;
  pScanData pScan;
  ptasUB ub;
  int iPOL;
  int iIgnore;                  /* in order to ignore writing scan points again 
                                   in polarisation mode;
                                 */
  char out[MAXADD][10];
  int addCount;
  int iFileNO;
  int iFast;
  char scanVar[80];
} TASdata, *pTASdata;
/*---------------------- interface ----------------------------------*/

int TASUBScanFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
                     int argc, char *argv[]);
int TASUBScan(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[]);

#endif
