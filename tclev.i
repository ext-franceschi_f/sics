
/*---------------------------------------------------------------------------
   Tcl environment device driver data structure definition file. 

   No general usable header file.

   Mark Koenencke, February 1998
----------------------------------------------------------------------------*/

   typedef struct __TclEv {
                               char *pArray;
                               char *pInit;
                               char *pClose;
                               char *pSetValue;
                               char *pGetValue;
                               char *pSend;
                               char *pGetError;
                               char *pTryFixIt;
                               char *pWrapper;
                               char *pName;
                               void *pTcl;
                               int iLastError;
                              } TclEv, *pTclEv;

