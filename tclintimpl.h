/* -------------------------------------------------------------------------
  This class allows for the implementation off SICS internal interfaces 
  through Tcl scripts. Additionally, helper functions for supporting such 
  scripts are provided.

  This is the first implementation which only supports saving additional 
  data into status files. This is the object decriptor interface. 

  copyright: see file COPYRIGHT

  Mark Koennecke, May 2003
  ------------------------------------------------------------------------*/
#ifndef TCLINTIMPL
#define TCLINTIMPL

#include "sics.h"

int MakeTclInt(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[]);

int TclIntAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[]);

#endif
