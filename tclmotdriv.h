/*---------------------------------------------------------------------------
 This is a motor driver which is implemented in Tcl. This means
 this code is only a wrapper which calls Tcl functions to do the 
 actual work.

 copyright: see file COPYRIGHT

 Mark Koennecke, December 2005
 --------------------------------------------------------------------------*/
#ifndef TCLMOTDRIV
#define TCLMOTDRIV
#include "stringdict.h"

typedef struct ___TclDriv {
  /* general motor driver interface 
     fields. REQUIRED!
   */
  float fUpper;                 /* upper limit */
  float fLower;                 /* lower limit */
  char *name;
  int (*GetPosition) (void *self, float *fPos);
  int (*RunTo) (void *self, float fNewVal);
  int (*GetStatus) (void *self);
  void (*GetError) (void *self, int *iCode, char *buffer, int iBufLen);
  int (*TryAndFixIt) (void *self, int iError, float fNew);
  int (*Halt) (void *self);
  int (*GetDriverPar) (void *self, char *name, float *value);
  int (*SetDriverPar) (void *self, SConnection * pCon,
                       char *name, float newValue);
  void (*ListDriverPar) (void *self, char *motorName, SConnection * pCon);
  void (*KillPrivate) (void *self);

  /* Tcl specific fields */
  pStringDict mappings;
  int errorCode;
  char tclError[1024];
  char motName[132];
} TCLDriv;

MotorDriver *CreateTclMotDriv(SConnection * pCon, int argc, char *argv[]);

#endif
