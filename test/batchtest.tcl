#------------------------------------------------------------------------------
# This is a set of regression tests for the batch processing feauture
# in SICS
#
# Mark Koennecke, October 2006
#------------------------------------------------------------------------------

puts stdout "Testing batch processing" 

test batch-1.0 {Test Batch File Execution} -body {
	config rights User User
	set result [eval exe job1.tcl]
	if {[string first TERMINATED $result] < 0} {
		error "Failed to process batch file"
	}
	if {[string first Job1 $result] < 0} {
		error "Output from batch file missing, received: $result"
	}
	return OK
} -result OK

test batch-1.1 {Test Batch File Interruption} -body {
	config rights User user
        exec ./interrupt.tcl &
	set result [eval exe job1.tcl]
	if {[string first TERMINATED $result] < 0} {
		error "Failed to process batch file"
	}
	if {[string first interrupted $result] < 0} {
		error "Interrupting did not work"
	}
	return OK
} -result OK

test batch-1.2 {Test Nested Batch File Execution} -body {
	config rights User user
	set result [eval exe job2.tcl]
	if {[string first TERMINATED $result] < 0} {
		error "Failed to process batch file"
	}
	if {[string first NestOne $result] < 0} {
		error "Output from batch file missing"
	}
	if {[string first NestTwo $result] < 0} {
		error "Output from batch file missing"
	}
	if {[string first NestThree $result] < 0} {
		error "Output from batch file missing"
	}
	return OK
} -result OK

test batch-1.3 {Test Nested Batch File Interruption} -body {
	config rights User user
        exec ./interrupt.tcl &
	set result [eval exe job2.tcl]
	if {[string first TERMINATED $result] < 0} {
		error "Failed to process batch file"
	}
	if {[string first NestOne $result] < 0} {
		error "Output from batch file missing"
	}
	if {[string first NestTwo $result] < 0} {
		error "Output from batch file missing"
	}
	if {[string first NestThree $result] < 0} {
		error "Output from batch file missing"
	}
	if {[string first interrupted $result] < 0} {
		error "Interrupting did not work"
	}
	return OK
} -result OK

test batch-1.4 {Test Path Parameters} -body {
        testPar "exe batchpath" tmp User
        testPar "exe syspath" tmp Mugger
	return OK
} -result OK

test batch-1.5 {Test Path Failure} -body {
    config rights Mugger Mugger
    exe batchpath tmp
    exe syspath tmp
    set result [exe job4.tcl]
    if {[string first "not found" $result] < 0} {
	error "Batch file found which should not"
    }
    exe batchpath ./
    exe syspath ./
    return OK
} -result OK

test batch-1.6 {Test Uploading} -body {
    config rights User User
    catch {exec rm hugo.job}
    testOK "exe upload"
    testOK "exe append clientput hugo"
    testOK "exe append wait 2"
    testOK "exe save hugo.job"
    testOK "exe upload"
    testOK "exe append clientput hugo"
    testOK "exe append wait 2"
    set stat [catch {testOK "exe save hugo.job" } msg ]
    if {$stat == 0} {
	error "Failed to trigger overwrite error"
    }
    testOK "exe forcesave hugo.job"
    return OK
} -result OK

test batch-1.7 {Test Notifications} -body {
    config rights User User
    testOK "exe interest"
    set result [eval exe job4.tcl]
    if {[string first BATCHSTART $result] < 0} {
	error "BATCHSTART missing"
    }
    if {[string first BATCHEND $result] < 0} {
	error "BATCHEND missing"
    }
    if {[string first job4.tcl.range $result] < 0} {
	error "Range entries missing"
    }
    return OK
} -result OK


