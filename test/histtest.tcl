#---------------------------------------------------------------------------
# This is for testing the histogram memory code
# The errortypes are the same as in the counter module
#
# Mark Koennecke, October 2006
#---------------------------------------------------------------------------
puts stdout "Testing Histogram Memory"
config rights Mugger Mugegr
hm config errotype 0
hm init

test hm-1.0 {Test Mode Setting} -body {
	config rights Spy Spy
        set res [eval hm countmode monitor]
	if {[string first ERROR $res] < 0} {
		error "Managed to set parameter even if not allowed"
	}
	config rights User User
	set res [eval hm countmode monitor]
	if {[string first ERROR $res] >= 0} {
		error "Setting parameter failed with $res"
	}
	set readback [SICSValue "hm countmode"]
        compareValue [string tolower $readback] monitor
	config rights Spy Spy
        set res [eval hm countmode timer]
	if {[string first ERROR $res] < 0} {
		error "Managed to set parameter even if not allowed"
	}
	config rights User User
	set res [eval hm countmode timer]
	if {[string first ERROR $res] >= 0} {
		error "Setting parameter failed with $res"
	}
	set readback [SICSValue "hm countmode"]
        compareValue [string tolower $readback] timer
	return OK
} -result OK
#-------------------------------------------------------------------
test hm-1.1 {Test Preset Setting} -body {
	config rights Spy Spy
        set val 12
        set res [eval hm preset $val]
	if {[string first ERROR $res] < 0} {
		error "Managed to set parameter even if not allowed"
	}
	config rights User User
	set res [eval hm preset $val]
	if {[string first ERROR $res] >= 0} {
		error "Setting parameter failed with $res"
	}
	set readback [SICSValue "hm preset"]
        compareValue $readback $val
	return "OK"
} -result OK
#---------------------------------------------------------------------
test hm-1.3 {Test Normal Counting} -body {
    config rights Spy Spy 
    set status [catch {testNBCounting "hm count" 11} msg]
    if {$status == 0} {
	error "Counted in spite of lacking privilege"
    }
    config rights User User
    testOK "hm countmode timer"
    testOK "hm preset 5"
    testNBCounting "hm count" 11
} -result OK
#---------------------------------------------------------------------
test hm-1.4 {Test Blocking  Counting} -body {
    config rights Spy Spy 
    set status [catch {testBlockCounting "hm countblock" 11} msg]
    if {$status == 0} {
	error "Counted in spite of lacking privilege"
    }
    config rights User User
    testBlockCounting "hm countblock" 11
} -result OK
#--------------------------------------------------------------------
test hm-1.5 {Interrupted Counting} -body {
    hm preset 10
    testInterruptedCount "hm count"
} -result OK
#--------------------------------------------------------------------
config rights User User

test hm-1.51 {Pause Counting Test} -body {
    global socke
    hm preset 300
    hm count
    exec sleep 1
    set ans [status]
    if {[string first Counting $ans] < 0} {
	error "Failed to start counting: $ans"
    }
    pause
    exec sleep 1
    set ans [status]
    if {[string first Paus $ans] < 0} {
	error "Failed to pause counting: $ans"
    }
    puts $socke continue
    flush $socke
    exec sleep 1
    set ans [status]
    if {[string first Count $ans] < 0} {
	error "Failed to continue counting: $ans"
    }
    puts $socke "INT1712 3"
    flush $socke
    set ans [status]
    return OK
} -result OK
#---------------------------------------------------------
test hm-1.52 {Pause Interrupt Test} -body {
    global socke
    hm count 300
    exec sleep 2
    set ans [status]
    if {[string first Counting $ans] < 0} {
	error "Failed to start counting: $ans"
    }
    pause
    exec sleep 1
    set ans [status]
    if {[string first Paus $ans] < 0} {
	error "Failed to pause counting: $ans"
    }
    puts $socke "INT1712 3"
    flush $socke
    set ans [status]
    if {[string first Eager $ans] < 0} {
	error "Failed to interrupt paused counting: $ans"
    }
    return OK
} -result OK
#--------------------------------------------------------------------
config rights Mugger Mugger
hm configure errortype 1 
hm configure recover 0
hm init
hm preset 10

test hm-1.6 {Counting Start Failure} -body {
    set ans [hm count]
    if { [string first "cannot start" $ans] < 0} {
	error "Failed to trigger count start failure: $ans"
    }
    return OK
} -result OK
#-------------------------------------------------------------------------
hm configure  errortype  1
hm configure recover  1
hm init

test hm-1.7 {Counting Start Failure with Recovery} -body {
    set ans [hm count]
    if { [string first "WARNING" $ans] < 0} {
	error "Failed to trigger count start failure: $ans"
    }
    set ans [SICSValue status]
    if {[string first Counting $ans] < 0} {
	error "Did not recover from start failure"
    }
    exec sleep 15
    set ans [SICSValue status]
    if {[string first Eager $ans] < 0} {
	error "Did not stop counting after start failure"
    }
    return OK
} -result OK
#----------------------------------------------------------------------
hm configure errortype 2
hm configure  recover 0
hm init

test hm-1.8 {Counting Status Failure} -body {
    set ans [hm count]
    set ans [status]
    if { [string first "Full Stop called" $ans] < 0} {
	error "Failed to trigger count start failure: $ans"
    }
    return OK
} -result OK
#---------------------------------------------------------------
hm configure errortype  2
hm configure recover 1
hm init

test hm-1.9 {Counting Status Failure with Recovery} -body {
    set ans [hm count 10]
    set ans [status]
    if { [string first "WARNING" $ans] < 0} {
	error "Failed to trigger count start failure: $ans"
    }
    if {[string first Counting $ans] < 0} {
	error "Did not recover from status failure"
    }
    exec sleep 12
    set ans [SICSValue status]
    if {[string first Eager $ans] < 0} {
	error "Did not stop counting after status failure"
    }
    return OK
} -result OK
#----------------------------------------------------------------------
hm configure errortype 0
hm configure recover 0
hm configure testval 3
hm init
hm preset 2
hm countblock

test hm-1.10 {Test data} -body {
    set expected [SICSValue "hm configure dim0"]
    set data [hm get 0]
    if {[string first ERROR $data] >= 0} {
	error "Failed to read HM"
    }
    if {[string first Histogram $data] < 0} {
	error "Bad response from HM"
    }
    set l [split $data =]
    set data [lindex $l 1]
    set l [split $data]
    set count 0
    foreach e $l {
	if {![string is space $e]} {  
	    incr count
	    if {$e != 3} {
		error "Bad value in HM, got $e, expected 3"
	    }
	}
    }
    if {$count != $expected} {
	error "HM Datasize does not match, expected $expected, got $count" 
    }
    return OK
} -result OK

hm configure testval  7
hm init
hm countblock

test hm-1.11 {Test data second} -body {
    set expected [SICSValue "hm configure dim0"]
    set data [hm get 0]
    if {[string first ERROR $data] >= 0} {
	error "Failed to read HM"
    }
    if {[string first Histogram $data] < 0} {
	error "Bad response from HM"
    }
    set l [split $data =]
    set data [lindex $l 1]
    set l [split $data]
    set count 0
    foreach e $l {
	if {![string is space $e]} {  
	    incr count
	    if {$e != 7} {
		error "Bad value in HM, got $e, expected 3"
	    }
	}
    }
    if {$count != $expected} {
	error "HM Datasize does not match, expected $expected, got $count" 
    }
    return OK
} -result OK
#-------------------------- ---------------------------------------------
test hm-1.13 {Test hm sum} -body {
    set test [SICSValue "hm sum 2 4"]
    if {$test != 14} {
	error "Summing HM failed, expected 14 got $test"
    }
    return OK
} -result OK
#--------------------------------------------------------------------------
test hm-1.14 {Test Setting Time Binning} -body {
    config rights Mugger Mugger
    testOK "tof genbin 50 20 70"
    testOK "tof configure testval 1"
    testOK "tof init"
    return OK
} -result OK
#-------------------------------------------------------------------------
test hm-1.15 {Test Reading Time Binning} -body {
    set tst [SICSValue "tof notimebin"]
    if {$tst != 70} {
	error "NTOF bad, expected 70, got $tst"
    }
    set tdata [SICSValue "tof timebin"]
    set l [split $tdata]
    set count 0
    foreach v $l {
	if {![string is space $v]} {
            set tval [expr 50.0 + $count*20]
	    if {abs($v - $tval) > 1} {
		error "Bad time value $v, expected $tval"
	    }
            incr count
        } 
    }
    if {$count < $tst} {
      error "Insufficient number of timebins: $count, expected $tst"
    }
    return OK
} -result OK
#----------------------------------------------------------------------
tof countmode timer
tof preset 2
tof countblock
test hm-1.16 {Test Reading TOF Data} -body {
    set ntof [SICSValue "tof notimebin"]
    set dim  [SICSValue "tof configure dim0"]
    set alldata [expr $ntof*$dim]
    set tdata [SICSValue "tof get 0"]
    set l [split $tdata]
    set count 0
    foreach v $l {
	if {![string is space $v]} {
	    if {abs($v - 1) > .1} {
		error "Bad data value $v, expected $tval"
	    }
            incr count
        } 
    }
    if {$count < $alldata} {
      error "Datapoints missing, got $count, expected $alldata"
    }
    return OK
} -result OK
#----------------------------------------------------------------------
tof initval 1
test hm-1.16 {Test TOF Sum} -body {
    set val [SICSValue "tof sum 2 3 0 20"]
    if {$val != 20 } {
	error " tof sum failed, expected 20 received $val"
    }
    return OK
} -result OK

