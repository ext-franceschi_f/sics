#---------------------------------------------------------
# This tests TRICS list operations. Tests will be performed 
# by using various list configuration commands and 
# saving the list to a file. This file  will be compared 
# with a known good one. Thus a failed test just means that
# the text generated changed. This may or may not be critical, 
# a further examination with tkdiff will reveal if this needs 
# to be fixed or if the generated file has simply to be 
# renamed to become the comparison file. 
#
# Mark Koennecke, February 2009
#--------------------------------------------------------------

puts stdout "Testing TRICS List Processing"
config rights Mugger Mugger
mode bi
config rights User User
set testcell "5.4202 5.4202 12.3228 90. 90. 90."
cell $testcell

proc configureBiList {} {
    refclear
    refadd ang 12 6 120.3 321.77
    refadd idx 2 0 0
    refadd idxang 1 0 0 11 5.5 77.8 167.56
    refdel 0001
    refadd idx 3 0 0
    refadd idx 4 0 0
    refadd idx 5 0 0
    refadd idx 6 0 0
    refhkl 0004 7 1 1
    refang 0005 22.2 12.3 77.8 129.8
} 

test tricslist-1.0 {Test Reflectionlist Bisecting} -body {
    configureBiList
    configureBiList
    refsave test/testbi.dat
    set status [catch {exec diff testbi.dat testbi.ref} msg]
    if {$status != 0} {
	error "Differences found in reflection list for bisecting, investigate!"
    }
    return OK
} -result OK

config rights Mugger Mugger
mode nb
config rights User User

proc configureNBList {} {
    refclear
    refadd ang 12 70 10.3
    refadd idx 2 0 0
    refadd idxang 1 0 0 11 5.5 77.8 
    refdel 0001
    refadd idx 3 0 0
    refadd idx 4 0 0
    refadd idx 5 0 0
    refadd idx 6 0 0
    refhkl 0004 7 1 1
    refang 0005 22.2 72.3 77.8 
} 

test tricslist-1.1 {Test Reflectionlist Normal Beam} -body {
    configureNBList
    configureNBList
    refsave test/test.dat
    set status [catch {exec diff test.dat testnb.ref} msg]
    if {$status != 0} {
	error "Differences found in reflection list for normal beam, investigate!"
    }
    return OK
} -result OK


config rights Mugger Mugger
mode tas
config rights User User

proc configureTASList {} {
    refclear
    refadd ang 12 70 3.5 7.8
    refadd idx 2 0 0
    refadd idxang 1 0 0 11 7.5 7.2 11.  
    refdel 0001
    refadd idx 3 0 0
    refadd idx 4 0 0
    refadd idx 5 0 0
    refadd idx 6 0 0
    refhkl 0004 7 1 1
    refang 0005 22.2 7.8 8.1 2.1
} 

test tricslist-1.2 {Test Reflectionlist TAS} -body {
    configureTASList
    configureTASList
    refsave test/test.dat
    set status [catch {exec diff test.dat testtas.ref} msg]
    if {$status != 0} {
	error "Differences found in reflection list for TAS, investigate!"
    }
    return OK
} -result OK

  
config rights Mugger Mugger
mode bi
config rights User User


proc configureDCList {} {
    tabclear
    tabadd 30 om .1 10 10000
    tabadd 50 om .2 12 20000
    tabadd 60 o2t .1 20 30000
}

  30.000         om    0.100 10 10000.000
  50.000         om    0.200 12 20000.000
  60.000        o2t    0.100 20 30000.000

test tricslist-1.4 {Test Data Collection Configuration } -body {
    set should   "30.000         om    0.100 10 10000.000\n  50.000         om    0.200 12 20000.000\n  60.000        o2t    0.100 20 30000.000"
    configureDCList
    configureDCList
    set msg [string trim [tablist]]
    if {[string compare [string trim $msg] $should] != 0} {
	error "Received $msg, should have been $should"
    }
    return OK
} -result OK

hkllimit -3 -3 -3 6 8 8 7 35.
spgrp P4

test tricslist-1.5 {Test Data Collection List Bisecting } -body {
    hklgen sup
    indw .3 0 .3
    indsort
    indsave test/test.dat
    set status [catch {exec diff test.dat dcbi.ref} msg]
    if {$status != 0} {
	error "Differences found in data collection list for bisecting, investigate!"
    }
    return OK
} -result OK

config rights Mugger Mugger
mode nb
config rights User User


test tricslist-1.6 {Test Data Collection List Normal Beam } -body {
    hklgen sup
    indw .3 0 .3
    indsort
    indsave test/test.dat
    set status [catch {exec diff test.dat dcbi.ref} msg]
    if {$status != 0} {
	error "Differences found in data collection for normal beam, investigate!"
    }
    return OK
} -result OK

config rights Mugger Mugger
mode tas
config rights User User

test tricslist-1.7 {Test Data Collection List TAS} -body {
    hklgen sup
    indw .3 0 .3
    indsort
    indsave test/test.dat
    set status [catch {exec diff test.dat dctas.ref} msg]
    if {$status != 0} {
	error "Differences found in reflection list for TAS, investigate!"
    }
    return OK
} -result OK


config rights Mugger Mugger
mode bi
config rights User User

