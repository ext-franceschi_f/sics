#------------------------------------------------------------
# This is part of the regression test suite for TRICS. This 
# test the existence and proper operation of several variables.
#
# Mark Koennecke, February 2009
#--------------------------------------------------------------
puts stdout "Testing TRICS variables "
set testub "0.1215666 -0.138694 -0.0021278 -0.1386887 -0.1216454 0.0010515 -0.0049867 0.0020612 -0.081156"

test tricsvar-1.0 {Test projectdir} -body {
        testPar projectdir /home/user/batch User 
	return OK
} -result OK

test tricsvar-1.1 {Test title} -body {
        testPar title Oksanaoxid User 
	return OK
} -result OK

test tricsvar-1.2 {Test user} -body {
        testPar User "Willi Wuergehals" User 
	return OK
} -result OK

test tricsvar-1.3 {Test phone} -body {
        testPar phone +41-56-3102512 User 
	return OK
} -result OK

test tricsvar-1.4 {Test address} -body {
        testPar address "Kurkenstrasse 27" User 
	return OK
} -result OK

test tricsvar-1.5 {Test sample} -body {
        testPar sample GurkenSulfid User 
	return OK
} -result OK

test tricsvar-1.6 {Test lambda} -body {
        testPar lambda 1.179 User 
	return OK
} -result OK

test tricsvar-1.7 {Test spacegroup} -body {
        testPar spgrp P4 User 
	return OK
} -result OK

test tricsvar-1.8 {Test mode} -body {
        testPar mode bi Mugger
        testPar mode nb Mugger
        testPar mode tas Mugger
	return OK
} -result OK

test tricsvar-1.9 {Test detmode} -body {
        testPar detmode single Mugger 
        testPar detmode area Mugger
	return OK
} -result OK

test tricsvar-1.10 {Test cell} -body {
        testPar cell   "1 2 3 90 120 90" User 
	return OK
} -result OK


test tricsvar-1.11 {Test ub } -body {
        testMultiPar ub "$testub" User 
	return OK
} -result OK

config rights User User


test tricsvar-1.12 {Test confsearch } -body {
    set should "min2t =  5,step2t =  2,max2t =  10,stepchi =  10,stepphi =  1,chimin =  90,chimax =  180,phimin =  0,phimax =  180"
    set status [catch {confsearch 5 2 10 10 1 90 180 0 180} msg]
    if {$status != 0} {
	error $msg
    }
    set status [catch {confsearch} msg]
    if {$status != 0} {
	error $msg
    }    
    if {[string compare [string trim $msg] $should] != 0} {
	error "Received $msg, should have been $should"
    }
    return OK
} -result OK

test tricsvar-1.13 {Test confsearchnb } -body {
    set should "min2t =  5,step2t =  2,max2t =  10,stepom =  2,stepnu =  2"
    set status [catch {confsearchnb 5 2 10 2 2} msg]
    if {$status != 0} {
	error $msg
    }
    set status [catch {confsearchnb} msg]
    if {$status != 0} {
	error $msg
    }    
    if {[string compare [string trim $msg] $should] != 0} {
	error "Received $msg, should have been $should"
    }
    return OK
} -result OK

test tricsvar-1.14 {Test coneconf} -body {
    set should "coneconf =  0001   1 1 1  1"
    set status [catch {coneconf 0001 1 1 1} msg]
    if {$status != 0} {
	error $msg
    }
    set status [catch {coneconf} msg]
    if {$status != 0} {
	error $msg
    }    
    if {[string compare [string trim $msg] $should] != 0} {
	error "Received $msg, should have been $should"
    }
    return OK
} -result OK


test tricsvar-1.15 {Test indexconf} -body {
    set should "simidxconf =  0.3,  0.5"
    set status [catch {indexconf .3 .5} msg]
    if {$status != 0} {
	error $msg
    }
    set status [catch {indexconf} msg]
    if {$status != 0} {
	error $msg
    }    
    if {[string compare [string trim $msg] $should] != 0} {
	error "Received $msg, should have been $should"
    }
    return OK
} -result OK

test tricsvar-1.16 {Test hkllimit} -body {
    set should "indconf =   0 0 0 6 6 6   5 40"
    set status [catch {hkllimit 0 0 0 6 6 6 5 40} msg]
    if {$status != 0} {
	error $msg
    }
    set status [catch {hkllimit} msg]
    if {$status != 0} {
	error $msg
    }    
    if {[string compare [string trim $msg] $should] != 0} {
	error "Received $msg, should have been $should"
    }
    return OK
} -result OK


test tricsvar-1.17 {Test collconf} -body {
    set should "timer  1  100"
    set status [catch {collconf timer 1 100} msg]
    if {$status != 0} {
	error $msg
    }
    set status [catch {collconf} msg]
    if {$status != 0} {
	error $msg
    }    
    if {[string compare [string trim $msg] $should] != 0} {
	error "Received $msg, should have been $should"
    }
    return OK
} -result OK
