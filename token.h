
/*---------------------------------------------------------------
                    S I C S  T O K E N
                    
   This is the header file for the SICS token management
   functions. It implements the token command.
   
   Mark Koenencke, January 1998
   
   copyright: see copyright.h
   
----------------------------------------------------------------*/
#ifndef SICSTOKEN
#define SICSTOKEN
int TokenInit(SConnection * pCon, SicsInterp * pSics,
              void *pData, int argc, char *argv[]);
  /*
     The Token Factory function.
   */
int TokenWrapper(SConnection * pCon, SicsInterp * pSics,
                 void *pData, int argc, char *argv[]);

   /* 
      The wrapper function for the token command.
    */

int TokenGrabActive(void);
   /*
      Returns 1, if a token grab is active, else 0
    */

void TokenRelease(void);
   /* 
      Releases a Token Grab
    */
#endif
