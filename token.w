\subsection{Token Management}
In SICS any client can issue commands to the SICS server. This
is a potential cause for trouble with users issuing conflicting
commands without knowing. In order to deal with this problem a
token mechanism has been developed. A connection can grab a
token and then has full control over the SICS server. Any other
connection will not be priviledged to do anything useful, except
looking at things. A token can be released manually with a
special command or is automatically released when the connection
dies. Another command exists which allows a SICS manager to
force his way into the SICS server.

This token scheme is implemented using the signal system of the
task management facility and the connection object. The connection
object has a field called iGrap which is true if the connection is the
ultimate and only control connection. This field will be modified
either directly through commands or via the signal system of the SICS
task manager. This must be so as all connections need to be notified
if one connection has grabbed control. 

This object implements the user interface to the token system.
It checks privileges and issues the necessary signals. No own
data structure is needed. Just a wrapper function is
implemented.

@o token.h @{
/*---------------------------------------------------------------
                    S I C S  T O K E N
                    
   This is the header file for the SICS token management
   functions. It implements the token command.
   
   Mark Koenencke, January 1998
   
   copyright: see copyright.h
   
----------------------------------------------------------------*/
#ifndef SICSTOKEN
#define SICSTOKEN
  int TokenInit(SConnection *pCon, SicsInterp *pSics, 
                void *pData, int argc, char *argv[]);
  /*
      The Token Factory function.
  */                  
  int TokenWrapper(SConnection *pCon, SicsInterp *pSics, 
                void *pData, int argc, char *argv[]);
    
   /* 
   The wrapper function for the token command.
   */               
   
   int TokenGrabActive(void);
   /*
      Returns 1, if a token grab is active, else 0
   */  
   
   void TokenRelease(void);
   /* 
     Releases a Token Grab
   */   
#endif                       
@}
 
The functions are:
\begin{description}
\item[TokenInit] is the installation routine which configures the
token system into SICS. Called from the initialisation script.
\item[TokenWrapper] is the interpreter interface function for the
token system. Implements the active run time commands.
\item[TokenGrapActive] returns true (1) if a connection has grabbed
control or false (0) otherwise.
\item[TokenRelease] releases the current control token.   
\end{description}

