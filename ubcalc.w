\subsection{UB Matrix Calculation for Four Circle Diffractometers}
This module helps in the calculation of UB matrices for four
 circle diffraction. This is only an interpreter interface, the
 actual functionality is in the ubfour.h, .c files. These are documented
 in ubfour.h in order to be able to give this away separatly as a
 library.

@o ubcalc.h @{
/*----------------------------------------------------------------------
  UB calculation routines for four circle diffraction.
  This is the interpreter interface to functionality implemented
  in fourlib.c

  copyright: see file COPYRIGHT

  Mark Koennecke, March 2005
-----------------------------------------------------------------------*/
#ifndef SICSUBCALC
#define SICSUBCALC
#include "sics.h"
#include "matrix/matrix.h"
#include "cell.h"
#include "hkl.h"
#include "ubfour.h"
/*---------------------------------------------------------------------*/
typedef struct {
  pObjectDescriptor pDes;
  pHKL hkl;
  lattice direct;
  reflection r1, r2, r3;
  MATRIX UB;
  double allowedDeviation;
  int indexSearchLimit;
  int maxSuggestions;
} UBCALC, *pUBCALC;
/*-------------------------------------------------------------------*/
int MakeUBCalc(SConnection *pCon,SicsInterp *pSics, void *pData,		
		int argc, char *argv[]);
int CreateUBCalc(SConnection *pCon,SicsInterp *pSics, char *name, char *hkl);
int UBCalcWrapper(SConnection *pCon, SicsInterp *pSics, void *pData,		
		int argc, char *argv[]);
		
reflection getReflection(void *ubcalc, int no);
#endif   
@} 
 