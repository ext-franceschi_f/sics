/*-------------------------------------------------------------------------
                           U D P Q U I E C K
                           
   This file implements the UDP broadcasting of SICS messages. Especially
   the update file message.
   
   Mark Koennecke, August 1998
---------------------------------------------------------------------------*/
#include <stdlib.h>
#include <assert.h>
#include "fortify.h"
#include "sics.h"
#include "udpquieck.h"

/* the UDP port */
static mkChannel *port = NULL;
static char pError[256];
/* the UDP port number, -1 means not configured */
static int iPort = -1;
/*----------------------------------------------------------------------*/
void KillQuieck(void *pData)
{
  if (port) {
    free(port);
  }
  if (pData)
    KillDummy(pData);
}

/*------------------------------------------------------------------------*/
int SendQuieck(int iType, char *pText)
{
  char *pPtr = NULL;
  int iRet;
  char pMessage[512];

  /* do we know the type */
  if (iType != QUIECK) {
    strcpy(pError, "ERROR: unknown UDP broadcast message type");
    return 0;
  }

  /* if no port number, get it from ServerOptions */
  if (iPort < 0) {
    pPtr = IFindOption(pSICSOptions, "QuieckPort");
    if (pPtr) {
      iPort = atoi(pPtr);
    } else {
      iPort = 2108;
    }
  }

  /* do we have a channel ? */
  if (port == NULL) {
    port = UDPConnect("localhost", iPort);
    if (port == NULL) {
      strcpy(pError,
             "ERROR: cannot connect to UDP port, may be nobody listening");
      return 0;
    }
  }

  /* format a message and blast into space */
  switch (iType) {
  case QUIECK:
    strcpy(pMessage, "QUIECK/");
    strlcat(pMessage, pText,512);
    break;
  default:
    strcpy(pMessage, "Error");
    break;
  }
  iRet = UDPWrite(port, pMessage, strlen(pMessage));
  if (iRet != 1) {
    strcpy(pError, "ERROR: failed to send UDP message");
    free(port);
    port = NULL;
    return 0;
  }
  return 1;
}

/*--------------------------------------------------------------------------*/
int QuieckAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[])
{
  int iRet;

  if (argc < 2) {
    SCWrite(pCon, "ERROR: expected a message for udpquieck", eError);
    return 0;
  }

  if (!SCMatchRights(pCon, usMugger)) {
    SCWrite(pCon,
            "ERROR: permission denied to use internal command udpquieck",
            eError);
    return 0;
  }

  iRet = SendQuieck(QUIECK, argv[1]);
  if (iRet != 1) {
    SCWrite(pCon, pError, eError);
    return 0;
  }
  SCSendOK(pCon);
  return 1;
}
/*--------------------------------------------------------------------------*/
void UdpInit(void)
{
  AddCommand(pServ->pSics, "udpquieck", QuieckAction, KillQuieck, NULL);
}
