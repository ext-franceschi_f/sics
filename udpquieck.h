
/*------------------------------------------------------------------------
                   U D P Q U I E C K
 
  udpquieck sends a notification message on a UDP port when a new data
  file has been created. 

  Mark Koennecke, August 1998
---------------------------------------------------------------------------*/
#ifndef UDPQUIECK
#define UDPQUIECK


#define QUIECK 1

void KillQuieck(void *pData);

int SendQuieck(int iType, char *filename);

int QuieckAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[]);


#endif
