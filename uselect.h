#include <sys/select.h>

/* an uninterruptable version of select. M.Z. Oct 2008 */

int uselect(int nfds,
            fd_set * readfds, fd_set * writefds, fd_set * exceptfds,
            struct timeval *timeout);
