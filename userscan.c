/*------------------------------------------------------------------------
   User configurable scans. At each scan point a scripted procedure is
   called.

   More info: userscan.tex

   copyright: see copyright.h

   Mark Koennecke, June 2001
-------------------------------------------------------------------------*/
#include <stdlib.h>
#include <assert.h>
#include <tcl.h>
#include "fortify.h"
#include "sics.h"
#include "scan.h"
#include "scan.i"
#include "userscan.h"

/*-----------------------------------------------------------------------*/
static int UserScanPoints(pScanData self, int iPoint)
{
  /*
     do nothing as the user is supposed to take care of the data file
   */
  return 1;
}

/*-----------------------------------------------------------------------
  Execute the users special procedure
  ------------------------------------------------------------------------*/
static int UserCount(pScanData self, int iPoint)
{
  int status;
  char pBueffel[512];

  assert(self);
  assert(self->pSics);
  assert(self->pCon);

  /* check for existence of command to run */
  if (self->pCommand == NULL) {
    SCWrite(self->pCon,
            "ERROR: configuration error, need procedure to run for user scan",
            eLogError);
    SCSetInterrupt(self->pCon, eAbortScan);
    return 0;
  }

  /* invoke command */
  snprintf(pBueffel, 511, "%s %d", self->pCommand, iPoint);
  status = Tcl_Eval(self->pSics->pTcl, pBueffel);
  if (status != TCL_OK) {
    sprintf(pBueffel, "ERROR in count script: %s",
            Tcl_GetStringResult(self->pSics->pTcl));
    SCWrite(self->pCon, pBueffel, eLogError);
    return 0;
  }
  return 1;
}

/*------------------------------------------------------------------------*/
int UserCollect(pScanData self, int iPoint)
{
  /* do nothing, its the users job */
  return 1;
}

/*------------------------------------------------------------------------*/
void ConfigureUserScan(pScanData self)
{
  assert(self);
  self->WriteScanPoints = UserScanPoints;
  self->ScanCount = UserCount;
  self->CollectScanData = UserCollect;
}
