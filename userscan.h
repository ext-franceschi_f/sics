
/*------------------------------------------------------------------------
   User configurable scans. At each scan point a scripted procedure is
   called.

   More info: userscan.tex

   copyright: see copyright.h

   Mark Koennecke, June 2001
-------------------------------------------------------------------------*/
#ifndef USERSCAN
#define USERSCAN


void ConfigureUserScan(pScanData self);


#endif
