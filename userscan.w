\subsection{User Scans}
When operating with polarisation equipment a lot of things must be done
 at each scan point: count, switch sloppy flippers, drive magnetic
 fields, count again etc. In order to do that and other special things user
 configurable scans have been introduced. For such scans a configurable
 script procedure is called at each scan point with the current
 scan point number as an argument. This script then has to perform 
 the necessary counting operations, store the data collected, print 
 status information and update the internal scan data structures with 
 the collected data so that data may be plotted or analyzed in SICS. 

In the following section the internal scan object is referenced as xxxscan.
The script can contain the following commands:
\begin{itemize}
\item Normal SICS commands for driving magnets, counting, retrieving 
 data etc.
\item {\bf xxxscan line bla bla } appends everything after line to
 the data file.
\item {\bf xxxscan store 1 2 3 4} stores the numbers after store in the
  count data structure of the scan module. 
\item clientput allows to write information to the controlling terminal.
\end{itemize}

In order to do all this some configurable scan functions have to be 
overloaded. This is implemented in userscan.c. In terms of a public
interface only one function is defined:

@d usinter @{
	void ConfigureUserScan(pScanData self);
@}

which simply sets the necessary function pointers in pScanData.

@o userscan.h @{
/*------------------------------------------------------------------------
   User configurable scans. At each scan point a scripted procedure is
   called.

   More info: userscan.tex

   copyright: see copyright.h

   Mark Koennecke, June 2001
-------------------------------------------------------------------------*/
#ifndef USERSCAN
#define USERSCAN

@<usinter@>

#endif
@}
