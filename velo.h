
#line 246 "velo.w"

/*-------------------------------------------------------------------------

                V E L O C I T Y   S E L E C T O R

  Header file for the SICS velocity selector module. For documentation
  see velo.w.

  Mark Koennecke, Juli 1997

  copyright: see implementation file
-----------------------------------------------------------------------------*/
#ifndef SICSVELO
#define SICSVELO

/*------------------------- live & death & shape ------------------------*/

#line 100 "velo.w"

typedef struct __VelSel *pVelSel;
typedef struct __VelSelDriv *pVelSelDriv;

pVelSel VSCreate(pMotor pTilt, pVelSelDriv pDriv);
void VSDestroy(void *self);

int VSAddVerbot(pVelSel self, float fMin, float fMax);
int VSSetPar(pVelSel self, SConnection * pCon, char *name, float fVal);
int VSGetPar(pVelSel self, char *name, float *fVal);
int VSGetRotation(pVelSel self, float *fRot);
int VSGetTilt(pVelSel self, float *fTilt);

#line 262 "velo.w"

/*------------------------- drive around -----------------------------------*/

#line 142 "velo.w"

/*
    int VSSetRotation(pVelSel self, SConnection *pCon, float fNew);
    int VSSetTilt(pVelSel self, SConnection *pCon, float FNewTilt);
*/
int VSSetTiltRot(pVelSel self, SConnection * pCon,
                 float fNewRot, float fNewTilt);

#line 264 "velo.w"

#line 164 "velo.w"

int VSGetLossCurrent(pVelSel self, SConnection * pCon, float *fLoss);

#line 265 "velo.w"

/*------------------------- Interpreter interface ------------------------*/

#line 173 "velo.w"

int VelSelFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
                  int argc, char *argv[]);
int VelSelAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[]);


#line 267 "velo.w"

#endif
