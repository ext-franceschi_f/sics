/**
 * This is the header file for a second generation velocity selector 
 * object. This is an extension to the drivable object.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, April 2009
 */
#ifndef VELOSEC_H_
#define VELOSEC_H_

/**
 * Usual style factory function for creating a 
 * NVS object.
 */
int MakeSecNVS(SConnection * pCon, SicsInterp * pSics,
                       void *object, int argc, char *argv[]);

#endif /*VELOSEC_H_*/
