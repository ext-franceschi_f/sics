\subsection{Xytable}
Xytable is an object which holds a list of x-y values. This can be used as a 
 tool for doing very special scans or storing interesting data from command
 files.  This class allows to retrieve the data in uuencoded form which
 is understood and displayed by the variable watcher SICS client. Furthermore
 data can be written to a file. The table entries are kept in a linked
list in the following data structure.

The following data structures will be used:
@d xydata @{

    typedef struct {
                     float x;
                     float y;
                   } TableEntry, *pTableEntry;

    typedef struct __XYTABLE {
                              pObjectDescriptor pDes;
                              int iList;            
                              int iCount;
                             }XYTable;
@}  
The entries in the TableEntry are self explianing: just the
values. The XYTable structure holds the following fields:
\begin{description}
\item[pDes] The standard object descriptor.
\item[iList] the handle for the lld-list holding the table entries.
\item[iCount] The number of entries in the table.
\end{description}



The following functions are defined:
@d xyfunc @{
   typedef struct __XYTABLE *pXYTable;
/*------------------------------------------------------------------------*/
   int XYClear(pXYTable self);
   int XYAdd(pXYTable self, float x, float y);
   int XYWrite(pXYTable self, FILE *fd);
   int XYSendUU(pXYTable self, SConnection *pCon);
   int XYList(pXYTable self, SConnection *pCon);
/*----------------------- interpreter interface --------------------------*/
   int XYFactory(SConnection *pCon, SicsInterp *pSics, void *pData,
                 int argc, char *argv[]);
   int XYAction(SConnection *pCon, SicsInterp *pSics, void *pData,
                 int argc, char *argv[]);
@} 

Most functions take a pointer to an XYTbale data structure as fist
parameter and return 1 on success and 0 on failure. The functions are:
\begin{description}
\item[XYClear] clears all entries.
\item[XYAdd] appends a new pair of x and y values to the list.
\item[XYWrite] writes the XY-list to the file fd. This file must
already have been opened for writing by the calling routine. 
[item[XYSendUU] sends the XY-table in uuencoded form compatible with
the SICS variabel watcher to connection pCon. 
\item[XYList] prints the content of the XY-table to connection pCon.
\item[XYFactory] is the implentation of the installation command for
the SICS initialisation process.
\item[XYAction] implements the interface to the SICS interpreter for
the xytable command.   
\end{description}


@o xytable.i @{
/*----------------------------------------------------------------------
  Internal data structures for the XYTable class. Do not modify
------------------------------------------------------------------------*/
@<xydata@>
@}

@o xytable.h @{
/*------------------------------------------------------------------------
                            X Y T A B L E 

 a helper class for holding an X-Y list of values.

 copyright: see copyright.h

 Mark Koennecke, June 1999
----------------------------------------------------------------------------*/
#ifndef XYTABLE
#define XYTABLE
@<xyfunc@>
#endif
@}


